﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class PriceHUD : MonoBehaviour
{

	Text text;
	public GameObject slotsObject;
	Slots slotsController;
	public float timeColorText;

	// Use this for initialization
	void Start ()
	{
		slotsController = slotsObject.GetComponent<Slots> ();
		text = GetComponent<Text> ();

	}

	public void UpdateTextWithBlink ()
	{
		text.text = "PRICE   " + slotsController.price.ToString ();
		StartCoroutine (BlinkText (timeColorText));

//		text.color = new Color (Random.Range (0f, 0.1f), Random.Range (0f, 1f), Random.Range (0f, 1f));

	}

	public void UpdateText ()
	{
		text.text = "PRICE   " + slotsController.price.ToString ();
//		StartCoroutine (BlinkText (timeColorText));

		//		text.color = new Color (Random.Range (0f, 0.1f), Random.Range (0f, 1f), Random.Range (0f, 1f));

	}

	IEnumerator BlinkText (float t)
	{


		while (t > 0) {

			t -= Time.time;

			text.text = "PRICE   " + slotsController.price.ToString ();
//			text.color = new Color (Random.Range (0f, 0.1f), Random.Range (0f, 1f), Random.Range (0f, 1f));

			yield return new WaitForSeconds (0.5f);
			text.text = "PRICE   ";
//			text.color = new Color (Random.Range (0f, 0.1f), Random.Range (0f, 1f), Random.Range (0f, 1f));

			yield return new WaitForSeconds (0.5f);


		}
	
		text.text = "PRICE   " + slotsController.price.ToString ();
	}
}
