﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Stats : MonoBehaviour
{

	//	public GameObject Food;
	//	public GameObject Drink;
	//	public GameObject Shield;
	//	public GameObject Coins;
	//	public GameObject Clothes;
	//	public GameObject Poison;

	public Text FoodText;
	public Text DrinkText;
	public Text ShieldText;
	public Text CoinsText;
	public Text ClothesText;
	public Text PoisonText;

	public int food;
	public int drink;
	public int shield;
	public int coins;
	public int clothes;
	public int poison;

	public int foodFactor;
	public int drinkFactor;
	public int shieldFactor;
	public int coinsFactor;
	public int clothesFactor;
	public int poisonFactor;

	public int[] StatsList;

	// Use this for initialization
	void Start ()
	{
		StatsList = new int[]{ food, drink, shield, coins, clothes, poison };

//		FoodText = Food.GetComponent<Text> ();
//		DrinkText = Drink.GetComponent<Text> ();
//		ShieldText = Shield.GetComponent<Text> ();
//		CoinsText = Coins.GetComponent<Text> ();
//		ClothesText = Clothes.GetComponent<Text> ();
//		PoisonText = Poison.GetComponent<Text> ();

		food = Random.Range (8, 20);
		drink = Random.Range (8, 30);
		shield = Random.Range (2, 4);
		coins = Random.Range (4, 8);
		clothes = Random.Range (4, 8);
		poison = Random.Range (0, 3);

		UpdateText ();
	}

	public void UpdateText ()
	{
		FoodText.text = food.ToString ();
		DrinkText.text = drink.ToString ();
		ShieldText.text = shield.ToString ();
		CoinsText.text = coins.ToString ();
		ClothesText.text = clothes.ToString ();
		PoisonText.text = poison.ToString ();
	}

	public void UpdateStats ()
	{

		food -= Random.Range (0, foodFactor);
		drink -= Random.Range (0, drinkFactor);
		shield -= Random.Range (0, shieldFactor);
		coins -= Random.Range (0, coinsFactor);
		clothes -= Random.Range (0, clothesFactor);
		poison -= Random.Range (0, poisonFactor);

		UpdateText ();

	}


	public void addFood ()
	{
		food += Random.Range (1, foodFactor);
	}

	public void addDrink ()
	{
		drink += Random.Range (1, drinkFactor);

	}

	public void addShield ()
	{
		shield += Random.Range (1, shieldFactor);

	}

	public void addCoins ()
	{
		coins += Random.Range (1, coinsFactor);

	}

	public void addClothes ()
	{
		clothes += Random.Range (1, clothesFactor);

	}

	public void addPoison ()
	{
		poison += Random.Range (1, poisonFactor);

	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}
