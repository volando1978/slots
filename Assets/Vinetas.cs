﻿using UnityEngine;
using System.Collections;

public class Vinetas : MonoBehaviour
{

	Sprite camino, bosque, casa, puerta, felpudo, llave, cerradura, entrada, reloj, escaleras, alcoba, lienzo, metafisica, tunel, escalera, sotano, caverna, master, piedra;

	public Sprite[] vinetas;
	public Sprite[] listaSprite;


	// Use this for initialization
	void Start ()
	{

		#region FOTOS
		camino = vinetas [0];
		bosque = vinetas [1];
		casa = vinetas [2];
		puerta = vinetas [3];
		felpudo = vinetas [4];
		llave = vinetas [5];
		cerradura = vinetas [6];
		entrada = vinetas [7];
		reloj = vinetas [8];
		escaleras = vinetas [9];
		alcoba = vinetas [10];
		lienzo = vinetas [11];
		metafisica = vinetas [12];
		tunel = vinetas [13];
		escalera = vinetas [14];
		sotano = vinetas [15];
		caverna = vinetas [16];
		master = vinetas [17];
		piedra = vinetas [18];
		#endregion


	}


}
