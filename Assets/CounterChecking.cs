﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CounterChecking : MonoBehaviour
{

	public Slots gc;
	Combinaciones combinaciones;
	Vinetas vi;
	Sprite ojo, huella, hablar, escudo, manzana, gafas, pocion, cruz, llavero, tenazas, muerte, corazon;
	Sprite c1, c2, c3;
	public GameObject Tabla;

	// Use this for initialization
	void Start ()
	{
		gc = GetComponent<Slots> ();
		combinaciones = Tabla.GetComponent<Combinaciones> ();
		vi = Tabla.GetComponent<Vinetas> ();


		#region ICONS
		ojo = vi.listaSprite [0]; 
		huella = vi.listaSprite [1];
		hablar = vi.listaSprite [2];
		escudo = vi.listaSprite [3];
		manzana = vi.listaSprite [4];
		gafas = vi.listaSprite [5];
		pocion = vi.listaSprite [6];
		cruz = vi.listaSprite [7];
		llavero = vi.listaSprite [8];
		tenazas = vi.listaSprite [9];
		muerte = vi.listaSprite [10];
		corazon = vi.listaSprite [11];
		#endregion

		c1 = gc.counters [0].GetComponent<Image> ().sprite;
		c2 = gc.counters [1].GetComponent<Image> ().sprite;
		c3 = gc.counters [2].GetComponent<Image> ().sprite;

	}


	void ActualizaCounters ()
	{

		c1 = gc.counters [0].GetComponent<Image> ().sprite;
		c2 = gc.counters [1].GetComponent<Image> ().sprite;
		c3 = gc.counters [2].GetComponent<Image> ().sprite;

	}

	void ActualizaCharacterCounter (Sprite spr)
	{
		gc.counterChar.GetComponent<Image> ().sprite = spr;
	}

	public void CounterCheck ()
	{
		ActualizaCounters ();



		switch (combinaciones.currentPic) {

		case 0: /*nopic*/
			if (c1 == ojo && c2 == ojo && c3 == ojo) { //patita huella
				gc.getPicture (0);
				combinaciones.currentPic = 1;
				ActualizaCharacterCounter (vi.listaSprite [1]);
				print ("D");
			}
			break;
		case 1: /*camino*/
			if (c1 == huella && c2 == huella && c3 == huella) { //patita huella
				gc.getPicture (1);
				combinaciones.currentPic = 2;
				ActualizaCharacterCounter (vi.listaSprite [2]);

			}
			break;
		case 2: //bosque
			if (c1 == gafas && c2 == gafas && c3 == gafas) { //patita huella
				gc.getPicture (2);
				combinaciones.currentPic = 3;
				ActualizaCharacterCounter (vi.listaSprite [3]);

			}
			break;
		case 3: //camino 2
			if (c1 == huella && c2 == huella && c3 == huella) { //patita huella
				gc.getPicture (3);
				combinaciones.currentPic = 4;
				ActualizaCharacterCounter (vi.listaSprite [4]);

			}
			break;
		case 4: //casa
			if (c1 == hablar && c2 == hablar && c3 == hablar) { //patita huella
				gc.getPicture (4);
				combinaciones.currentPic = 5;
				ActualizaCharacterCounter (vi.listaSprite [5]);

			}
			break;
		case 5: //puerta
			if (c1 == gafas && c2 == gafas && c3 == gafas) { //patita huella
				gc.getPicture (5);
				combinaciones.currentPic = 6;
				ActualizaCharacterCounter (vi.listaSprite [6]);

			}
			break;
		case 6:// felpudo
			if (c1 == vi.listaSprite [3] && c2 == vi.listaSprite [3] && c3 == vi.listaSprite [3]) { //patita huella
				gc.getPicture (6);
				combinaciones.currentPic = 7;
				ActualizaCharacterCounter (vi.listaSprite [7]);

			}
			break;
		case 7://llave
			if (c1 == vi.listaSprite [3] && c2 == vi.listaSprite [3] && c3 == vi.listaSprite [3]) { //patita huella
				combinaciones.currentPic = 7;
				gc.getPicture (8);
				ActualizaCharacterCounter (vi.listaSprite [8]);

			}
			break;
		case 8://cerradura
			if (c1 == vi.listaSprite [3] && c2 == vi.listaSprite [3] && c3 == vi.listaSprite [3]) { //patita huella
				combinaciones.currentPic = 8;
				gc.getPicture (9);
				ActualizaCharacterCounter (vi.listaSprite [9]);

			}
			break;
		case 9://escaleras
			if (c1 == vi.listaSprite [3] && c2 == vi.listaSprite [3] && c3 == vi.listaSprite [3]) { //patita huella
				combinaciones.currentPic = 9;
				gc.getPicture (10);
				ActualizaCharacterCounter (vi.listaSprite [10]);

			}
			break;
		}
	}
}
