﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BotonInventario : MonoBehaviour
{

	public GameObject inventario;
	public int identificador;

	public void Start ()
	{

		if (gameObject.transform.GetChild (0).gameObject.activeSelf)
			GetComponentInChildren<Text> ().text = inventario.GetComponent<Inventario> ().amounts [identificador].ToString ();
		
	}

	public void MePulso ()
	{
		if (gameObject.transform.GetChild (0).gameObject.activeSelf)
			GetComponentInChildren<Text> ().text = inventario.GetComponent<Inventario> ().amounts [identificador].ToString ();

		print ("me pulso " + inventario.GetComponent<Inventario> ().amounts [identificador]);
	}
}
