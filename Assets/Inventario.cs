﻿
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class Inventario : MonoBehaviour
{

	public Sprite[] listaSprite;
	public int[] amounts;
	public Button CellPrefab;

	public float timeColorPic;


	// Use this for initialization
	public void UpdateInventario ()
	{
		Clear ();
		for (int i = 0; i < listaSprite.Length; i++) {
			if (i < Slots.numLock) {
				Button newCell = Instantiate (CellPrefab) as Button;
				newCell.GetComponent<Image> ().sprite = listaSprite [i];
				newCell.transform.SetParent (this.gameObject.transform, false);
				newCell.GetComponent<BotonInventario> ().identificador = i;
				newCell.transform.GetChild (0).gameObject.SetActive (true);
			} else {
				Button newCell = Instantiate (CellPrefab) as Button;
				newCell.GetComponent<Image> ().sprite = listaSprite [listaSprite.Length - 1];
				newCell.transform.SetParent (this.gameObject.transform, false);
			}
		}
	}

	public void IncreaseAmount (int index)
	{
//		for (int i = 0; i < amounts.Length; i++) {
		amounts [index] = +1;
			
	}



	public void UpdateInventoryWithBlink ()
	{
		UpdateInventario ();
		StartCoroutine (BlinkPic (timeColorPic));

	}

	IEnumerator BlinkPic (float t)
	{
		for (int i = 0; i < transform.childCount; i++) {
			
			if (i == (Slots.numLock - 1)) {


				while (t > 0) {
					t -= Time.time;
//						newCell.GetComponent<Image> ().sprite = listaSprite [i];
//						newCell.SetActive (true);
					transform.GetChild (i).transform.GetComponent<Image> ().sprite = listaSprite [i];

					yield return new WaitForSeconds (0.1f);
//						newCell.SetActive (false);
//						newCell.GetComponent<Image> ().sprite = null;
					transform.GetChild (i).transform.GetComponent<Image> ().sprite = listaSprite [listaSprite.Length - 1];

					yield return new WaitForSeconds (0.1f);

//					transform.GetChild (i).transform.GetComponent<Image> ().sprite = listaSprite [i];
				}
					
			}
		}

		UpdateInventario ();
	}

	void Clear ()
	{
		foreach (Transform child in transform) {
			Destroy (child.gameObject);
		}
	}

}
