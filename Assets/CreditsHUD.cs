﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class CreditsHUD : MonoBehaviour
{

	Text text;
	public GameObject slotsObject;
	Slots slotsController;
	public float timeColorText;


	// Use this for initialization
	void Start ()
	{
		slotsController = slotsObject.GetComponent<Slots> ();
		text = GetComponent<Text> ();

	}

	public void UpdateTextWithBlink ()
	{
		text.text = "CREDITS " + slotsController.credits.ToString ();
		StartCoroutine (BlinkText (timeColorText));

	}

	public void UpdateText ()
	{
		text.text = "CREDITS " + slotsController.credits.ToString ();
//		StartCoroutine (BlinkText (timeColorText));

	}

	IEnumerator BlinkText (float t)
	{


		while (t > 0) {

			t -= Time.time;

			text.text = "CREDITS " + slotsController.credits.ToString ();
			//			text.color = new Color (Random.Range (0f, 0.1f), Random.Range (0f, 1f), Random.Range (0f, 1f));

			yield return new WaitForSeconds (0.5f);
			text.text = "CREDITS ";
			//			text.color = new Color (Random.Range (0f, 0.1f), Random.Range (0f, 1f), Random.Range (0f, 1f));

			yield return new WaitForSeconds (0.5f);


		}

		text.text = "CREDITS " + slotsController.credits.ToString ();
	}


}
