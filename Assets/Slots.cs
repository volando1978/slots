﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class Slots : MonoBehaviour
{

	PriceHUD priceHUD;
	CreditsHUD creditsHUD;
	AudioSource audio;
	public  GameObject SlotpParent, slot1, slot2, slot3, HUD, HitButton, ThreeButtons, inventario, StartButton, Audio, Caption, PicChar, PicCanvas, Tabla, HitStopText;
	public  GameObject counterScreen1, counterScreen2, counterScreen3, counterChar, panelInventario, panelGame;
	public GameObject[] counters;
	public GameObject[] slotsG;

	public AudioClip win, winBig, result, start, hit, coin;
	public Color blueFrame;
	public Canvas canvas;
	Text caption;
	Sprite picChar, picCanvas;
	Combinaciones combinaciones;
	CounterChecking counterChecking;


	float currentTimer;
	public float timer;
	public float timeHightlight;

	bool rolling = false;
	bool choice = false;
	public bool[] frozenSlots;
	public bool frozenRound = false;

	public Sprite[] slots;

	public Vinetas vi;
	public Stats stat;

	public int aNumber, bNumber, cNumber, aAux, bAux, cAux;
	public int tempFrozen = -1;
	public int price;
	public int credits;
	public float heightHUD;
	public static int numLock = 3;


	public enum State
	{
		MENU = 0,
		INGAME = 1,
		INVENTORY = 2,
		INTERLUDE = 3,
		MAP = 4

	}

	public State currentState;


	void Start ()
	{
		currentState = State.MENU;
		vi = Tabla.GetComponent<Vinetas> ();
		stat = Tabla.GetComponent<Stats> ();
		audio = Audio.GetComponent<AudioSource> ();
		priceHUD = HUD.transform.GetChild (0).GetComponent<PriceHUD> ();
		creditsHUD = HUD.transform.GetChild (1).GetComponent<CreditsHUD> ();
		caption = Caption.GetComponent<Text> ();
		picChar = PicChar.GetComponent<Image> ().sprite;
		picCanvas = PicCanvas.GetComponent<Image> ().sprite;
		combinaciones = Tabla.GetComponent<Combinaciones> ();
		counterChecking = GetComponent<CounterChecking> ();
		float co = Screen.height;//canvas.GetComponent<RectTransform> ().rect.height * canvas.scaleFactor;

		Vector3 n = Camera.main.ScreenToWorldPoint (new Vector3 (canvas.GetComponent<RectTransform> ().rect.width * canvas.scaleFactor, co, canvas.planeDistance));
		heightHUD = n.y;
		print ("CO " + co);

	}

	public void StartGame ()
	{
		currentState = State.INGAME;
		StartButton.SetActive (false);



		resetValues ();
		resetTimer ();
		UpdateAll ();
//		StartCoroutine ("BringHUD");
		audio.PlayOneShot (start);
		SlotpParent.SetActive (true);

	}

	public void BackToGame ()
	{

//		StartCoroutine ("BringBackHUD");
		currentState = State.INGAME;
		SlotpParent.SetActive (true);

	}

	public void BackToInventory ()
	{
//		StartCoroutine ("BringInventory");
		panelInventario.GetComponent<EasyTween> ().OpenCloseObjectAnimation ();
		panelGame.GetComponent<EasyTween> ().OpenCloseObjectAnimation ();
		SlotsRackSwitch ();
		currentState = State.INVENTORY;

	}

	public void BackToMap ()
	{
		StartCoroutine ("BringMap");
		currentState = State.MAP;

	}



	void resetSlotColor ()
	{
		if (!frozenSlots [0])
			slot1.GetComponent<SpriteRenderer> ().color = blueFrame;

		if (!frozenSlots [1])
			slot2.GetComponent<SpriteRenderer> ().color = blueFrame;

		if (!frozenSlots [2])
			slot3.GetComponent<SpriteRenderer> ().color = blueFrame;

	}

	void resetValues ()
	{
		price = 0;
		credits = 10;
	}

	void switchHitButtons (bool b)
	{
		if (b) {
			HitButton.SetActive (false);
			ThreeButtons.SetActive (true);

		} else {
			HitButton.SetActive (true);
			ThreeButtons.SetActive (false);

		}
	}

	void switchChoice ()
	{
		if (choice) {
			choice = false;
		} else {
			choice = true;
		}
	}

	public void freeze1 ()
	{
		frozenSlots [0] = true;
		Unfreeze (0);
		tempFrozen = 0;
		setColorFrame (slot1);
		switchChoice ();
		switchHitButtons (choice);
		SetCounter (0);
		GivePrice (0);
		counterChecking.CounterCheck ();

	}

	public void freeze2 ()
	{
		frozenSlots [1] = true;
		Unfreeze (1);
		tempFrozen = 1;
		setColorFrame (slot2);
		switchChoice ();
		switchHitButtons (choice);
		SetCounter (1);
		GivePrice (1);

		counterChecking.CounterCheck ();

	
	}

	public void freeze3 ()
	{
		frozenSlots [2] = true;
		Unfreeze (2);
		tempFrozen = 2;
		setColorFrame (slot3);
		switchChoice ();
		switchHitButtons (choice);
		SetCounter (2);
		GivePrice (2);

		counterChecking.CounterCheck ();
	
	}

	void Unfreeze (int frozen)
	{
		for (int i = 0; i < 3; i++) {
			
			if (i != frozen)
				frozenSlots [i] = false;
		
			resetSlotColor ();

		}
	}

	void HardUnfreeze ()
	{
		for (int i = 0; i < 3; i++) {
			frozenSlots [i] = false;
		}
		tempFrozen = -1;
		resetSlotColor ();
	}

	void resetTimer ()
	{
		currentTimer = timer;
		rolling = false;
	}

	void resetVars ()
	{
		aNumber = 0;
		bNumber = 0;
		cNumber = 0;
	}

	void setFrozenRound ()
	{
		if (frozenRound) {
			frozenRound = false;
			HardUnfreeze ();
		} else {
			frozenRound = true;
		}
	}

	void SetPosInList ()
	{

		Sprite s0 = slots [0] as Sprite;
		Sprite s1 = slots [1] as Sprite;
		Sprite s2 = slots [2] as Sprite;

		slot1.GetComponent<SpriteRenderer> ().sprite = s0;
		slot2.GetComponent<SpriteRenderer> ().sprite = s1;
		slot3.GetComponent<SpriteRenderer> ().sprite = s2;

	}

	public void HitSlots ()
	{
		resetSlotColor ();
		ResetNumbers ();
		if (credits > 0) {
			if (!rolling) {
				audio.pitch = 1;
				audio.pitch = Random.Range (1.2f, 1.3f);
				audio.PlayOneShot (hit);
				aNumber = Random.Range (0, numLock);
				bNumber = Random.Range (0, numLock);
				cNumber = Random.Range (0, numLock);
				StartCoroutine ("roll");
				HitStopText.GetComponent<Text> ().text = "STOP";

			} else {
				setCurrentTimer0 ();
				HitStopText.GetComponent<Text> ().text = "HIT";
				stat.UpdateStats ();
			}
		}
	}

	void setCurrentTimer0 ()
	{
		currentTimer = -1;
	}


	IEnumerator roll ()
	{
		credits -= 1;
		creditsHUD.UpdateText ();




		while (currentTimer > 0) {
			getCaption (0);

			rolling = true;
//			currentTimer -= Time.deltaTime;

			if (aNumber < numLock) {
				aAux = aNumber += Random.Range (1, 2);
				slots [0] = vi.listaSprite [aAux];
			} else {
				slots [0] = vi.listaSprite [0];
				aNumber = 0;
			}

			SetPosInList ();

			if (bNumber < numLock) {
				bAux = bNumber += Random.Range (1, 2);
				slots [1] = vi.listaSprite [bAux];
			} else {
				slots [1] = vi.listaSprite [0];
				bNumber = 0;
			}
			SetPosInList ();

			if (cNumber < numLock) {
				cAux = cNumber += Random.Range (1, 2);
				slots [2] = vi.listaSprite [cAux];
			} else {
				slots [2] = vi.listaSprite [0];
				cNumber = 0;
			}
			SetPosInList ();

			yield return new WaitForSeconds (0.05f);
		}

		rolling = false;
		CheckCombination ();
		resetTimer ();
		resetVars ();
		//		setFrozenRound ();

	}

	void ResetNumbers ()
	{
		aNumber = 0;
		bNumber = 0;
		cNumber = 0;
	}

	void IncreaseAmount (int index)
	{
		
		inventario.GetComponent<Inventario> ().IncreaseAmount (index);
	}




	void CheckCombination ()
	{
		//compara sprites
		if (slots [0] != null && slots [1] != null && slots [2] != null) {
			
			for (int i = 0; i < 3; i++) {
				if (slots [0] == slots [1] && slots [1] == slots [2]) {
					
					IncreasePrice (3);
					IncreaseNumLock ();
					IncreaseCredit ();
					IncreaseAmount (aNumber);

					BackToInventory ();

					audio.PlayOneShot (winBig);

					resetTimer ();
					HardUnfreeze ();

					StartCoroutine (HightlightSlot (slot1, timeHightlight));
					StartCoroutine (HightlightSlot (slot2, timeHightlight));
					StartCoroutine (HightlightSlot (slot3, timeHightlight));

					return;
				}

				if (slots [0] == slots [1]) {
					
					IncreasePrice (2);
					IncreaseCredit ();

					audio.PlayOneShot (win);
					audio.PlayOneShot (win);

					getCaption (aNumber);

					resetTimer ();
					StartCoroutine (HightlightSlot (slot1, timeHightlight));
					StartCoroutine (HightlightSlot (slot2, timeHightlight));
//					UpdateInventory ();
					switchChoice ();
					switchHitButtons (choice);
					resetSlotColor ();
					return;
				}

				if (slots [1] == slots [2]) {
					
					IncreasePrice (2);
					IncreaseCredit ();

					audio.PlayOneShot (win);
					audio.PlayOneShot (win);

//					SetCounter (a);
					getCaption (bNumber);
//					getPicture (a);

//					print ("B a: " + a + " b: " + b + " c: " + c);

					resetTimer ();
					StartCoroutine (HightlightSlot (slot2, timeHightlight));
					StartCoroutine (HightlightSlot (slot3, timeHightlight));
//					UpdateInventory ();
					switchChoice ();
					switchHitButtons (choice);
					resetSlotColor ();

					return;
				}

				if (slots [0] == slots [2]) {
					
					IncreasePrice (2);
					IncreaseCredit ();

					audio.PlayOneShot (win);
					audio.PlayOneShot (win);

					getCaption (aNumber);
//					getPicture (a);

//					print ("C a: " + a + " b: " + b + " c: " + c);

					resetTimer ();
					StartCoroutine (HightlightSlot (slot1, timeHightlight));
					StartCoroutine (HightlightSlot (slot3, timeHightlight));
//					UpdateInventory ();
					switchChoice ();
					switchHitButtons (choice);
					resetSlotColor ();

					return;
				}

				if (slots [0] != slots [1] && slots [0] != slots [2] && slots [1] != slots [2]) {
					HardUnfreeze ();
					resetSlotColor ();
					getCaption (9);

				}

			}

		}
	}

	void getCaption (int i)
	{
		caption.text = combinaciones.getText (i);

	}

	void SetCounter (int i)
	{

		switch (i) {
		case 0:
			counters [i].GetComponent<Image> ().sprite = slot1.GetComponent<SpriteRenderer> ().sprite;

			break;
		case 1:
			counters [i].GetComponent<Image> ().sprite = slot2.GetComponent<SpriteRenderer> ().sprite;

			break;
		case 2:
			counters [i].GetComponent<Image> ().sprite = slot3.GetComponent<SpriteRenderer> ().sprite;

			break;
		}

	}

	void GivePrice (int i)
	{
		int numberItem = -1;

		switch (i) {
		case 0:
			numberItem = aAux;

			break;
		case 1:
			numberItem = bAux;

			break;
		case 2:
			numberItem = cAux;

			break;
		}

		print ("Number item " + numberItem);

		switch (numberItem) {
		case 0: //ojo
			break;
		case 1: //bota
			stat.addClothes ();

			break;
		case 2: // hablar
			break;
		case 3: // escudo
			stat.addShield ();

			break;
		case 4: //manzana
			stat.addFood ();

			break;
		case 5: //gafas
			stat.addClothes ();

			break;
		case 6:
			break;

		}


//		stat.addFood ();
		stat.UpdateText ();
	}


	public void getPicture (int i)
	{
		StartCoroutine (ChangePic (picCanvas, i));

	}

	IEnumerator ChangePic (Sprite pic, int i)
	{

		Color c = new Color (PicCanvas.GetComponent<Image> ().color.r, PicCanvas.GetComponent<Image> ().color.g, PicCanvas.GetComponent<Image> ().color.b, 0);
		PicCanvas.GetComponent<Image> ().color = c;
		picCanvas = combinaciones.getPic (i);

		while (PicCanvas.GetComponent<Image> ().color.a < 1) {

//			print ("CORRRROOO a" + i + " a " + c.a);
			c.a += 0.01f;
			PicCanvas.GetComponent<Image> ().color = c;
			PicCanvas.GetComponent<Image> ().sprite = picCanvas;
			yield return new WaitForSeconds (0.01f);

		}


		if (PicCanvas.GetComponent<Image> ().color.a >= 1)
			StopCoroutine ("ChangePic");


	}


	void IncreaseNumLock ()
	{
		numLock += 1;

		return;
	}

	void IncreaseCredit ()
	{
		credits += 1;
		UpdateCreditHUDWithBlink ();
		return;
	}

	void IncreasePrice (int p)
	{
		price += p;
		UpdatePriceHUDWithBlink ();
	}

	public void ConvertToCredits ()
	{
		if (price > 1) {
			price -= 2;
			credits += 1;
			UpdatePriceHUD ();
			UpdatePriceHUDWithBlink ();
			UpdateCreditHUD ();
			UpdateCreditHUDWithBlink ();
		}
		audio.PlayOneShot (coin);

			
	}

	void UpdatePriceHUD ()
	{
		
		priceHUD.UpdateText ();

	}

	void UpdatePriceHUDWithBlink ()
	{
		priceHUD.UpdateTextWithBlink ();

	}

	void UpdateCreditHUD ()
	{
		creditsHUD.UpdateText ();

	}

	void UpdateCreditHUDWithBlink ()
	{
		creditsHUD.UpdateTextWithBlink ();

	}

	void UpdateInventoryWithBlink ()
	{
		
		inventario.GetComponent<Inventario> ().UpdateInventoryWithBlink ();

	}

	void UpdateInventory ()
	{
		inventario.GetComponent<Inventario> ().UpdateInventario ();
	}

	void UpdateAll ()
	{
		UpdatePriceHUD ();
		UpdateCreditHUD ();
		UpdateInventory ();
	}

	IEnumerator BringMan ()
	{
		float start = HUD.transform.position.y;
		float startTime = Time.time;
		float overTime = 0.5f;

		print ("dentro");

		while (true) {

			float movement = Mathf.Lerp (HUD.transform.position.y, -heightHUD, (Time.time - startTime) / overTime);
			//			print (movement);

			Vector2 pos = new Vector2 (HUD.transform.position.x, movement);
			HUD.transform.position = pos;

			if (HUD.transform.position.y <= -heightHUD) {
				SlotpParent.SetActive (true);
				StopCoroutine ("BringMan");
			}
			yield return null;
		}
		yield return null;
	}

	IEnumerator BringHUD ()
	{
		float start = HUD.transform.position.y;
		float startTime = Time.time;
		float overTime = 0.5f;

//		print ("dentro");

		while (true) {

			float movement = Mathf.Lerp (HUD.transform.position.y, 0, (Time.time - startTime) / overTime);
			//			print (movement);

			Vector2 pos = new Vector2 (HUD.transform.position.x, movement);
			HUD.transform.position = pos;

			if (HUD.transform.position.y >= 0) {
				StopCoroutine ("BringHUD");
			}
			yield return null;
		}
		yield return null;
	}

	IEnumerator BringBackHUD ()
	{
		float start = HUD.transform.position.y;
		float startTime = Time.time;
		float overTime = 0.5f;

//		print ("dentro");

		while (true) {

			float movement = Mathf.Lerp (HUD.transform.position.y, 0, (Time.time - startTime) / overTime);
			//			print (movement);

			Vector2 pos = new Vector2 (HUD.transform.position.x, movement);
			HUD.transform.position = pos;

			if (HUD.transform.position.y <= 0) {
				SlotpParent.SetActive (true);
				StopCoroutine ("BringBackHUD");
			}
			yield return null;
		}
		yield return null;
	}

	IEnumerator BringMap ()
	{
		float start = HUD.transform.position.y;
		float startTime = Time.time;
		float overTime = 0.1f;



		while (true) {

			yield return new WaitForSeconds (1f);
			SlotpParent.SetActive (false);

			float movement = Mathf.Lerp (HUD.transform.position.y, heightHUD * 3, (Time.time - startTime) / overTime);
			print ((HUD.GetComponent<RectTransform> ().rect.height * canvas.scaleFactor) / 2);

			print ("dentroImap");

			Vector2 pos = new Vector2 (transform.position.x, movement);
			HUD.transform.position = pos;

			if (HUD.transform.position.y >= heightHUD * 3) {
				StopCoroutine ("BringMap");

			}
			yield return null;
		}
		yield return null;
	}


	IEnumerator BringInventory ()
	{
		float start = HUD.transform.position.y;
		float startTime = Time.time;
		float overTime = 0.5f;

		UpdateInventoryWithBlink ();

//		print ("dentroInvent");

		while (true) {

			yield return new WaitForSeconds (1f);
			SlotpParent.SetActive (false);

			float movement = Mathf.Lerp (HUD.transform.position.y, heightHUD, (Time.time - startTime) / overTime);
//			print ((HUD.GetComponent<RectTransform> ().rect.height * canvas.scaleFactor) / 2);


			Vector2 pos = new Vector2 (transform.position.x, movement);
			HUD.transform.position = pos;

			print ("INV " + movement + " " + heightHUD);

			if (HUD.transform.position.y >= heightHUD) {
				StopCoroutine ("BringInventory");

			}
			yield return null;
		}
		yield return null;
	}

	IEnumerator HightlightSlot (GameObject slot, float t)
	{
//		Color c = slot.GetComponent<SpriteRenderer> ().color;
		while (t > 0) {
//			t -= Time.time;
//			slot.GetComponent<SpriteRenderer> ().color = Color.white;
//			yield return new WaitForSeconds (0.1f);
//			slot.GetComponent<SpriteRenderer> ().color = c;
			yield return new WaitForSeconds (0.1f);
		}


		resetTimer ();
	}

	void setColorFrame (GameObject slot)
	{

		slot.GetComponent<SpriteRenderer> ().color = Color.grey;

//		resetTimer ();
	}


	public void SlotsRackSwitch ()
	{
		if (SlotpParent.activeSelf)
			SlotpParent.SetActive (false);
		else
			SlotpParent.SetActive (true);
		
	}

}