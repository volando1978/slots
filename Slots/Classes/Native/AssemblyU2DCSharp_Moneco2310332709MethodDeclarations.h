﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Moneco
struct Moneco_t2310332709;

#include "codegen/il2cpp-codegen.h"

// System.Void Moneco::.ctor()
extern "C"  void Moneco__ctor_m1209859350 (Moneco_t2310332709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Moneco::Start()
extern "C"  void Moneco_Start_m156997142 (Moneco_t2310332709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Moneco::Update()
extern "C"  void Moneco_Update_m577796279 (Moneco_t2310332709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
