﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Slots/<BringInventory>c__Iterator9
struct U3CBringInventoryU3Ec__Iterator9_t2737813690;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Slots/<BringInventory>c__Iterator9::.ctor()
extern "C"  void U3CBringInventoryU3Ec__Iterator9__ctor_m4202580257 (U3CBringInventoryU3Ec__Iterator9_t2737813690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Slots/<BringInventory>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBringInventoryU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4019722779 (U3CBringInventoryU3Ec__Iterator9_t2737813690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Slots/<BringInventory>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBringInventoryU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m3158291375 (U3CBringInventoryU3Ec__Iterator9_t2737813690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Slots/<BringInventory>c__Iterator9::MoveNext()
extern "C"  bool U3CBringInventoryU3Ec__Iterator9_MoveNext_m576004123 (U3CBringInventoryU3Ec__Iterator9_t2737813690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots/<BringInventory>c__Iterator9::Dispose()
extern "C"  void U3CBringInventoryU3Ec__Iterator9_Dispose_m1308651806 (U3CBringInventoryU3Ec__Iterator9_t2737813690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots/<BringInventory>c__Iterator9::Reset()
extern "C"  void U3CBringInventoryU3Ec__Iterator9_Reset_m1849013198 (U3CBringInventoryU3Ec__Iterator9_t2737813690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
