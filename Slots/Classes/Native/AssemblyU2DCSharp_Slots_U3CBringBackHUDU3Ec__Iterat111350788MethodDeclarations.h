﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Slots/<BringBackHUD>c__Iterator7
struct U3CBringBackHUDU3Ec__Iterator7_t111350788;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Slots/<BringBackHUD>c__Iterator7::.ctor()
extern "C"  void U3CBringBackHUDU3Ec__Iterator7__ctor_m2315200279 (U3CBringBackHUDU3Ec__Iterator7_t111350788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Slots/<BringBackHUD>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBringBackHUDU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m721269541 (U3CBringBackHUDU3Ec__Iterator7_t111350788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Slots/<BringBackHUD>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBringBackHUDU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m2256314553 (U3CBringBackHUDU3Ec__Iterator7_t111350788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Slots/<BringBackHUD>c__Iterator7::MoveNext()
extern "C"  bool U3CBringBackHUDU3Ec__Iterator7_MoveNext_m2489602917 (U3CBringBackHUDU3Ec__Iterator7_t111350788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots/<BringBackHUD>c__Iterator7::Dispose()
extern "C"  void U3CBringBackHUDU3Ec__Iterator7_Dispose_m12691860 (U3CBringBackHUDU3Ec__Iterator7_t111350788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots/<BringBackHUD>c__Iterator7::Reset()
extern "C"  void U3CBringBackHUDU3Ec__Iterator7_Reset_m4256600516 (U3CBringBackHUDU3Ec__Iterator7_t111350788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
