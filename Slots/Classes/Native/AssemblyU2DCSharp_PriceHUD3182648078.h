﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// Slots
struct Slots_t79980053;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PriceHUD
struct  PriceHUD_t3182648078  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Text PriceHUD::text
	Text_t9039225 * ___text_2;
	// UnityEngine.GameObject PriceHUD::slotsObject
	GameObject_t3674682005 * ___slotsObject_3;
	// Slots PriceHUD::slotsController
	Slots_t79980053 * ___slotsController_4;
	// System.Single PriceHUD::timeColorText
	float ___timeColorText_5;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(PriceHUD_t3182648078, ___text_2)); }
	inline Text_t9039225 * get_text_2() const { return ___text_2; }
	inline Text_t9039225 ** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(Text_t9039225 * value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier(&___text_2, value);
	}

	inline static int32_t get_offset_of_slotsObject_3() { return static_cast<int32_t>(offsetof(PriceHUD_t3182648078, ___slotsObject_3)); }
	inline GameObject_t3674682005 * get_slotsObject_3() const { return ___slotsObject_3; }
	inline GameObject_t3674682005 ** get_address_of_slotsObject_3() { return &___slotsObject_3; }
	inline void set_slotsObject_3(GameObject_t3674682005 * value)
	{
		___slotsObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___slotsObject_3, value);
	}

	inline static int32_t get_offset_of_slotsController_4() { return static_cast<int32_t>(offsetof(PriceHUD_t3182648078, ___slotsController_4)); }
	inline Slots_t79980053 * get_slotsController_4() const { return ___slotsController_4; }
	inline Slots_t79980053 ** get_address_of_slotsController_4() { return &___slotsController_4; }
	inline void set_slotsController_4(Slots_t79980053 * value)
	{
		___slotsController_4 = value;
		Il2CppCodeGenWriteBarrier(&___slotsController_4, value);
	}

	inline static int32_t get_offset_of_timeColorText_5() { return static_cast<int32_t>(offsetof(PriceHUD_t3182648078, ___timeColorText_5)); }
	inline float get_timeColorText_5() const { return ___timeColorText_5; }
	inline float* get_address_of_timeColorText_5() { return &___timeColorText_5; }
	inline void set_timeColorText_5(float value)
	{
		___timeColorText_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
