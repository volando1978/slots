﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// Inventario
struct Inventario_t3778973585;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Inventario/<BlinkPic>c__Iterator1
struct  U3CBlinkPicU3Ec__Iterator1_t852100114  : public Il2CppObject
{
public:
	// System.Int32 Inventario/<BlinkPic>c__Iterator1::<i>__0
	int32_t ___U3CiU3E__0_0;
	// System.Single Inventario/<BlinkPic>c__Iterator1::t
	float ___t_1;
	// System.Int32 Inventario/<BlinkPic>c__Iterator1::$PC
	int32_t ___U24PC_2;
	// System.Object Inventario/<BlinkPic>c__Iterator1::$current
	Il2CppObject * ___U24current_3;
	// System.Single Inventario/<BlinkPic>c__Iterator1::<$>t
	float ___U3CU24U3Et_4;
	// Inventario Inventario/<BlinkPic>c__Iterator1::<>f__this
	Inventario_t3778973585 * ___U3CU3Ef__this_5;

public:
	inline static int32_t get_offset_of_U3CiU3E__0_0() { return static_cast<int32_t>(offsetof(U3CBlinkPicU3Ec__Iterator1_t852100114, ___U3CiU3E__0_0)); }
	inline int32_t get_U3CiU3E__0_0() const { return ___U3CiU3E__0_0; }
	inline int32_t* get_address_of_U3CiU3E__0_0() { return &___U3CiU3E__0_0; }
	inline void set_U3CiU3E__0_0(int32_t value)
	{
		___U3CiU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_t_1() { return static_cast<int32_t>(offsetof(U3CBlinkPicU3Ec__Iterator1_t852100114, ___t_1)); }
	inline float get_t_1() const { return ___t_1; }
	inline float* get_address_of_t_1() { return &___t_1; }
	inline void set_t_1(float value)
	{
		___t_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CBlinkPicU3Ec__Iterator1_t852100114, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CBlinkPicU3Ec__Iterator1_t852100114, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Et_4() { return static_cast<int32_t>(offsetof(U3CBlinkPicU3Ec__Iterator1_t852100114, ___U3CU24U3Et_4)); }
	inline float get_U3CU24U3Et_4() const { return ___U3CU24U3Et_4; }
	inline float* get_address_of_U3CU24U3Et_4() { return &___U3CU24U3Et_4; }
	inline void set_U3CU24U3Et_4(float value)
	{
		___U3CU24U3Et_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_5() { return static_cast<int32_t>(offsetof(U3CBlinkPicU3Ec__Iterator1_t852100114, ___U3CU3Ef__this_5)); }
	inline Inventario_t3778973585 * get_U3CU3Ef__this_5() const { return ___U3CU3Ef__this_5; }
	inline Inventario_t3778973585 ** get_address_of_U3CU3Ef__this_5() { return &___U3CU3Ef__this_5; }
	inline void set_U3CU3Ef__this_5(Inventario_t3778973585 * value)
	{
		___U3CU3Ef__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
