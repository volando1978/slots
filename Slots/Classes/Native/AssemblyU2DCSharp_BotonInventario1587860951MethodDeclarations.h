﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BotonInventario
struct BotonInventario_t1587860951;

#include "codegen/il2cpp-codegen.h"

// System.Void BotonInventario::.ctor()
extern "C"  void BotonInventario__ctor_m4040201076 (BotonInventario_t1587860951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BotonInventario::Start()
extern "C"  void BotonInventario_Start_m2987338868 (BotonInventario_t1587860951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BotonInventario::MePulso()
extern "C"  void BotonInventario_MePulso_m1555080829 (BotonInventario_t1587860951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
