﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PriceHUD/<BlinkText>c__Iterator2
struct U3CBlinkTextU3Ec__Iterator2_t3201916881;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PriceHUD/<BlinkText>c__Iterator2::.ctor()
extern "C"  void U3CBlinkTextU3Ec__Iterator2__ctor_m3514618346 (U3CBlinkTextU3Ec__Iterator2_t3201916881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PriceHUD/<BlinkText>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBlinkTextU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3730810418 (U3CBlinkTextU3Ec__Iterator2_t3201916881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PriceHUD/<BlinkText>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBlinkTextU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m741887430 (U3CBlinkTextU3Ec__Iterator2_t3201916881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PriceHUD/<BlinkText>c__Iterator2::MoveNext()
extern "C"  bool U3CBlinkTextU3Ec__Iterator2_MoveNext_m225334194 (U3CBlinkTextU3Ec__Iterator2_t3201916881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PriceHUD/<BlinkText>c__Iterator2::Dispose()
extern "C"  void U3CBlinkTextU3Ec__Iterator2_Dispose_m1602218919 (U3CBlinkTextU3Ec__Iterator2_t3201916881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PriceHUD/<BlinkText>c__Iterator2::Reset()
extern "C"  void U3CBlinkTextU3Ec__Iterator2_Reset_m1161051287 (U3CBlinkTextU3Ec__Iterator2_t3201916881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
