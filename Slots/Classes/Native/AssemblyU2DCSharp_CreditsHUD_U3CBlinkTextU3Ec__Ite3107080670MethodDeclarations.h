﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CreditsHUD/<BlinkText>c__Iterator0
struct U3CBlinkTextU3Ec__Iterator0_t3107080670;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CreditsHUD/<BlinkText>c__Iterator0::.ctor()
extern "C"  void U3CBlinkTextU3Ec__Iterator0__ctor_m2049097853 (U3CBlinkTextU3Ec__Iterator0_t3107080670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CreditsHUD/<BlinkText>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBlinkTextU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m405898303 (U3CBlinkTextU3Ec__Iterator0_t3107080670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CreditsHUD/<BlinkText>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBlinkTextU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1402594771 (U3CBlinkTextU3Ec__Iterator0_t3107080670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CreditsHUD/<BlinkText>c__Iterator0::MoveNext()
extern "C"  bool U3CBlinkTextU3Ec__Iterator0_MoveNext_m108206911 (U3CBlinkTextU3Ec__Iterator0_t3107080670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreditsHUD/<BlinkText>c__Iterator0::Dispose()
extern "C"  void U3CBlinkTextU3Ec__Iterator0_Dispose_m1986298234 (U3CBlinkTextU3Ec__Iterator0_t3107080670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreditsHUD/<BlinkText>c__Iterator0::Reset()
extern "C"  void U3CBlinkTextU3Ec__Iterator0_Reset_m3990498090 (U3CBlinkTextU3Ec__Iterator0_t3107080670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
