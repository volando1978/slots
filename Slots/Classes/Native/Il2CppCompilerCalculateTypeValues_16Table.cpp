﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_ScrollEven3541123425.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Axis4294105229.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_U3CClickRepe99988271.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect3606982749.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_MovementTy300513412.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollbarV184977789.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollRec1643322606.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1885181538.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transitio1922345195.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Selection1293548283.h"
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtility1171612705.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider79469677.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction94975348.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEvent2627072750.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis3565360268.h"
#include "UnityEngine_UI_UnityEngine_UI_SpriteState2895308594.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial639665897.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatE1574154081.h"
#include "UnityEngine_UI_UnityEngine_UI_Text9039225.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle110812896.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransit2757337633.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent2331340366.h"
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup1990156785.h"
#include "UnityEngine_UI_UnityEngine_UI_ClipperRegistry1074114320.h"
#include "UnityEngine_UI_UnityEngine_UI_Clipping1257491342.h"
#include "UnityEngine_UI_UnityEngine_UI_RectangularVertexCli1294793591.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter436718473.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_As2149445162.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler2777732396.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMo2493957633.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenM1153512176.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit1837657360.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter1285073872.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_Fit909765868.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup169317941.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corne284493240.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis1399125956.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Cons1640775616.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGrou1336501463.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVertical2052396382.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement1596995480.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup352294875.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder1942933988.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility3144854024.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup423167365.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper3377436606.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect3555037586.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect2306480155.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline3745177896.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV14062429115.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow75537580.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_3379220348.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_BotonInventario1587860951.h"
#include "AssemblyU2DCSharp_Combinaciones3637091598.h"
#include "AssemblyU2DCSharp_CounterChecking39316438.h"
#include "AssemblyU2DCSharp_CreditsHUD1430045213.h"
#include "AssemblyU2DCSharp_CreditsHUD_U3CBlinkTextU3Ec__Ite3107080670.h"
#include "AssemblyU2DCSharp_Graficos156140270.h"
#include "AssemblyU2DCSharp_Inventario3778973585.h"
#include "AssemblyU2DCSharp_Inventario_U3CBlinkPicU3Ec__Itera852100114.h"
#include "AssemblyU2DCSharp_Moneco2310332709.h"
#include "AssemblyU2DCSharp_PriceHUD3182648078.h"
#include "AssemblyU2DCSharp_PriceHUD_U3CBlinkTextU3Ec__Itera3201916881.h"
#include "AssemblyU2DCSharp_Slots79980053.h"
#include "AssemblyU2DCSharp_Slots_State3376307191.h"
#include "AssemblyU2DCSharp_Slots_U3CrollU3Ec__Iterator32129678497.h"
#include "AssemblyU2DCSharp_Slots_U3CChangePicU3Ec__Iterator1863457897.h"
#include "AssemblyU2DCSharp_Slots_U3CBringManU3Ec__Iterator52482726264.h"
#include "AssemblyU2DCSharp_Slots_U3CBringHUDU3Ec__Iterator63883270396.h"
#include "AssemblyU2DCSharp_Slots_U3CBringBackHUDU3Ec__Iterat111350788.h"
#include "AssemblyU2DCSharp_Slots_U3CBringMapU3Ec__Iterator81471609017.h"
#include "AssemblyU2DCSharp_Slots_U3CBringInventoryU3Ec__Ite2737813690.h"
#include "AssemblyU2DCSharp_Slots_U3CHightlightSlotU3Ec__Ite3561306698.h"
#include "AssemblyU2DCSharp_ClickedWaveAnimation3762715588.h"
#include "AssemblyU2DCSharp_Pool2493500.h"
#include "AssemblyU2DCSharp_ActivateOnEnable2656425589.h"
#include "AssemblyU2DCSharp_ActivateOnEnable_U3CStartU3Ec__I1046475311.h"
#include "AssemblyU2DCSharp_CreateAnimImage2928946190.h"
#include "AssemblyU2DCSharp_FollowClick3238389815.h"
#include "AssemblyU2DCSharp_EasyTween3917628265.h"
#include "AssemblyU2DCSharp_ReferencedFrom1396252131.h"
#include "AssemblyU2DCSharp_UITween_CurrentAnimation3872301071.h"
#include "AssemblyU2DCSharp_UITween_CurrentAnimation_States3185248719.h"
#include "AssemblyU2DCSharp_UITween_PositionPropetiesAnim3297200579.h"
#include "AssemblyU2DCSharp_UITween_ScalePropetiesAnim205839632.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { sizeof (ScrollEvent_t3541123425), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { sizeof (Axis_t4294105229)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1601[3] = 
{
	Axis_t4294105229::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { sizeof (U3CClickRepeatU3Ec__Iterator5_t99988271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1602[7] = 
{
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_eventData_0(),
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_U3ClocalMousePosU3E__0_1(),
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_U3CaxisCoordinateU3E__1_2(),
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_U24PC_3(),
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_U24current_4(),
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_U3CU24U3EeventData_5(),
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_U3CU3Ef__this_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { sizeof (ScrollRect_t3606982749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1603[36] = 
{
	ScrollRect_t3606982749::get_offset_of_m_Content_2(),
	ScrollRect_t3606982749::get_offset_of_m_Horizontal_3(),
	ScrollRect_t3606982749::get_offset_of_m_Vertical_4(),
	ScrollRect_t3606982749::get_offset_of_m_MovementType_5(),
	ScrollRect_t3606982749::get_offset_of_m_Elasticity_6(),
	ScrollRect_t3606982749::get_offset_of_m_Inertia_7(),
	ScrollRect_t3606982749::get_offset_of_m_DecelerationRate_8(),
	ScrollRect_t3606982749::get_offset_of_m_ScrollSensitivity_9(),
	ScrollRect_t3606982749::get_offset_of_m_Viewport_10(),
	ScrollRect_t3606982749::get_offset_of_m_HorizontalScrollbar_11(),
	ScrollRect_t3606982749::get_offset_of_m_VerticalScrollbar_12(),
	ScrollRect_t3606982749::get_offset_of_m_HorizontalScrollbarVisibility_13(),
	ScrollRect_t3606982749::get_offset_of_m_VerticalScrollbarVisibility_14(),
	ScrollRect_t3606982749::get_offset_of_m_HorizontalScrollbarSpacing_15(),
	ScrollRect_t3606982749::get_offset_of_m_VerticalScrollbarSpacing_16(),
	ScrollRect_t3606982749::get_offset_of_m_OnValueChanged_17(),
	ScrollRect_t3606982749::get_offset_of_m_PointerStartLocalCursor_18(),
	ScrollRect_t3606982749::get_offset_of_m_ContentStartPosition_19(),
	ScrollRect_t3606982749::get_offset_of_m_ViewRect_20(),
	ScrollRect_t3606982749::get_offset_of_m_ContentBounds_21(),
	ScrollRect_t3606982749::get_offset_of_m_ViewBounds_22(),
	ScrollRect_t3606982749::get_offset_of_m_Velocity_23(),
	ScrollRect_t3606982749::get_offset_of_m_Dragging_24(),
	ScrollRect_t3606982749::get_offset_of_m_PrevPosition_25(),
	ScrollRect_t3606982749::get_offset_of_m_PrevContentBounds_26(),
	ScrollRect_t3606982749::get_offset_of_m_PrevViewBounds_27(),
	ScrollRect_t3606982749::get_offset_of_m_HasRebuiltLayout_28(),
	ScrollRect_t3606982749::get_offset_of_m_HSliderExpand_29(),
	ScrollRect_t3606982749::get_offset_of_m_VSliderExpand_30(),
	ScrollRect_t3606982749::get_offset_of_m_HSliderHeight_31(),
	ScrollRect_t3606982749::get_offset_of_m_VSliderWidth_32(),
	ScrollRect_t3606982749::get_offset_of_m_Rect_33(),
	ScrollRect_t3606982749::get_offset_of_m_HorizontalScrollbarRect_34(),
	ScrollRect_t3606982749::get_offset_of_m_VerticalScrollbarRect_35(),
	ScrollRect_t3606982749::get_offset_of_m_Tracker_36(),
	ScrollRect_t3606982749::get_offset_of_m_Corners_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { sizeof (MovementType_t300513412)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1604[4] = 
{
	MovementType_t300513412::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { sizeof (ScrollbarVisibility_t184977789)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1605[4] = 
{
	ScrollbarVisibility_t184977789::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { sizeof (ScrollRectEvent_t1643322606), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { sizeof (Selectable_t1885181538), -1, sizeof(Selectable_t1885181538_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1607[14] = 
{
	Selectable_t1885181538_StaticFields::get_offset_of_s_List_2(),
	Selectable_t1885181538::get_offset_of_m_Navigation_3(),
	Selectable_t1885181538::get_offset_of_m_Transition_4(),
	Selectable_t1885181538::get_offset_of_m_Colors_5(),
	Selectable_t1885181538::get_offset_of_m_SpriteState_6(),
	Selectable_t1885181538::get_offset_of_m_AnimationTriggers_7(),
	Selectable_t1885181538::get_offset_of_m_Interactable_8(),
	Selectable_t1885181538::get_offset_of_m_TargetGraphic_9(),
	Selectable_t1885181538::get_offset_of_m_GroupsAllowInteraction_10(),
	Selectable_t1885181538::get_offset_of_m_CurrentSelectionState_11(),
	Selectable_t1885181538::get_offset_of_m_CanvasGroupCache_12(),
	Selectable_t1885181538::get_offset_of_U3CisPointerInsideU3Ek__BackingField_13(),
	Selectable_t1885181538::get_offset_of_U3CisPointerDownU3Ek__BackingField_14(),
	Selectable_t1885181538::get_offset_of_U3ChasSelectionU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { sizeof (Transition_t1922345195)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1608[5] = 
{
	Transition_t1922345195::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { sizeof (SelectionState_t1293548283)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1609[5] = 
{
	SelectionState_t1293548283::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { sizeof (SetPropertyUtility_t1171612705), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { sizeof (Slider_t79469677), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1611[15] = 
{
	Slider_t79469677::get_offset_of_m_FillRect_16(),
	Slider_t79469677::get_offset_of_m_HandleRect_17(),
	Slider_t79469677::get_offset_of_m_Direction_18(),
	Slider_t79469677::get_offset_of_m_MinValue_19(),
	Slider_t79469677::get_offset_of_m_MaxValue_20(),
	Slider_t79469677::get_offset_of_m_WholeNumbers_21(),
	Slider_t79469677::get_offset_of_m_Value_22(),
	Slider_t79469677::get_offset_of_m_OnValueChanged_23(),
	Slider_t79469677::get_offset_of_m_FillImage_24(),
	Slider_t79469677::get_offset_of_m_FillTransform_25(),
	Slider_t79469677::get_offset_of_m_FillContainerRect_26(),
	Slider_t79469677::get_offset_of_m_HandleTransform_27(),
	Slider_t79469677::get_offset_of_m_HandleContainerRect_28(),
	Slider_t79469677::get_offset_of_m_Offset_29(),
	Slider_t79469677::get_offset_of_m_Tracker_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { sizeof (Direction_t94975348)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1612[5] = 
{
	Direction_t94975348::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { sizeof (SliderEvent_t2627072750), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { sizeof (Axis_t3565360268)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1614[3] = 
{
	Axis_t3565360268::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { sizeof (SpriteState_t2895308594)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1615[3] = 
{
	SpriteState_t2895308594::get_offset_of_m_HighlightedSprite_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t2895308594::get_offset_of_m_PressedSprite_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t2895308594::get_offset_of_m_DisabledSprite_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { sizeof (StencilMaterial_t639665897), -1, sizeof(StencilMaterial_t639665897_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1616[1] = 
{
	StencilMaterial_t639665897_StaticFields::get_offset_of_m_List_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { sizeof (MatEntry_t1574154081), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1617[10] = 
{
	MatEntry_t1574154081::get_offset_of_baseMat_0(),
	MatEntry_t1574154081::get_offset_of_customMat_1(),
	MatEntry_t1574154081::get_offset_of_count_2(),
	MatEntry_t1574154081::get_offset_of_stencilId_3(),
	MatEntry_t1574154081::get_offset_of_operation_4(),
	MatEntry_t1574154081::get_offset_of_compareFunction_5(),
	MatEntry_t1574154081::get_offset_of_readMask_6(),
	MatEntry_t1574154081::get_offset_of_writeMask_7(),
	MatEntry_t1574154081::get_offset_of_useAlphaClip_8(),
	MatEntry_t1574154081::get_offset_of_colorMask_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { sizeof (Text_t9039225), -1, sizeof(Text_t9039225_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1618[7] = 
{
	Text_t9039225::get_offset_of_m_FontData_28(),
	Text_t9039225::get_offset_of_m_Text_29(),
	Text_t9039225::get_offset_of_m_TextCache_30(),
	Text_t9039225::get_offset_of_m_TextCacheForLayout_31(),
	Text_t9039225_StaticFields::get_offset_of_s_DefaultText_32(),
	Text_t9039225::get_offset_of_m_DisableFontTextureRebuiltCallback_33(),
	Text_t9039225::get_offset_of_m_TempVerts_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { sizeof (Toggle_t110812896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1619[5] = 
{
	Toggle_t110812896::get_offset_of_toggleTransition_16(),
	Toggle_t110812896::get_offset_of_graphic_17(),
	Toggle_t110812896::get_offset_of_m_Group_18(),
	Toggle_t110812896::get_offset_of_onValueChanged_19(),
	Toggle_t110812896::get_offset_of_m_IsOn_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { sizeof (ToggleTransition_t2757337633)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1620[3] = 
{
	ToggleTransition_t2757337633::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { sizeof (ToggleEvent_t2331340366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { sizeof (ToggleGroup_t1990156785), -1, sizeof(ToggleGroup_t1990156785_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1622[4] = 
{
	ToggleGroup_t1990156785::get_offset_of_m_AllowSwitchOff_2(),
	ToggleGroup_t1990156785::get_offset_of_m_Toggles_3(),
	ToggleGroup_t1990156785_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_4(),
	ToggleGroup_t1990156785_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { sizeof (ClipperRegistry_t1074114320), -1, sizeof(ClipperRegistry_t1074114320_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1623[2] = 
{
	ClipperRegistry_t1074114320_StaticFields::get_offset_of_s_Instance_0(),
	ClipperRegistry_t1074114320::get_offset_of_m_Clippers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { sizeof (Clipping_t1257491342), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { sizeof (RectangularVertexClipper_t1294793591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1627[2] = 
{
	RectangularVertexClipper_t1294793591::get_offset_of_m_WorldCorners_0(),
	RectangularVertexClipper_t1294793591::get_offset_of_m_CanvasCorners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { sizeof (AspectRatioFitter_t436718473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1628[4] = 
{
	AspectRatioFitter_t436718473::get_offset_of_m_AspectMode_2(),
	AspectRatioFitter_t436718473::get_offset_of_m_AspectRatio_3(),
	AspectRatioFitter_t436718473::get_offset_of_m_Rect_4(),
	AspectRatioFitter_t436718473::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (AspectMode_t2149445162)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1629[6] = 
{
	AspectMode_t2149445162::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { sizeof (CanvasScaler_t2777732396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1630[14] = 
{
	0,
	CanvasScaler_t2777732396::get_offset_of_m_UiScaleMode_3(),
	CanvasScaler_t2777732396::get_offset_of_m_ReferencePixelsPerUnit_4(),
	CanvasScaler_t2777732396::get_offset_of_m_ScaleFactor_5(),
	CanvasScaler_t2777732396::get_offset_of_m_ReferenceResolution_6(),
	CanvasScaler_t2777732396::get_offset_of_m_ScreenMatchMode_7(),
	CanvasScaler_t2777732396::get_offset_of_m_MatchWidthOrHeight_8(),
	CanvasScaler_t2777732396::get_offset_of_m_PhysicalUnit_9(),
	CanvasScaler_t2777732396::get_offset_of_m_FallbackScreenDPI_10(),
	CanvasScaler_t2777732396::get_offset_of_m_DefaultSpriteDPI_11(),
	CanvasScaler_t2777732396::get_offset_of_m_DynamicPixelsPerUnit_12(),
	CanvasScaler_t2777732396::get_offset_of_m_Canvas_13(),
	CanvasScaler_t2777732396::get_offset_of_m_PrevScaleFactor_14(),
	CanvasScaler_t2777732396::get_offset_of_m_PrevReferencePixelsPerUnit_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { sizeof (ScaleMode_t2493957633)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1631[4] = 
{
	ScaleMode_t2493957633::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { sizeof (ScreenMatchMode_t1153512176)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1632[4] = 
{
	ScreenMatchMode_t1153512176::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { sizeof (Unit_t1837657360)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1633[6] = 
{
	Unit_t1837657360::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { sizeof (ContentSizeFitter_t1285073872), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1634[4] = 
{
	ContentSizeFitter_t1285073872::get_offset_of_m_HorizontalFit_2(),
	ContentSizeFitter_t1285073872::get_offset_of_m_VerticalFit_3(),
	ContentSizeFitter_t1285073872::get_offset_of_m_Rect_4(),
	ContentSizeFitter_t1285073872::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { sizeof (FitMode_t909765868)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1635[4] = 
{
	FitMode_t909765868::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { sizeof (GridLayoutGroup_t169317941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1636[6] = 
{
	GridLayoutGroup_t169317941::get_offset_of_m_StartCorner_10(),
	GridLayoutGroup_t169317941::get_offset_of_m_StartAxis_11(),
	GridLayoutGroup_t169317941::get_offset_of_m_CellSize_12(),
	GridLayoutGroup_t169317941::get_offset_of_m_Spacing_13(),
	GridLayoutGroup_t169317941::get_offset_of_m_Constraint_14(),
	GridLayoutGroup_t169317941::get_offset_of_m_ConstraintCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { sizeof (Corner_t284493240)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1637[5] = 
{
	Corner_t284493240::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { sizeof (Axis_t1399125956)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1638[3] = 
{
	Axis_t1399125956::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { sizeof (Constraint_t1640775616)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1639[4] = 
{
	Constraint_t1640775616::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { sizeof (HorizontalLayoutGroup_t1336501463), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { sizeof (HorizontalOrVerticalLayoutGroup_t2052396382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1641[3] = 
{
	HorizontalOrVerticalLayoutGroup_t2052396382::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t2052396382::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t2052396382::get_offset_of_m_ChildForceExpandHeight_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { sizeof (LayoutElement_t1596995480), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1647[7] = 
{
	LayoutElement_t1596995480::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t1596995480::get_offset_of_m_MinWidth_3(),
	LayoutElement_t1596995480::get_offset_of_m_MinHeight_4(),
	LayoutElement_t1596995480::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t1596995480::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t1596995480::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t1596995480::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { sizeof (LayoutGroup_t352294875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1648[8] = 
{
	LayoutGroup_t352294875::get_offset_of_m_Padding_2(),
	LayoutGroup_t352294875::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t352294875::get_offset_of_m_Rect_4(),
	LayoutGroup_t352294875::get_offset_of_m_Tracker_5(),
	LayoutGroup_t352294875::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t352294875::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t352294875::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t352294875::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { sizeof (LayoutRebuilder_t1942933988), -1, sizeof(LayoutRebuilder_t1942933988_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1649[9] = 
{
	LayoutRebuilder_t1942933988::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t1942933988::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { sizeof (LayoutUtility_t3144854024), -1, sizeof(LayoutUtility_t3144854024_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1650[8] = 
{
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1651 = { sizeof (VerticalLayoutGroup_t423167365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1652 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1653 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1653[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1654 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1654[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1655 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1655[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1656 = { sizeof (VertexHelper_t3377436606), -1, sizeof(VertexHelper_t3377436606_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1656[9] = 
{
	VertexHelper_t3377436606::get_offset_of_m_Positions_0(),
	VertexHelper_t3377436606::get_offset_of_m_Colors_1(),
	VertexHelper_t3377436606::get_offset_of_m_Uv0S_2(),
	VertexHelper_t3377436606::get_offset_of_m_Uv1S_3(),
	VertexHelper_t3377436606::get_offset_of_m_Normals_4(),
	VertexHelper_t3377436606::get_offset_of_m_Tangents_5(),
	VertexHelper_t3377436606::get_offset_of_m_Indices_6(),
	VertexHelper_t3377436606_StaticFields::get_offset_of_s_DefaultTangent_7(),
	VertexHelper_t3377436606_StaticFields::get_offset_of_s_DefaultNormal_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1657 = { sizeof (BaseVertexEffect_t3555037586), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1658 = { sizeof (BaseMeshEffect_t2306480155), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1658[1] = 
{
	BaseMeshEffect_t2306480155::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1659 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1660 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1661 = { sizeof (Outline_t3745177896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1662 = { sizeof (PositionAsUV1_t4062429115), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1663 = { sizeof (Shadow_t75537580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1663[4] = 
{
	0,
	Shadow_t75537580::get_offset_of_m_EffectColor_4(),
	Shadow_t75537580::get_offset_of_m_EffectDistance_5(),
	Shadow_t75537580::get_offset_of_m_UseGraphicAlpha_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1664 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238937), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1664[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1665 = { sizeof (U24ArrayTypeU2412_t3379220351)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3379220351_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1666 = { sizeof (U3CModuleU3E_t86524796), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1667 = { sizeof (BotonInventario_t1587860951), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1667[2] = 
{
	BotonInventario_t1587860951::get_offset_of_inventario_2(),
	BotonInventario_t1587860951::get_offset_of_identificador_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1668 = { sizeof (Combinaciones_t3637091598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1668[4] = 
{
	Combinaciones_t3637091598::get_offset_of_chars_2(),
	Combinaciones_t3637091598::get_offset_of_tablaTextos_3(),
	Combinaciones_t3637091598::get_offset_of_currentPic_4(),
	Combinaciones_t3637091598::get_offset_of_vi_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1669 = { sizeof (CounterChecking_t39316438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1669[19] = 
{
	CounterChecking_t39316438::get_offset_of_gc_2(),
	CounterChecking_t39316438::get_offset_of_combinaciones_3(),
	CounterChecking_t39316438::get_offset_of_vi_4(),
	CounterChecking_t39316438::get_offset_of_ojo_5(),
	CounterChecking_t39316438::get_offset_of_huella_6(),
	CounterChecking_t39316438::get_offset_of_hablar_7(),
	CounterChecking_t39316438::get_offset_of_escudo_8(),
	CounterChecking_t39316438::get_offset_of_manzana_9(),
	CounterChecking_t39316438::get_offset_of_gafas_10(),
	CounterChecking_t39316438::get_offset_of_pocion_11(),
	CounterChecking_t39316438::get_offset_of_cruz_12(),
	CounterChecking_t39316438::get_offset_of_llavero_13(),
	CounterChecking_t39316438::get_offset_of_tenazas_14(),
	CounterChecking_t39316438::get_offset_of_muerte_15(),
	CounterChecking_t39316438::get_offset_of_corazon_16(),
	CounterChecking_t39316438::get_offset_of_c1_17(),
	CounterChecking_t39316438::get_offset_of_c2_18(),
	CounterChecking_t39316438::get_offset_of_c3_19(),
	CounterChecking_t39316438::get_offset_of_Tabla_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1670 = { sizeof (CreditsHUD_t1430045213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1670[4] = 
{
	CreditsHUD_t1430045213::get_offset_of_text_2(),
	CreditsHUD_t1430045213::get_offset_of_slotsObject_3(),
	CreditsHUD_t1430045213::get_offset_of_slotsController_4(),
	CreditsHUD_t1430045213::get_offset_of_timeColorText_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1671 = { sizeof (U3CBlinkTextU3Ec__Iterator0_t3107080670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1671[5] = 
{
	U3CBlinkTextU3Ec__Iterator0_t3107080670::get_offset_of_t_0(),
	U3CBlinkTextU3Ec__Iterator0_t3107080670::get_offset_of_U24PC_1(),
	U3CBlinkTextU3Ec__Iterator0_t3107080670::get_offset_of_U24current_2(),
	U3CBlinkTextU3Ec__Iterator0_t3107080670::get_offset_of_U3CU24U3Et_3(),
	U3CBlinkTextU3Ec__Iterator0_t3107080670::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1672 = { sizeof (Graficos_t156140270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1672[1] = 
{
	Graficos_t156140270::get_offset_of_sprites_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1673 = { sizeof (Inventario_t3778973585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1673[4] = 
{
	Inventario_t3778973585::get_offset_of_listaSprite_2(),
	Inventario_t3778973585::get_offset_of_amounts_3(),
	Inventario_t3778973585::get_offset_of_CellPrefab_4(),
	Inventario_t3778973585::get_offset_of_timeColorPic_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1674 = { sizeof (U3CBlinkPicU3Ec__Iterator1_t852100114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1674[6] = 
{
	U3CBlinkPicU3Ec__Iterator1_t852100114::get_offset_of_U3CiU3E__0_0(),
	U3CBlinkPicU3Ec__Iterator1_t852100114::get_offset_of_t_1(),
	U3CBlinkPicU3Ec__Iterator1_t852100114::get_offset_of_U24PC_2(),
	U3CBlinkPicU3Ec__Iterator1_t852100114::get_offset_of_U24current_3(),
	U3CBlinkPicU3Ec__Iterator1_t852100114::get_offset_of_U3CU24U3Et_4(),
	U3CBlinkPicU3Ec__Iterator1_t852100114::get_offset_of_U3CU3Ef__this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1675 = { sizeof (Moneco_t2310332709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1675[1] = 
{
	Moneco_t2310332709::get_offset_of_caras_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1676 = { sizeof (PriceHUD_t3182648078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1676[4] = 
{
	PriceHUD_t3182648078::get_offset_of_text_2(),
	PriceHUD_t3182648078::get_offset_of_slotsObject_3(),
	PriceHUD_t3182648078::get_offset_of_slotsController_4(),
	PriceHUD_t3182648078::get_offset_of_timeColorText_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1677 = { sizeof (U3CBlinkTextU3Ec__Iterator2_t3201916881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1677[5] = 
{
	U3CBlinkTextU3Ec__Iterator2_t3201916881::get_offset_of_t_0(),
	U3CBlinkTextU3Ec__Iterator2_t3201916881::get_offset_of_U24PC_1(),
	U3CBlinkTextU3Ec__Iterator2_t3201916881::get_offset_of_U24current_2(),
	U3CBlinkTextU3Ec__Iterator2_t3201916881::get_offset_of_U3CU24U3Et_3(),
	U3CBlinkTextU3Ec__Iterator2_t3201916881::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1678 = { sizeof (Slots_t79980053), -1, sizeof(Slots_t79980053_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1678[57] = 
{
	Slots_t79980053::get_offset_of_priceHUD_2(),
	Slots_t79980053::get_offset_of_creditsHUD_3(),
	Slots_t79980053::get_offset_of_audio_4(),
	Slots_t79980053::get_offset_of_SlotpParent_5(),
	Slots_t79980053::get_offset_of_slot1_6(),
	Slots_t79980053::get_offset_of_slot2_7(),
	Slots_t79980053::get_offset_of_slot3_8(),
	Slots_t79980053::get_offset_of_HUD_9(),
	Slots_t79980053::get_offset_of_HitButton_10(),
	Slots_t79980053::get_offset_of_ThreeButtons_11(),
	Slots_t79980053::get_offset_of_inventario_12(),
	Slots_t79980053::get_offset_of_StartButton_13(),
	Slots_t79980053::get_offset_of_Audio_14(),
	Slots_t79980053::get_offset_of_Caption_15(),
	Slots_t79980053::get_offset_of_PicChar_16(),
	Slots_t79980053::get_offset_of_PicCanvas_17(),
	Slots_t79980053::get_offset_of_Tabla_18(),
	Slots_t79980053::get_offset_of_HitStopText_19(),
	Slots_t79980053::get_offset_of_counterScreen1_20(),
	Slots_t79980053::get_offset_of_counterScreen2_21(),
	Slots_t79980053::get_offset_of_counterScreen3_22(),
	Slots_t79980053::get_offset_of_counterChar_23(),
	Slots_t79980053::get_offset_of_panelInventario_24(),
	Slots_t79980053::get_offset_of_panelGame_25(),
	Slots_t79980053::get_offset_of_counters_26(),
	Slots_t79980053::get_offset_of_slotsG_27(),
	Slots_t79980053::get_offset_of_win_28(),
	Slots_t79980053::get_offset_of_winBig_29(),
	Slots_t79980053::get_offset_of_result_30(),
	Slots_t79980053::get_offset_of_start_31(),
	Slots_t79980053::get_offset_of_hit_32(),
	Slots_t79980053::get_offset_of_coin_33(),
	Slots_t79980053::get_offset_of_blueFrame_34(),
	Slots_t79980053::get_offset_of_canvas_35(),
	Slots_t79980053::get_offset_of_caption_36(),
	Slots_t79980053::get_offset_of_picChar_37(),
	Slots_t79980053::get_offset_of_picCanvas_38(),
	Slots_t79980053::get_offset_of_combinaciones_39(),
	Slots_t79980053::get_offset_of_counterChecking_40(),
	Slots_t79980053::get_offset_of_currentTimer_41(),
	Slots_t79980053::get_offset_of_timer_42(),
	Slots_t79980053::get_offset_of_timeHightlight_43(),
	Slots_t79980053::get_offset_of_rolling_44(),
	Slots_t79980053::get_offset_of_choice_45(),
	Slots_t79980053::get_offset_of_frozenSlots_46(),
	Slots_t79980053::get_offset_of_frozenRound_47(),
	Slots_t79980053::get_offset_of_slots_48(),
	Slots_t79980053::get_offset_of_vi_49(),
	Slots_t79980053::get_offset_of_a_50(),
	Slots_t79980053::get_offset_of_b_51(),
	Slots_t79980053::get_offset_of_c_52(),
	Slots_t79980053::get_offset_of_tempFrozen_53(),
	Slots_t79980053::get_offset_of_price_54(),
	Slots_t79980053::get_offset_of_credits_55(),
	Slots_t79980053::get_offset_of_heightHUD_56(),
	Slots_t79980053_StaticFields::get_offset_of_numLock_57(),
	Slots_t79980053::get_offset_of_currentState_58(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1679 = { sizeof (State_t3376307191)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1679[6] = 
{
	State_t3376307191::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1680 = { sizeof (U3CrollU3Ec__Iterator3_t2129678497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1680[3] = 
{
	U3CrollU3Ec__Iterator3_t2129678497::get_offset_of_U24PC_0(),
	U3CrollU3Ec__Iterator3_t2129678497::get_offset_of_U24current_1(),
	U3CrollU3Ec__Iterator3_t2129678497::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1681 = { sizeof (U3CChangePicU3Ec__Iterator4_t1863457897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1681[6] = 
{
	U3CChangePicU3Ec__Iterator4_t1863457897::get_offset_of_U3CcU3E__0_0(),
	U3CChangePicU3Ec__Iterator4_t1863457897::get_offset_of_i_1(),
	U3CChangePicU3Ec__Iterator4_t1863457897::get_offset_of_U24PC_2(),
	U3CChangePicU3Ec__Iterator4_t1863457897::get_offset_of_U24current_3(),
	U3CChangePicU3Ec__Iterator4_t1863457897::get_offset_of_U3CU24U3Ei_4(),
	U3CChangePicU3Ec__Iterator4_t1863457897::get_offset_of_U3CU3Ef__this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1682 = { sizeof (U3CBringManU3Ec__Iterator5_t2482726264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1682[8] = 
{
	U3CBringManU3Ec__Iterator5_t2482726264::get_offset_of_U3CstartU3E__0_0(),
	U3CBringManU3Ec__Iterator5_t2482726264::get_offset_of_U3CstartTimeU3E__1_1(),
	U3CBringManU3Ec__Iterator5_t2482726264::get_offset_of_U3CoverTimeU3E__2_2(),
	U3CBringManU3Ec__Iterator5_t2482726264::get_offset_of_U3CmovementU3E__3_3(),
	U3CBringManU3Ec__Iterator5_t2482726264::get_offset_of_U3CposU3E__4_4(),
	U3CBringManU3Ec__Iterator5_t2482726264::get_offset_of_U24PC_5(),
	U3CBringManU3Ec__Iterator5_t2482726264::get_offset_of_U24current_6(),
	U3CBringManU3Ec__Iterator5_t2482726264::get_offset_of_U3CU3Ef__this_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1683 = { sizeof (U3CBringHUDU3Ec__Iterator6_t3883270396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1683[8] = 
{
	U3CBringHUDU3Ec__Iterator6_t3883270396::get_offset_of_U3CstartU3E__0_0(),
	U3CBringHUDU3Ec__Iterator6_t3883270396::get_offset_of_U3CstartTimeU3E__1_1(),
	U3CBringHUDU3Ec__Iterator6_t3883270396::get_offset_of_U3CoverTimeU3E__2_2(),
	U3CBringHUDU3Ec__Iterator6_t3883270396::get_offset_of_U3CmovementU3E__3_3(),
	U3CBringHUDU3Ec__Iterator6_t3883270396::get_offset_of_U3CposU3E__4_4(),
	U3CBringHUDU3Ec__Iterator6_t3883270396::get_offset_of_U24PC_5(),
	U3CBringHUDU3Ec__Iterator6_t3883270396::get_offset_of_U24current_6(),
	U3CBringHUDU3Ec__Iterator6_t3883270396::get_offset_of_U3CU3Ef__this_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1684 = { sizeof (U3CBringBackHUDU3Ec__Iterator7_t111350788), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1684[8] = 
{
	U3CBringBackHUDU3Ec__Iterator7_t111350788::get_offset_of_U3CstartU3E__0_0(),
	U3CBringBackHUDU3Ec__Iterator7_t111350788::get_offset_of_U3CstartTimeU3E__1_1(),
	U3CBringBackHUDU3Ec__Iterator7_t111350788::get_offset_of_U3CoverTimeU3E__2_2(),
	U3CBringBackHUDU3Ec__Iterator7_t111350788::get_offset_of_U3CmovementU3E__3_3(),
	U3CBringBackHUDU3Ec__Iterator7_t111350788::get_offset_of_U3CposU3E__4_4(),
	U3CBringBackHUDU3Ec__Iterator7_t111350788::get_offset_of_U24PC_5(),
	U3CBringBackHUDU3Ec__Iterator7_t111350788::get_offset_of_U24current_6(),
	U3CBringBackHUDU3Ec__Iterator7_t111350788::get_offset_of_U3CU3Ef__this_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1685 = { sizeof (U3CBringMapU3Ec__Iterator8_t1471609017), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1685[8] = 
{
	U3CBringMapU3Ec__Iterator8_t1471609017::get_offset_of_U3CstartU3E__0_0(),
	U3CBringMapU3Ec__Iterator8_t1471609017::get_offset_of_U3CstartTimeU3E__1_1(),
	U3CBringMapU3Ec__Iterator8_t1471609017::get_offset_of_U3CoverTimeU3E__2_2(),
	U3CBringMapU3Ec__Iterator8_t1471609017::get_offset_of_U3CmovementU3E__3_3(),
	U3CBringMapU3Ec__Iterator8_t1471609017::get_offset_of_U3CposU3E__4_4(),
	U3CBringMapU3Ec__Iterator8_t1471609017::get_offset_of_U24PC_5(),
	U3CBringMapU3Ec__Iterator8_t1471609017::get_offset_of_U24current_6(),
	U3CBringMapU3Ec__Iterator8_t1471609017::get_offset_of_U3CU3Ef__this_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1686 = { sizeof (U3CBringInventoryU3Ec__Iterator9_t2737813690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1686[8] = 
{
	U3CBringInventoryU3Ec__Iterator9_t2737813690::get_offset_of_U3CstartU3E__0_0(),
	U3CBringInventoryU3Ec__Iterator9_t2737813690::get_offset_of_U3CstartTimeU3E__1_1(),
	U3CBringInventoryU3Ec__Iterator9_t2737813690::get_offset_of_U3CoverTimeU3E__2_2(),
	U3CBringInventoryU3Ec__Iterator9_t2737813690::get_offset_of_U3CmovementU3E__3_3(),
	U3CBringInventoryU3Ec__Iterator9_t2737813690::get_offset_of_U3CposU3E__4_4(),
	U3CBringInventoryU3Ec__Iterator9_t2737813690::get_offset_of_U24PC_5(),
	U3CBringInventoryU3Ec__Iterator9_t2737813690::get_offset_of_U24current_6(),
	U3CBringInventoryU3Ec__Iterator9_t2737813690::get_offset_of_U3CU3Ef__this_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1687 = { sizeof (U3CHightlightSlotU3Ec__IteratorA_t3561306698), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1687[5] = 
{
	U3CHightlightSlotU3Ec__IteratorA_t3561306698::get_offset_of_t_0(),
	U3CHightlightSlotU3Ec__IteratorA_t3561306698::get_offset_of_U24PC_1(),
	U3CHightlightSlotU3Ec__IteratorA_t3561306698::get_offset_of_U24current_2(),
	U3CHightlightSlotU3Ec__IteratorA_t3561306698::get_offset_of_U3CU24U3Et_3(),
	U3CHightlightSlotU3Ec__IteratorA_t3561306698::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1688 = { sizeof (ClickedWaveAnimation_t3762715588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1688[4] = 
{
	ClickedWaveAnimation_t3762715588::get_offset_of_WaveObject_2(),
	ClickedWaveAnimation_t3762715588::get_offset_of_CanvasMain_3(),
	ClickedWaveAnimation_t3762715588::get_offset_of_PoolSize_4(),
	ClickedWaveAnimation_t3762715588::get_offset_of_poolClass_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1689 = { sizeof (Pool_t2493500), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1689[2] = 
{
	Pool_t2493500::get_offset_of_ObjectPool_2(),
	Pool_t2493500::get_offset_of_ObjectToPool_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1690 = { sizeof (ActivateOnEnable_t2656425589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1690[1] = 
{
	ActivateOnEnable_t2656425589::get_offset_of_EasyTweenStart_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1691 = { sizeof (U3CStartU3Ec__IteratorB_t1046475311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1691[3] = 
{
	U3CStartU3Ec__IteratorB_t1046475311::get_offset_of_U24PC_0(),
	U3CStartU3Ec__IteratorB_t1046475311::get_offset_of_U24current_1(),
	U3CStartU3Ec__IteratorB_t1046475311::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1692 = { sizeof (CreateAnimImage_t2928946190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1692[13] = 
{
	CreateAnimImage_t2928946190::get_offset_of_createImageOtherReference_2(),
	CreateAnimImage_t2928946190::get_offset_of_CreateInstance_3(),
	CreateAnimImage_t2928946190::get_offset_of_HowManyButtons_4(),
	CreateAnimImage_t2928946190::get_offset_of_StartAnim_5(),
	CreateAnimImage_t2928946190::get_offset_of_EndAnim_6(),
	CreateAnimImage_t2928946190::get_offset_of_Offset_7(),
	CreateAnimImage_t2928946190::get_offset_of_EnterAnim_8(),
	CreateAnimImage_t2928946190::get_offset_of_ExitAnim_9(),
	CreateAnimImage_t2928946190::get_offset_of_RootRect_10(),
	CreateAnimImage_t2928946190::get_offset_of_RootCanvas_11(),
	CreateAnimImage_t2928946190::get_offset_of_Created_12(),
	CreateAnimImage_t2928946190::get_offset_of_InitialCanvasScrollSize_13(),
	CreateAnimImage_t2928946190::get_offset_of_totalWidth_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1693 = { sizeof (FollowClick_t3238389815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1693[4] = 
{
	FollowClick_t3238389815::get_offset_of_LeftClick_2(),
	FollowClick_t3238389815::get_offset_of_RightClick_3(),
	FollowClick_t3238389815::get_offset_of_TweenToControl_4(),
	FollowClick_t3238389815::get_offset_of_RootCanvas_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1694 = { sizeof (EasyTween_t3917628265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1694[3] = 
{
	EasyTween_t3917628265::get_offset_of_rectTransform_2(),
	EasyTween_t3917628265::get_offset_of_animationParts_3(),
	EasyTween_t3917628265::get_offset_of_currentAnimationGoing_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1695 = { sizeof (ReferencedFrom_t1396252131), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1696 = { sizeof (CurrentAnimation_t3872301071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1696[14] = 
{
	CurrentAnimation_t3872301071::get_offset_of_animationPart_0(),
	CurrentAnimation_t3872301071::get_offset_of_counterTween_1(),
	CurrentAnimation_t3872301071::get_offset_of_AnimationStates_2(),
	CurrentAnimation_t3872301071::get_offset_of_currentAnimationCurvePos_3(),
	CurrentAnimation_t3872301071::get_offset_of_currentStartPos_4(),
	CurrentAnimation_t3872301071::get_offset_of_currentEndPos_5(),
	CurrentAnimation_t3872301071::get_offset_of_currentAnimationCurveScale_6(),
	CurrentAnimation_t3872301071::get_offset_of_currentStartScale_7(),
	CurrentAnimation_t3872301071::get_offset_of_currentEndScale_8(),
	CurrentAnimation_t3872301071::get_offset_of_currentAnimationCurveRot_9(),
	CurrentAnimation_t3872301071::get_offset_of_currentStartRot_10(),
	CurrentAnimation_t3872301071::get_offset_of_currentEndRot_11(),
	CurrentAnimation_t3872301071::get_offset_of_startAlphaValue_12(),
	CurrentAnimation_t3872301071::get_offset_of_endAlphaValue_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1697 = { sizeof (States_t3185248719)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1697[7] = 
{
	States_t3185248719::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1698 = { sizeof (PositionPropetiesAnim_t3297200579), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1698[5] = 
{
	PositionPropetiesAnim_t3297200579::get_offset_of_positionEnabled_0(),
	PositionPropetiesAnim_t3297200579::get_offset_of_TweenCurveEnterPos_1(),
	PositionPropetiesAnim_t3297200579::get_offset_of_TweenCurveExitPos_2(),
	PositionPropetiesAnim_t3297200579::get_offset_of_StartPos_3(),
	PositionPropetiesAnim_t3297200579::get_offset_of_EndPos_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1699 = { sizeof (ScalePropetiesAnim_t205839632), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1699[5] = 
{
	ScalePropetiesAnim_t205839632::get_offset_of_scaleEnabled_0(),
	ScalePropetiesAnim_t205839632::get_offset_of_TweenCurveEnterScale_1(),
	ScalePropetiesAnim_t205839632::get_offset_of_TweenCurveExitScale_2(),
	ScalePropetiesAnim_t205839632::get_offset_of_StartScale_3(),
	ScalePropetiesAnim_t205839632::get_offset_of_EndScale_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
