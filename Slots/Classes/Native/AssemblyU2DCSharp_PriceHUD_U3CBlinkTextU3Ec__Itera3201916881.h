﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// PriceHUD
struct PriceHUD_t3182648078;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PriceHUD/<BlinkText>c__Iterator2
struct  U3CBlinkTextU3Ec__Iterator2_t3201916881  : public Il2CppObject
{
public:
	// System.Single PriceHUD/<BlinkText>c__Iterator2::t
	float ___t_0;
	// System.Int32 PriceHUD/<BlinkText>c__Iterator2::$PC
	int32_t ___U24PC_1;
	// System.Object PriceHUD/<BlinkText>c__Iterator2::$current
	Il2CppObject * ___U24current_2;
	// System.Single PriceHUD/<BlinkText>c__Iterator2::<$>t
	float ___U3CU24U3Et_3;
	// PriceHUD PriceHUD/<BlinkText>c__Iterator2::<>f__this
	PriceHUD_t3182648078 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(U3CBlinkTextU3Ec__Iterator2_t3201916881, ___t_0)); }
	inline float get_t_0() const { return ___t_0; }
	inline float* get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(float value)
	{
		___t_0 = value;
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CBlinkTextU3Ec__Iterator2_t3201916881, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CBlinkTextU3Ec__Iterator2_t3201916881, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Et_3() { return static_cast<int32_t>(offsetof(U3CBlinkTextU3Ec__Iterator2_t3201916881, ___U3CU24U3Et_3)); }
	inline float get_U3CU24U3Et_3() const { return ___U3CU24U3Et_3; }
	inline float* get_address_of_U3CU24U3Et_3() { return &___U3CU24U3Et_3; }
	inline void set_U3CU24U3Et_3(float value)
	{
		___U3CU24U3Et_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CBlinkTextU3Ec__Iterator2_t3201916881, ___U3CU3Ef__this_4)); }
	inline PriceHUD_t3182648078 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline PriceHUD_t3182648078 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(PriceHUD_t3182648078 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
