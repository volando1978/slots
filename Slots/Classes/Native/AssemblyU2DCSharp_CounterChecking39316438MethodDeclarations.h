﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CounterChecking
struct CounterChecking_t39316438;

#include "codegen/il2cpp-codegen.h"

// System.Void CounterChecking::.ctor()
extern "C"  void CounterChecking__ctor_m569166933 (CounterChecking_t39316438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CounterChecking::Start()
extern "C"  void CounterChecking_Start_m3811272021 (CounterChecking_t39316438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CounterChecking::Update()
extern "C"  void CounterChecking_Update_m2191167832 (CounterChecking_t39316438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CounterChecking::ActualizaCounters()
extern "C"  void CounterChecking_ActualizaCounters_m3196632396 (CounterChecking_t39316438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CounterChecking::CounterCheck()
extern "C"  void CounterChecking_CounterCheck_m2647319995 (CounterChecking_t39316438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
