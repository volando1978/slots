﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Slots/<BringMap>c__Iterator8
struct U3CBringMapU3Ec__Iterator8_t1471609017;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Slots/<BringMap>c__Iterator8::.ctor()
extern "C"  void U3CBringMapU3Ec__Iterator8__ctor_m3075705346 (U3CBringMapU3Ec__Iterator8_t1471609017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Slots/<BringMap>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBringMapU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2442465178 (U3CBringMapU3Ec__Iterator8_t1471609017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Slots/<BringMap>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBringMapU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m1912440622 (U3CBringMapU3Ec__Iterator8_t1471609017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Slots/<BringMap>c__Iterator8::MoveNext()
extern "C"  bool U3CBringMapU3Ec__Iterator8_MoveNext_m4274257562 (U3CBringMapU3Ec__Iterator8_t1471609017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots/<BringMap>c__Iterator8::Dispose()
extern "C"  void U3CBringMapU3Ec__Iterator8_Dispose_m713620927 (U3CBringMapU3Ec__Iterator8_t1471609017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots/<BringMap>c__Iterator8::Reset()
extern "C"  void U3CBringMapU3Ec__Iterator8_Reset_m722138287 (U3CBringMapU3Ec__Iterator8_t1471609017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
