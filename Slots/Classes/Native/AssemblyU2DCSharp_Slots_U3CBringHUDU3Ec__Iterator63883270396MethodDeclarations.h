﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Slots/<BringHUD>c__Iterator6
struct U3CBringHUDU3Ec__Iterator6_t3883270396;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Slots/<BringHUD>c__Iterator6::.ctor()
extern "C"  void U3CBringHUDU3Ec__Iterator6__ctor_m2838895903 (U3CBringHUDU3Ec__Iterator6_t3883270396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Slots/<BringHUD>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBringHUDU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3351622813 (U3CBringHUDU3Ec__Iterator6_t3883270396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Slots/<BringHUD>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBringHUDU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m3685126705 (U3CBringHUDU3Ec__Iterator6_t3883270396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Slots/<BringHUD>c__Iterator6::MoveNext()
extern "C"  bool U3CBringHUDU3Ec__Iterator6_MoveNext_m1820441181 (U3CBringHUDU3Ec__Iterator6_t3883270396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots/<BringHUD>c__Iterator6::Dispose()
extern "C"  void U3CBringHUDU3Ec__Iterator6_Dispose_m773012892 (U3CBringHUDU3Ec__Iterator6_t3883270396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots/<BringHUD>c__Iterator6::Reset()
extern "C"  void U3CBringHUDU3Ec__Iterator6_Reset_m485328844 (U3CBringHUDU3Ec__Iterator6_t3883270396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
