﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2761310900;
// System.String[]
struct StringU5BU5D_t4054002952;
// Vinetas
struct Vinetas_t2126676892;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Combinaciones
struct  Combinaciones_t3637091598  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Sprite[] Combinaciones::chars
	SpriteU5BU5D_t2761310900* ___chars_2;
	// System.String[] Combinaciones::tablaTextos
	StringU5BU5D_t4054002952* ___tablaTextos_3;
	// System.Int32 Combinaciones::currentPic
	int32_t ___currentPic_4;
	// Vinetas Combinaciones::vi
	Vinetas_t2126676892 * ___vi_5;

public:
	inline static int32_t get_offset_of_chars_2() { return static_cast<int32_t>(offsetof(Combinaciones_t3637091598, ___chars_2)); }
	inline SpriteU5BU5D_t2761310900* get_chars_2() const { return ___chars_2; }
	inline SpriteU5BU5D_t2761310900** get_address_of_chars_2() { return &___chars_2; }
	inline void set_chars_2(SpriteU5BU5D_t2761310900* value)
	{
		___chars_2 = value;
		Il2CppCodeGenWriteBarrier(&___chars_2, value);
	}

	inline static int32_t get_offset_of_tablaTextos_3() { return static_cast<int32_t>(offsetof(Combinaciones_t3637091598, ___tablaTextos_3)); }
	inline StringU5BU5D_t4054002952* get_tablaTextos_3() const { return ___tablaTextos_3; }
	inline StringU5BU5D_t4054002952** get_address_of_tablaTextos_3() { return &___tablaTextos_3; }
	inline void set_tablaTextos_3(StringU5BU5D_t4054002952* value)
	{
		___tablaTextos_3 = value;
		Il2CppCodeGenWriteBarrier(&___tablaTextos_3, value);
	}

	inline static int32_t get_offset_of_currentPic_4() { return static_cast<int32_t>(offsetof(Combinaciones_t3637091598, ___currentPic_4)); }
	inline int32_t get_currentPic_4() const { return ___currentPic_4; }
	inline int32_t* get_address_of_currentPic_4() { return &___currentPic_4; }
	inline void set_currentPic_4(int32_t value)
	{
		___currentPic_4 = value;
	}

	inline static int32_t get_offset_of_vi_5() { return static_cast<int32_t>(offsetof(Combinaciones_t3637091598, ___vi_5)); }
	inline Vinetas_t2126676892 * get_vi_5() const { return ___vi_5; }
	inline Vinetas_t2126676892 ** get_address_of_vi_5() { return &___vi_5; }
	inline void set_vi_5(Vinetas_t2126676892 * value)
	{
		___vi_5 = value;
		Il2CppCodeGenWriteBarrier(&___vi_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
