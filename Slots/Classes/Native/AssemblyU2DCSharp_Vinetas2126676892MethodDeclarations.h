﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vinetas
struct Vinetas_t2126676892;

#include "codegen/il2cpp-codegen.h"

// System.Void Vinetas::.ctor()
extern "C"  void Vinetas__ctor_m525007695 (Vinetas_t2126676892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vinetas::Start()
extern "C"  void Vinetas_Start_m3767112783 (Vinetas_t2126676892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
