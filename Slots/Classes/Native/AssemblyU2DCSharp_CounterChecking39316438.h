﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Slots
struct Slots_t79980053;
// Combinaciones
struct Combinaciones_t3637091598;
// Vinetas
struct Vinetas_t2126676892;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CounterChecking
struct  CounterChecking_t39316438  : public MonoBehaviour_t667441552
{
public:
	// Slots CounterChecking::gc
	Slots_t79980053 * ___gc_2;
	// Combinaciones CounterChecking::combinaciones
	Combinaciones_t3637091598 * ___combinaciones_3;
	// Vinetas CounterChecking::vi
	Vinetas_t2126676892 * ___vi_4;
	// UnityEngine.Sprite CounterChecking::ojo
	Sprite_t3199167241 * ___ojo_5;
	// UnityEngine.Sprite CounterChecking::huella
	Sprite_t3199167241 * ___huella_6;
	// UnityEngine.Sprite CounterChecking::hablar
	Sprite_t3199167241 * ___hablar_7;
	// UnityEngine.Sprite CounterChecking::escudo
	Sprite_t3199167241 * ___escudo_8;
	// UnityEngine.Sprite CounterChecking::manzana
	Sprite_t3199167241 * ___manzana_9;
	// UnityEngine.Sprite CounterChecking::gafas
	Sprite_t3199167241 * ___gafas_10;
	// UnityEngine.Sprite CounterChecking::pocion
	Sprite_t3199167241 * ___pocion_11;
	// UnityEngine.Sprite CounterChecking::cruz
	Sprite_t3199167241 * ___cruz_12;
	// UnityEngine.Sprite CounterChecking::llavero
	Sprite_t3199167241 * ___llavero_13;
	// UnityEngine.Sprite CounterChecking::tenazas
	Sprite_t3199167241 * ___tenazas_14;
	// UnityEngine.Sprite CounterChecking::muerte
	Sprite_t3199167241 * ___muerte_15;
	// UnityEngine.Sprite CounterChecking::corazon
	Sprite_t3199167241 * ___corazon_16;
	// UnityEngine.Sprite CounterChecking::c1
	Sprite_t3199167241 * ___c1_17;
	// UnityEngine.Sprite CounterChecking::c2
	Sprite_t3199167241 * ___c2_18;
	// UnityEngine.Sprite CounterChecking::c3
	Sprite_t3199167241 * ___c3_19;
	// UnityEngine.GameObject CounterChecking::Tabla
	GameObject_t3674682005 * ___Tabla_20;

public:
	inline static int32_t get_offset_of_gc_2() { return static_cast<int32_t>(offsetof(CounterChecking_t39316438, ___gc_2)); }
	inline Slots_t79980053 * get_gc_2() const { return ___gc_2; }
	inline Slots_t79980053 ** get_address_of_gc_2() { return &___gc_2; }
	inline void set_gc_2(Slots_t79980053 * value)
	{
		___gc_2 = value;
		Il2CppCodeGenWriteBarrier(&___gc_2, value);
	}

	inline static int32_t get_offset_of_combinaciones_3() { return static_cast<int32_t>(offsetof(CounterChecking_t39316438, ___combinaciones_3)); }
	inline Combinaciones_t3637091598 * get_combinaciones_3() const { return ___combinaciones_3; }
	inline Combinaciones_t3637091598 ** get_address_of_combinaciones_3() { return &___combinaciones_3; }
	inline void set_combinaciones_3(Combinaciones_t3637091598 * value)
	{
		___combinaciones_3 = value;
		Il2CppCodeGenWriteBarrier(&___combinaciones_3, value);
	}

	inline static int32_t get_offset_of_vi_4() { return static_cast<int32_t>(offsetof(CounterChecking_t39316438, ___vi_4)); }
	inline Vinetas_t2126676892 * get_vi_4() const { return ___vi_4; }
	inline Vinetas_t2126676892 ** get_address_of_vi_4() { return &___vi_4; }
	inline void set_vi_4(Vinetas_t2126676892 * value)
	{
		___vi_4 = value;
		Il2CppCodeGenWriteBarrier(&___vi_4, value);
	}

	inline static int32_t get_offset_of_ojo_5() { return static_cast<int32_t>(offsetof(CounterChecking_t39316438, ___ojo_5)); }
	inline Sprite_t3199167241 * get_ojo_5() const { return ___ojo_5; }
	inline Sprite_t3199167241 ** get_address_of_ojo_5() { return &___ojo_5; }
	inline void set_ojo_5(Sprite_t3199167241 * value)
	{
		___ojo_5 = value;
		Il2CppCodeGenWriteBarrier(&___ojo_5, value);
	}

	inline static int32_t get_offset_of_huella_6() { return static_cast<int32_t>(offsetof(CounterChecking_t39316438, ___huella_6)); }
	inline Sprite_t3199167241 * get_huella_6() const { return ___huella_6; }
	inline Sprite_t3199167241 ** get_address_of_huella_6() { return &___huella_6; }
	inline void set_huella_6(Sprite_t3199167241 * value)
	{
		___huella_6 = value;
		Il2CppCodeGenWriteBarrier(&___huella_6, value);
	}

	inline static int32_t get_offset_of_hablar_7() { return static_cast<int32_t>(offsetof(CounterChecking_t39316438, ___hablar_7)); }
	inline Sprite_t3199167241 * get_hablar_7() const { return ___hablar_7; }
	inline Sprite_t3199167241 ** get_address_of_hablar_7() { return &___hablar_7; }
	inline void set_hablar_7(Sprite_t3199167241 * value)
	{
		___hablar_7 = value;
		Il2CppCodeGenWriteBarrier(&___hablar_7, value);
	}

	inline static int32_t get_offset_of_escudo_8() { return static_cast<int32_t>(offsetof(CounterChecking_t39316438, ___escudo_8)); }
	inline Sprite_t3199167241 * get_escudo_8() const { return ___escudo_8; }
	inline Sprite_t3199167241 ** get_address_of_escudo_8() { return &___escudo_8; }
	inline void set_escudo_8(Sprite_t3199167241 * value)
	{
		___escudo_8 = value;
		Il2CppCodeGenWriteBarrier(&___escudo_8, value);
	}

	inline static int32_t get_offset_of_manzana_9() { return static_cast<int32_t>(offsetof(CounterChecking_t39316438, ___manzana_9)); }
	inline Sprite_t3199167241 * get_manzana_9() const { return ___manzana_9; }
	inline Sprite_t3199167241 ** get_address_of_manzana_9() { return &___manzana_9; }
	inline void set_manzana_9(Sprite_t3199167241 * value)
	{
		___manzana_9 = value;
		Il2CppCodeGenWriteBarrier(&___manzana_9, value);
	}

	inline static int32_t get_offset_of_gafas_10() { return static_cast<int32_t>(offsetof(CounterChecking_t39316438, ___gafas_10)); }
	inline Sprite_t3199167241 * get_gafas_10() const { return ___gafas_10; }
	inline Sprite_t3199167241 ** get_address_of_gafas_10() { return &___gafas_10; }
	inline void set_gafas_10(Sprite_t3199167241 * value)
	{
		___gafas_10 = value;
		Il2CppCodeGenWriteBarrier(&___gafas_10, value);
	}

	inline static int32_t get_offset_of_pocion_11() { return static_cast<int32_t>(offsetof(CounterChecking_t39316438, ___pocion_11)); }
	inline Sprite_t3199167241 * get_pocion_11() const { return ___pocion_11; }
	inline Sprite_t3199167241 ** get_address_of_pocion_11() { return &___pocion_11; }
	inline void set_pocion_11(Sprite_t3199167241 * value)
	{
		___pocion_11 = value;
		Il2CppCodeGenWriteBarrier(&___pocion_11, value);
	}

	inline static int32_t get_offset_of_cruz_12() { return static_cast<int32_t>(offsetof(CounterChecking_t39316438, ___cruz_12)); }
	inline Sprite_t3199167241 * get_cruz_12() const { return ___cruz_12; }
	inline Sprite_t3199167241 ** get_address_of_cruz_12() { return &___cruz_12; }
	inline void set_cruz_12(Sprite_t3199167241 * value)
	{
		___cruz_12 = value;
		Il2CppCodeGenWriteBarrier(&___cruz_12, value);
	}

	inline static int32_t get_offset_of_llavero_13() { return static_cast<int32_t>(offsetof(CounterChecking_t39316438, ___llavero_13)); }
	inline Sprite_t3199167241 * get_llavero_13() const { return ___llavero_13; }
	inline Sprite_t3199167241 ** get_address_of_llavero_13() { return &___llavero_13; }
	inline void set_llavero_13(Sprite_t3199167241 * value)
	{
		___llavero_13 = value;
		Il2CppCodeGenWriteBarrier(&___llavero_13, value);
	}

	inline static int32_t get_offset_of_tenazas_14() { return static_cast<int32_t>(offsetof(CounterChecking_t39316438, ___tenazas_14)); }
	inline Sprite_t3199167241 * get_tenazas_14() const { return ___tenazas_14; }
	inline Sprite_t3199167241 ** get_address_of_tenazas_14() { return &___tenazas_14; }
	inline void set_tenazas_14(Sprite_t3199167241 * value)
	{
		___tenazas_14 = value;
		Il2CppCodeGenWriteBarrier(&___tenazas_14, value);
	}

	inline static int32_t get_offset_of_muerte_15() { return static_cast<int32_t>(offsetof(CounterChecking_t39316438, ___muerte_15)); }
	inline Sprite_t3199167241 * get_muerte_15() const { return ___muerte_15; }
	inline Sprite_t3199167241 ** get_address_of_muerte_15() { return &___muerte_15; }
	inline void set_muerte_15(Sprite_t3199167241 * value)
	{
		___muerte_15 = value;
		Il2CppCodeGenWriteBarrier(&___muerte_15, value);
	}

	inline static int32_t get_offset_of_corazon_16() { return static_cast<int32_t>(offsetof(CounterChecking_t39316438, ___corazon_16)); }
	inline Sprite_t3199167241 * get_corazon_16() const { return ___corazon_16; }
	inline Sprite_t3199167241 ** get_address_of_corazon_16() { return &___corazon_16; }
	inline void set_corazon_16(Sprite_t3199167241 * value)
	{
		___corazon_16 = value;
		Il2CppCodeGenWriteBarrier(&___corazon_16, value);
	}

	inline static int32_t get_offset_of_c1_17() { return static_cast<int32_t>(offsetof(CounterChecking_t39316438, ___c1_17)); }
	inline Sprite_t3199167241 * get_c1_17() const { return ___c1_17; }
	inline Sprite_t3199167241 ** get_address_of_c1_17() { return &___c1_17; }
	inline void set_c1_17(Sprite_t3199167241 * value)
	{
		___c1_17 = value;
		Il2CppCodeGenWriteBarrier(&___c1_17, value);
	}

	inline static int32_t get_offset_of_c2_18() { return static_cast<int32_t>(offsetof(CounterChecking_t39316438, ___c2_18)); }
	inline Sprite_t3199167241 * get_c2_18() const { return ___c2_18; }
	inline Sprite_t3199167241 ** get_address_of_c2_18() { return &___c2_18; }
	inline void set_c2_18(Sprite_t3199167241 * value)
	{
		___c2_18 = value;
		Il2CppCodeGenWriteBarrier(&___c2_18, value);
	}

	inline static int32_t get_offset_of_c3_19() { return static_cast<int32_t>(offsetof(CounterChecking_t39316438, ___c3_19)); }
	inline Sprite_t3199167241 * get_c3_19() const { return ___c3_19; }
	inline Sprite_t3199167241 ** get_address_of_c3_19() { return &___c3_19; }
	inline void set_c3_19(Sprite_t3199167241 * value)
	{
		___c3_19 = value;
		Il2CppCodeGenWriteBarrier(&___c3_19, value);
	}

	inline static int32_t get_offset_of_Tabla_20() { return static_cast<int32_t>(offsetof(CounterChecking_t39316438, ___Tabla_20)); }
	inline GameObject_t3674682005 * get_Tabla_20() const { return ___Tabla_20; }
	inline GameObject_t3674682005 ** get_address_of_Tabla_20() { return &___Tabla_20; }
	inline void set_Tabla_20(GameObject_t3674682005 * value)
	{
		___Tabla_20 = value;
		Il2CppCodeGenWriteBarrier(&___Tabla_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
