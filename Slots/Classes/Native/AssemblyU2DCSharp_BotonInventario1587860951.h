﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BotonInventario
struct  BotonInventario_t1587860951  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject BotonInventario::inventario
	GameObject_t3674682005 * ___inventario_2;
	// System.Int32 BotonInventario::identificador
	int32_t ___identificador_3;

public:
	inline static int32_t get_offset_of_inventario_2() { return static_cast<int32_t>(offsetof(BotonInventario_t1587860951, ___inventario_2)); }
	inline GameObject_t3674682005 * get_inventario_2() const { return ___inventario_2; }
	inline GameObject_t3674682005 ** get_address_of_inventario_2() { return &___inventario_2; }
	inline void set_inventario_2(GameObject_t3674682005 * value)
	{
		___inventario_2 = value;
		Il2CppCodeGenWriteBarrier(&___inventario_2, value);
	}

	inline static int32_t get_offset_of_identificador_3() { return static_cast<int32_t>(offsetof(BotonInventario_t1587860951, ___identificador_3)); }
	inline int32_t get_identificador_3() const { return ___identificador_3; }
	inline int32_t* get_address_of_identificador_3() { return &___identificador_3; }
	inline void set_identificador_3(int32_t value)
	{
		___identificador_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
