﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Slots/<roll>c__Iterator3
struct U3CrollU3Ec__Iterator3_t2129678497;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Slots/<roll>c__Iterator3::.ctor()
extern "C"  void U3CrollU3Ec__Iterator3__ctor_m1708876570 (U3CrollU3Ec__Iterator3_t2129678497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Slots/<roll>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CrollU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3524343810 (U3CrollU3Ec__Iterator3_t2129678497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Slots/<roll>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CrollU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1348960150 (U3CrollU3Ec__Iterator3_t2129678497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Slots/<roll>c__Iterator3::MoveNext()
extern "C"  bool U3CrollU3Ec__Iterator3_MoveNext_m1645602946 (U3CrollU3Ec__Iterator3_t2129678497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots/<roll>c__Iterator3::Dispose()
extern "C"  void U3CrollU3Ec__Iterator3_Dispose_m1451159767 (U3CrollU3Ec__Iterator3_t2129678497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots/<roll>c__Iterator3::Reset()
extern "C"  void U3CrollU3Ec__Iterator3_Reset_m3650276807 (U3CrollU3Ec__Iterator3_t2129678497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
