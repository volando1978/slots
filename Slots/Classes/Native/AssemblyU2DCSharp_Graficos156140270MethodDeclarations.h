﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Graficos
struct Graficos_t156140270;

#include "codegen/il2cpp-codegen.h"

// System.Void Graficos::.ctor()
extern "C"  void Graficos__ctor_m4272662381 (Graficos_t156140270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Graficos::Start()
extern "C"  void Graficos_Start_m3219800173 (Graficos_t156140270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Graficos::Update()
extern "C"  void Graficos_Update_m1035409728 (Graficos_t156140270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
