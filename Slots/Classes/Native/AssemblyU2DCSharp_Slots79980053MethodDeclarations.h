﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Slots
struct Slots_t79980053;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Sprite3199167241.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void Slots::.ctor()
extern "C"  void Slots__ctor_m2362075894 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::.cctor()
extern "C"  void Slots__cctor_m4022779767 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::Start()
extern "C"  void Slots_Start_m1309213686 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::StartGame()
extern "C"  void Slots_StartGame_m2441643272 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::BackToGame()
extern "C"  void Slots_BackToGame_m88590754 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::BackToInventory()
extern "C"  void Slots_BackToInventory_m114061198 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::BackToMap()
extern "C"  void Slots_BackToMap_m2225157198 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::resetSlotColor()
extern "C"  void Slots_resetSlotColor_m3253019940 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::resetValues()
extern "C"  void Slots_resetValues_m3854918917 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::switchHitButtons(System.Boolean)
extern "C"  void Slots_switchHitButtons_m296069031 (Slots_t79980053 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::switchChoice()
extern "C"  void Slots_switchChoice_m274301731 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::freeze1()
extern "C"  void Slots_freeze1_m3741587822 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::freeze2()
extern "C"  void Slots_freeze2_m3741588783 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::freeze3()
extern "C"  void Slots_freeze3_m3741589744 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::Unfreeze(System.Int32)
extern "C"  void Slots_Unfreeze_m2568076239 (Slots_t79980053 * __this, int32_t ___frozen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::HardUnfreeze()
extern "C"  void Slots_HardUnfreeze_m3907598409 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::resetTimer()
extern "C"  void Slots_resetTimer_m934139748 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::resetVars()
extern "C"  void Slots_resetVars_m2296921103 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::setFrozenRound()
extern "C"  void Slots_setFrozenRound_m421770554 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::SetPosInList()
extern "C"  void Slots_SetPosInList_m3074373091 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::HitSlots()
extern "C"  void Slots_HitSlots_m1900413680 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::setCurrentTimer0()
extern "C"  void Slots_setCurrentTimer0_m3306525328 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Slots::roll()
extern "C"  Il2CppObject * Slots_roll_m4257639267 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::IncreaseAmount(System.Int32)
extern "C"  void Slots_IncreaseAmount_m1773142873 (Slots_t79980053 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::CheckCombination()
extern "C"  void Slots_CheckCombination_m4066843381 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::getCaption(System.Int32)
extern "C"  void Slots_getCaption_m1409653679 (Slots_t79980053 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::SetCounter(System.Int32)
extern "C"  void Slots_SetCounter_m3333443545 (Slots_t79980053 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::getPicture(System.Int32)
extern "C"  void Slots_getPicture_m120001735 (Slots_t79980053 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Slots::ChangePic(UnityEngine.Sprite,System.Int32)
extern "C"  Il2CppObject * Slots_ChangePic_m1339616125 (Slots_t79980053 * __this, Sprite_t3199167241 * ___pic0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::IncreaseNumLock()
extern "C"  void Slots_IncreaseNumLock_m4178141635 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::IncreaseCredit()
extern "C"  void Slots_IncreaseCredit_m538988649 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::IncreasePrice(System.Int32)
extern "C"  void Slots_IncreasePrice_m1585369164 (Slots_t79980053 * __this, int32_t ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::ConvertToCredits()
extern "C"  void Slots_ConvertToCredits_m2564345402 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::UpdatePriceHUD()
extern "C"  void Slots_UpdatePriceHUD_m2862440549 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::UpdatePriceHUDWithBlink()
extern "C"  void Slots_UpdatePriceHUDWithBlink_m232769203 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::UpdateCreditHUD()
extern "C"  void Slots_UpdateCreditHUD_m1312826249 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::UpdateCreditHUDWithBlink()
extern "C"  void Slots_UpdateCreditHUDWithBlink_m2885438479 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::UpdateInventoryWithBlink()
extern "C"  void Slots_UpdateInventoryWithBlink_m2931405617 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::UpdateInventory()
extern "C"  void Slots_UpdateInventory_m1167445543 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::UpdateAll()
extern "C"  void Slots_UpdateAll_m4067659948 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Slots::BringMan()
extern "C"  Il2CppObject * Slots_BringMan_m585588782 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Slots::BringHUD()
extern "C"  Il2CppObject * Slots_BringHUD_m580573323 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Slots::BringBackHUD()
extern "C"  Il2CppObject * Slots_BringBackHUD_m1914403428 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Slots::BringMap()
extern "C"  Il2CppObject * Slots_BringMap_m585590704 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Slots::BringInventory()
extern "C"  Il2CppObject * Slots_BringInventory_m1988301168 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Slots::HightlightSlot(UnityEngine.GameObject,System.Single)
extern "C"  Il2CppObject * Slots_HightlightSlot_m2437507397 (Slots_t79980053 * __this, GameObject_t3674682005 * ___slot0, float ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::setColorFrame(UnityEngine.GameObject)
extern "C"  void Slots_setColorFrame_m2389784440 (Slots_t79980053 * __this, GameObject_t3674682005 * ___slot0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots::SlotsRackSwitch()
extern "C"  void Slots_SlotsRackSwitch_m3493919060 (Slots_t79980053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
