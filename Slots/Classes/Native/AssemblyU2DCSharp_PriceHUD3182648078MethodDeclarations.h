﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PriceHUD
struct PriceHUD_t3182648078;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void PriceHUD::.ctor()
extern "C"  void PriceHUD__ctor_m2070410061 (PriceHUD_t3182648078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PriceHUD::Start()
extern "C"  void PriceHUD_Start_m1017547853 (PriceHUD_t3182648078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PriceHUD::UpdateTextWithBlink()
extern "C"  void PriceHUD_UpdateTextWithBlink_m3409445931 (PriceHUD_t3182648078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PriceHUD::UpdateText()
extern "C"  void PriceHUD_UpdateText_m3474693101 (PriceHUD_t3182648078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PriceHUD::BlinkText(System.Single)
extern "C"  Il2CppObject * PriceHUD_BlinkText_m1826381471 (PriceHUD_t3182648078 * __this, float ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
