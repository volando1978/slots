﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Slots/<BringMan>c__Iterator5
struct U3CBringManU3Ec__Iterator5_t2482726264;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Slots/<BringMan>c__Iterator5::.ctor()
extern "C"  void U3CBringManU3Ec__Iterator5__ctor_m1175716899 (U3CBringManU3Ec__Iterator5_t2482726264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Slots/<BringMan>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBringManU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m850929689 (U3CBringManU3Ec__Iterator5_t2482726264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Slots/<BringMan>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBringManU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m92765613 (U3CBringManU3Ec__Iterator5_t2482726264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Slots/<BringMan>c__Iterator5::MoveNext()
extern "C"  bool U3CBringManU3Ec__Iterator5_MoveNext_m797459673 (U3CBringManU3Ec__Iterator5_t2482726264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots/<BringMan>c__Iterator5::Dispose()
extern "C"  void U3CBringManU3Ec__Iterator5_Dispose_m185824160 (U3CBringManU3Ec__Iterator5_t2482726264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots/<BringMan>c__Iterator5::Reset()
extern "C"  void U3CBringManU3Ec__Iterator5_Reset_m3117117136 (U3CBringManU3Ec__Iterator5_t2482726264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
