﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CreditsHUD
struct CreditsHUD_t1430045213;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void CreditsHUD::.ctor()
extern "C"  void CreditsHUD__ctor_m3189434142 (CreditsHUD_t1430045213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreditsHUD::Start()
extern "C"  void CreditsHUD_Start_m2136571934 (CreditsHUD_t1430045213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreditsHUD::UpdateTextWithBlink()
extern "C"  void CreditsHUD_UpdateTextWithBlink_m275215932 (CreditsHUD_t1430045213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreditsHUD::UpdateText()
extern "C"  void CreditsHUD_UpdateText_m2045600444 (CreditsHUD_t1430045213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CreditsHUD::BlinkText(System.Single)
extern "C"  Il2CppObject * CreditsHUD_BlinkText_m2350740718 (CreditsHUD_t1430045213 * __this, float ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
