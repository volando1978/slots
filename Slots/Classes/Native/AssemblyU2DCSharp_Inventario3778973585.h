﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2761310900;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// UnityEngine.UI.Button
struct Button_t3896396478;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Inventario
struct  Inventario_t3778973585  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Sprite[] Inventario::listaSprite
	SpriteU5BU5D_t2761310900* ___listaSprite_2;
	// System.Int32[] Inventario::amounts
	Int32U5BU5D_t3230847821* ___amounts_3;
	// UnityEngine.UI.Button Inventario::CellPrefab
	Button_t3896396478 * ___CellPrefab_4;
	// System.Single Inventario::timeColorPic
	float ___timeColorPic_5;

public:
	inline static int32_t get_offset_of_listaSprite_2() { return static_cast<int32_t>(offsetof(Inventario_t3778973585, ___listaSprite_2)); }
	inline SpriteU5BU5D_t2761310900* get_listaSprite_2() const { return ___listaSprite_2; }
	inline SpriteU5BU5D_t2761310900** get_address_of_listaSprite_2() { return &___listaSprite_2; }
	inline void set_listaSprite_2(SpriteU5BU5D_t2761310900* value)
	{
		___listaSprite_2 = value;
		Il2CppCodeGenWriteBarrier(&___listaSprite_2, value);
	}

	inline static int32_t get_offset_of_amounts_3() { return static_cast<int32_t>(offsetof(Inventario_t3778973585, ___amounts_3)); }
	inline Int32U5BU5D_t3230847821* get_amounts_3() const { return ___amounts_3; }
	inline Int32U5BU5D_t3230847821** get_address_of_amounts_3() { return &___amounts_3; }
	inline void set_amounts_3(Int32U5BU5D_t3230847821* value)
	{
		___amounts_3 = value;
		Il2CppCodeGenWriteBarrier(&___amounts_3, value);
	}

	inline static int32_t get_offset_of_CellPrefab_4() { return static_cast<int32_t>(offsetof(Inventario_t3778973585, ___CellPrefab_4)); }
	inline Button_t3896396478 * get_CellPrefab_4() const { return ___CellPrefab_4; }
	inline Button_t3896396478 ** get_address_of_CellPrefab_4() { return &___CellPrefab_4; }
	inline void set_CellPrefab_4(Button_t3896396478 * value)
	{
		___CellPrefab_4 = value;
		Il2CppCodeGenWriteBarrier(&___CellPrefab_4, value);
	}

	inline static int32_t get_offset_of_timeColorPic_5() { return static_cast<int32_t>(offsetof(Inventario_t3778973585, ___timeColorPic_5)); }
	inline float get_timeColorPic_5() const { return ___timeColorPic_5; }
	inline float* get_address_of_timeColorPic_5() { return &___timeColorPic_5; }
	inline void set_timeColorPic_5(float value)
	{
		___timeColorPic_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
