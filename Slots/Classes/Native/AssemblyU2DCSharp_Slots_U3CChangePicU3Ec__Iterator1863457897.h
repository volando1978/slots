﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// Slots
struct Slots_t79980053;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Slots/<ChangePic>c__Iterator4
struct  U3CChangePicU3Ec__Iterator4_t1863457897  : public Il2CppObject
{
public:
	// UnityEngine.Color Slots/<ChangePic>c__Iterator4::<c>__0
	Color_t4194546905  ___U3CcU3E__0_0;
	// System.Int32 Slots/<ChangePic>c__Iterator4::i
	int32_t ___i_1;
	// System.Int32 Slots/<ChangePic>c__Iterator4::$PC
	int32_t ___U24PC_2;
	// System.Object Slots/<ChangePic>c__Iterator4::$current
	Il2CppObject * ___U24current_3;
	// System.Int32 Slots/<ChangePic>c__Iterator4::<$>i
	int32_t ___U3CU24U3Ei_4;
	// Slots Slots/<ChangePic>c__Iterator4::<>f__this
	Slots_t79980053 * ___U3CU3Ef__this_5;

public:
	inline static int32_t get_offset_of_U3CcU3E__0_0() { return static_cast<int32_t>(offsetof(U3CChangePicU3Ec__Iterator4_t1863457897, ___U3CcU3E__0_0)); }
	inline Color_t4194546905  get_U3CcU3E__0_0() const { return ___U3CcU3E__0_0; }
	inline Color_t4194546905 * get_address_of_U3CcU3E__0_0() { return &___U3CcU3E__0_0; }
	inline void set_U3CcU3E__0_0(Color_t4194546905  value)
	{
		___U3CcU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_i_1() { return static_cast<int32_t>(offsetof(U3CChangePicU3Ec__Iterator4_t1863457897, ___i_1)); }
	inline int32_t get_i_1() const { return ___i_1; }
	inline int32_t* get_address_of_i_1() { return &___i_1; }
	inline void set_i_1(int32_t value)
	{
		___i_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CChangePicU3Ec__Iterator4_t1863457897, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CChangePicU3Ec__Iterator4_t1863457897, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ei_4() { return static_cast<int32_t>(offsetof(U3CChangePicU3Ec__Iterator4_t1863457897, ___U3CU24U3Ei_4)); }
	inline int32_t get_U3CU24U3Ei_4() const { return ___U3CU24U3Ei_4; }
	inline int32_t* get_address_of_U3CU24U3Ei_4() { return &___U3CU24U3Ei_4; }
	inline void set_U3CU24U3Ei_4(int32_t value)
	{
		___U3CU24U3Ei_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_5() { return static_cast<int32_t>(offsetof(U3CChangePicU3Ec__Iterator4_t1863457897, ___U3CU3Ef__this_5)); }
	inline Slots_t79980053 * get_U3CU3Ef__this_5() const { return ___U3CU3Ef__this_5; }
	inline Slots_t79980053 ** get_address_of_U3CU3Ef__this_5() { return &___U3CU3Ef__this_5; }
	inline void set_U3CU3Ef__this_5(Slots_t79980053 * value)
	{
		___U3CU3Ef__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
