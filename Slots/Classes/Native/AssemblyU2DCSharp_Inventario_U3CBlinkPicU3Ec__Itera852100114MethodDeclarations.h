﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Inventario/<BlinkPic>c__Iterator1
struct U3CBlinkPicU3Ec__Iterator1_t852100114;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Inventario/<BlinkPic>c__Iterator1::.ctor()
extern "C"  void U3CBlinkPicU3Ec__Iterator1__ctor_m1180781977 (U3CBlinkPicU3Ec__Iterator1_t852100114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Inventario/<BlinkPic>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBlinkPicU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2489074841 (U3CBlinkPicU3Ec__Iterator1_t852100114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Inventario/<BlinkPic>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBlinkPicU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m4160002605 (U3CBlinkPicU3Ec__Iterator1_t852100114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Inventario/<BlinkPic>c__Iterator1::MoveNext()
extern "C"  bool U3CBlinkPicU3Ec__Iterator1_MoveNext_m266222267 (U3CBlinkPicU3Ec__Iterator1_t852100114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Inventario/<BlinkPic>c__Iterator1::Dispose()
extern "C"  void U3CBlinkPicU3Ec__Iterator1_Dispose_m758396822 (U3CBlinkPicU3Ec__Iterator1_t852100114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Inventario/<BlinkPic>c__Iterator1::Reset()
extern "C"  void U3CBlinkPicU3Ec__Iterator1_Reset_m3122182214 (U3CBlinkPicU3Ec__Iterator1_t852100114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
