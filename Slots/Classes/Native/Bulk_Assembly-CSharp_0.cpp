﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// ActivateOnEnable
struct ActivateOnEnable_t2656425589;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// ActivateOnEnable/<Start>c__IteratorB
struct U3CStartU3Ec__IteratorB_t1046475311;
// System.Object
struct Il2CppObject;
// BotonInventario
struct BotonInventario_t1587860951;
// UnityEngine.UI.Text
struct Text_t9039225;
// Inventario
struct Inventario_t3778973585;
// ClickedWaveAnimation
struct ClickedWaveAnimation_t3762715588;
// Pool
struct Pool_t2493500;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_t3186046376;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// EasyTween
struct EasyTween_t3917628265;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.UI.Button
struct Button_t3896396478;
// UnityEngine.UI.Mask
struct Mask_t8826680;
// Combinaciones
struct Combinaciones_t3637091598;
// Vinetas
struct Vinetas_t2126676892;
// System.String
struct String_t;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// CounterChecking
struct CounterChecking_t39316438;
// Slots
struct Slots_t79980053;
// UnityEngine.UI.Image
struct Image_t538875265;
// CreateAnimImage
struct CreateAnimImage_t2928946190;
// CreditsHUD
struct CreditsHUD_t1430045213;
// CreditsHUD/<BlinkText>c__Iterator0
struct U3CBlinkTextU3Ec__Iterator0_t3107080670;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3667593487;
// UITween.AnimationParts
struct AnimationParts_t4290477440;
// FollowClick
struct FollowClick_t3238389815;
// Graficos
struct Graficos_t156140270;
// Inventario/<BlinkPic>c__Iterator1
struct U3CBlinkPicU3Ec__Iterator1_t852100114;
// Moneco
struct Moneco_t2310332709;
// PriceHUD
struct PriceHUD_t3182648078;
// PriceHUD/<BlinkText>c__Iterator2
struct U3CBlinkTextU3Ec__Iterator2_t3201916881;
// ReferencedFrom
struct ReferencedFrom_t1396252131;
// UnityEngine.AudioSource
struct AudioSource_t1740077639;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t2548470764;
// Slots/<BringBackHUD>c__Iterator7
struct U3CBringBackHUDU3Ec__Iterator7_t111350788;
// Slots/<BringHUD>c__Iterator6
struct U3CBringHUDU3Ec__Iterator6_t3883270396;
// Slots/<BringInventory>c__Iterator9
struct U3CBringInventoryU3Ec__Iterator9_t2737813690;
// Slots/<BringMan>c__Iterator5
struct U3CBringManU3Ec__Iterator5_t2482726264;
// Slots/<BringMap>c__Iterator8
struct U3CBringMapU3Ec__Iterator8_t1471609017;
// Slots/<ChangePic>c__Iterator4
struct U3CChangePicU3Ec__Iterator4_t1863457897;
// Slots/<HightlightSlot>c__IteratorA
struct U3CHightlightSlotU3Ec__IteratorA_t3561306698;
// Slots/<roll>c__Iterator3
struct U3CrollU3Ec__Iterator3_t2129678497;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t1266085011;
// UITween.AnimationParts/DisableOrDestroy
struct DisableOrDestroy_t4252453067;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// UITween.CurrentAnimation
struct CurrentAnimation_t3872301071;
// UITween.FadePropetiesAnim
struct FadePropetiesAnim_t145139664;
// UITween.PositionPropetiesAnim
struct PositionPropetiesAnim_t3297200579;
// UITween.RotationPropetiesAnim
struct RotationPropetiesAnim_t2262074766;
// UITween.ScalePropetiesAnim
struct ScalePropetiesAnim_t205839632;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DCSharp_ActivateOnEnable2656425589.h"
#include "AssemblyU2DCSharp_ActivateOnEnable2656425589MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "AssemblyU2DCSharp_ActivateOnEnable_U3CStartU3Ec__I1046475311MethodDeclarations.h"
#include "AssemblyU2DCSharp_ActivateOnEnable_U3CStartU3Ec__I1046475311.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Boolean476798718.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame2372756133MethodDeclarations.h"
#include "AssemblyU2DCSharp_EasyTween3917628265MethodDeclarations.h"
#include "mscorlib_System_UInt3224667981.h"
#include "mscorlib_System_Int321153838500.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame2372756133.h"
#include "AssemblyU2DCSharp_EasyTween3917628265.h"
#include "mscorlib_System_NotSupportedException1732551818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1732551818.h"
#include "AssemblyU2DCSharp_BotonInventario1587860951.h"
#include "AssemblyU2DCSharp_BotonInventario1587860951MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UI_UnityEngine_UI_Text9039225.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "AssemblyU2DCSharp_Inventario3778973585.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Int321153838500MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UI_UnityEngine_UI_Text9039225MethodDeclarations.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "AssemblyU2DCSharp_ClickedWaveAnimation3762715588.h"
#include "AssemblyU2DCSharp_ClickedWaveAnimation3762715588MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pool2493500MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pool2493500.h"
#include "UnityEngine_UnityEngine_Input4200062272MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_Color4194546905MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2727095145MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen3187157168MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic3186046376.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic836799438MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic836799438.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "mscorlib_System_Single4291918972.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_RectTransform972643934.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste2276120119MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen835879620MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"
#include "mscorlib_System_Collections_Generic_List_1_gen835879620.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastRes3762661364.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste2276120119.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastRes3762661364MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Button3896396478.h"
#include "UnityEngine_UI_UnityEngine_UI_Mask8826680.h"
#include "AssemblyU2DCSharp_Combinaciones3637091598.h"
#include "AssemblyU2DCSharp_Combinaciones3637091598MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vinetas2126676892.h"
#include "UnityEngine_UnityEngine_Sprite3199167241.h"
#include "UnityEngine_ArrayTypes.h"
#include "AssemblyU2DCSharp_CounterChecking39316438.h"
#include "AssemblyU2DCSharp_CounterChecking39316438MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image538875265MethodDeclarations.h"
#include "AssemblyU2DCSharp_Slots79980053.h"
#include "UnityEngine_UI_UnityEngine_UI_Image538875265.h"
#include "AssemblyU2DCSharp_Slots79980053MethodDeclarations.h"
#include "AssemblyU2DCSharp_Slots_State3376307191.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986.h"
#include "AssemblyU2DCSharp_CreateAnimImage2928946190.h"
#include "AssemblyU2DCSharp_CreateAnimImage2928946190MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen990846521MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen990846521.h"
#include "UnityEngine_UnityEngine_RectTransform972643934MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Rect4241904616MethodDeclarations.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "UnityEngine_UnityEngine_AnimationCurve3667593487.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "AssemblyU2DCSharp_CreditsHUD1430045213.h"
#include "AssemblyU2DCSharp_CreditsHUD1430045213MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine3621161934.h"
#include "AssemblyU2DCSharp_CreditsHUD_U3CBlinkTextU3Ec__Ite3107080670MethodDeclarations.h"
#include "AssemblyU2DCSharp_CreditsHUD_U3CBlinkTextU3Ec__Ite3107080670.h"
#include "UnityEngine_UnityEngine_Time4241968337MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1615819279MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1615819279.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent1266085011MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITween_AnimationParts4290477440MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent1266085011.h"
#include "AssemblyU2DCSharp_UITween_AnimationParts4290477440.h"
#include "AssemblyU2DCSharp_UITween_AnimationParts_State451125205.h"
#include "AssemblyU2DCSharp_UITween_AnimationParts_EndTweenC1761908940.h"
#include "AssemblyU2DCSharp_UITween_AnimationParts_CallbackC2423460991.h"
#include "AssemblyU2DCSharp_UITween_CurrentAnimation3872301071MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITween_CurrentAnimation3872301071.h"
#include "AssemblyU2DCSharp_UITween_AnimationParts_DisableOr4252453067MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITween_AnimationParts_DisableOr4252453067.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_FollowClick3238389815.h"
#include "AssemblyU2DCSharp_FollowClick3238389815MethodDeclarations.h"
#include "AssemblyU2DCSharp_Graficos156140270.h"
#include "AssemblyU2DCSharp_Graficos156140270MethodDeclarations.h"
#include "AssemblyU2DCSharp_Inventario3778973585MethodDeclarations.h"
#include "AssemblyU2DCSharp_Inventario_U3CBlinkPicU3Ec__Itera852100114MethodDeclarations.h"
#include "AssemblyU2DCSharp_Inventario_U3CBlinkPicU3Ec__Itera852100114.h"
#include "AssemblyU2DCSharp_Moneco2310332709.h"
#include "AssemblyU2DCSharp_Moneco2310332709MethodDeclarations.h"
#include "AssemblyU2DCSharp_PriceHUD3182648078.h"
#include "AssemblyU2DCSharp_PriceHUD3182648078MethodDeclarations.h"
#include "AssemblyU2DCSharp_PriceHUD_U3CBlinkTextU3Ec__Itera3201916881MethodDeclarations.h"
#include "AssemblyU2DCSharp_PriceHUD_U3CBlinkTextU3Ec__Itera3201916881.h"
#include "AssemblyU2DCSharp_ReferencedFrom1396252131.h"
#include "AssemblyU2DCSharp_ReferencedFrom1396252131MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpriteRenderer2548470764MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Canvas2727140764MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1740077639.h"
#include "UnityEngine_UnityEngine_SpriteRenderer2548470764.h"
#include "UnityEngine_UnityEngine_Canvas2727140764.h"
#include "UnityEngine_UnityEngine_AudioSource1740077639MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip794140988.h"
#include "UnityEngine_UnityEngine_Random3156561159MethodDeclarations.h"
#include "AssemblyU2DCSharp_Slots_U3CrollU3Ec__Iterator32129678497MethodDeclarations.h"
#include "AssemblyU2DCSharp_Slots_U3CrollU3Ec__Iterator32129678497.h"
#include "AssemblyU2DCSharp_Slots_U3CChangePicU3Ec__Iterator1863457897MethodDeclarations.h"
#include "AssemblyU2DCSharp_Slots_U3CChangePicU3Ec__Iterator1863457897.h"
#include "AssemblyU2DCSharp_Slots_U3CBringManU3Ec__Iterator52482726264MethodDeclarations.h"
#include "AssemblyU2DCSharp_Slots_U3CBringManU3Ec__Iterator52482726264.h"
#include "AssemblyU2DCSharp_Slots_U3CBringHUDU3Ec__Iterator63883270396MethodDeclarations.h"
#include "AssemblyU2DCSharp_Slots_U3CBringHUDU3Ec__Iterator63883270396.h"
#include "AssemblyU2DCSharp_Slots_U3CBringBackHUDU3Ec__Iterat111350788MethodDeclarations.h"
#include "AssemblyU2DCSharp_Slots_U3CBringBackHUDU3Ec__Iterat111350788.h"
#include "AssemblyU2DCSharp_Slots_U3CBringMapU3Ec__Iterator81471609017MethodDeclarations.h"
#include "AssemblyU2DCSharp_Slots_U3CBringMapU3Ec__Iterator81471609017.h"
#include "AssemblyU2DCSharp_Slots_U3CBringInventoryU3Ec__Ite2737813690MethodDeclarations.h"
#include "AssemblyU2DCSharp_Slots_U3CBringInventoryU3Ec__Ite2737813690.h"
#include "AssemblyU2DCSharp_Slots_U3CHightlightSlotU3Ec__Ite3561306698MethodDeclarations.h"
#include "AssemblyU2DCSharp_Slots_U3CHightlightSlotU3Ec__Ite3561306698.h"
#include "AssemblyU2DCSharp_Slots_State3376307191MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITween_PositionPropetiesAnim3297200579MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITween_ScalePropetiesAnim205839632MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITween_RotationPropetiesAnim2262074766MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITween_FadePropetiesAnim145139664MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITween_PositionPropetiesAnim3297200579.h"
#include "AssemblyU2DCSharp_UITween_ScalePropetiesAnim205839632.h"
#include "AssemblyU2DCSharp_UITween_RotationPropetiesAnim2262074766.h"
#include "AssemblyU2DCSharp_UITween_FadePropetiesAnim145139664.h"
#include "mscorlib_System_Delegate3310234105MethodDeclarations.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "AssemblyU2DCSharp_UITween_AnimationParts_CallbackC2423460991MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "AssemblyU2DCSharp_UITween_AnimationParts_EndTweenC1761908940MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITween_AnimationParts_State451125205MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITween_CurrentAnimation_States3185248719.h"
#include "UnityEngine_UnityEngine_AnimationCurve3667593487MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITween_CurrentAnimation_States3185248719MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vinetas2126676892MethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m900797242(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.UI.Text>()
#define Component_GetComponentInChildren_TisText_t9039225_m582470501(__this, method) ((  Text_t9039225 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2447772384(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Inventario>()
#define GameObject_GetComponent_TisInventario_t3778973585_m555484204(__this, method) ((  Inventario_t3778973585 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m337943659_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m337943659(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Pool>()
#define GameObject_AddComponent_TisPool_t2493500_m739887158(__this, method) ((  Pool_t2493500 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.MaskableGraphic>()
#define GameObject_GetComponent_TisMaskableGraphic_t3186046376_m1717659230(__this, method) ((  MaskableGraphic_t3186046376 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.MaskableGraphic>()
#define Component_GetComponent_TisMaskableGraphic_t3186046376_m2118237702(__this, method) ((  MaskableGraphic_t3186046376 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.RectTransform>()
#define GameObject_GetComponent_TisRectTransform_t972643934_m406276429(__this, method) ((  RectTransform_t972643934 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<EasyTween>()
#define GameObject_GetComponent_TisEasyTween_t3917628265_m1697414744(__this, method) ((  EasyTween_t3917628265 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Button>()
#define GameObject_GetComponent_TisButton_t3896396478_m2812094092(__this, method) ((  Button_t3896396478 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Mask>()
#define GameObject_GetComponent_TisMask_t8826680_m2460923986(__this, method) ((  Mask_t8826680 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<Vinetas>()
#define Component_GetComponent_TisVinetas_t2126676892_m4099167005(__this, method) ((  Vinetas_t2126676892 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<Slots>()
#define Component_GetComponent_TisSlots_t79980053_m3147794564(__this, method) ((  Slots_t79980053 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Combinaciones>()
#define GameObject_GetComponent_TisCombinaciones_t3637091598_m1358194067(__this, method) ((  Combinaciones_t3637091598 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Vinetas>()
#define GameObject_GetComponent_TisVinetas_t2126676892_m3739561413(__this, method) ((  Vinetas_t2126676892 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
#define GameObject_GetComponent_TisImage_t538875265_m2140199269(__this, method) ((  Image_t538875265 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3133387403_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m3133387403(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3133387403_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t3674682005_m3917608929(__this /* static, unused */, p0, method) ((  GameObject_t3674682005 * (*) (Il2CppObject * /* static, unused */, GameObject_t3674682005 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3133387403_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<Slots>()
#define GameObject_GetComponent_TisSlots_t79980053_m4269206828(__this, method) ((  Slots_t79980053 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
#define Component_GetComponent_TisText_t9039225_m1610753993(__this, method) ((  Text_t9039225 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.UI.Button>(!!0)
#define Object_Instantiate_TisButton_t3896396478_m3384885189(__this /* static, unused */, p0, method) ((  Button_t3896396478 * (*) (Il2CppObject * /* static, unused */, Button_t3896396478 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3133387403_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t538875265_m3706520426(__this, method) ((  Image_t538875265 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<BotonInventario>()
#define Component_GetComponent_TisBotonInventario_t1587860951_m2255649026(__this, method) ((  BotonInventario_t1587860951 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.AudioSource>()
#define GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151(__this, method) ((  AudioSource_t1740077639 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<PriceHUD>()
#define Component_GetComponent_TisPriceHUD_t3182648078_m3029074103(__this, method) ((  PriceHUD_t3182648078 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<CreditsHUD>()
#define Component_GetComponent_TisCreditsHUD_t1430045213_m2894262728(__this, method) ((  CreditsHUD_t1430045213 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t9039225_m202917489(__this, method) ((  Text_t9039225 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.SpriteRenderer>()
#define GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(__this, method) ((  SpriteRenderer_t2548470764 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<CounterChecking>()
#define Component_GetComponent_TisCounterChecking_t39316438_m1809788579(__this, method) ((  CounterChecking_t39316438 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
#define Component_GetComponent_TisRectTransform_t972643934_m1940403147(__this, method) ((  RectTransform_t972643934 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<ReferencedFrom>()
#define Component_GetComponent_TisReferencedFrom_t1396252131_m2689967426(__this, method) ((  ReferencedFrom_t1396252131 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ActivateOnEnable::.ctor()
extern "C"  void ActivateOnEnable__ctor_m2747319238 (ActivateOnEnable_t2656425589 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator ActivateOnEnable::Start()
extern Il2CppClass* U3CStartU3Ec__IteratorB_t1046475311_il2cpp_TypeInfo_var;
extern const uint32_t ActivateOnEnable_Start_m947650430_MetadataUsageId;
extern "C"  Il2CppObject * ActivateOnEnable_Start_m947650430 (ActivateOnEnable_t2656425589 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ActivateOnEnable_Start_m947650430_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CStartU3Ec__IteratorB_t1046475311 * V_0 = NULL;
	{
		U3CStartU3Ec__IteratorB_t1046475311 * L_0 = (U3CStartU3Ec__IteratorB_t1046475311 *)il2cpp_codegen_object_new(U3CStartU3Ec__IteratorB_t1046475311_il2cpp_TypeInfo_var);
		U3CStartU3Ec__IteratorB__ctor_m2820488524(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStartU3Ec__IteratorB_t1046475311 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CStartU3Ec__IteratorB_t1046475311 * L_2 = V_0;
		return L_2;
	}
}
// System.Void ActivateOnEnable/<Start>c__IteratorB::.ctor()
extern "C"  void U3CStartU3Ec__IteratorB__ctor_m2820488524 (U3CStartU3Ec__IteratorB_t1046475311 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object ActivateOnEnable/<Start>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m937595280 (U3CStartU3Ec__IteratorB_t1046475311 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object ActivateOnEnable/<Start>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m1359590692 (U3CStartU3Ec__IteratorB_t1046475311 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean ActivateOnEnable/<Start>c__IteratorB::MoveNext()
extern Il2CppClass* WaitForEndOfFrame_t2372756133_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__IteratorB_MoveNext_m1328487184_MetadataUsageId;
extern "C"  bool U3CStartU3Ec__IteratorB_MoveNext_m1328487184 (U3CStartU3Ec__IteratorB_t1046475311 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__IteratorB_MoveNext_m1328487184_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0038;
		}
	}
	{
		goto IL_004f;
	}

IL_0021:
	{
		WaitForEndOfFrame_t2372756133 * L_2 = (WaitForEndOfFrame_t2372756133 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t2372756133_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m4124201226(L_2, /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		__this->set_U24PC_0(1);
		goto IL_0051;
	}

IL_0038:
	{
		ActivateOnEnable_t2656425589 * L_3 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_3);
		EasyTween_t3917628265 * L_4 = L_3->get_EasyTweenStart_2();
		NullCheck(L_4);
		EasyTween_OpenCloseObjectAnimation_m645046265(L_4, /*hidden argument*/NULL);
		__this->set_U24PC_0((-1));
	}

IL_004f:
	{
		return (bool)0;
	}

IL_0051:
	{
		return (bool)1;
	}
	// Dead block : IL_0053: ldloc.1
}
// System.Void ActivateOnEnable/<Start>c__IteratorB::Dispose()
extern "C"  void U3CStartU3Ec__IteratorB_Dispose_m263390857 (U3CStartU3Ec__IteratorB_t1046475311 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void ActivateOnEnable/<Start>c__IteratorB::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__IteratorB_Reset_m466921465_MetadataUsageId;
extern "C"  void U3CStartU3Ec__IteratorB_Reset_m466921465 (U3CStartU3Ec__IteratorB_t1046475311 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__IteratorB_Reset_m466921465_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void BotonInventario::.ctor()
extern "C"  void BotonInventario__ctor_m4040201076 (BotonInventario_t1587860951 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BotonInventario::Start()
extern const MethodInfo* Component_GetComponentInChildren_TisText_t9039225_m582470501_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisInventario_t3778973585_m555484204_MethodInfo_var;
extern const uint32_t BotonInventario_Start_m2987338868_MetadataUsageId;
extern "C"  void BotonInventario_Start_m2987338868 (BotonInventario_t1587860951 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BotonInventario_Start_m2987338868_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = GameObject_get_transform_m1278640159(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t1659122786 * L_2 = Transform_GetChild_m4040462992(L_1, 0, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = Component_get_gameObject_m1170635899(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_4 = GameObject_get_activeSelf_m3858025161(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004b;
		}
	}
	{
		Text_t9039225 * L_5 = Component_GetComponentInChildren_TisText_t9039225_m582470501(__this, /*hidden argument*/Component_GetComponentInChildren_TisText_t9039225_m582470501_MethodInfo_var);
		GameObject_t3674682005 * L_6 = __this->get_inventario_2();
		NullCheck(L_6);
		Inventario_t3778973585 * L_7 = GameObject_GetComponent_TisInventario_t3778973585_m555484204(L_6, /*hidden argument*/GameObject_GetComponent_TisInventario_t3778973585_m555484204_MethodInfo_var);
		NullCheck(L_7);
		Int32U5BU5D_t3230847821* L_8 = L_7->get_amounts_3();
		int32_t L_9 = __this->get_identificador_3();
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		String_t* L_10 = Int32_ToString_m1286526384(((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9))), /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_10);
	}

IL_004b:
	{
		return;
	}
}
// System.Void BotonInventario::MePulso()
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentInChildren_TisText_t9039225_m582470501_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisInventario_t3778973585_m555484204_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2139338853;
extern const uint32_t BotonInventario_MePulso_m1555080829_MetadataUsageId;
extern "C"  void BotonInventario_MePulso_m1555080829 (BotonInventario_t1587860951 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BotonInventario_MePulso_m1555080829_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = GameObject_get_transform_m1278640159(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t1659122786 * L_2 = Transform_GetChild_m4040462992(L_1, 0, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = Component_get_gameObject_m1170635899(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_4 = GameObject_get_activeSelf_m3858025161(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004b;
		}
	}
	{
		Text_t9039225 * L_5 = Component_GetComponentInChildren_TisText_t9039225_m582470501(__this, /*hidden argument*/Component_GetComponentInChildren_TisText_t9039225_m582470501_MethodInfo_var);
		GameObject_t3674682005 * L_6 = __this->get_inventario_2();
		NullCheck(L_6);
		Inventario_t3778973585 * L_7 = GameObject_GetComponent_TisInventario_t3778973585_m555484204(L_6, /*hidden argument*/GameObject_GetComponent_TisInventario_t3778973585_m555484204_MethodInfo_var);
		NullCheck(L_7);
		Int32U5BU5D_t3230847821* L_8 = L_7->get_amounts_3();
		int32_t L_9 = __this->get_identificador_3();
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		String_t* L_10 = Int32_ToString_m1286526384(((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9))), /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_10);
	}

IL_004b:
	{
		GameObject_t3674682005 * L_11 = __this->get_inventario_2();
		NullCheck(L_11);
		Inventario_t3778973585 * L_12 = GameObject_GetComponent_TisInventario_t3778973585_m555484204(L_11, /*hidden argument*/GameObject_GetComponent_TisInventario_t3778973585_m555484204_MethodInfo_var);
		NullCheck(L_12);
		Int32U5BU5D_t3230847821* L_13 = L_12->get_amounts_3();
		int32_t L_14 = __this->get_identificador_3();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		int32_t L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		int32_t L_17 = L_16;
		Il2CppObject * L_18 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2139338853, L_18, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ClickedWaveAnimation::.ctor()
extern "C"  void ClickedWaveAnimation__ctor_m2000083799 (ClickedWaveAnimation_t3762715588 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ClickedWaveAnimation::Start()
extern const MethodInfo* GameObject_AddComponent_TisPool_t2493500_m739887158_MethodInfo_var;
extern const uint32_t ClickedWaveAnimation_Start_m947221591_MetadataUsageId;
extern "C"  void ClickedWaveAnimation_Start_m947221591 (ClickedWaveAnimation_t3762715588 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ClickedWaveAnimation_Start_m947221591_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Pool_t2493500 * L_1 = GameObject_AddComponent_TisPool_t2493500_m739887158(L_0, /*hidden argument*/GameObject_AddComponent_TisPool_t2493500_m739887158_MethodInfo_var);
		__this->set_poolClass_5(L_1);
		Pool_t2493500 * L_2 = __this->get_poolClass_5();
		GameObject_t3674682005 * L_3 = __this->get_WaveObject_2();
		int32_t L_4 = __this->get_PoolSize_4();
		NullCheck(L_2);
		Pool_CreatePool_m1264299394(L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ClickedWaveAnimation::Update()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t ClickedWaveAnimation_Update_m3599917718_MetadataUsageId;
extern "C"  void ClickedWaveAnimation_Update_m3599917718 (ClickedWaveAnimation_t3762715588 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ClickedWaveAnimation_Update_m3599917718_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonDown_m2031691843(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		GameObject_t3674682005 * L_1 = ClickedWaveAnimation_UiHitted_m1639154198(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t3674682005 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		GameObject_t3674682005 * L_4 = V_0;
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = GameObject_get_transform_m1278640159(L_4, /*hidden argument*/NULL);
		ClickedWaveAnimation_CreateWave_m3233605019(__this, L_5, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Void ClickedWaveAnimation::CreateWave(UnityEngine.Transform)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMaskableGraphic_t3186046376_m1717659230_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisMaskableGraphic_t3186046376_m2118237702_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisEasyTween_t3917628265_m1697414744_MethodInfo_var;
extern const uint32_t ClickedWaveAnimation_CreateWave_m3233605019_MetadataUsageId;
extern "C"  void ClickedWaveAnimation_CreateWave_m3233605019 (ClickedWaveAnimation_t3762715588 * __this, Transform_t1659122786 * ___Parent0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ClickedWaveAnimation_CreateWave_m3233605019_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Pool_t2493500 * L_0 = __this->get_poolClass_5();
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = Pool_GetObject_m1066736605(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t3674682005 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_00fc;
		}
	}
	{
		GameObject_t3674682005 * L_4 = V_0;
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = GameObject_get_transform_m1278640159(L_4, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_6 = __this->get_CanvasMain_3();
		NullCheck(L_6);
		Transform_t1659122786 * L_7 = GameObject_get_transform_m1278640159(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_SetParent_m3449663462(L_5, L_7, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_8 = V_0;
		NullCheck(L_8);
		MaskableGraphic_t3186046376 * L_9 = GameObject_GetComponent_TisMaskableGraphic_t3186046376_m1717659230(L_8, /*hidden argument*/GameObject_GetComponent_TisMaskableGraphic_t3186046376_m1717659230_MethodInfo_var);
		Transform_t1659122786 * L_10 = ___Parent0;
		NullCheck(L_10);
		MaskableGraphic_t3186046376 * L_11 = Component_GetComponent_TisMaskableGraphic_t3186046376_m2118237702(L_10, /*hidden argument*/Component_GetComponent_TisMaskableGraphic_t3186046376_m2118237702_MethodInfo_var);
		NullCheck(L_11);
		Color_t4194546905  L_12 = VirtFuncInvoker0< Color_t4194546905  >::Invoke(21 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_11);
		Color_t4194546905  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Color__ctor_m103496991(&L_13, (0.1f), (0.1f), (0.1f), /*hidden argument*/NULL);
		Color_t4194546905  L_14 = Color_op_Subtraction_m2751495817(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtActionInvoker1< Color_t4194546905  >::Invoke(22 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_9, L_14);
		Camera_t2727095145 * L_15 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		Vector3_t4282066566  L_16 = Input_get_mousePosition_m4020233228(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_t4282066566  L_17 = Camera_ScreenToViewportPoint_m3727203754(L_15, L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		float L_18 = (&V_1)->get_x_1();
		int32_t L_19 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_20 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_1)->set_x_1(((float)((float)((float)((float)L_18*(float)(((float)((float)L_19)))))-(float)((float)((float)(((float)((float)L_20)))/(float)(2.0f))))));
		float L_21 = (&V_1)->get_y_2();
		int32_t L_22 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_23 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_1)->set_y_2(((float)((float)((float)((float)L_21*(float)(((float)((float)L_22)))))-(float)((float)((float)(((float)((float)L_23)))/(float)(2.0f))))));
		(&V_1)->set_z_3((0.0f));
		GameObject_t3674682005 * L_24 = V_0;
		NullCheck(L_24);
		RectTransform_t972643934 * L_25 = GameObject_GetComponent_TisRectTransform_t972643934_m406276429(L_24, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var);
		Vector3_t4282066566  L_26 = V_1;
		GameObject_t3674682005 * L_27 = __this->get_CanvasMain_3();
		NullCheck(L_27);
		Transform_t1659122786 * L_28 = GameObject_get_transform_m1278640159(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Vector3_t4282066566  L_29 = Transform_get_localScale_m3886572677(L_28, /*hidden argument*/NULL);
		V_2 = L_29;
		float L_30 = (&V_2)->get_x_1();
		Vector3_t4282066566  L_31 = Vector3_op_Division_m4277988370(NULL /*static, unused*/, L_26, L_30, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_set_localPosition_m3504330903(L_25, L_31, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_32 = V_0;
		NullCheck(L_32);
		Transform_t1659122786 * L_33 = GameObject_get_transform_m1278640159(L_32, /*hidden argument*/NULL);
		Transform_t1659122786 * L_34 = ___Parent0;
		NullCheck(L_33);
		Transform_SetParent_m3449663462(L_33, L_34, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_35 = V_0;
		NullCheck(L_35);
		EasyTween_t3917628265 * L_36 = GameObject_GetComponent_TisEasyTween_t3917628265_m1697414744(L_35, /*hidden argument*/GameObject_GetComponent_TisEasyTween_t3917628265_m1697414744_MethodInfo_var);
		NullCheck(L_36);
		EasyTween_OpenCloseObjectAnimation_m645046265(L_36, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		return;
	}
}
// UnityEngine.GameObject ClickedWaveAnimation::UiHitted()
extern Il2CppClass* EventSystem_t2276120119_il2cpp_TypeInfo_var;
extern Il2CppClass* PointerEventData_t1848751023_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t835879620_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2373821799_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3063858146_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMask_t8826680_m2460923986_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m1457590483_MethodInfo_var;
extern const uint32_t ClickedWaveAnimation_UiHitted_m1639154198_MetadataUsageId;
extern "C"  GameObject_t3674682005 * ClickedWaveAnimation_UiHitted_m1639154198 (ClickedWaveAnimation_t3762715588 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ClickedWaveAnimation_UiHitted_m1639154198_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	PointerEventData_t1848751023 * V_0 = NULL;
	List_1_t835879620 * V_1 = NULL;
	int32_t V_2 = 0;
	RaycastResult_t3762661364  V_3;
	memset(&V_3, 0, sizeof(V_3));
	RaycastResult_t3762661364  V_4;
	memset(&V_4, 0, sizeof(V_4));
	RaycastResult_t3762661364  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventSystem_t2276120119_il2cpp_TypeInfo_var);
		EventSystem_t2276120119 * L_0 = EventSystem_get_current_m3483537871(NULL /*static, unused*/, /*hidden argument*/NULL);
		PointerEventData_t1848751023 * L_1 = (PointerEventData_t1848751023 *)il2cpp_codegen_object_new(PointerEventData_t1848751023_il2cpp_TypeInfo_var);
		PointerEventData__ctor_m82194942(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		PointerEventData_t1848751023 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		Vector3_t4282066566  L_3 = Input_get_mousePosition_m4020233228(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t4282066565  L_4 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		PointerEventData_set_position_m1287828138(L_2, L_4, /*hidden argument*/NULL);
		List_1_t835879620 * L_5 = (List_1_t835879620 *)il2cpp_codegen_object_new(List_1_t835879620_il2cpp_TypeInfo_var);
		List_1__ctor_m2373821799(L_5, /*hidden argument*/List_1__ctor_m2373821799_MethodInfo_var);
		V_1 = L_5;
		EventSystem_t2276120119 * L_6 = EventSystem_get_current_m3483537871(NULL /*static, unused*/, /*hidden argument*/NULL);
		PointerEventData_t1848751023 * L_7 = V_0;
		List_1_t835879620 * L_8 = V_1;
		NullCheck(L_6);
		EventSystem_RaycastAll_m538533603(L_6, L_7, L_8, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_0086;
	}

IL_0034:
	{
		List_1_t835879620 * L_9 = V_1;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		RaycastResult_t3762661364  L_11 = List_1_get_Item_m3063858146(L_9, L_10, /*hidden argument*/List_1_get_Item_m3063858146_MethodInfo_var);
		V_3 = L_11;
		GameObject_t3674682005 * L_12 = RaycastResult_get_gameObject_m1138099278((&V_3), /*hidden argument*/NULL);
		NullCheck(L_12);
		Button_t3896396478 * L_13 = GameObject_GetComponent_TisButton_t3896396478_m2812094092(L_12, /*hidden argument*/GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0082;
		}
	}
	{
		List_1_t835879620 * L_15 = V_1;
		int32_t L_16 = V_2;
		NullCheck(L_15);
		RaycastResult_t3762661364  L_17 = List_1_get_Item_m3063858146(L_15, L_16, /*hidden argument*/List_1_get_Item_m3063858146_MethodInfo_var);
		V_4 = L_17;
		GameObject_t3674682005 * L_18 = RaycastResult_get_gameObject_m1138099278((&V_4), /*hidden argument*/NULL);
		NullCheck(L_18);
		Mask_t8826680 * L_19 = GameObject_GetComponent_TisMask_t8826680_m2460923986(L_18, /*hidden argument*/GameObject_GetComponent_TisMask_t8826680_m2460923986_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0082;
		}
	}
	{
		List_1_t835879620 * L_21 = V_1;
		int32_t L_22 = V_2;
		NullCheck(L_21);
		RaycastResult_t3762661364  L_23 = List_1_get_Item_m3063858146(L_21, L_22, /*hidden argument*/List_1_get_Item_m3063858146_MethodInfo_var);
		V_5 = L_23;
		GameObject_t3674682005 * L_24 = RaycastResult_get_gameObject_m1138099278((&V_5), /*hidden argument*/NULL);
		return L_24;
	}

IL_0082:
	{
		int32_t L_25 = V_2;
		V_2 = ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_0086:
	{
		int32_t L_26 = V_2;
		List_1_t835879620 * L_27 = V_1;
		NullCheck(L_27);
		int32_t L_28 = List_1_get_Count_m1457590483(L_27, /*hidden argument*/List_1_get_Count_m1457590483_MethodInfo_var);
		if ((((int32_t)L_26) < ((int32_t)L_28)))
		{
			goto IL_0034;
		}
	}
	{
		return (GameObject_t3674682005 *)NULL;
	}
}
// System.Void Combinaciones::.ctor()
extern "C"  void Combinaciones__ctor_m1182110749 (Combinaciones_t3637091598 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Combinaciones::Start()
extern const MethodInfo* Component_GetComponent_TisVinetas_t2126676892_m4099167005_MethodInfo_var;
extern const uint32_t Combinaciones_Start_m129248541_MetadataUsageId;
extern "C"  void Combinaciones_Start_m129248541 (Combinaciones_t3637091598 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Combinaciones_Start_m129248541_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vinetas_t2126676892 * L_0 = Component_GetComponent_TisVinetas_t2126676892_m4099167005(__this, /*hidden argument*/Component_GetComponent_TisVinetas_t2126676892_m4099167005_MethodInfo_var);
		__this->set_vi_5(L_0);
		return;
	}
}
// System.String Combinaciones::getText(System.Int32)
extern "C"  String_t* Combinaciones_getText_m3595448498 (Combinaciones_t3637091598 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		StringU5BU5D_t4054002952* L_0 = __this->get_tablaTextos_3();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		String_t* L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		return L_3;
	}
}
// UnityEngine.Sprite Combinaciones::getPic(System.Int32)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral110245580;
extern const uint32_t Combinaciones_getPic_m3530152747_MetadataUsageId;
extern "C"  Sprite_t3199167241 * Combinaciones_getPic_m3530152747 (Combinaciones_t3637091598 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Combinaciones_getPic_m3530152747_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_currentPic_4();
		V_0 = L_0;
		int32_t L_1 = ___i0;
		V_1 = L_1;
		int32_t L_2 = V_1;
		if (L_2 == 0)
		{
			goto IL_0038;
		}
		if (L_2 == 1)
		{
			goto IL_003f;
		}
		if (L_2 == 2)
		{
			goto IL_0046;
		}
		if (L_2 == 3)
		{
			goto IL_004d;
		}
		if (L_2 == 4)
		{
			goto IL_0054;
		}
		if (L_2 == 5)
		{
			goto IL_005b;
		}
		if (L_2 == 6)
		{
			goto IL_0062;
		}
		if (L_2 == 7)
		{
			goto IL_0069;
		}
		if (L_2 == 8)
		{
			goto IL_0070;
		}
	}
	{
		goto IL_0078;
	}

IL_0038:
	{
		V_0 = 0;
		goto IL_0078;
	}

IL_003f:
	{
		V_0 = 1;
		goto IL_0078;
	}

IL_0046:
	{
		V_0 = 2;
		goto IL_0078;
	}

IL_004d:
	{
		V_0 = 4;
		goto IL_0078;
	}

IL_0054:
	{
		V_0 = 5;
		goto IL_0078;
	}

IL_005b:
	{
		V_0 = 6;
		goto IL_0078;
	}

IL_0062:
	{
		V_0 = 7;
		goto IL_0078;
	}

IL_0069:
	{
		V_0 = 8;
		goto IL_0078;
	}

IL_0070:
	{
		V_0 = ((int32_t)9);
		goto IL_0078;
	}

IL_0078:
	{
		int32_t L_3 = V_0;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral110245580, L_5, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Vinetas_t2126676892 * L_7 = __this->get_vi_5();
		NullCheck(L_7);
		SpriteU5BU5D_t2761310900* L_8 = L_7->get_vinetas_21();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		Sprite_t3199167241 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		return L_11;
	}
}
// UnityEngine.Sprite Combinaciones::getChar(System.Int32)
extern "C"  Sprite_t3199167241 * Combinaciones_getChar_m294333753 (Combinaciones_t3637091598 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		SpriteU5BU5D_t2761310900* L_0 = __this->get_chars_2();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		Sprite_t3199167241 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		return L_3;
	}
}
// System.Void CounterChecking::.ctor()
extern "C"  void CounterChecking__ctor_m569166933 (CounterChecking_t39316438 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CounterChecking::Start()
extern const MethodInfo* Component_GetComponent_TisSlots_t79980053_m3147794564_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCombinaciones_t3637091598_m1358194067_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisVinetas_t2126676892_m3739561413_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var;
extern const uint32_t CounterChecking_Start_m3811272021_MetadataUsageId;
extern "C"  void CounterChecking_Start_m3811272021 (CounterChecking_t39316438 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CounterChecking_Start_m3811272021_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Slots_t79980053 * L_0 = Component_GetComponent_TisSlots_t79980053_m3147794564(__this, /*hidden argument*/Component_GetComponent_TisSlots_t79980053_m3147794564_MethodInfo_var);
		__this->set_gc_2(L_0);
		GameObject_t3674682005 * L_1 = __this->get_Tabla_20();
		NullCheck(L_1);
		Combinaciones_t3637091598 * L_2 = GameObject_GetComponent_TisCombinaciones_t3637091598_m1358194067(L_1, /*hidden argument*/GameObject_GetComponent_TisCombinaciones_t3637091598_m1358194067_MethodInfo_var);
		__this->set_combinaciones_3(L_2);
		GameObject_t3674682005 * L_3 = __this->get_Tabla_20();
		NullCheck(L_3);
		Vinetas_t2126676892 * L_4 = GameObject_GetComponent_TisVinetas_t2126676892_m3739561413(L_3, /*hidden argument*/GameObject_GetComponent_TisVinetas_t2126676892_m3739561413_MethodInfo_var);
		__this->set_vi_4(L_4);
		Vinetas_t2126676892 * L_5 = __this->get_vi_4();
		NullCheck(L_5);
		SpriteU5BU5D_t2761310900* L_6 = L_5->get_listaSprite_22();
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		int32_t L_7 = 0;
		Sprite_t3199167241 * L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		__this->set_ojo_5(L_8);
		Vinetas_t2126676892 * L_9 = __this->get_vi_4();
		NullCheck(L_9);
		SpriteU5BU5D_t2761310900* L_10 = L_9->get_listaSprite_22();
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		int32_t L_11 = 1;
		Sprite_t3199167241 * L_12 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		__this->set_huella_6(L_12);
		Vinetas_t2126676892 * L_13 = __this->get_vi_4();
		NullCheck(L_13);
		SpriteU5BU5D_t2761310900* L_14 = L_13->get_listaSprite_22();
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 2);
		int32_t L_15 = 2;
		Sprite_t3199167241 * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		__this->set_hablar_7(L_16);
		Vinetas_t2126676892 * L_17 = __this->get_vi_4();
		NullCheck(L_17);
		SpriteU5BU5D_t2761310900* L_18 = L_17->get_listaSprite_22();
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 3);
		int32_t L_19 = 3;
		Sprite_t3199167241 * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		__this->set_escudo_8(L_20);
		Vinetas_t2126676892 * L_21 = __this->get_vi_4();
		NullCheck(L_21);
		SpriteU5BU5D_t2761310900* L_22 = L_21->get_listaSprite_22();
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 4);
		int32_t L_23 = 4;
		Sprite_t3199167241 * L_24 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		__this->set_manzana_9(L_24);
		Vinetas_t2126676892 * L_25 = __this->get_vi_4();
		NullCheck(L_25);
		SpriteU5BU5D_t2761310900* L_26 = L_25->get_listaSprite_22();
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 5);
		int32_t L_27 = 5;
		Sprite_t3199167241 * L_28 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		__this->set_gafas_10(L_28);
		Vinetas_t2126676892 * L_29 = __this->get_vi_4();
		NullCheck(L_29);
		SpriteU5BU5D_t2761310900* L_30 = L_29->get_listaSprite_22();
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 6);
		int32_t L_31 = 6;
		Sprite_t3199167241 * L_32 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		__this->set_pocion_11(L_32);
		Vinetas_t2126676892 * L_33 = __this->get_vi_4();
		NullCheck(L_33);
		SpriteU5BU5D_t2761310900* L_34 = L_33->get_listaSprite_22();
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 7);
		int32_t L_35 = 7;
		Sprite_t3199167241 * L_36 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		__this->set_cruz_12(L_36);
		Vinetas_t2126676892 * L_37 = __this->get_vi_4();
		NullCheck(L_37);
		SpriteU5BU5D_t2761310900* L_38 = L_37->get_listaSprite_22();
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 8);
		int32_t L_39 = 8;
		Sprite_t3199167241 * L_40 = (L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		__this->set_llavero_13(L_40);
		Vinetas_t2126676892 * L_41 = __this->get_vi_4();
		NullCheck(L_41);
		SpriteU5BU5D_t2761310900* L_42 = L_41->get_listaSprite_22();
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, ((int32_t)9));
		int32_t L_43 = ((int32_t)9);
		Sprite_t3199167241 * L_44 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		__this->set_tenazas_14(L_44);
		Vinetas_t2126676892 * L_45 = __this->get_vi_4();
		NullCheck(L_45);
		SpriteU5BU5D_t2761310900* L_46 = L_45->get_listaSprite_22();
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, ((int32_t)10));
		int32_t L_47 = ((int32_t)10);
		Sprite_t3199167241 * L_48 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		__this->set_muerte_15(L_48);
		Vinetas_t2126676892 * L_49 = __this->get_vi_4();
		NullCheck(L_49);
		SpriteU5BU5D_t2761310900* L_50 = L_49->get_listaSprite_22();
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, ((int32_t)11));
		int32_t L_51 = ((int32_t)11);
		Sprite_t3199167241 * L_52 = (L_50)->GetAt(static_cast<il2cpp_array_size_t>(L_51));
		__this->set_corazon_16(L_52);
		Slots_t79980053 * L_53 = __this->get_gc_2();
		NullCheck(L_53);
		GameObjectU5BU5D_t2662109048* L_54 = L_53->get_counters_26();
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, 0);
		int32_t L_55 = 0;
		GameObject_t3674682005 * L_56 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
		NullCheck(L_56);
		Image_t538875265 * L_57 = GameObject_GetComponent_TisImage_t538875265_m2140199269(L_56, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var);
		NullCheck(L_57);
		Sprite_t3199167241 * L_58 = Image_get_sprite_m3572636301(L_57, /*hidden argument*/NULL);
		__this->set_c1_17(L_58);
		Slots_t79980053 * L_59 = __this->get_gc_2();
		NullCheck(L_59);
		GameObjectU5BU5D_t2662109048* L_60 = L_59->get_counters_26();
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, 1);
		int32_t L_61 = 1;
		GameObject_t3674682005 * L_62 = (L_60)->GetAt(static_cast<il2cpp_array_size_t>(L_61));
		NullCheck(L_62);
		Image_t538875265 * L_63 = GameObject_GetComponent_TisImage_t538875265_m2140199269(L_62, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var);
		NullCheck(L_63);
		Sprite_t3199167241 * L_64 = Image_get_sprite_m3572636301(L_63, /*hidden argument*/NULL);
		__this->set_c2_18(L_64);
		Slots_t79980053 * L_65 = __this->get_gc_2();
		NullCheck(L_65);
		GameObjectU5BU5D_t2662109048* L_66 = L_65->get_counters_26();
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, 2);
		int32_t L_67 = 2;
		GameObject_t3674682005 * L_68 = (L_66)->GetAt(static_cast<il2cpp_array_size_t>(L_67));
		NullCheck(L_68);
		Image_t538875265 * L_69 = GameObject_GetComponent_TisImage_t538875265_m2140199269(L_68, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var);
		NullCheck(L_69);
		Sprite_t3199167241 * L_70 = Image_get_sprite_m3572636301(L_69, /*hidden argument*/NULL);
		__this->set_c3_19(L_70);
		return;
	}
}
// System.Void CounterChecking::Update()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t CounterChecking_Update_m2191167832_MetadataUsageId;
extern "C"  void CounterChecking_Update_m2191167832 (CounterChecking_t39316438 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CounterChecking_Update_m2191167832_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Slots_t79980053 * L_0 = __this->get_gc_2();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_currentState_58();
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)97), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		Slots_t79980053 * L_3 = __this->get_gc_2();
		NullCheck(L_3);
		Slots_getPicture_m120001735(L_3, 0, /*hidden argument*/NULL);
	}

IL_0029:
	{
		Slots_t79980053 * L_4 = __this->get_gc_2();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_currentState_58();
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_0052;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_6 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)115), /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0052;
		}
	}
	{
		Slots_t79980053 * L_7 = __this->get_gc_2();
		NullCheck(L_7);
		Slots_getPicture_m120001735(L_7, 1, /*hidden argument*/NULL);
	}

IL_0052:
	{
		Slots_t79980053 * L_8 = __this->get_gc_2();
		NullCheck(L_8);
		int32_t L_9 = L_8->get_currentState_58();
		if ((!(((uint32_t)L_9) == ((uint32_t)1))))
		{
			goto IL_007b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_10 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)100), /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_007b;
		}
	}
	{
		Slots_t79980053 * L_11 = __this->get_gc_2();
		NullCheck(L_11);
		Slots_getPicture_m120001735(L_11, 2, /*hidden argument*/NULL);
	}

IL_007b:
	{
		return;
	}
}
// System.Void CounterChecking::ActualizaCounters()
extern const MethodInfo* GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var;
extern const uint32_t CounterChecking_ActualizaCounters_m3196632396_MetadataUsageId;
extern "C"  void CounterChecking_ActualizaCounters_m3196632396 (CounterChecking_t39316438 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CounterChecking_ActualizaCounters_m3196632396_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Slots_t79980053 * L_0 = __this->get_gc_2();
		NullCheck(L_0);
		GameObjectU5BU5D_t2662109048* L_1 = L_0->get_counters_26();
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		int32_t L_2 = 0;
		GameObject_t3674682005 * L_3 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		Image_t538875265 * L_4 = GameObject_GetComponent_TisImage_t538875265_m2140199269(L_3, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var);
		NullCheck(L_4);
		Sprite_t3199167241 * L_5 = Image_get_sprite_m3572636301(L_4, /*hidden argument*/NULL);
		__this->set_c1_17(L_5);
		Slots_t79980053 * L_6 = __this->get_gc_2();
		NullCheck(L_6);
		GameObjectU5BU5D_t2662109048* L_7 = L_6->get_counters_26();
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
		int32_t L_8 = 1;
		GameObject_t3674682005 * L_9 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_9);
		Image_t538875265 * L_10 = GameObject_GetComponent_TisImage_t538875265_m2140199269(L_9, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var);
		NullCheck(L_10);
		Sprite_t3199167241 * L_11 = Image_get_sprite_m3572636301(L_10, /*hidden argument*/NULL);
		__this->set_c2_18(L_11);
		Slots_t79980053 * L_12 = __this->get_gc_2();
		NullCheck(L_12);
		GameObjectU5BU5D_t2662109048* L_13 = L_12->get_counters_26();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 2);
		int32_t L_14 = 2;
		GameObject_t3674682005 * L_15 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		NullCheck(L_15);
		Image_t538875265 * L_16 = GameObject_GetComponent_TisImage_t538875265_m2140199269(L_15, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var);
		NullCheck(L_16);
		Sprite_t3199167241 * L_17 = Image_get_sprite_m3572636301(L_16, /*hidden argument*/NULL);
		__this->set_c3_19(L_17);
		return;
	}
}
// System.Void CounterChecking::CounterCheck()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral68;
extern const uint32_t CounterChecking_CounterCheck_m2647319995_MetadataUsageId;
extern "C"  void CounterChecking_CounterCheck_m2647319995 (CounterChecking_t39316438 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CounterChecking_CounterCheck_m2647319995_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		CounterChecking_ActualizaCounters_m3196632396(__this, /*hidden argument*/NULL);
		Combinaciones_t3637091598 * L_0 = __this->get_combinaciones_3();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_currentPic_4();
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (L_2 == 0)
		{
			goto IL_0045;
		}
		if (L_2 == 1)
		{
			goto IL_00ae;
		}
		if (L_2 == 2)
		{
			goto IL_010d;
		}
		if (L_2 == 3)
		{
			goto IL_016c;
		}
		if (L_2 == 4)
		{
			goto IL_01cb;
		}
		if (L_2 == 5)
		{
			goto IL_022a;
		}
		if (L_2 == 6)
		{
			goto IL_0289;
		}
		if (L_2 == 7)
		{
			goto IL_02fd;
		}
		if (L_2 == 8)
		{
			goto IL_0371;
		}
		if (L_2 == 9)
		{
			goto IL_03e5;
		}
	}
	{
		goto IL_045a;
	}

IL_0045:
	{
		Sprite_t3199167241 * L_3 = __this->get_c1_17();
		Sprite_t3199167241 * L_4 = __this->get_ojo_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_00a9;
		}
	}
	{
		Sprite_t3199167241 * L_6 = __this->get_c2_18();
		Sprite_t3199167241 * L_7 = __this->get_ojo_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_00a9;
		}
	}
	{
		Sprite_t3199167241 * L_9 = __this->get_c3_19();
		Sprite_t3199167241 * L_10 = __this->get_ojo_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00a9;
		}
	}
	{
		Slots_t79980053 * L_12 = __this->get_gc_2();
		NullCheck(L_12);
		Slots_getPicture_m120001735(L_12, 0, /*hidden argument*/NULL);
		Combinaciones_t3637091598 * L_13 = __this->get_combinaciones_3();
		NullCheck(L_13);
		L_13->set_currentPic_4(1);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, _stringLiteral68, /*hidden argument*/NULL);
	}

IL_00a9:
	{
		goto IL_045a;
	}

IL_00ae:
	{
		Sprite_t3199167241 * L_14 = __this->get_c1_17();
		Sprite_t3199167241 * L_15 = __this->get_huella_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_16 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0108;
		}
	}
	{
		Sprite_t3199167241 * L_17 = __this->get_c2_18();
		Sprite_t3199167241 * L_18 = __this->get_huella_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0108;
		}
	}
	{
		Sprite_t3199167241 * L_20 = __this->get_c3_19();
		Sprite_t3199167241 * L_21 = __this->get_huella_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0108;
		}
	}
	{
		Slots_t79980053 * L_23 = __this->get_gc_2();
		NullCheck(L_23);
		Slots_getPicture_m120001735(L_23, 1, /*hidden argument*/NULL);
		Combinaciones_t3637091598 * L_24 = __this->get_combinaciones_3();
		NullCheck(L_24);
		L_24->set_currentPic_4(2);
	}

IL_0108:
	{
		goto IL_045a;
	}

IL_010d:
	{
		Sprite_t3199167241 * L_25 = __this->get_c1_17();
		Sprite_t3199167241 * L_26 = __this->get_gafas_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_27 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0167;
		}
	}
	{
		Sprite_t3199167241 * L_28 = __this->get_c2_18();
		Sprite_t3199167241 * L_29 = __this->get_gafas_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_30 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_0167;
		}
	}
	{
		Sprite_t3199167241 * L_31 = __this->get_c3_19();
		Sprite_t3199167241 * L_32 = __this->get_gafas_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_33 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0167;
		}
	}
	{
		Slots_t79980053 * L_34 = __this->get_gc_2();
		NullCheck(L_34);
		Slots_getPicture_m120001735(L_34, 2, /*hidden argument*/NULL);
		Combinaciones_t3637091598 * L_35 = __this->get_combinaciones_3();
		NullCheck(L_35);
		L_35->set_currentPic_4(3);
	}

IL_0167:
	{
		goto IL_045a;
	}

IL_016c:
	{
		Sprite_t3199167241 * L_36 = __this->get_c1_17();
		Sprite_t3199167241 * L_37 = __this->get_huella_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_38 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_36, L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_01c6;
		}
	}
	{
		Sprite_t3199167241 * L_39 = __this->get_c2_18();
		Sprite_t3199167241 * L_40 = __this->get_huella_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_41 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_01c6;
		}
	}
	{
		Sprite_t3199167241 * L_42 = __this->get_c3_19();
		Sprite_t3199167241 * L_43 = __this->get_huella_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_44 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_42, L_43, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_01c6;
		}
	}
	{
		Slots_t79980053 * L_45 = __this->get_gc_2();
		NullCheck(L_45);
		Slots_getPicture_m120001735(L_45, 3, /*hidden argument*/NULL);
		Combinaciones_t3637091598 * L_46 = __this->get_combinaciones_3();
		NullCheck(L_46);
		L_46->set_currentPic_4(4);
	}

IL_01c6:
	{
		goto IL_045a;
	}

IL_01cb:
	{
		Sprite_t3199167241 * L_47 = __this->get_c1_17();
		Sprite_t3199167241 * L_48 = __this->get_hablar_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_49 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_47, L_48, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_0225;
		}
	}
	{
		Sprite_t3199167241 * L_50 = __this->get_c2_18();
		Sprite_t3199167241 * L_51 = __this->get_hablar_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_52 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_50, L_51, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_0225;
		}
	}
	{
		Sprite_t3199167241 * L_53 = __this->get_c3_19();
		Sprite_t3199167241 * L_54 = __this->get_hablar_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_55 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_53, L_54, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_0225;
		}
	}
	{
		Slots_t79980053 * L_56 = __this->get_gc_2();
		NullCheck(L_56);
		Slots_getPicture_m120001735(L_56, 4, /*hidden argument*/NULL);
		Combinaciones_t3637091598 * L_57 = __this->get_combinaciones_3();
		NullCheck(L_57);
		L_57->set_currentPic_4(5);
	}

IL_0225:
	{
		goto IL_045a;
	}

IL_022a:
	{
		Sprite_t3199167241 * L_58 = __this->get_c1_17();
		Sprite_t3199167241 * L_59 = __this->get_gafas_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_60 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_58, L_59, /*hidden argument*/NULL);
		if (!L_60)
		{
			goto IL_0284;
		}
	}
	{
		Sprite_t3199167241 * L_61 = __this->get_c2_18();
		Sprite_t3199167241 * L_62 = __this->get_gafas_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_63 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_61, L_62, /*hidden argument*/NULL);
		if (!L_63)
		{
			goto IL_0284;
		}
	}
	{
		Sprite_t3199167241 * L_64 = __this->get_c3_19();
		Sprite_t3199167241 * L_65 = __this->get_gafas_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_66 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_64, L_65, /*hidden argument*/NULL);
		if (!L_66)
		{
			goto IL_0284;
		}
	}
	{
		Slots_t79980053 * L_67 = __this->get_gc_2();
		NullCheck(L_67);
		Slots_getPicture_m120001735(L_67, 5, /*hidden argument*/NULL);
		Combinaciones_t3637091598 * L_68 = __this->get_combinaciones_3();
		NullCheck(L_68);
		L_68->set_currentPic_4(6);
	}

IL_0284:
	{
		goto IL_045a;
	}

IL_0289:
	{
		Sprite_t3199167241 * L_69 = __this->get_c1_17();
		Vinetas_t2126676892 * L_70 = __this->get_vi_4();
		NullCheck(L_70);
		SpriteU5BU5D_t2761310900* L_71 = L_70->get_listaSprite_22();
		NullCheck(L_71);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_71, 3);
		int32_t L_72 = 3;
		Sprite_t3199167241 * L_73 = (L_71)->GetAt(static_cast<il2cpp_array_size_t>(L_72));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_74 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_69, L_73, /*hidden argument*/NULL);
		if (!L_74)
		{
			goto IL_02f8;
		}
	}
	{
		Sprite_t3199167241 * L_75 = __this->get_c2_18();
		Vinetas_t2126676892 * L_76 = __this->get_vi_4();
		NullCheck(L_76);
		SpriteU5BU5D_t2761310900* L_77 = L_76->get_listaSprite_22();
		NullCheck(L_77);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_77, 3);
		int32_t L_78 = 3;
		Sprite_t3199167241 * L_79 = (L_77)->GetAt(static_cast<il2cpp_array_size_t>(L_78));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_80 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_75, L_79, /*hidden argument*/NULL);
		if (!L_80)
		{
			goto IL_02f8;
		}
	}
	{
		Sprite_t3199167241 * L_81 = __this->get_c3_19();
		Vinetas_t2126676892 * L_82 = __this->get_vi_4();
		NullCheck(L_82);
		SpriteU5BU5D_t2761310900* L_83 = L_82->get_listaSprite_22();
		NullCheck(L_83);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_83, 3);
		int32_t L_84 = 3;
		Sprite_t3199167241 * L_85 = (L_83)->GetAt(static_cast<il2cpp_array_size_t>(L_84));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_86 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_81, L_85, /*hidden argument*/NULL);
		if (!L_86)
		{
			goto IL_02f8;
		}
	}
	{
		Slots_t79980053 * L_87 = __this->get_gc_2();
		NullCheck(L_87);
		Slots_getPicture_m120001735(L_87, 6, /*hidden argument*/NULL);
		Combinaciones_t3637091598 * L_88 = __this->get_combinaciones_3();
		NullCheck(L_88);
		L_88->set_currentPic_4(7);
	}

IL_02f8:
	{
		goto IL_045a;
	}

IL_02fd:
	{
		Sprite_t3199167241 * L_89 = __this->get_c1_17();
		Vinetas_t2126676892 * L_90 = __this->get_vi_4();
		NullCheck(L_90);
		SpriteU5BU5D_t2761310900* L_91 = L_90->get_listaSprite_22();
		NullCheck(L_91);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_91, 3);
		int32_t L_92 = 3;
		Sprite_t3199167241 * L_93 = (L_91)->GetAt(static_cast<il2cpp_array_size_t>(L_92));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_94 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_89, L_93, /*hidden argument*/NULL);
		if (!L_94)
		{
			goto IL_036c;
		}
	}
	{
		Sprite_t3199167241 * L_95 = __this->get_c2_18();
		Vinetas_t2126676892 * L_96 = __this->get_vi_4();
		NullCheck(L_96);
		SpriteU5BU5D_t2761310900* L_97 = L_96->get_listaSprite_22();
		NullCheck(L_97);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_97, 3);
		int32_t L_98 = 3;
		Sprite_t3199167241 * L_99 = (L_97)->GetAt(static_cast<il2cpp_array_size_t>(L_98));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_100 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_95, L_99, /*hidden argument*/NULL);
		if (!L_100)
		{
			goto IL_036c;
		}
	}
	{
		Sprite_t3199167241 * L_101 = __this->get_c3_19();
		Vinetas_t2126676892 * L_102 = __this->get_vi_4();
		NullCheck(L_102);
		SpriteU5BU5D_t2761310900* L_103 = L_102->get_listaSprite_22();
		NullCheck(L_103);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_103, 3);
		int32_t L_104 = 3;
		Sprite_t3199167241 * L_105 = (L_103)->GetAt(static_cast<il2cpp_array_size_t>(L_104));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_106 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_101, L_105, /*hidden argument*/NULL);
		if (!L_106)
		{
			goto IL_036c;
		}
	}
	{
		Combinaciones_t3637091598 * L_107 = __this->get_combinaciones_3();
		NullCheck(L_107);
		L_107->set_currentPic_4(7);
		Slots_t79980053 * L_108 = __this->get_gc_2();
		NullCheck(L_108);
		Slots_getPicture_m120001735(L_108, 7, /*hidden argument*/NULL);
	}

IL_036c:
	{
		goto IL_045a;
	}

IL_0371:
	{
		Sprite_t3199167241 * L_109 = __this->get_c1_17();
		Vinetas_t2126676892 * L_110 = __this->get_vi_4();
		NullCheck(L_110);
		SpriteU5BU5D_t2761310900* L_111 = L_110->get_listaSprite_22();
		NullCheck(L_111);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_111, 3);
		int32_t L_112 = 3;
		Sprite_t3199167241 * L_113 = (L_111)->GetAt(static_cast<il2cpp_array_size_t>(L_112));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_114 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_109, L_113, /*hidden argument*/NULL);
		if (!L_114)
		{
			goto IL_03e0;
		}
	}
	{
		Sprite_t3199167241 * L_115 = __this->get_c2_18();
		Vinetas_t2126676892 * L_116 = __this->get_vi_4();
		NullCheck(L_116);
		SpriteU5BU5D_t2761310900* L_117 = L_116->get_listaSprite_22();
		NullCheck(L_117);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_117, 3);
		int32_t L_118 = 3;
		Sprite_t3199167241 * L_119 = (L_117)->GetAt(static_cast<il2cpp_array_size_t>(L_118));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_120 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_115, L_119, /*hidden argument*/NULL);
		if (!L_120)
		{
			goto IL_03e0;
		}
	}
	{
		Sprite_t3199167241 * L_121 = __this->get_c3_19();
		Vinetas_t2126676892 * L_122 = __this->get_vi_4();
		NullCheck(L_122);
		SpriteU5BU5D_t2761310900* L_123 = L_122->get_listaSprite_22();
		NullCheck(L_123);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_123, 3);
		int32_t L_124 = 3;
		Sprite_t3199167241 * L_125 = (L_123)->GetAt(static_cast<il2cpp_array_size_t>(L_124));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_126 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_121, L_125, /*hidden argument*/NULL);
		if (!L_126)
		{
			goto IL_03e0;
		}
	}
	{
		Combinaciones_t3637091598 * L_127 = __this->get_combinaciones_3();
		NullCheck(L_127);
		L_127->set_currentPic_4(8);
		Slots_t79980053 * L_128 = __this->get_gc_2();
		NullCheck(L_128);
		Slots_getPicture_m120001735(L_128, 8, /*hidden argument*/NULL);
	}

IL_03e0:
	{
		goto IL_045a;
	}

IL_03e5:
	{
		Sprite_t3199167241 * L_129 = __this->get_c1_17();
		Vinetas_t2126676892 * L_130 = __this->get_vi_4();
		NullCheck(L_130);
		SpriteU5BU5D_t2761310900* L_131 = L_130->get_listaSprite_22();
		NullCheck(L_131);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_131, 3);
		int32_t L_132 = 3;
		Sprite_t3199167241 * L_133 = (L_131)->GetAt(static_cast<il2cpp_array_size_t>(L_132));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_134 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_129, L_133, /*hidden argument*/NULL);
		if (!L_134)
		{
			goto IL_0455;
		}
	}
	{
		Sprite_t3199167241 * L_135 = __this->get_c2_18();
		Vinetas_t2126676892 * L_136 = __this->get_vi_4();
		NullCheck(L_136);
		SpriteU5BU5D_t2761310900* L_137 = L_136->get_listaSprite_22();
		NullCheck(L_137);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_137, 3);
		int32_t L_138 = 3;
		Sprite_t3199167241 * L_139 = (L_137)->GetAt(static_cast<il2cpp_array_size_t>(L_138));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_140 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_135, L_139, /*hidden argument*/NULL);
		if (!L_140)
		{
			goto IL_0455;
		}
	}
	{
		Sprite_t3199167241 * L_141 = __this->get_c3_19();
		Vinetas_t2126676892 * L_142 = __this->get_vi_4();
		NullCheck(L_142);
		SpriteU5BU5D_t2761310900* L_143 = L_142->get_listaSprite_22();
		NullCheck(L_143);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_143, 3);
		int32_t L_144 = 3;
		Sprite_t3199167241 * L_145 = (L_143)->GetAt(static_cast<il2cpp_array_size_t>(L_144));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_146 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_141, L_145, /*hidden argument*/NULL);
		if (!L_146)
		{
			goto IL_0455;
		}
	}
	{
		Combinaciones_t3637091598 * L_147 = __this->get_combinaciones_3();
		NullCheck(L_147);
		L_147->set_currentPic_4(1);
		Slots_t79980053 * L_148 = __this->get_gc_2();
		NullCheck(L_148);
		Slots_getPicture_m120001735(L_148, ((int32_t)9), /*hidden argument*/NULL);
	}

IL_0455:
	{
		goto IL_045a;
	}

IL_045a:
	{
		return;
	}
}
// System.Void CreateAnimImage::.ctor()
extern Il2CppClass* List_1_t990846521_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m811724104_MethodInfo_var;
extern const uint32_t CreateAnimImage__ctor_m1166625565_MetadataUsageId;
extern "C"  void CreateAnimImage__ctor_m1166625565 (CreateAnimImage_t2928946190 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CreateAnimImage__ctor_m1166625565_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t990846521 * L_0 = (List_1_t990846521 *)il2cpp_codegen_object_new(List_1_t990846521_il2cpp_TypeInfo_var);
		List_1__ctor_m811724104(L_0, /*hidden argument*/List_1__ctor_m811724104_MethodInfo_var);
		__this->set_Created_12(L_0);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CreateAnimImage::Start()
extern "C"  void CreateAnimImage_Start_m113763357 (CreateAnimImage_t2928946190 * __this, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_t972643934 * L_0 = __this->get_RootRect_10();
		NullCheck(L_0);
		Rect_t4241904616  L_1 = RectTransform_get_rect_m1566017036(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = Rect_get_height_m2154960823((&V_0), /*hidden argument*/NULL);
		RectTransform_t972643934 * L_3 = __this->get_RootRect_10();
		NullCheck(L_3);
		Rect_t4241904616  L_4 = RectTransform_get_rect_m1566017036(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = Rect_get_width_m2824209432((&V_1), /*hidden argument*/NULL);
		Vector2_t4282066565  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector2__ctor_m1517109030(&L_6, L_2, L_5, /*hidden argument*/NULL);
		__this->set_InitialCanvasScrollSize_13(L_6);
		return;
	}
}
// System.Void CreateAnimImage::CallBack()
extern const MethodInfo* List_1_get_Count_m2545038078_MethodInfo_var;
extern const uint32_t CreateAnimImage_CallBack_m3105089196_MetadataUsageId;
extern "C"  void CreateAnimImage_CallBack_m3105089196 (CreateAnimImage_t2928946190 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CreateAnimImage_CallBack_m3105089196_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		List_1_t990846521 * L_0 = __this->get_Created_12();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m2545038078(L_0, /*hidden argument*/List_1_get_Count_m2545038078_MethodInfo_var);
		if (L_1)
		{
			goto IL_003c;
		}
	}
	{
		V_0 = 0;
		goto IL_0028;
	}

IL_0017:
	{
		CreateAnimImageU5BU5D_t2857274363* L_2 = __this->get_createImageOtherReference_2();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		CreateAnimImage_t2928946190 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		CreateAnimImage_DestroyButtons_m209852270(L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_7 = V_0;
		CreateAnimImageU5BU5D_t2857274363* L_8 = __this->get_createImageOtherReference_2();
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0017;
		}
	}
	{
		CreateAnimImage_CreateButtons_m3537867744(__this, /*hidden argument*/NULL);
	}

IL_003c:
	{
		return;
	}
}
// System.Void CreateAnimImage::DestroyButtons()
extern const MethodInfo* List_1_get_Item_m1284089175_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2545038078_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m2512824691_MethodInfo_var;
extern const uint32_t CreateAnimImage_DestroyButtons_m209852270_MetadataUsageId;
extern "C"  void CreateAnimImage_DestroyButtons_m209852270 (CreateAnimImage_t2928946190 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CreateAnimImage_DestroyButtons_m209852270_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_001c;
	}

IL_0007:
	{
		List_1_t990846521 * L_0 = __this->get_Created_12();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		EasyTween_t3917628265 * L_2 = List_1_get_Item_m1284089175(L_0, L_1, /*hidden argument*/List_1_get_Item_m1284089175_MethodInfo_var);
		NullCheck(L_2);
		EasyTween_OpenCloseObjectAnimation_m645046265(L_2, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		V_0 = ((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_001c:
	{
		int32_t L_4 = V_0;
		List_1_t990846521 * L_5 = __this->get_Created_12();
		NullCheck(L_5);
		int32_t L_6 = List_1_get_Count_m2545038078(L_5, /*hidden argument*/List_1_get_Count_m2545038078_MethodInfo_var);
		if ((((int32_t)L_4) < ((int32_t)L_6)))
		{
			goto IL_0007;
		}
	}
	{
		List_1_t990846521 * L_7 = __this->get_Created_12();
		NullCheck(L_7);
		List_1_Clear_m2512824691(L_7, /*hidden argument*/List_1_Clear_m2512824691_MethodInfo_var);
		return;
	}
}
// System.Void CreateAnimImage::CreateButtons()
extern "C"  void CreateAnimImage_CreateButtons_m3537867744 (CreateAnimImage_t2928946190 * __this, const MethodInfo* method)
{
	{
		CreateAnimImage_CreatePanels_m2698355922(__this, /*hidden argument*/NULL);
		CreateAnimImage_AdaptCanvas_m3237762389(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CreateAnimImage::CreatePanels()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisEasyTween_t3917628265_m1697414744_MethodInfo_var;
extern const MethodInfo* List_1_Add_m506084920_MethodInfo_var;
extern const uint32_t CreateAnimImage_CreatePanels_m2698355922_MetadataUsageId;
extern "C"  void CreateAnimImage_CreatePanels_m2698355922 (CreateAnimImage_t2928946190 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CreateAnimImage_CreatePanels_m2698355922_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	GameObject_t3674682005 * V_2 = NULL;
	EasyTween_t3917628265 * V_3 = NULL;
	{
		Vector3_t4282066566  L_0 = __this->get_EndAnim_6();
		V_0 = L_0;
		__this->set_totalWidth_14((0.0f));
		V_1 = 0;
		goto IL_00b6;
	}

IL_0019:
	{
		GameObject_t3674682005 * L_1 = __this->get_CreateInstance_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		GameObject_t3674682005 * L_2 = Object_Instantiate_TisGameObject_t3674682005_m3917608929(NULL /*static, unused*/, L_1, /*hidden argument*/Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var);
		V_2 = L_2;
		GameObject_t3674682005 * L_3 = V_2;
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = GameObject_get_transform_m1278640159(L_3, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_5 = __this->get_RootRect_10();
		NullCheck(L_4);
		Transform_SetParent_m263985879(L_4, L_5, (bool)0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_6 = V_2;
		NullCheck(L_6);
		EasyTween_t3917628265 * L_7 = GameObject_GetComponent_TisEasyTween_t3917628265_m1697414744(L_6, /*hidden argument*/GameObject_GetComponent_TisEasyTween_t3917628265_m1697414744_MethodInfo_var);
		V_3 = L_7;
		List_1_t990846521 * L_8 = __this->get_Created_12();
		EasyTween_t3917628265 * L_9 = V_3;
		NullCheck(L_8);
		List_1_Add_m506084920(L_8, L_9, /*hidden argument*/List_1_Add_m506084920_MethodInfo_var);
		Vector3_t4282066566 * L_10 = __this->get_address_of_StartAnim_5();
		float L_11 = (&V_0)->get_y_2();
		L_10->set_y_2(L_11);
		EasyTween_t3917628265 * L_12 = V_3;
		Vector3_t4282066566  L_13 = __this->get_StartAnim_5();
		Vector2_t4282066565  L_14 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		Vector3_t4282066566  L_15 = V_0;
		Vector2_t4282066565  L_16 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		AnimationCurve_t3667593487 * L_17 = __this->get_EnterAnim_8();
		AnimationCurve_t3667593487 * L_18 = __this->get_ExitAnim_9();
		NullCheck(L_12);
		EasyTween_SetAnimationPosition_m3963557281(L_12, L_14, L_16, L_17, L_18, /*hidden argument*/NULL);
		EasyTween_t3917628265 * L_19 = V_3;
		NullCheck(L_19);
		EasyTween_SetFade_m2968152062(L_19, /*hidden argument*/NULL);
		EasyTween_t3917628265 * L_20 = V_3;
		NullCheck(L_20);
		EasyTween_OpenCloseObjectAnimation_m645046265(L_20, /*hidden argument*/NULL);
		Vector3_t4282066566 * L_21 = (&V_0);
		float L_22 = L_21->get_y_2();
		float L_23 = __this->get_Offset_7();
		L_21->set_y_2(((float)((float)L_22+(float)L_23)));
		float L_24 = __this->get_totalWidth_14();
		float L_25 = __this->get_Offset_7();
		__this->set_totalWidth_14(((float)((float)L_24+(float)L_25)));
		int32_t L_26 = V_1;
		V_1 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00b6:
	{
		int32_t L_27 = V_1;
		int32_t L_28 = __this->get_HowManyButtons_4();
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_0019;
		}
	}
	{
		return;
	}
}
// System.Void CreateAnimImage::AdaptCanvas()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t CreateAnimImage_AdaptCanvas_m3237762389_MetadataUsageId;
extern "C"  void CreateAnimImage_AdaptCanvas_m3237762389 (CreateAnimImage_t2928946190 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CreateAnimImage_AdaptCanvas_m3237762389_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t4282066565  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Vector2_t4282066565 * L_0 = __this->get_address_of_InitialCanvasScrollSize_13();
		float L_1 = L_0->get_x_1();
		float L_2 = __this->get_totalWidth_14();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_3 = fabsf(L_2);
		if ((!(((float)L_1) < ((float)L_3))))
		{
			goto IL_0064;
		}
	}
	{
		RectTransform_t972643934 * L_4 = __this->get_RootRect_10();
		RectTransform_t972643934 * L_5 = __this->get_RootRect_10();
		NullCheck(L_5);
		Vector2_t4282066565  L_6 = RectTransform_get_offsetMin_m3509071456(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		float L_7 = (&V_0)->get_x_1();
		float L_8 = __this->get_totalWidth_14();
		Vector2_t4282066565 * L_9 = __this->get_address_of_InitialCanvasScrollSize_13();
		float L_10 = L_9->get_x_1();
		RectTransform_t972643934 * L_11 = __this->get_RootRect_10();
		NullCheck(L_11);
		Vector2_t4282066565  L_12 = RectTransform_get_offsetMax_m3508842738(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		float L_13 = (&V_1)->get_y_2();
		Vector2_t4282066565  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector2__ctor_m1517109030(&L_14, L_7, ((float)((float)((float)((float)L_8+(float)L_10))+(float)L_13)), /*hidden argument*/NULL);
		NullCheck(L_4);
		RectTransform_set_offsetMin_m951793481(L_4, L_14, /*hidden argument*/NULL);
	}

IL_0064:
	{
		return;
	}
}
// System.Void CreditsHUD::.ctor()
extern "C"  void CreditsHUD__ctor_m3189434142 (CreditsHUD_t1430045213 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CreditsHUD::Start()
extern const MethodInfo* GameObject_GetComponent_TisSlots_t79980053_m4269206828_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisText_t9039225_m1610753993_MethodInfo_var;
extern const uint32_t CreditsHUD_Start_m2136571934_MetadataUsageId;
extern "C"  void CreditsHUD_Start_m2136571934 (CreditsHUD_t1430045213 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CreditsHUD_Start_m2136571934_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_slotsObject_3();
		NullCheck(L_0);
		Slots_t79980053 * L_1 = GameObject_GetComponent_TisSlots_t79980053_m4269206828(L_0, /*hidden argument*/GameObject_GetComponent_TisSlots_t79980053_m4269206828_MethodInfo_var);
		__this->set_slotsController_4(L_1);
		Text_t9039225 * L_2 = Component_GetComponent_TisText_t9039225_m1610753993(__this, /*hidden argument*/Component_GetComponent_TisText_t9039225_m1610753993_MethodInfo_var);
		__this->set_text_2(L_2);
		return;
	}
}
// System.Void CreditsHUD::UpdateTextWithBlink()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2605502182;
extern const uint32_t CreditsHUD_UpdateTextWithBlink_m275215932_MetadataUsageId;
extern "C"  void CreditsHUD_UpdateTextWithBlink_m275215932 (CreditsHUD_t1430045213 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CreditsHUD_UpdateTextWithBlink_m275215932_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Text_t9039225 * L_0 = __this->get_text_2();
		Slots_t79980053 * L_1 = __this->get_slotsController_4();
		NullCheck(L_1);
		int32_t* L_2 = L_1->get_address_of_credits_55();
		String_t* L_3 = Int32_ToString_m1286526384(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2605502182, L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_4);
		float L_5 = __this->get_timeColorText_5();
		Il2CppObject * L_6 = CreditsHUD_BlinkText_m2350740718(__this, L_5, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CreditsHUD::UpdateText()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2605502182;
extern const uint32_t CreditsHUD_UpdateText_m2045600444_MetadataUsageId;
extern "C"  void CreditsHUD_UpdateText_m2045600444 (CreditsHUD_t1430045213 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CreditsHUD_UpdateText_m2045600444_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Text_t9039225 * L_0 = __this->get_text_2();
		Slots_t79980053 * L_1 = __this->get_slotsController_4();
		NullCheck(L_1);
		int32_t* L_2 = L_1->get_address_of_credits_55();
		String_t* L_3 = Int32_ToString_m1286526384(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2605502182, L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_4);
		return;
	}
}
// System.Collections.IEnumerator CreditsHUD::BlinkText(System.Single)
extern Il2CppClass* U3CBlinkTextU3Ec__Iterator0_t3107080670_il2cpp_TypeInfo_var;
extern const uint32_t CreditsHUD_BlinkText_m2350740718_MetadataUsageId;
extern "C"  Il2CppObject * CreditsHUD_BlinkText_m2350740718 (CreditsHUD_t1430045213 * __this, float ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CreditsHUD_BlinkText_m2350740718_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CBlinkTextU3Ec__Iterator0_t3107080670 * V_0 = NULL;
	{
		U3CBlinkTextU3Ec__Iterator0_t3107080670 * L_0 = (U3CBlinkTextU3Ec__Iterator0_t3107080670 *)il2cpp_codegen_object_new(U3CBlinkTextU3Ec__Iterator0_t3107080670_il2cpp_TypeInfo_var);
		U3CBlinkTextU3Ec__Iterator0__ctor_m2049097853(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CBlinkTextU3Ec__Iterator0_t3107080670 * L_1 = V_0;
		float L_2 = ___t0;
		NullCheck(L_1);
		L_1->set_t_0(L_2);
		U3CBlinkTextU3Ec__Iterator0_t3107080670 * L_3 = V_0;
		float L_4 = ___t0;
		NullCheck(L_3);
		L_3->set_U3CU24U3Et_3(L_4);
		U3CBlinkTextU3Ec__Iterator0_t3107080670 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_U3CU3Ef__this_4(__this);
		U3CBlinkTextU3Ec__Iterator0_t3107080670 * L_6 = V_0;
		return L_6;
	}
}
// System.Void CreditsHUD/<BlinkText>c__Iterator0::.ctor()
extern "C"  void U3CBlinkTextU3Ec__Iterator0__ctor_m2049097853 (U3CBlinkTextU3Ec__Iterator0_t3107080670 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object CreditsHUD/<BlinkText>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBlinkTextU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m405898303 (U3CBlinkTextU3Ec__Iterator0_t3107080670 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object CreditsHUD/<BlinkText>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBlinkTextU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1402594771 (U3CBlinkTextU3Ec__Iterator0_t3107080670 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean CreditsHUD/<BlinkText>c__Iterator0::MoveNext()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2605502182;
extern const uint32_t U3CBlinkTextU3Ec__Iterator0_MoveNext_m108206911_MetadataUsageId;
extern "C"  bool U3CBlinkTextU3Ec__Iterator0_MoveNext_m108206911 (U3CBlinkTextU3Ec__Iterator0_t3107080670 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CBlinkTextU3Ec__Iterator0_MoveNext_m108206911_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0087;
		}
		if (L_1 == 2)
		{
			goto IL_00b8;
		}
	}
	{
		goto IL_00fe;
	}

IL_0025:
	{
		goto IL_00b8;
	}

IL_002a:
	{
		float L_2 = __this->get_t_0();
		float L_3 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_t_0(((float)((float)L_2-(float)L_3)));
		CreditsHUD_t1430045213 * L_4 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_4);
		Text_t9039225 * L_5 = L_4->get_text_2();
		CreditsHUD_t1430045213 * L_6 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_6);
		Slots_t79980053 * L_7 = L_6->get_slotsController_4();
		NullCheck(L_7);
		int32_t* L_8 = L_7->get_address_of_credits_55();
		String_t* L_9 = Int32_ToString_m1286526384(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2605502182, L_9, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_10);
		WaitForSeconds_t1615819279 * L_11 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_11, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_11);
		__this->set_U24PC_1(1);
		goto IL_0100;
	}

IL_0087:
	{
		CreditsHUD_t1430045213 * L_12 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_12);
		Text_t9039225 * L_13 = L_12->get_text_2();
		NullCheck(L_13);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_13, _stringLiteral2605502182);
		WaitForSeconds_t1615819279 * L_14 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_14, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_14);
		__this->set_U24PC_1(2);
		goto IL_0100;
	}

IL_00b8:
	{
		float L_15 = __this->get_t_0();
		if ((((float)L_15) > ((float)(0.0f))))
		{
			goto IL_002a;
		}
	}
	{
		CreditsHUD_t1430045213 * L_16 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_16);
		Text_t9039225 * L_17 = L_16->get_text_2();
		CreditsHUD_t1430045213 * L_18 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_18);
		Slots_t79980053 * L_19 = L_18->get_slotsController_4();
		NullCheck(L_19);
		int32_t* L_20 = L_19->get_address_of_credits_55();
		String_t* L_21 = Int32_ToString_m1286526384(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2605502182, L_21, /*hidden argument*/NULL);
		NullCheck(L_17);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_17, L_22);
		__this->set_U24PC_1((-1));
	}

IL_00fe:
	{
		return (bool)0;
	}

IL_0100:
	{
		return (bool)1;
	}
	// Dead block : IL_0102: ldloc.1
}
// System.Void CreditsHUD/<BlinkText>c__Iterator0::Dispose()
extern "C"  void U3CBlinkTextU3Ec__Iterator0_Dispose_m1986298234 (U3CBlinkTextU3Ec__Iterator0_t3107080670 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void CreditsHUD/<BlinkText>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CBlinkTextU3Ec__Iterator0_Reset_m3990498090_MetadataUsageId;
extern "C"  void U3CBlinkTextU3Ec__Iterator0_Reset_m3990498090 (U3CBlinkTextU3Ec__Iterator0_t3107080670 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CBlinkTextU3Ec__Iterator0_Reset_m3990498090_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void EasyTween::.ctor()
extern Il2CppClass* UnityEvent_t1266085011_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationParts_t4290477440_il2cpp_TypeInfo_var;
extern const uint32_t EasyTween__ctor_m2425591842_MetadataUsageId;
extern "C"  void EasyTween__ctor_m2425591842 (EasyTween_t3917628265 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EasyTween__ctor_m2425591842_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityEvent_t1266085011 * L_0 = (UnityEvent_t1266085011 *)il2cpp_codegen_object_new(UnityEvent_t1266085011_il2cpp_TypeInfo_var);
		UnityEvent__ctor_m1715209183(L_0, /*hidden argument*/NULL);
		UnityEvent_t1266085011 * L_1 = (UnityEvent_t1266085011 *)il2cpp_codegen_object_new(UnityEvent_t1266085011_il2cpp_TypeInfo_var);
		UnityEvent__ctor_m1715209183(L_1, /*hidden argument*/NULL);
		AnimationParts_t4290477440 * L_2 = (AnimationParts_t4290477440 *)il2cpp_codegen_object_new(AnimationParts_t4290477440_il2cpp_TypeInfo_var);
		AnimationParts__ctor_m743217775(L_2, 1, (bool)0, (bool)0, (bool)0, 0, ((int32_t)15), L_0, L_1, /*hidden argument*/NULL);
		__this->set_animationParts_3(L_2);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		EasyTween_CheckIfCurrenAnimationGoingExits_m1184422497(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EasyTween::OpenCloseObjectAnimation()
extern "C"  void EasyTween_OpenCloseObjectAnimation_m645046265 (EasyTween_t3917628265 * __this, const MethodInfo* method)
{
	{
		RectTransform_t972643934 * L_0 = __this->get_rectTransform_2();
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = Component_get_gameObject_m1170635899(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m3538205401(L_1, (bool)1, /*hidden argument*/NULL);
		EasyTween_TriggerOpenClose_m712947928(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean EasyTween::IsObjectOpened()
extern "C"  bool EasyTween_IsObjectOpened_m402888384 (EasyTween_t3917628265 * __this, const MethodInfo* method)
{
	{
		CurrentAnimation_t3872301071 * L_0 = __this->get_currentAnimationGoing_4();
		NullCheck(L_0);
		bool L_1 = CurrentAnimation_IsObjectOpened_m1592603601(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void EasyTween::SetUnscaledTimeAnimation(System.Boolean)
extern "C"  void EasyTween_SetUnscaledTimeAnimation_m2802250459 (EasyTween_t3917628265 * __this, bool ___UnscaledTimeAnimation0, const MethodInfo* method)
{
	{
		AnimationParts_t4290477440 * L_0 = __this->get_animationParts_3();
		bool L_1 = ___UnscaledTimeAnimation0;
		NullCheck(L_0);
		L_0->set_UnscaledTimeAnimation_4(L_1);
		return;
	}
}
// System.Void EasyTween::SetAnimatioDuration(System.Single)
extern "C"  void EasyTween_SetAnimatioDuration_m2453087307 (EasyTween_t3917628265 * __this, float ___duration0, const MethodInfo* method)
{
	{
		float L_0 = ___duration0;
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_001c;
		}
	}
	{
		CurrentAnimation_t3872301071 * L_1 = __this->get_currentAnimationGoing_4();
		float L_2 = ___duration0;
		NullCheck(L_1);
		CurrentAnimation_SetAniamtioDuration_m816778444(L_1, L_2, /*hidden argument*/NULL);
		goto IL_002c;
	}

IL_001c:
	{
		CurrentAnimation_t3872301071 * L_3 = __this->get_currentAnimationGoing_4();
		NullCheck(L_3);
		CurrentAnimation_SetAniamtioDuration_m816778444(L_3, (0.01f), /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Single EasyTween::GetAnimationDuration()
extern "C"  float EasyTween_GetAnimationDuration_m3541486488 (EasyTween_t3917628265 * __this, const MethodInfo* method)
{
	{
		CurrentAnimation_t3872301071 * L_0 = __this->get_currentAnimationGoing_4();
		NullCheck(L_0);
		float L_1 = CurrentAnimation_GetAnimationDuration_m719667481(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void EasyTween::SetAnimationPosition(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.AnimationCurve,UnityEngine.AnimationCurve)
extern "C"  void EasyTween_SetAnimationPosition_m3963557281 (EasyTween_t3917628265 * __this, Vector2_t4282066565  ___StartAnchoredPos0, Vector2_t4282066565  ___EndAnchoredPos1, AnimationCurve_t3667593487 * ___EntryTween2, AnimationCurve_t3667593487 * ___ExitTween3, const MethodInfo* method)
{
	{
		CurrentAnimation_t3872301071 * L_0 = __this->get_currentAnimationGoing_4();
		Vector2_t4282066565  L_1 = ___StartAnchoredPos0;
		Vector2_t4282066565  L_2 = ___EndAnchoredPos1;
		AnimationCurve_t3667593487 * L_3 = ___EntryTween2;
		AnimationCurve_t3667593487 * L_4 = ___ExitTween3;
		RectTransform_t972643934 * L_5 = __this->get_rectTransform_2();
		NullCheck(L_0);
		CurrentAnimation_SetAnimationPos_m4170296672(L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EasyTween::SetAnimationScale(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.AnimationCurve,UnityEngine.AnimationCurve)
extern "C"  void EasyTween_SetAnimationScale_m841679492 (EasyTween_t3917628265 * __this, Vector3_t4282066566  ___StartAnchoredScale0, Vector3_t4282066566  ___EndAnchoredScale1, AnimationCurve_t3667593487 * ___EntryTween2, AnimationCurve_t3667593487 * ___ExitTween3, const MethodInfo* method)
{
	{
		CurrentAnimation_t3872301071 * L_0 = __this->get_currentAnimationGoing_4();
		Vector3_t4282066566  L_1 = ___StartAnchoredScale0;
		Vector2_t4282066565  L_2 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Vector3_t4282066566  L_3 = ___EndAnchoredScale1;
		Vector2_t4282066565  L_4 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		AnimationCurve_t3667593487 * L_5 = ___EntryTween2;
		AnimationCurve_t3667593487 * L_6 = ___ExitTween3;
		NullCheck(L_0);
		CurrentAnimation_SetAnimationScale_m3631654511(L_0, L_2, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EasyTween::SetAnimationRotation(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.AnimationCurve,UnityEngine.AnimationCurve)
extern "C"  void EasyTween_SetAnimationRotation_m1258208490 (EasyTween_t3917628265 * __this, Vector3_t4282066566  ___StartAnchoredEulerAng0, Vector3_t4282066566  ___EndAnchoredEulerAng1, AnimationCurve_t3667593487 * ___EntryTween2, AnimationCurve_t3667593487 * ___ExitTween3, const MethodInfo* method)
{
	{
		CurrentAnimation_t3872301071 * L_0 = __this->get_currentAnimationGoing_4();
		Vector3_t4282066566  L_1 = ___StartAnchoredEulerAng0;
		Vector2_t4282066565  L_2 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Vector3_t4282066566  L_3 = ___EndAnchoredEulerAng1;
		Vector2_t4282066565  L_4 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		AnimationCurve_t3667593487 * L_5 = ___EntryTween2;
		AnimationCurve_t3667593487 * L_6 = ___ExitTween3;
		NullCheck(L_0);
		CurrentAnimation_SetAnimationRotation_m468204515(L_0, L_2, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EasyTween::SetFade(System.Boolean)
extern "C"  void EasyTween_SetFade_m1094994165 (EasyTween_t3917628265 * __this, bool ___OverrideFade0, const MethodInfo* method)
{
	{
		CurrentAnimation_t3872301071 * L_0 = __this->get_currentAnimationGoing_4();
		bool L_1 = ___OverrideFade0;
		NullCheck(L_0);
		CurrentAnimation_SetFade_m4004030636(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EasyTween::SetFade()
extern "C"  void EasyTween_SetFade_m2968152062 (EasyTween_t3917628265 * __this, const MethodInfo* method)
{
	{
		CurrentAnimation_t3872301071 * L_0 = __this->get_currentAnimationGoing_4();
		NullCheck(L_0);
		CurrentAnimation_SetFade_m4004030636(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EasyTween::SetFadeStartEndValues(System.Single,System.Single)
extern "C"  void EasyTween_SetFadeStartEndValues_m4012706679 (EasyTween_t3917628265 * __this, float ___startValua0, float ___endValue1, const MethodInfo* method)
{
	{
		CurrentAnimation_t3872301071 * L_0 = __this->get_currentAnimationGoing_4();
		float L_1 = ___startValua0;
		float L_2 = ___endValue1;
		NullCheck(L_0);
		CurrentAnimation_SetFadeValuesStartEnd_m1730649248(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EasyTween::SetAnimationProperties(UITween.AnimationParts)
extern Il2CppClass* CurrentAnimation_t3872301071_il2cpp_TypeInfo_var;
extern const uint32_t EasyTween_SetAnimationProperties_m2536121572_MetadataUsageId;
extern "C"  void EasyTween_SetAnimationProperties_m2536121572 (EasyTween_t3917628265 * __this, AnimationParts_t4290477440 * ___animationParts0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EasyTween_SetAnimationProperties_m2536121572_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AnimationParts_t4290477440 * L_0 = ___animationParts0;
		__this->set_animationParts_3(L_0);
		AnimationParts_t4290477440 * L_1 = ___animationParts0;
		CurrentAnimation_t3872301071 * L_2 = (CurrentAnimation_t3872301071 *)il2cpp_codegen_object_new(CurrentAnimation_t3872301071_il2cpp_TypeInfo_var);
		CurrentAnimation__ctor_m921667526(L_2, L_1, /*hidden argument*/NULL);
		__this->set_currentAnimationGoing_4(L_2);
		return;
	}
}
// System.Void EasyTween::ChangeSetState(System.Boolean)
extern "C"  void EasyTween_ChangeSetState_m595772600 (EasyTween_t3917628265 * __this, bool ___opened0, const MethodInfo* method)
{
	{
		CurrentAnimation_t3872301071 * L_0 = __this->get_currentAnimationGoing_4();
		bool L_1 = ___opened0;
		NullCheck(L_0);
		CurrentAnimation_SetStatus_m337475618(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EasyTween::Start()
extern Il2CppClass* DisableOrDestroy_t4252453067_il2cpp_TypeInfo_var;
extern const MethodInfo* EasyTween_CheckTriggerEndState_m3648472788_MethodInfo_var;
extern const uint32_t EasyTween_Start_m1372729634_MetadataUsageId;
extern "C"  void EasyTween_Start_m1372729634 (EasyTween_t3917628265 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EasyTween_Start_m1372729634_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)EasyTween_CheckTriggerEndState_m3648472788_MethodInfo_var);
		DisableOrDestroy_t4252453067 * L_1 = (DisableOrDestroy_t4252453067 *)il2cpp_codegen_object_new(DisableOrDestroy_t4252453067_il2cpp_TypeInfo_var);
		DisableOrDestroy__ctor_m4277289250(L_1, __this, L_0, /*hidden argument*/NULL);
		AnimationParts_add_OnDisableOrDestroy_m475795999(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EasyTween::OnDestroy()
extern Il2CppClass* DisableOrDestroy_t4252453067_il2cpp_TypeInfo_var;
extern const MethodInfo* EasyTween_CheckTriggerEndState_m3648472788_MethodInfo_var;
extern const uint32_t EasyTween_OnDestroy_m4273507355_MetadataUsageId;
extern "C"  void EasyTween_OnDestroy_m4273507355 (EasyTween_t3917628265 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EasyTween_OnDestroy_m4273507355_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)EasyTween_CheckTriggerEndState_m3648472788_MethodInfo_var);
		DisableOrDestroy_t4252453067 * L_1 = (DisableOrDestroy_t4252453067 *)il2cpp_codegen_object_new(DisableOrDestroy_t4252453067_il2cpp_TypeInfo_var);
		DisableOrDestroy__ctor_m4277289250(L_1, __this, L_0, /*hidden argument*/NULL);
		AnimationParts_remove_OnDisableOrDestroy_m3821145048(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EasyTween::Update()
extern "C"  void EasyTween_Update_m3905765163 (EasyTween_t3917628265 * __this, const MethodInfo* method)
{
	{
		CurrentAnimation_t3872301071 * L_0 = __this->get_currentAnimationGoing_4();
		RectTransform_t972643934 * L_1 = __this->get_rectTransform_2();
		NullCheck(L_0);
		CurrentAnimation_AnimationFrame_m2355919085(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EasyTween::LateUpdate()
extern "C"  void EasyTween_LateUpdate_m3871700849 (EasyTween_t3917628265 * __this, const MethodInfo* method)
{
	{
		CurrentAnimation_t3872301071 * L_0 = __this->get_currentAnimationGoing_4();
		RectTransform_t972643934 * L_1 = __this->get_rectTransform_2();
		NullCheck(L_0);
		CurrentAnimation_LateAnimationFrame_m927731175(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EasyTween::TriggerOpenClose()
extern "C"  void EasyTween_TriggerOpenClose_m712947928 (EasyTween_t3917628265 * __this, const MethodInfo* method)
{
	{
		CurrentAnimation_t3872301071 * L_0 = __this->get_currentAnimationGoing_4();
		NullCheck(L_0);
		bool L_1 = CurrentAnimation_IsObjectOpened_m1592603601(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		CurrentAnimation_t3872301071 * L_2 = __this->get_currentAnimationGoing_4();
		NullCheck(L_2);
		CurrentAnimation_PlayOpenAnimations_m3798939448(L_2, /*hidden argument*/NULL);
		goto IL_002b;
	}

IL_0020:
	{
		CurrentAnimation_t3872301071 * L_3 = __this->get_currentAnimationGoing_4();
		NullCheck(L_3);
		CurrentAnimation_PlayCloseAnimations_m3386424170(L_3, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void EasyTween::CheckTriggerEndState(System.Boolean,UITween.AnimationParts)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t EasyTween_CheckTriggerEndState_m3648472788_MetadataUsageId;
extern "C"  void EasyTween_CheckTriggerEndState_m3648472788 (EasyTween_t3917628265 * __this, bool ___disable0, AnimationParts_t4290477440 * ___part1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EasyTween_CheckTriggerEndState_m3648472788_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AnimationParts_t4290477440 * L_0 = ___part1;
		AnimationParts_t4290477440 * L_1 = __this->get_animationParts_3();
		if ((((Il2CppObject*)(AnimationParts_t4290477440 *)L_0) == ((Il2CppObject*)(AnimationParts_t4290477440 *)L_1)))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_2 = ___disable0;
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		RectTransform_t972643934 * L_3 = __this->get_rectTransform_2();
		NullCheck(L_3);
		GameObject_t3674682005 * L_4 = Component_get_gameObject_m1170635899(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_SetActive_m3538205401(L_4, (bool)0, /*hidden argument*/NULL);
		goto IL_0077;
	}

IL_0029:
	{
		GameObject_t3674682005 * L_5 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0067;
		}
	}
	{
		RectTransform_t972643934 * L_7 = __this->get_rectTransform_2();
		NullCheck(L_7);
		GameObject_t3674682005 * L_8 = Component_get_gameObject_m1170635899(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_10 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		bool L_11 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if ((!(((uint32_t)((((int32_t)L_9) == ((int32_t)0))? 1 : 0)) == ((uint32_t)L_11))))
		{
			goto IL_0067;
		}
	}
	{
		GameObject_t3674682005 * L_12 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
	}

IL_0067:
	{
		RectTransform_t972643934 * L_13 = __this->get_rectTransform_2();
		NullCheck(L_13);
		GameObject_t3674682005 * L_14 = Component_get_gameObject_m1170635899(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
	}

IL_0077:
	{
		return;
	}
}
// System.Void EasyTween::CheckIfCurrenAnimationGoingExits()
extern "C"  void EasyTween_CheckIfCurrenAnimationGoingExits_m1184422497 (EasyTween_t3917628265 * __this, const MethodInfo* method)
{
	{
		CurrentAnimation_t3872301071 * L_0 = __this->get_currentAnimationGoing_4();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		AnimationParts_t4290477440 * L_1 = __this->get_animationParts_3();
		EasyTween_SetAnimationProperties_m2536121572(__this, L_1, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void FollowClick::.ctor()
extern "C"  void FollowClick__ctor_m4061258004 (FollowClick_t3238389815 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FollowClick::Update()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t FollowClick_Update_m3071808633_MetadataUsageId;
extern "C"  void FollowClick_Update_m3071808633 (FollowClick_t3238389815 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FollowClick_Update_m3071808633_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonDown_m2031691843(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		AnimationCurve_t3667593487 * L_1 = __this->get_LeftClick_2();
		FollowClick_MoveToMouseClick_m2681490077(__this, L_1, /*hidden argument*/NULL);
		goto IL_0033;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetMouseButtonDown_m2031691843(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		AnimationCurve_t3667593487 * L_3 = __this->get_RightClick_3();
		FollowClick_MoveToMouseClick_m2681490077(__this, L_3, /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// System.Void FollowClick::MoveToMouseClick(UnityEngine.AnimationCurve)
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t FollowClick_MoveToMouseClick_m2681490077_MetadataUsageId;
extern "C"  void FollowClick_MoveToMouseClick_m2681490077 (FollowClick_t3238389815 * __this, AnimationCurve_t3667593487 * ___animationCurve0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FollowClick_MoveToMouseClick_m2681490077_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Camera_t2727095145 * L_0 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		Vector3_t4282066566  L_1 = Input_get_mousePosition_m4020233228(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t4282066566  L_2 = Camera_ScreenToViewportPoint_m3727203754(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_x_1();
		int32_t L_4 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t1659122786 * L_5 = __this->get_RootCanvas_5();
		NullCheck(L_5);
		Vector3_t4282066566  L_6 = Transform_get_localScale_m3886572677(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = (&V_1)->get_x_1();
		float L_8 = (&V_0)->get_y_2();
		int32_t L_9 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t1659122786 * L_10 = __this->get_RootCanvas_5();
		NullCheck(L_10);
		Vector3_t4282066566  L_11 = Transform_get_localScale_m3886572677(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		float L_12 = (&V_2)->get_y_2();
		Vector3__ctor_m2926210380((&V_0), ((float)((float)((float)((float)L_3*(float)(((float)((float)L_4)))))/(float)L_7)), ((float)((float)((float)((float)L_8*(float)(((float)((float)L_9)))))/(float)L_12)), (0.0f), /*hidden argument*/NULL);
		EasyTween_t3917628265 * L_13 = __this->get_TweenToControl_4();
		NullCheck(L_13);
		bool L_14 = EasyTween_IsObjectOpened_m402888384(L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0098;
		}
	}
	{
		EasyTween_t3917628265 * L_15 = __this->get_TweenToControl_4();
		EasyTween_t3917628265 * L_16 = __this->get_TweenToControl_4();
		NullCheck(L_16);
		RectTransform_t972643934 * L_17 = L_16->get_rectTransform_2();
		NullCheck(L_17);
		Vector2_t4282066565  L_18 = RectTransform_get_anchoredPosition_m2318455998(L_17, /*hidden argument*/NULL);
		Vector3_t4282066566  L_19 = V_0;
		Vector2_t4282066565  L_20 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		AnimationCurve_t3667593487 * L_21 = ___animationCurve0;
		AnimationCurve_t3667593487 * L_22 = ___animationCurve0;
		NullCheck(L_15);
		EasyTween_SetAnimationPosition_m3963557281(L_15, L_18, L_20, L_21, L_22, /*hidden argument*/NULL);
		goto IL_00bb;
	}

IL_0098:
	{
		EasyTween_t3917628265 * L_23 = __this->get_TweenToControl_4();
		Vector3_t4282066566  L_24 = V_0;
		Vector2_t4282066565  L_25 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		EasyTween_t3917628265 * L_26 = __this->get_TweenToControl_4();
		NullCheck(L_26);
		RectTransform_t972643934 * L_27 = L_26->get_rectTransform_2();
		NullCheck(L_27);
		Vector2_t4282066565  L_28 = RectTransform_get_anchoredPosition_m2318455998(L_27, /*hidden argument*/NULL);
		AnimationCurve_t3667593487 * L_29 = ___animationCurve0;
		AnimationCurve_t3667593487 * L_30 = ___animationCurve0;
		NullCheck(L_23);
		EasyTween_SetAnimationPosition_m3963557281(L_23, L_25, L_28, L_29, L_30, /*hidden argument*/NULL);
	}

IL_00bb:
	{
		EasyTween_t3917628265 * L_31 = __this->get_TweenToControl_4();
		NullCheck(L_31);
		EasyTween_OpenCloseObjectAnimation_m645046265(L_31, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Graficos::.ctor()
extern "C"  void Graficos__ctor_m4272662381 (Graficos_t156140270 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Graficos::Start()
extern "C"  void Graficos_Start_m3219800173 (Graficos_t156140270 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Graficos::Update()
extern "C"  void Graficos_Update_m1035409728 (Graficos_t156140270 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Inventario::.ctor()
extern "C"  void Inventario__ctor_m2752969770 (Inventario_t3778973585 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Inventario::UpdateInventario()
extern Il2CppClass* Slots_t79980053_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisButton_t3896396478_m3384885189_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisImage_t538875265_m3706520426_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisBotonInventario_t1587860951_m2255649026_MethodInfo_var;
extern const uint32_t Inventario_UpdateInventario_m623790324_MetadataUsageId;
extern "C"  void Inventario_UpdateInventario_m623790324 (Inventario_t3778973585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Inventario_UpdateInventario_m623790324_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Button_t3896396478 * V_1 = NULL;
	Button_t3896396478 * V_2 = NULL;
	{
		Inventario_Clear_m159103061(__this, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_00b9;
	}

IL_000d:
	{
		int32_t L_0 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Slots_t79980053_il2cpp_TypeInfo_var);
		int32_t L_1 = ((Slots_t79980053_StaticFields*)Slots_t79980053_il2cpp_TypeInfo_var->static_fields)->get_numLock_57();
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0076;
		}
	}
	{
		Button_t3896396478 * L_2 = __this->get_CellPrefab_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Button_t3896396478 * L_3 = Object_Instantiate_TisButton_t3896396478_m3384885189(NULL /*static, unused*/, L_2, /*hidden argument*/Object_Instantiate_TisButton_t3896396478_m3384885189_MethodInfo_var);
		V_1 = L_3;
		Button_t3896396478 * L_4 = V_1;
		NullCheck(L_4);
		Image_t538875265 * L_5 = Component_GetComponent_TisImage_t538875265_m3706520426(L_4, /*hidden argument*/Component_GetComponent_TisImage_t538875265_m3706520426_MethodInfo_var);
		SpriteU5BU5D_t2761310900* L_6 = __this->get_listaSprite_2();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		Sprite_t3199167241 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_5);
		Image_set_sprite_m572551402(L_5, L_9, /*hidden argument*/NULL);
		Button_t3896396478 * L_10 = V_1;
		NullCheck(L_10);
		Transform_t1659122786 * L_11 = Component_get_transform_m4257140443(L_10, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_12 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_t1659122786 * L_13 = GameObject_get_transform_m1278640159(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_SetParent_m263985879(L_11, L_13, (bool)0, /*hidden argument*/NULL);
		Button_t3896396478 * L_14 = V_1;
		NullCheck(L_14);
		BotonInventario_t1587860951 * L_15 = Component_GetComponent_TisBotonInventario_t1587860951_m2255649026(L_14, /*hidden argument*/Component_GetComponent_TisBotonInventario_t1587860951_m2255649026_MethodInfo_var);
		int32_t L_16 = V_0;
		NullCheck(L_15);
		L_15->set_identificador_3(L_16);
		Button_t3896396478 * L_17 = V_1;
		NullCheck(L_17);
		Transform_t1659122786 * L_18 = Component_get_transform_m4257140443(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Transform_t1659122786 * L_19 = Transform_GetChild_m4040462992(L_18, 0, /*hidden argument*/NULL);
		NullCheck(L_19);
		GameObject_t3674682005 * L_20 = Component_get_gameObject_m1170635899(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		GameObject_SetActive_m3538205401(L_20, (bool)1, /*hidden argument*/NULL);
		goto IL_00b5;
	}

IL_0076:
	{
		Button_t3896396478 * L_21 = __this->get_CellPrefab_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Button_t3896396478 * L_22 = Object_Instantiate_TisButton_t3896396478_m3384885189(NULL /*static, unused*/, L_21, /*hidden argument*/Object_Instantiate_TisButton_t3896396478_m3384885189_MethodInfo_var);
		V_2 = L_22;
		Button_t3896396478 * L_23 = V_2;
		NullCheck(L_23);
		Image_t538875265 * L_24 = Component_GetComponent_TisImage_t538875265_m3706520426(L_23, /*hidden argument*/Component_GetComponent_TisImage_t538875265_m3706520426_MethodInfo_var);
		SpriteU5BU5D_t2761310900* L_25 = __this->get_listaSprite_2();
		SpriteU5BU5D_t2761310900* L_26 = __this->get_listaSprite_2();
		NullCheck(L_26);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_26)->max_length))))-(int32_t)1)));
		int32_t L_27 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_26)->max_length))))-(int32_t)1));
		Sprite_t3199167241 * L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		Image_set_sprite_m572551402(L_24, L_28, /*hidden argument*/NULL);
		Button_t3896396478 * L_29 = V_2;
		NullCheck(L_29);
		Transform_t1659122786 * L_30 = Component_get_transform_m4257140443(L_29, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_31 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_31);
		Transform_t1659122786 * L_32 = GameObject_get_transform_m1278640159(L_31, /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_SetParent_m263985879(L_30, L_32, (bool)0, /*hidden argument*/NULL);
	}

IL_00b5:
	{
		int32_t L_33 = V_0;
		V_0 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00b9:
	{
		int32_t L_34 = V_0;
		SpriteU5BU5D_t2761310900* L_35 = __this->get_listaSprite_2();
		NullCheck(L_35);
		if ((((int32_t)L_34) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_35)->max_length)))))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}
}
// System.Void Inventario::IncreaseAmount(System.Int32)
extern "C"  void Inventario_IncreaseAmount_m734132901 (Inventario_t3778973585 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3230847821* L_0 = __this->get_amounts_3();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (int32_t)1);
		return;
	}
}
// System.Void Inventario::UpdateInventoryWithBlink()
extern "C"  void Inventario_UpdateInventoryWithBlink_m2469989757 (Inventario_t3778973585 * __this, const MethodInfo* method)
{
	{
		Inventario_UpdateInventario_m623790324(__this, /*hidden argument*/NULL);
		float L_0 = __this->get_timeColorPic_5();
		Il2CppObject * L_1 = Inventario_BlinkPic_m470484731(__this, L_0, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Inventario::BlinkPic(System.Single)
extern Il2CppClass* U3CBlinkPicU3Ec__Iterator1_t852100114_il2cpp_TypeInfo_var;
extern const uint32_t Inventario_BlinkPic_m470484731_MetadataUsageId;
extern "C"  Il2CppObject * Inventario_BlinkPic_m470484731 (Inventario_t3778973585 * __this, float ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Inventario_BlinkPic_m470484731_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CBlinkPicU3Ec__Iterator1_t852100114 * V_0 = NULL;
	{
		U3CBlinkPicU3Ec__Iterator1_t852100114 * L_0 = (U3CBlinkPicU3Ec__Iterator1_t852100114 *)il2cpp_codegen_object_new(U3CBlinkPicU3Ec__Iterator1_t852100114_il2cpp_TypeInfo_var);
		U3CBlinkPicU3Ec__Iterator1__ctor_m1180781977(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CBlinkPicU3Ec__Iterator1_t852100114 * L_1 = V_0;
		float L_2 = ___t0;
		NullCheck(L_1);
		L_1->set_t_1(L_2);
		U3CBlinkPicU3Ec__Iterator1_t852100114 * L_3 = V_0;
		float L_4 = ___t0;
		NullCheck(L_3);
		L_3->set_U3CU24U3Et_4(L_4);
		U3CBlinkPicU3Ec__Iterator1_t852100114 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_U3CU3Ef__this_5(__this);
		U3CBlinkPicU3Ec__Iterator1_t852100114 * L_6 = V_0;
		return L_6;
	}
}
// System.Void Inventario::Clear()
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* Transform_t1659122786_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t Inventario_Clear_m159103061_MetadataUsageId;
extern "C"  void Inventario_Clear_m159103061 (Inventario_t3778973585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Inventario_Clear_m159103061_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t1659122786 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Transform_t1659122786 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject * L_1 = Transform_GetEnumerator_m688365631(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0028;
		}

IL_0011:
		{
			Il2CppObject * L_2 = V_1;
			NullCheck(L_2);
			Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_2);
			V_0 = ((Transform_t1659122786 *)CastclassClass(L_3, Transform_t1659122786_il2cpp_TypeInfo_var));
			Transform_t1659122786 * L_4 = V_0;
			NullCheck(L_4);
			GameObject_t3674682005 * L_5 = Component_get_gameObject_m1170635899(L_4, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
			Object_Destroy_m176400816(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		}

IL_0028:
		{
			Il2CppObject * L_6 = V_1;
			NullCheck(L_6);
			bool L_7 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_6);
			if (L_7)
			{
				goto IL_0011;
			}
		}

IL_0033:
		{
			IL2CPP_LEAVE(0x4A, FINALLY_0038);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0038;
	}

FINALLY_0038:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_8 = V_1;
			V_2 = ((Il2CppObject *)IsInst(L_8, IDisposable_t1423340799_il2cpp_TypeInfo_var));
			Il2CppObject * L_9 = V_2;
			if (L_9)
			{
				goto IL_0043;
			}
		}

IL_0042:
		{
			IL2CPP_END_FINALLY(56)
		}

IL_0043:
		{
			Il2CppObject * L_10 = V_2;
			NullCheck(L_10);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_10);
			IL2CPP_END_FINALLY(56)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(56)
	{
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_004a:
	{
		return;
	}
}
// System.Void Inventario/<BlinkPic>c__Iterator1::.ctor()
extern "C"  void U3CBlinkPicU3Ec__Iterator1__ctor_m1180781977 (U3CBlinkPicU3Ec__Iterator1_t852100114 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Inventario/<BlinkPic>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBlinkPicU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2489074841 (U3CBlinkPicU3Ec__Iterator1_t852100114 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object Inventario/<BlinkPic>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBlinkPicU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m4160002605 (U3CBlinkPicU3Ec__Iterator1_t852100114 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Boolean Inventario/<BlinkPic>c__Iterator1::MoveNext()
extern Il2CppClass* Slots_t79980053_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisImage_t538875265_m3706520426_MethodInfo_var;
extern const uint32_t U3CBlinkPicU3Ec__Iterator1_MoveNext_m266222267_MetadataUsageId;
extern "C"  bool U3CBlinkPicU3Ec__Iterator1_MoveNext_m266222267 (U3CBlinkPicU3Ec__Iterator1_t852100114 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CBlinkPicU3Ec__Iterator1_MoveNext_m266222267_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_00ad;
		}
		if (L_1 == 2)
		{
			goto IL_0109;
		}
	}
	{
		goto IL_0154;
	}

IL_0025:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0127;
	}

IL_0031:
	{
		int32_t L_2 = __this->get_U3CiU3E__0_0();
		IL2CPP_RUNTIME_CLASS_INIT(Slots_t79980053_il2cpp_TypeInfo_var);
		int32_t L_3 = ((Slots_t79980053_StaticFields*)Slots_t79980053_il2cpp_TypeInfo_var->static_fields)->get_numLock_57();
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)((int32_t)L_3-(int32_t)1))))))
		{
			goto IL_0119;
		}
	}
	{
		goto IL_0109;
	}

IL_0048:
	{
		float L_4 = __this->get_t_1();
		float L_5 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_t_1(((float)((float)L_4-(float)L_5)));
		Inventario_t3778973585 * L_6 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_6);
		Transform_t1659122786 * L_7 = Component_get_transform_m4257140443(L_6, /*hidden argument*/NULL);
		int32_t L_8 = __this->get_U3CiU3E__0_0();
		NullCheck(L_7);
		Transform_t1659122786 * L_9 = Transform_GetChild_m4040462992(L_7, L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_t1659122786 * L_10 = Component_get_transform_m4257140443(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Image_t538875265 * L_11 = Component_GetComponent_TisImage_t538875265_m3706520426(L_10, /*hidden argument*/Component_GetComponent_TisImage_t538875265_m3706520426_MethodInfo_var);
		Inventario_t3778973585 * L_12 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_12);
		SpriteU5BU5D_t2761310900* L_13 = L_12->get_listaSprite_2();
		int32_t L_14 = __this->get_U3CiU3E__0_0();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		Sprite_t3199167241 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_11);
		Image_set_sprite_m572551402(L_11, L_16, /*hidden argument*/NULL);
		WaitForSeconds_t1615819279 * L_17 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_17, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_3(L_17);
		__this->set_U24PC_2(1);
		goto IL_0156;
	}

IL_00ad:
	{
		Inventario_t3778973585 * L_18 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_18);
		Transform_t1659122786 * L_19 = Component_get_transform_m4257140443(L_18, /*hidden argument*/NULL);
		int32_t L_20 = __this->get_U3CiU3E__0_0();
		NullCheck(L_19);
		Transform_t1659122786 * L_21 = Transform_GetChild_m4040462992(L_19, L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_t1659122786 * L_22 = Component_get_transform_m4257140443(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		Image_t538875265 * L_23 = Component_GetComponent_TisImage_t538875265_m3706520426(L_22, /*hidden argument*/Component_GetComponent_TisImage_t538875265_m3706520426_MethodInfo_var);
		Inventario_t3778973585 * L_24 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_24);
		SpriteU5BU5D_t2761310900* L_25 = L_24->get_listaSprite_2();
		Inventario_t3778973585 * L_26 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_26);
		SpriteU5BU5D_t2761310900* L_27 = L_26->get_listaSprite_2();
		NullCheck(L_27);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_27)->max_length))))-(int32_t)1)));
		int32_t L_28 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_27)->max_length))))-(int32_t)1));
		Sprite_t3199167241 * L_29 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_23);
		Image_set_sprite_m572551402(L_23, L_29, /*hidden argument*/NULL);
		WaitForSeconds_t1615819279 * L_30 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_30, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_3(L_30);
		__this->set_U24PC_2(2);
		goto IL_0156;
	}

IL_0109:
	{
		float L_31 = __this->get_t_1();
		if ((((float)L_31) > ((float)(0.0f))))
		{
			goto IL_0048;
		}
	}

IL_0119:
	{
		int32_t L_32 = __this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_32+(int32_t)1)));
	}

IL_0127:
	{
		int32_t L_33 = __this->get_U3CiU3E__0_0();
		Inventario_t3778973585 * L_34 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_34);
		Transform_t1659122786 * L_35 = Component_get_transform_m4257140443(L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		int32_t L_36 = Transform_get_childCount_m2107810675(L_35, /*hidden argument*/NULL);
		if ((((int32_t)L_33) < ((int32_t)L_36)))
		{
			goto IL_0031;
		}
	}
	{
		Inventario_t3778973585 * L_37 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_37);
		Inventario_UpdateInventario_m623790324(L_37, /*hidden argument*/NULL);
		__this->set_U24PC_2((-1));
	}

IL_0154:
	{
		return (bool)0;
	}

IL_0156:
	{
		return (bool)1;
	}
	// Dead block : IL_0158: ldloc.1
}
// System.Void Inventario/<BlinkPic>c__Iterator1::Dispose()
extern "C"  void U3CBlinkPicU3Ec__Iterator1_Dispose_m758396822 (U3CBlinkPicU3Ec__Iterator1_t852100114 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_2((-1));
		return;
	}
}
// System.Void Inventario/<BlinkPic>c__Iterator1::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CBlinkPicU3Ec__Iterator1_Reset_m3122182214_MetadataUsageId;
extern "C"  void U3CBlinkPicU3Ec__Iterator1_Reset_m3122182214 (U3CBlinkPicU3Ec__Iterator1_t852100114 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CBlinkPicU3Ec__Iterator1_Reset_m3122182214_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Moneco::.ctor()
extern "C"  void Moneco__ctor_m1209859350 (Moneco_t2310332709 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Moneco::Start()
extern "C"  void Moneco_Start_m156997142 (Moneco_t2310332709 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Moneco::Update()
extern "C"  void Moneco_Update_m577796279 (Moneco_t2310332709 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Pool::.ctor()
extern "C"  void Pool__ctor_m2057142751 (Pool_t2493500 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pool::CreatePool(UnityEngine.GameObject,System.Int32)
extern Il2CppClass* GameObjectU5BU5D_t2662109048_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var;
extern const uint32_t Pool_CreatePool_m1264299394_MetadataUsageId;
extern "C"  void Pool_CreatePool_m1264299394 (Pool_t2493500 * __this, GameObject_t3674682005 * ___ObjectToPool0, int32_t ___numberOfObjects1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Pool_CreatePool_m1264299394_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___numberOfObjects1;
		__this->set_ObjectPool_2(((GameObjectU5BU5D_t2662109048*)SZArrayNew(GameObjectU5BU5D_t2662109048_il2cpp_TypeInfo_var, (uint32_t)L_0)));
		GameObject_t3674682005 * L_1 = ___ObjectToPool0;
		__this->set_ObjectToPool_3(L_1);
		V_0 = 0;
		goto IL_003a;
	}

IL_001a:
	{
		GameObjectU5BU5D_t2662109048* L_2 = __this->get_ObjectPool_2();
		int32_t L_3 = V_0;
		GameObject_t3674682005 * L_4 = ___ObjectToPool0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		GameObject_t3674682005 * L_5 = Object_Instantiate_TisGameObject_t3674682005_m3917608929(NULL /*static, unused*/, L_4, /*hidden argument*/Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (GameObject_t3674682005 *)L_5);
		GameObjectU5BU5D_t2662109048* L_6 = __this->get_ObjectPool_2();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		GameObject_t3674682005 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_9);
		GameObject_SetActive_m3538205401(L_9, (bool)0, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_003a:
	{
		int32_t L_11 = V_0;
		GameObjectU5BU5D_t2662109048* L_12 = __this->get_ObjectPool_2();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		return;
	}
}
// UnityEngine.GameObject Pool::GetObject()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var;
extern const uint32_t Pool_GetObject_m1066736605_MetadataUsageId;
extern "C"  GameObject_t3674682005 * Pool_GetObject_m1066736605 (Pool_t2493500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Pool_GetObject_m1066736605_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_006c;
	}

IL_0007:
	{
		GameObjectU5BU5D_t2662109048* L_0 = __this->get_ObjectPool_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		GameObject_t3674682005 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0047;
		}
	}
	{
		GameObjectU5BU5D_t2662109048* L_5 = __this->get_ObjectPool_2();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		GameObject_t3674682005 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_8);
		bool L_9 = GameObject_get_activeSelf_m3858025161(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0042;
		}
	}
	{
		GameObjectU5BU5D_t2662109048* L_10 = __this->get_ObjectPool_2();
		int32_t L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		GameObject_t3674682005 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_13);
		GameObject_SetActive_m3538205401(L_13, (bool)1, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_14 = __this->get_ObjectPool_2();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		GameObject_t3674682005 * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		return L_17;
	}

IL_0042:
	{
		goto IL_0068;
	}

IL_0047:
	{
		GameObjectU5BU5D_t2662109048* L_18 = __this->get_ObjectPool_2();
		int32_t L_19 = V_0;
		GameObject_t3674682005 * L_20 = __this->get_ObjectToPool_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		GameObject_t3674682005 * L_21 = Object_Instantiate_TisGameObject_t3674682005_m3917608929(NULL /*static, unused*/, L_20, /*hidden argument*/Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		ArrayElementTypeCheck (L_18, L_21);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_19), (GameObject_t3674682005 *)L_21);
		GameObjectU5BU5D_t2662109048* L_22 = __this->get_ObjectPool_2();
		int32_t L_23 = V_0;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		GameObject_t3674682005 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		NullCheck(L_25);
		GameObject_SetActive_m3538205401(L_25, (bool)0, /*hidden argument*/NULL);
	}

IL_0068:
	{
		int32_t L_26 = V_0;
		V_0 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_006c:
	{
		int32_t L_27 = V_0;
		GameObjectU5BU5D_t2662109048* L_28 = __this->get_ObjectPool_2();
		NullCheck(L_28);
		if ((((int32_t)L_27) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_28)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return (GameObject_t3674682005 *)NULL;
	}
}
// System.Void PriceHUD::.ctor()
extern "C"  void PriceHUD__ctor_m2070410061 (PriceHUD_t3182648078 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PriceHUD::Start()
extern const MethodInfo* GameObject_GetComponent_TisSlots_t79980053_m4269206828_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisText_t9039225_m1610753993_MethodInfo_var;
extern const uint32_t PriceHUD_Start_m1017547853_MetadataUsageId;
extern "C"  void PriceHUD_Start_m1017547853 (PriceHUD_t3182648078 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PriceHUD_Start_m1017547853_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_slotsObject_3();
		NullCheck(L_0);
		Slots_t79980053 * L_1 = GameObject_GetComponent_TisSlots_t79980053_m4269206828(L_0, /*hidden argument*/GameObject_GetComponent_TisSlots_t79980053_m4269206828_MethodInfo_var);
		__this->set_slotsController_4(L_1);
		Text_t9039225 * L_2 = Component_GetComponent_TisText_t9039225_m1610753993(__this, /*hidden argument*/Component_GetComponent_TisText_t9039225_m1610753993_MethodInfo_var);
		__this->set_text_2(L_2);
		return;
	}
}
// System.Void PriceHUD::UpdateTextWithBlink()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3900622423;
extern const uint32_t PriceHUD_UpdateTextWithBlink_m3409445931_MetadataUsageId;
extern "C"  void PriceHUD_UpdateTextWithBlink_m3409445931 (PriceHUD_t3182648078 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PriceHUD_UpdateTextWithBlink_m3409445931_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Text_t9039225 * L_0 = __this->get_text_2();
		Slots_t79980053 * L_1 = __this->get_slotsController_4();
		NullCheck(L_1);
		int32_t* L_2 = L_1->get_address_of_price_54();
		String_t* L_3 = Int32_ToString_m1286526384(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3900622423, L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_4);
		float L_5 = __this->get_timeColorText_5();
		Il2CppObject * L_6 = PriceHUD_BlinkText_m1826381471(__this, L_5, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PriceHUD::UpdateText()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3900622423;
extern const uint32_t PriceHUD_UpdateText_m3474693101_MetadataUsageId;
extern "C"  void PriceHUD_UpdateText_m3474693101 (PriceHUD_t3182648078 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PriceHUD_UpdateText_m3474693101_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Text_t9039225 * L_0 = __this->get_text_2();
		Slots_t79980053 * L_1 = __this->get_slotsController_4();
		NullCheck(L_1);
		int32_t* L_2 = L_1->get_address_of_price_54();
		String_t* L_3 = Int32_ToString_m1286526384(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3900622423, L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_4);
		return;
	}
}
// System.Collections.IEnumerator PriceHUD::BlinkText(System.Single)
extern Il2CppClass* U3CBlinkTextU3Ec__Iterator2_t3201916881_il2cpp_TypeInfo_var;
extern const uint32_t PriceHUD_BlinkText_m1826381471_MetadataUsageId;
extern "C"  Il2CppObject * PriceHUD_BlinkText_m1826381471 (PriceHUD_t3182648078 * __this, float ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PriceHUD_BlinkText_m1826381471_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CBlinkTextU3Ec__Iterator2_t3201916881 * V_0 = NULL;
	{
		U3CBlinkTextU3Ec__Iterator2_t3201916881 * L_0 = (U3CBlinkTextU3Ec__Iterator2_t3201916881 *)il2cpp_codegen_object_new(U3CBlinkTextU3Ec__Iterator2_t3201916881_il2cpp_TypeInfo_var);
		U3CBlinkTextU3Ec__Iterator2__ctor_m3514618346(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CBlinkTextU3Ec__Iterator2_t3201916881 * L_1 = V_0;
		float L_2 = ___t0;
		NullCheck(L_1);
		L_1->set_t_0(L_2);
		U3CBlinkTextU3Ec__Iterator2_t3201916881 * L_3 = V_0;
		float L_4 = ___t0;
		NullCheck(L_3);
		L_3->set_U3CU24U3Et_3(L_4);
		U3CBlinkTextU3Ec__Iterator2_t3201916881 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_U3CU3Ef__this_4(__this);
		U3CBlinkTextU3Ec__Iterator2_t3201916881 * L_6 = V_0;
		return L_6;
	}
}
// System.Void PriceHUD/<BlinkText>c__Iterator2::.ctor()
extern "C"  void U3CBlinkTextU3Ec__Iterator2__ctor_m3514618346 (U3CBlinkTextU3Ec__Iterator2_t3201916881 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object PriceHUD/<BlinkText>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBlinkTextU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3730810418 (U3CBlinkTextU3Ec__Iterator2_t3201916881 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object PriceHUD/<BlinkText>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBlinkTextU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m741887430 (U3CBlinkTextU3Ec__Iterator2_t3201916881 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean PriceHUD/<BlinkText>c__Iterator2::MoveNext()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3900622423;
extern const uint32_t U3CBlinkTextU3Ec__Iterator2_MoveNext_m225334194_MetadataUsageId;
extern "C"  bool U3CBlinkTextU3Ec__Iterator2_MoveNext_m225334194 (U3CBlinkTextU3Ec__Iterator2_t3201916881 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CBlinkTextU3Ec__Iterator2_MoveNext_m225334194_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0087;
		}
		if (L_1 == 2)
		{
			goto IL_00b8;
		}
	}
	{
		goto IL_00fe;
	}

IL_0025:
	{
		goto IL_00b8;
	}

IL_002a:
	{
		float L_2 = __this->get_t_0();
		float L_3 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_t_0(((float)((float)L_2-(float)L_3)));
		PriceHUD_t3182648078 * L_4 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_4);
		Text_t9039225 * L_5 = L_4->get_text_2();
		PriceHUD_t3182648078 * L_6 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_6);
		Slots_t79980053 * L_7 = L_6->get_slotsController_4();
		NullCheck(L_7);
		int32_t* L_8 = L_7->get_address_of_price_54();
		String_t* L_9 = Int32_ToString_m1286526384(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3900622423, L_9, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_10);
		WaitForSeconds_t1615819279 * L_11 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_11, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_11);
		__this->set_U24PC_1(1);
		goto IL_0100;
	}

IL_0087:
	{
		PriceHUD_t3182648078 * L_12 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_12);
		Text_t9039225 * L_13 = L_12->get_text_2();
		NullCheck(L_13);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_13, _stringLiteral3900622423);
		WaitForSeconds_t1615819279 * L_14 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_14, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_14);
		__this->set_U24PC_1(2);
		goto IL_0100;
	}

IL_00b8:
	{
		float L_15 = __this->get_t_0();
		if ((((float)L_15) > ((float)(0.0f))))
		{
			goto IL_002a;
		}
	}
	{
		PriceHUD_t3182648078 * L_16 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_16);
		Text_t9039225 * L_17 = L_16->get_text_2();
		PriceHUD_t3182648078 * L_18 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_18);
		Slots_t79980053 * L_19 = L_18->get_slotsController_4();
		NullCheck(L_19);
		int32_t* L_20 = L_19->get_address_of_price_54();
		String_t* L_21 = Int32_ToString_m1286526384(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3900622423, L_21, /*hidden argument*/NULL);
		NullCheck(L_17);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_17, L_22);
		__this->set_U24PC_1((-1));
	}

IL_00fe:
	{
		return (bool)0;
	}

IL_0100:
	{
		return (bool)1;
	}
	// Dead block : IL_0102: ldloc.1
}
// System.Void PriceHUD/<BlinkText>c__Iterator2::Dispose()
extern "C"  void U3CBlinkTextU3Ec__Iterator2_Dispose_m1602218919 (U3CBlinkTextU3Ec__Iterator2_t3201916881 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void PriceHUD/<BlinkText>c__Iterator2::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CBlinkTextU3Ec__Iterator2_Reset_m1161051287_MetadataUsageId;
extern "C"  void U3CBlinkTextU3Ec__Iterator2_Reset_m1161051287 (U3CBlinkTextU3Ec__Iterator2_t3201916881 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CBlinkTextU3Ec__Iterator2_Reset_m1161051287_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ReferencedFrom::.ctor()
extern "C"  void ReferencedFrom__ctor_m2448203288 (ReferencedFrom_t1396252131 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Slots::.ctor()
extern "C"  void Slots__ctor_m2362075894 (Slots_t79980053 * __this, const MethodInfo* method)
{
	{
		__this->set_tempFrozen_53((-1));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Slots::.cctor()
extern Il2CppClass* Slots_t79980053_il2cpp_TypeInfo_var;
extern const uint32_t Slots__cctor_m4022779767_MetadataUsageId;
extern "C"  void Slots__cctor_m4022779767 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Slots__cctor_m4022779767_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((Slots_t79980053_StaticFields*)Slots_t79980053_il2cpp_TypeInfo_var->static_fields)->set_numLock_57(3);
		return;
	}
}
// System.Void Slots::Start()
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisVinetas_t2126676892_m3739561413_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisPriceHUD_t3182648078_m3029074103_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCreditsHUD_t1430045213_m2894262728_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCombinaciones_t3637091598_m1358194067_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCounterChecking_t39316438_m1809788579_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRectTransform_t972643934_m1940403147_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral66868;
extern const uint32_t Slots_Start_m1309213686_MetadataUsageId;
extern "C"  void Slots_Start_m1309213686 (Slots_t79980053 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Slots_Start_m1309213686_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t4241904616  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		__this->set_currentState_58(0);
		GameObject_t3674682005 * L_0 = __this->get_Tabla_18();
		NullCheck(L_0);
		Vinetas_t2126676892 * L_1 = GameObject_GetComponent_TisVinetas_t2126676892_m3739561413(L_0, /*hidden argument*/GameObject_GetComponent_TisVinetas_t2126676892_m3739561413_MethodInfo_var);
		__this->set_vi_49(L_1);
		GameObject_t3674682005 * L_2 = __this->get_Audio_14();
		NullCheck(L_2);
		AudioSource_t1740077639 * L_3 = GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151(L_2, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151_MethodInfo_var);
		__this->set_audio_4(L_3);
		GameObject_t3674682005 * L_4 = __this->get_HUD_9();
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = GameObject_get_transform_m1278640159(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t1659122786 * L_6 = Transform_GetChild_m4040462992(L_5, 0, /*hidden argument*/NULL);
		NullCheck(L_6);
		PriceHUD_t3182648078 * L_7 = Component_GetComponent_TisPriceHUD_t3182648078_m3029074103(L_6, /*hidden argument*/Component_GetComponent_TisPriceHUD_t3182648078_m3029074103_MethodInfo_var);
		__this->set_priceHUD_2(L_7);
		GameObject_t3674682005 * L_8 = __this->get_HUD_9();
		NullCheck(L_8);
		Transform_t1659122786 * L_9 = GameObject_get_transform_m1278640159(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_t1659122786 * L_10 = Transform_GetChild_m4040462992(L_9, 1, /*hidden argument*/NULL);
		NullCheck(L_10);
		CreditsHUD_t1430045213 * L_11 = Component_GetComponent_TisCreditsHUD_t1430045213_m2894262728(L_10, /*hidden argument*/Component_GetComponent_TisCreditsHUD_t1430045213_m2894262728_MethodInfo_var);
		__this->set_creditsHUD_3(L_11);
		GameObject_t3674682005 * L_12 = __this->get_Caption_15();
		NullCheck(L_12);
		Text_t9039225 * L_13 = GameObject_GetComponent_TisText_t9039225_m202917489(L_12, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var);
		__this->set_caption_36(L_13);
		GameObject_t3674682005 * L_14 = __this->get_PicChar_16();
		NullCheck(L_14);
		SpriteRenderer_t2548470764 * L_15 = GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(L_14, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var);
		NullCheck(L_15);
		Sprite_t3199167241 * L_16 = SpriteRenderer_get_sprite_m3481747968(L_15, /*hidden argument*/NULL);
		__this->set_picChar_37(L_16);
		GameObject_t3674682005 * L_17 = __this->get_PicCanvas_17();
		NullCheck(L_17);
		SpriteRenderer_t2548470764 * L_18 = GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(L_17, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var);
		NullCheck(L_18);
		Sprite_t3199167241 * L_19 = SpriteRenderer_get_sprite_m3481747968(L_18, /*hidden argument*/NULL);
		__this->set_picCanvas_38(L_19);
		GameObject_t3674682005 * L_20 = __this->get_Tabla_18();
		NullCheck(L_20);
		Combinaciones_t3637091598 * L_21 = GameObject_GetComponent_TisCombinaciones_t3637091598_m1358194067(L_20, /*hidden argument*/GameObject_GetComponent_TisCombinaciones_t3637091598_m1358194067_MethodInfo_var);
		__this->set_combinaciones_39(L_21);
		CounterChecking_t39316438 * L_22 = Component_GetComponent_TisCounterChecking_t39316438_m1809788579(__this, /*hidden argument*/Component_GetComponent_TisCounterChecking_t39316438_m1809788579_MethodInfo_var);
		__this->set_counterChecking_40(L_22);
		int32_t L_23 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = (((float)((float)L_23)));
		Camera_t2727095145 * L_24 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		Canvas_t2727140764 * L_25 = __this->get_canvas_35();
		NullCheck(L_25);
		RectTransform_t972643934 * L_26 = Component_GetComponent_TisRectTransform_t972643934_m1940403147(L_25, /*hidden argument*/Component_GetComponent_TisRectTransform_t972643934_m1940403147_MethodInfo_var);
		NullCheck(L_26);
		Rect_t4241904616  L_27 = RectTransform_get_rect_m1566017036(L_26, /*hidden argument*/NULL);
		V_2 = L_27;
		float L_28 = Rect_get_width_m2824209432((&V_2), /*hidden argument*/NULL);
		Canvas_t2727140764 * L_29 = __this->get_canvas_35();
		NullCheck(L_29);
		float L_30 = Canvas_get_scaleFactor_m1187077271(L_29, /*hidden argument*/NULL);
		float L_31 = V_0;
		Canvas_t2727140764 * L_32 = __this->get_canvas_35();
		NullCheck(L_32);
		float L_33 = Canvas_get_planeDistance_m2921110159(L_32, /*hidden argument*/NULL);
		Vector3_t4282066566  L_34;
		memset(&L_34, 0, sizeof(L_34));
		Vector3__ctor_m2926210380(&L_34, ((float)((float)L_28*(float)L_30)), L_31, L_33, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_t4282066566  L_35 = Camera_ScreenToWorldPoint_m1572306334(L_24, L_34, /*hidden argument*/NULL);
		V_1 = L_35;
		float L_36 = (&V_1)->get_y_2();
		__this->set_heightHUD_56(L_36);
		float L_37 = V_0;
		float L_38 = L_37;
		Il2CppObject * L_39 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_38);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_40 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral66868, L_39, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Slots::StartGame()
extern "C"  void Slots_StartGame_m2441643272 (Slots_t79980053 * __this, const MethodInfo* method)
{
	{
		__this->set_currentState_58(1);
		GameObject_t3674682005 * L_0 = __this->get_StartButton_13();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)0, /*hidden argument*/NULL);
		Slots_resetValues_m3854918917(__this, /*hidden argument*/NULL);
		Slots_resetTimer_m934139748(__this, /*hidden argument*/NULL);
		Slots_UpdateAll_m4067659948(__this, /*hidden argument*/NULL);
		AudioSource_t1740077639 * L_1 = __this->get_audio_4();
		AudioClip_t794140988 * L_2 = __this->get_start_31();
		NullCheck(L_1);
		AudioSource_PlayOneShot_m1217449713(L_1, L_2, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_3 = __this->get_SlotpParent_5();
		NullCheck(L_3);
		GameObject_SetActive_m3538205401(L_3, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Slots::BackToGame()
extern "C"  void Slots_BackToGame_m88590754 (Slots_t79980053 * __this, const MethodInfo* method)
{
	{
		__this->set_currentState_58(1);
		GameObject_t3674682005 * L_0 = __this->get_SlotpParent_5();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Slots::BackToInventory()
extern const MethodInfo* GameObject_GetComponent_TisEasyTween_t3917628265_m1697414744_MethodInfo_var;
extern const uint32_t Slots_BackToInventory_m114061198_MetadataUsageId;
extern "C"  void Slots_BackToInventory_m114061198 (Slots_t79980053 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Slots_BackToInventory_m114061198_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_panelInventario_24();
		NullCheck(L_0);
		EasyTween_t3917628265 * L_1 = GameObject_GetComponent_TisEasyTween_t3917628265_m1697414744(L_0, /*hidden argument*/GameObject_GetComponent_TisEasyTween_t3917628265_m1697414744_MethodInfo_var);
		NullCheck(L_1);
		EasyTween_OpenCloseObjectAnimation_m645046265(L_1, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_2 = __this->get_panelGame_25();
		NullCheck(L_2);
		EasyTween_t3917628265 * L_3 = GameObject_GetComponent_TisEasyTween_t3917628265_m1697414744(L_2, /*hidden argument*/GameObject_GetComponent_TisEasyTween_t3917628265_m1697414744_MethodInfo_var);
		NullCheck(L_3);
		EasyTween_OpenCloseObjectAnimation_m645046265(L_3, /*hidden argument*/NULL);
		Slots_SlotsRackSwitch_m3493919060(__this, /*hidden argument*/NULL);
		__this->set_currentState_58(2);
		return;
	}
}
// System.Void Slots::BackToMap()
extern Il2CppCodeGenString* _stringLiteral268363402;
extern const uint32_t Slots_BackToMap_m2225157198_MetadataUsageId;
extern "C"  void Slots_BackToMap_m2225157198 (Slots_t79980053 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Slots_BackToMap_m2225157198_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_StartCoroutine_m2272783641(__this, _stringLiteral268363402, /*hidden argument*/NULL);
		__this->set_currentState_58(4);
		return;
	}
}
// System.Void Slots::resetSlotColor()
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var;
extern const uint32_t Slots_resetSlotColor_m3253019940_MetadataUsageId;
extern "C"  void Slots_resetSlotColor_m3253019940 (Slots_t79980053 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Slots_resetSlotColor_m3253019940_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BooleanU5BU5D_t3456302923* L_0 = __this->get_frozenSlots_46();
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		int32_t L_1 = 0;
		uint8_t L_2 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1));
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		GameObject_t3674682005 * L_3 = __this->get_slot1_6();
		NullCheck(L_3);
		SpriteRenderer_t2548470764 * L_4 = GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(L_3, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var);
		Color_t4194546905  L_5 = __this->get_blueFrame_34();
		NullCheck(L_4);
		SpriteRenderer_set_color_m2701854973(L_4, L_5, /*hidden argument*/NULL);
	}

IL_0023:
	{
		BooleanU5BU5D_t3456302923* L_6 = __this->get_frozenSlots_46();
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		int32_t L_7 = 1;
		uint8_t L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		if (L_8)
		{
			goto IL_0046;
		}
	}
	{
		GameObject_t3674682005 * L_9 = __this->get_slot2_7();
		NullCheck(L_9);
		SpriteRenderer_t2548470764 * L_10 = GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(L_9, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var);
		Color_t4194546905  L_11 = __this->get_blueFrame_34();
		NullCheck(L_10);
		SpriteRenderer_set_color_m2701854973(L_10, L_11, /*hidden argument*/NULL);
	}

IL_0046:
	{
		BooleanU5BU5D_t3456302923* L_12 = __this->get_frozenSlots_46();
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		int32_t L_13 = 2;
		uint8_t L_14 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		if (L_14)
		{
			goto IL_0069;
		}
	}
	{
		GameObject_t3674682005 * L_15 = __this->get_slot3_8();
		NullCheck(L_15);
		SpriteRenderer_t2548470764 * L_16 = GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(L_15, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var);
		Color_t4194546905  L_17 = __this->get_blueFrame_34();
		NullCheck(L_16);
		SpriteRenderer_set_color_m2701854973(L_16, L_17, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return;
	}
}
// System.Void Slots::resetValues()
extern "C"  void Slots_resetValues_m3854918917 (Slots_t79980053 * __this, const MethodInfo* method)
{
	{
		__this->set_price_54(0);
		__this->set_credits_55(((int32_t)10));
		return;
	}
}
// System.Void Slots::switchHitButtons(System.Boolean)
extern "C"  void Slots_switchHitButtons_m296069031 (Slots_t79980053 * __this, bool ___b0, const MethodInfo* method)
{
	{
		bool L_0 = ___b0;
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		GameObject_t3674682005 * L_1 = __this->get_HitButton_10();
		NullCheck(L_1);
		GameObject_SetActive_m3538205401(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_2 = __this->get_ThreeButtons_11();
		NullCheck(L_2);
		GameObject_SetActive_m3538205401(L_2, (bool)1, /*hidden argument*/NULL);
		goto IL_003b;
	}

IL_0023:
	{
		GameObject_t3674682005 * L_3 = __this->get_HitButton_10();
		NullCheck(L_3);
		GameObject_SetActive_m3538205401(L_3, (bool)1, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_4 = __this->get_ThreeButtons_11();
		NullCheck(L_4);
		GameObject_SetActive_m3538205401(L_4, (bool)0, /*hidden argument*/NULL);
	}

IL_003b:
	{
		return;
	}
}
// System.Void Slots::switchChoice()
extern "C"  void Slots_switchChoice_m274301731 (Slots_t79980053 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_choice_45();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		__this->set_choice_45((bool)0);
		goto IL_001e;
	}

IL_0017:
	{
		__this->set_choice_45((bool)1);
	}

IL_001e:
	{
		return;
	}
}
// System.Void Slots::freeze1()
extern "C"  void Slots_freeze1_m3741587822 (Slots_t79980053 * __this, const MethodInfo* method)
{
	{
		BooleanU5BU5D_t3456302923* L_0 = __this->get_frozenSlots_46();
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (bool)1);
		Slots_Unfreeze_m2568076239(__this, 0, /*hidden argument*/NULL);
		__this->set_tempFrozen_53(0);
		GameObject_t3674682005 * L_1 = __this->get_slot1_6();
		Slots_setColorFrame_m2389784440(__this, L_1, /*hidden argument*/NULL);
		Slots_switchChoice_m274301731(__this, /*hidden argument*/NULL);
		bool L_2 = __this->get_choice_45();
		Slots_switchHitButtons_m296069031(__this, L_2, /*hidden argument*/NULL);
		Slots_SetCounter_m3333443545(__this, 0, /*hidden argument*/NULL);
		CounterChecking_t39316438 * L_3 = __this->get_counterChecking_40();
		NullCheck(L_3);
		CounterChecking_CounterCheck_m2647319995(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Slots::freeze2()
extern "C"  void Slots_freeze2_m3741588783 (Slots_t79980053 * __this, const MethodInfo* method)
{
	{
		BooleanU5BU5D_t3456302923* L_0 = __this->get_frozenSlots_46();
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(1), (bool)1);
		Slots_Unfreeze_m2568076239(__this, 1, /*hidden argument*/NULL);
		__this->set_tempFrozen_53(1);
		GameObject_t3674682005 * L_1 = __this->get_slot2_7();
		Slots_setColorFrame_m2389784440(__this, L_1, /*hidden argument*/NULL);
		Slots_switchChoice_m274301731(__this, /*hidden argument*/NULL);
		bool L_2 = __this->get_choice_45();
		Slots_switchHitButtons_m296069031(__this, L_2, /*hidden argument*/NULL);
		Slots_SetCounter_m3333443545(__this, 1, /*hidden argument*/NULL);
		CounterChecking_t39316438 * L_3 = __this->get_counterChecking_40();
		NullCheck(L_3);
		CounterChecking_CounterCheck_m2647319995(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Slots::freeze3()
extern "C"  void Slots_freeze3_m3741589744 (Slots_t79980053 * __this, const MethodInfo* method)
{
	{
		BooleanU5BU5D_t3456302923* L_0 = __this->get_frozenSlots_46();
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 2);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(2), (bool)1);
		Slots_Unfreeze_m2568076239(__this, 2, /*hidden argument*/NULL);
		__this->set_tempFrozen_53(2);
		GameObject_t3674682005 * L_1 = __this->get_slot3_8();
		Slots_setColorFrame_m2389784440(__this, L_1, /*hidden argument*/NULL);
		Slots_switchChoice_m274301731(__this, /*hidden argument*/NULL);
		bool L_2 = __this->get_choice_45();
		Slots_switchHitButtons_m296069031(__this, L_2, /*hidden argument*/NULL);
		Slots_SetCounter_m3333443545(__this, 2, /*hidden argument*/NULL);
		CounterChecking_t39316438 * L_3 = __this->get_counterChecking_40();
		NullCheck(L_3);
		CounterChecking_CounterCheck_m2647319995(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Slots::Unfreeze(System.Int32)
extern "C"  void Slots_Unfreeze_m2568076239 (Slots_t79980053 * __this, int32_t ___frozen0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0021;
	}

IL_0007:
	{
		int32_t L_0 = V_0;
		int32_t L_1 = ___frozen0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0017;
		}
	}
	{
		BooleanU5BU5D_t3456302923* L_2 = __this->get_frozenSlots_46();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (bool)0);
	}

IL_0017:
	{
		Slots_resetSlotColor_m3253019940(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0021:
	{
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) < ((int32_t)3)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Slots::HardUnfreeze()
extern "C"  void Slots_HardUnfreeze_m3907598409 (Slots_t79980053 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0014;
	}

IL_0007:
	{
		BooleanU5BU5D_t3456302923* L_0 = __this->get_frozenSlots_46();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (bool)0);
		int32_t L_2 = V_0;
		V_0 = ((int32_t)((int32_t)L_2+(int32_t)1));
	}

IL_0014:
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) < ((int32_t)3)))
		{
			goto IL_0007;
		}
	}
	{
		__this->set_tempFrozen_53((-1));
		Slots_resetSlotColor_m3253019940(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Slots::resetTimer()
extern "C"  void Slots_resetTimer_m934139748 (Slots_t79980053 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_timer_42();
		__this->set_currentTimer_41(L_0);
		__this->set_rolling_44((bool)0);
		return;
	}
}
// System.Void Slots::resetVars()
extern "C"  void Slots_resetVars_m2296921103 (Slots_t79980053 * __this, const MethodInfo* method)
{
	{
		__this->set_a_50(0);
		__this->set_b_51(0);
		__this->set_c_52(0);
		return;
	}
}
// System.Void Slots::setFrozenRound()
extern "C"  void Slots_setFrozenRound_m421770554 (Slots_t79980053 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_frozenRound_47();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		__this->set_frozenRound_47((bool)0);
		Slots_HardUnfreeze_m3907598409(__this, /*hidden argument*/NULL);
		goto IL_0024;
	}

IL_001d:
	{
		__this->set_frozenRound_47((bool)1);
	}

IL_0024:
	{
		return;
	}
}
// System.Void Slots::SetPosInList()
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var;
extern const uint32_t Slots_SetPosInList_m3074373091_MetadataUsageId;
extern "C"  void Slots_SetPosInList_m3074373091 (Slots_t79980053 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Slots_SetPosInList_m3074373091_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Sprite_t3199167241 * V_0 = NULL;
	Sprite_t3199167241 * V_1 = NULL;
	Sprite_t3199167241 * V_2 = NULL;
	{
		SpriteU5BU5D_t2761310900* L_0 = __this->get_slots_48();
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		int32_t L_1 = 0;
		Sprite_t3199167241 * L_2 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1));
		V_0 = L_2;
		SpriteU5BU5D_t2761310900* L_3 = __this->get_slots_48();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		int32_t L_4 = 1;
		Sprite_t3199167241 * L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_1 = L_5;
		SpriteU5BU5D_t2761310900* L_6 = __this->get_slots_48();
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		int32_t L_7 = 2;
		Sprite_t3199167241 * L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_2 = L_8;
		GameObject_t3674682005 * L_9 = __this->get_slot1_6();
		NullCheck(L_9);
		SpriteRenderer_t2548470764 * L_10 = GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(L_9, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var);
		Sprite_t3199167241 * L_11 = V_0;
		NullCheck(L_10);
		SpriteRenderer_set_sprite_m1519408453(L_10, L_11, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_12 = __this->get_slot2_7();
		NullCheck(L_12);
		SpriteRenderer_t2548470764 * L_13 = GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(L_12, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var);
		Sprite_t3199167241 * L_14 = V_1;
		NullCheck(L_13);
		SpriteRenderer_set_sprite_m1519408453(L_13, L_14, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_15 = __this->get_slot3_8();
		NullCheck(L_15);
		SpriteRenderer_t2548470764 * L_16 = GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(L_15, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var);
		Sprite_t3199167241 * L_17 = V_2;
		NullCheck(L_16);
		SpriteRenderer_set_sprite_m1519408453(L_16, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Slots::HitSlots()
extern Il2CppClass* Slots_t79980053_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3506301;
extern Il2CppCodeGenString* _stringLiteral2555906;
extern Il2CppCodeGenString* _stringLiteral71539;
extern const uint32_t Slots_HitSlots_m1900413680_MetadataUsageId;
extern "C"  void Slots_HitSlots_m1900413680 (Slots_t79980053 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Slots_HitSlots_m1900413680_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Slots_resetSlotColor_m3253019940(__this, /*hidden argument*/NULL);
		int32_t L_0 = __this->get_credits_55();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_00cc;
		}
	}
	{
		bool L_1 = __this->get_rolling_44();
		if (L_1)
		{
			goto IL_00b1;
		}
	}
	{
		AudioSource_t1740077639 * L_2 = __this->get_audio_4();
		NullCheck(L_2);
		AudioSource_set_pitch_m1518407234(L_2, (1.0f), /*hidden argument*/NULL);
		AudioSource_t1740077639 * L_3 = __this->get_audio_4();
		float L_4 = Random_Range_m3362417303(NULL /*static, unused*/, (1.2f), (1.3f), /*hidden argument*/NULL);
		NullCheck(L_3);
		AudioSource_set_pitch_m1518407234(L_3, L_4, /*hidden argument*/NULL);
		AudioSource_t1740077639 * L_5 = __this->get_audio_4();
		AudioClip_t794140988 * L_6 = __this->get_hit_32();
		NullCheck(L_5);
		AudioSource_PlayOneShot_m1217449713(L_5, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Slots_t79980053_il2cpp_TypeInfo_var);
		int32_t L_7 = ((Slots_t79980053_StaticFields*)Slots_t79980053_il2cpp_TypeInfo_var->static_fields)->get_numLock_57();
		int32_t L_8 = Random_Range_m75452833(NULL /*static, unused*/, 0, L_7, /*hidden argument*/NULL);
		__this->set_a_50(L_8);
		int32_t L_9 = ((Slots_t79980053_StaticFields*)Slots_t79980053_il2cpp_TypeInfo_var->static_fields)->get_numLock_57();
		int32_t L_10 = Random_Range_m75452833(NULL /*static, unused*/, 0, L_9, /*hidden argument*/NULL);
		__this->set_b_51(L_10);
		int32_t L_11 = ((Slots_t79980053_StaticFields*)Slots_t79980053_il2cpp_TypeInfo_var->static_fields)->get_numLock_57();
		int32_t L_12 = Random_Range_m75452833(NULL /*static, unused*/, 0, L_11, /*hidden argument*/NULL);
		__this->set_c_52(L_12);
		MonoBehaviour_StartCoroutine_m2272783641(__this, _stringLiteral3506301, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_13 = __this->get_HitStopText_19();
		NullCheck(L_13);
		Text_t9039225 * L_14 = GameObject_GetComponent_TisText_t9039225_m202917489(L_13, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var);
		NullCheck(L_14);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_14, _stringLiteral2555906);
		goto IL_00cc;
	}

IL_00b1:
	{
		Slots_setCurrentTimer0_m3306525328(__this, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_15 = __this->get_HitStopText_19();
		NullCheck(L_15);
		Text_t9039225 * L_16 = GameObject_GetComponent_TisText_t9039225_m202917489(L_15, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var);
		NullCheck(L_16);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_16, _stringLiteral71539);
	}

IL_00cc:
	{
		return;
	}
}
// System.Void Slots::setCurrentTimer0()
extern "C"  void Slots_setCurrentTimer0_m3306525328 (Slots_t79980053 * __this, const MethodInfo* method)
{
	{
		__this->set_currentTimer_41((-1.0f));
		return;
	}
}
// System.Collections.IEnumerator Slots::roll()
extern Il2CppClass* U3CrollU3Ec__Iterator3_t2129678497_il2cpp_TypeInfo_var;
extern const uint32_t Slots_roll_m4257639267_MetadataUsageId;
extern "C"  Il2CppObject * Slots_roll_m4257639267 (Slots_t79980053 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Slots_roll_m4257639267_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CrollU3Ec__Iterator3_t2129678497 * V_0 = NULL;
	{
		U3CrollU3Ec__Iterator3_t2129678497 * L_0 = (U3CrollU3Ec__Iterator3_t2129678497 *)il2cpp_codegen_object_new(U3CrollU3Ec__Iterator3_t2129678497_il2cpp_TypeInfo_var);
		U3CrollU3Ec__Iterator3__ctor_m1708876570(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CrollU3Ec__Iterator3_t2129678497 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CrollU3Ec__Iterator3_t2129678497 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Slots::IncreaseAmount(System.Int32)
extern const MethodInfo* GameObject_GetComponent_TisInventario_t3778973585_m555484204_MethodInfo_var;
extern const uint32_t Slots_IncreaseAmount_m1773142873_MetadataUsageId;
extern "C"  void Slots_IncreaseAmount_m1773142873 (Slots_t79980053 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Slots_IncreaseAmount_m1773142873_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_inventario_12();
		NullCheck(L_0);
		Inventario_t3778973585 * L_1 = GameObject_GetComponent_TisInventario_t3778973585_m555484204(L_0, /*hidden argument*/GameObject_GetComponent_TisInventario_t3778973585_m555484204_MethodInfo_var);
		int32_t L_2 = ___index0;
		NullCheck(L_1);
		Inventario_IncreaseAmount_m734132901(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Slots::CheckCombination()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t Slots_CheckCombination_m4066843381_MetadataUsageId;
extern "C"  void Slots_CheckCombination_m4066843381 (Slots_t79980053 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Slots_CheckCombination_m4066843381_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		SpriteU5BU5D_t2761310900* L_0 = __this->get_slots_48();
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		int32_t L_1 = 0;
		Sprite_t3199167241 * L_2 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0373;
		}
	}
	{
		SpriteU5BU5D_t2761310900* L_4 = __this->get_slots_48();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		int32_t L_5 = 1;
		Sprite_t3199167241 * L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0373;
		}
	}
	{
		SpriteU5BU5D_t2761310900* L_8 = __this->get_slots_48();
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		int32_t L_9 = 2;
		Sprite_t3199167241 * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_10, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0373;
		}
	}
	{
		V_0 = 0;
		goto IL_036c;
	}

IL_0040:
	{
		SpriteU5BU5D_t2761310900* L_12 = __this->get_slots_48();
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 0);
		int32_t L_13 = 0;
		Sprite_t3199167241 * L_14 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		SpriteU5BU5D_t2761310900* L_15 = __this->get_slots_48();
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 1);
		int32_t L_16 = 1;
		Sprite_t3199167241 * L_17 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_18 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0102;
		}
	}
	{
		SpriteU5BU5D_t2761310900* L_19 = __this->get_slots_48();
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 1);
		int32_t L_20 = 1;
		Sprite_t3199167241 * L_21 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		SpriteU5BU5D_t2761310900* L_22 = __this->get_slots_48();
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 2);
		int32_t L_23 = 2;
		Sprite_t3199167241 * L_24 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_25 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_21, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_0102;
		}
	}
	{
		Slots_IncreasePrice_m1585369164(__this, 3, /*hidden argument*/NULL);
		Slots_IncreaseNumLock_m4178141635(__this, /*hidden argument*/NULL);
		Slots_IncreaseCredit_m538988649(__this, /*hidden argument*/NULL);
		int32_t L_26 = __this->get_a_50();
		Slots_IncreaseAmount_m1773142873(__this, L_26, /*hidden argument*/NULL);
		Slots_BackToInventory_m114061198(__this, /*hidden argument*/NULL);
		AudioSource_t1740077639 * L_27 = __this->get_audio_4();
		AudioClip_t794140988 * L_28 = __this->get_winBig_29();
		NullCheck(L_27);
		AudioSource_PlayOneShot_m1217449713(L_27, L_28, /*hidden argument*/NULL);
		Slots_resetTimer_m934139748(__this, /*hidden argument*/NULL);
		Slots_HardUnfreeze_m3907598409(__this, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_29 = __this->get_slot1_6();
		float L_30 = __this->get_timeHightlight_43();
		Il2CppObject * L_31 = Slots_HightlightSlot_m2437507397(__this, L_29, L_30, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_31, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_32 = __this->get_slot2_7();
		float L_33 = __this->get_timeHightlight_43();
		Il2CppObject * L_34 = Slots_HightlightSlot_m2437507397(__this, L_32, L_33, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_34, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_35 = __this->get_slot3_8();
		float L_36 = __this->get_timeHightlight_43();
		Il2CppObject * L_37 = Slots_HightlightSlot_m2437507397(__this, L_35, L_36, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_37, /*hidden argument*/NULL);
		return;
	}

IL_0102:
	{
		SpriteU5BU5D_t2761310900* L_38 = __this->get_slots_48();
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		int32_t L_39 = 0;
		Sprite_t3199167241 * L_40 = (L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		SpriteU5BU5D_t2761310900* L_41 = __this->get_slots_48();
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 1);
		int32_t L_42 = 1;
		Sprite_t3199167241 * L_43 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_44 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_40, L_43, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_01ae;
		}
	}
	{
		Slots_IncreasePrice_m1585369164(__this, 2, /*hidden argument*/NULL);
		Slots_IncreaseCredit_m538988649(__this, /*hidden argument*/NULL);
		AudioSource_t1740077639 * L_45 = __this->get_audio_4();
		AudioClip_t794140988 * L_46 = __this->get_win_28();
		NullCheck(L_45);
		AudioSource_PlayOneShot_m1217449713(L_45, L_46, /*hidden argument*/NULL);
		AudioSource_t1740077639 * L_47 = __this->get_audio_4();
		AudioClip_t794140988 * L_48 = __this->get_win_28();
		NullCheck(L_47);
		AudioSource_PlayOneShot_m1217449713(L_47, L_48, /*hidden argument*/NULL);
		int32_t L_49 = __this->get_a_50();
		Slots_getCaption_m1409653679(__this, L_49, /*hidden argument*/NULL);
		Slots_resetTimer_m934139748(__this, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_50 = __this->get_slot1_6();
		float L_51 = __this->get_timeHightlight_43();
		Il2CppObject * L_52 = Slots_HightlightSlot_m2437507397(__this, L_50, L_51, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_52, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_53 = __this->get_slot2_7();
		float L_54 = __this->get_timeHightlight_43();
		Il2CppObject * L_55 = Slots_HightlightSlot_m2437507397(__this, L_53, L_54, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_55, /*hidden argument*/NULL);
		Slots_UpdateInventory_m1167445543(__this, /*hidden argument*/NULL);
		Slots_switchChoice_m274301731(__this, /*hidden argument*/NULL);
		bool L_56 = __this->get_choice_45();
		Slots_switchHitButtons_m296069031(__this, L_56, /*hidden argument*/NULL);
		Slots_resetSlotColor_m3253019940(__this, /*hidden argument*/NULL);
		return;
	}

IL_01ae:
	{
		SpriteU5BU5D_t2761310900* L_57 = __this->get_slots_48();
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, 1);
		int32_t L_58 = 1;
		Sprite_t3199167241 * L_59 = (L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_58));
		SpriteU5BU5D_t2761310900* L_60 = __this->get_slots_48();
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, 2);
		int32_t L_61 = 2;
		Sprite_t3199167241 * L_62 = (L_60)->GetAt(static_cast<il2cpp_array_size_t>(L_61));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_63 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_59, L_62, /*hidden argument*/NULL);
		if (!L_63)
		{
			goto IL_025a;
		}
	}
	{
		Slots_IncreasePrice_m1585369164(__this, 2, /*hidden argument*/NULL);
		Slots_IncreaseCredit_m538988649(__this, /*hidden argument*/NULL);
		AudioSource_t1740077639 * L_64 = __this->get_audio_4();
		AudioClip_t794140988 * L_65 = __this->get_win_28();
		NullCheck(L_64);
		AudioSource_PlayOneShot_m1217449713(L_64, L_65, /*hidden argument*/NULL);
		AudioSource_t1740077639 * L_66 = __this->get_audio_4();
		AudioClip_t794140988 * L_67 = __this->get_win_28();
		NullCheck(L_66);
		AudioSource_PlayOneShot_m1217449713(L_66, L_67, /*hidden argument*/NULL);
		int32_t L_68 = __this->get_b_51();
		Slots_getCaption_m1409653679(__this, L_68, /*hidden argument*/NULL);
		Slots_resetTimer_m934139748(__this, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_69 = __this->get_slot2_7();
		float L_70 = __this->get_timeHightlight_43();
		Il2CppObject * L_71 = Slots_HightlightSlot_m2437507397(__this, L_69, L_70, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_71, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_72 = __this->get_slot3_8();
		float L_73 = __this->get_timeHightlight_43();
		Il2CppObject * L_74 = Slots_HightlightSlot_m2437507397(__this, L_72, L_73, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_74, /*hidden argument*/NULL);
		Slots_UpdateInventory_m1167445543(__this, /*hidden argument*/NULL);
		Slots_switchChoice_m274301731(__this, /*hidden argument*/NULL);
		bool L_75 = __this->get_choice_45();
		Slots_switchHitButtons_m296069031(__this, L_75, /*hidden argument*/NULL);
		Slots_resetSlotColor_m3253019940(__this, /*hidden argument*/NULL);
		return;
	}

IL_025a:
	{
		SpriteU5BU5D_t2761310900* L_76 = __this->get_slots_48();
		NullCheck(L_76);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_76, 0);
		int32_t L_77 = 0;
		Sprite_t3199167241 * L_78 = (L_76)->GetAt(static_cast<il2cpp_array_size_t>(L_77));
		SpriteU5BU5D_t2761310900* L_79 = __this->get_slots_48();
		NullCheck(L_79);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_79, 2);
		int32_t L_80 = 2;
		Sprite_t3199167241 * L_81 = (L_79)->GetAt(static_cast<il2cpp_array_size_t>(L_80));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_82 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_78, L_81, /*hidden argument*/NULL);
		if (!L_82)
		{
			goto IL_0306;
		}
	}
	{
		Slots_IncreasePrice_m1585369164(__this, 2, /*hidden argument*/NULL);
		Slots_IncreaseCredit_m538988649(__this, /*hidden argument*/NULL);
		AudioSource_t1740077639 * L_83 = __this->get_audio_4();
		AudioClip_t794140988 * L_84 = __this->get_win_28();
		NullCheck(L_83);
		AudioSource_PlayOneShot_m1217449713(L_83, L_84, /*hidden argument*/NULL);
		AudioSource_t1740077639 * L_85 = __this->get_audio_4();
		AudioClip_t794140988 * L_86 = __this->get_win_28();
		NullCheck(L_85);
		AudioSource_PlayOneShot_m1217449713(L_85, L_86, /*hidden argument*/NULL);
		int32_t L_87 = __this->get_a_50();
		Slots_getCaption_m1409653679(__this, L_87, /*hidden argument*/NULL);
		Slots_resetTimer_m934139748(__this, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_88 = __this->get_slot1_6();
		float L_89 = __this->get_timeHightlight_43();
		Il2CppObject * L_90 = Slots_HightlightSlot_m2437507397(__this, L_88, L_89, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_90, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_91 = __this->get_slot3_8();
		float L_92 = __this->get_timeHightlight_43();
		Il2CppObject * L_93 = Slots_HightlightSlot_m2437507397(__this, L_91, L_92, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_93, /*hidden argument*/NULL);
		Slots_UpdateInventory_m1167445543(__this, /*hidden argument*/NULL);
		Slots_switchChoice_m274301731(__this, /*hidden argument*/NULL);
		bool L_94 = __this->get_choice_45();
		Slots_switchHitButtons_m296069031(__this, L_94, /*hidden argument*/NULL);
		Slots_resetSlotColor_m3253019940(__this, /*hidden argument*/NULL);
		return;
	}

IL_0306:
	{
		SpriteU5BU5D_t2761310900* L_95 = __this->get_slots_48();
		NullCheck(L_95);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_95, 0);
		int32_t L_96 = 0;
		Sprite_t3199167241 * L_97 = (L_95)->GetAt(static_cast<il2cpp_array_size_t>(L_96));
		SpriteU5BU5D_t2761310900* L_98 = __this->get_slots_48();
		NullCheck(L_98);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_98, 1);
		int32_t L_99 = 1;
		Sprite_t3199167241 * L_100 = (L_98)->GetAt(static_cast<il2cpp_array_size_t>(L_99));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_101 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_97, L_100, /*hidden argument*/NULL);
		if (!L_101)
		{
			goto IL_0368;
		}
	}
	{
		SpriteU5BU5D_t2761310900* L_102 = __this->get_slots_48();
		NullCheck(L_102);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_102, 0);
		int32_t L_103 = 0;
		Sprite_t3199167241 * L_104 = (L_102)->GetAt(static_cast<il2cpp_array_size_t>(L_103));
		SpriteU5BU5D_t2761310900* L_105 = __this->get_slots_48();
		NullCheck(L_105);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_105, 2);
		int32_t L_106 = 2;
		Sprite_t3199167241 * L_107 = (L_105)->GetAt(static_cast<il2cpp_array_size_t>(L_106));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_108 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_104, L_107, /*hidden argument*/NULL);
		if (!L_108)
		{
			goto IL_0368;
		}
	}
	{
		SpriteU5BU5D_t2761310900* L_109 = __this->get_slots_48();
		NullCheck(L_109);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_109, 1);
		int32_t L_110 = 1;
		Sprite_t3199167241 * L_111 = (L_109)->GetAt(static_cast<il2cpp_array_size_t>(L_110));
		SpriteU5BU5D_t2761310900* L_112 = __this->get_slots_48();
		NullCheck(L_112);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_112, 2);
		int32_t L_113 = 2;
		Sprite_t3199167241 * L_114 = (L_112)->GetAt(static_cast<il2cpp_array_size_t>(L_113));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_115 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_111, L_114, /*hidden argument*/NULL);
		if (!L_115)
		{
			goto IL_0368;
		}
	}
	{
		Slots_HardUnfreeze_m3907598409(__this, /*hidden argument*/NULL);
		Slots_resetSlotColor_m3253019940(__this, /*hidden argument*/NULL);
		Slots_getCaption_m1409653679(__this, ((int32_t)9), /*hidden argument*/NULL);
	}

IL_0368:
	{
		int32_t L_116 = V_0;
		V_0 = ((int32_t)((int32_t)L_116+(int32_t)1));
	}

IL_036c:
	{
		int32_t L_117 = V_0;
		if ((((int32_t)L_117) < ((int32_t)3)))
		{
			goto IL_0040;
		}
	}

IL_0373:
	{
		return;
	}
}
// System.Void Slots::getCaption(System.Int32)
extern "C"  void Slots_getCaption_m1409653679 (Slots_t79980053 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		Text_t9039225 * L_0 = __this->get_caption_36();
		Combinaciones_t3637091598 * L_1 = __this->get_combinaciones_39();
		int32_t L_2 = ___i0;
		NullCheck(L_1);
		String_t* L_3 = Combinaciones_getText_m3595448498(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_3);
		return;
	}
}
// System.Void Slots::SetCounter(System.Int32)
extern const MethodInfo* GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var;
extern const uint32_t Slots_SetCounter_m3333443545_MetadataUsageId;
extern "C"  void Slots_SetCounter_m3333443545 (Slots_t79980053 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Slots_SetCounter_m3333443545_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___i0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0019;
		}
		if (L_1 == 1)
		{
			goto IL_0040;
		}
		if (L_1 == 2)
		{
			goto IL_0067;
		}
	}
	{
		goto IL_008e;
	}

IL_0019:
	{
		GameObjectU5BU5D_t2662109048* L_2 = __this->get_counters_26();
		int32_t L_3 = ___i0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		GameObject_t3674682005 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		Image_t538875265 * L_6 = GameObject_GetComponent_TisImage_t538875265_m2140199269(L_5, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var);
		GameObject_t3674682005 * L_7 = __this->get_slot1_6();
		NullCheck(L_7);
		SpriteRenderer_t2548470764 * L_8 = GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(L_7, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var);
		NullCheck(L_8);
		Sprite_t3199167241 * L_9 = SpriteRenderer_get_sprite_m3481747968(L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		Image_set_sprite_m572551402(L_6, L_9, /*hidden argument*/NULL);
		goto IL_008e;
	}

IL_0040:
	{
		GameObjectU5BU5D_t2662109048* L_10 = __this->get_counters_26();
		int32_t L_11 = ___i0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		GameObject_t3674682005 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_13);
		Image_t538875265 * L_14 = GameObject_GetComponent_TisImage_t538875265_m2140199269(L_13, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var);
		GameObject_t3674682005 * L_15 = __this->get_slot2_7();
		NullCheck(L_15);
		SpriteRenderer_t2548470764 * L_16 = GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(L_15, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var);
		NullCheck(L_16);
		Sprite_t3199167241 * L_17 = SpriteRenderer_get_sprite_m3481747968(L_16, /*hidden argument*/NULL);
		NullCheck(L_14);
		Image_set_sprite_m572551402(L_14, L_17, /*hidden argument*/NULL);
		goto IL_008e;
	}

IL_0067:
	{
		GameObjectU5BU5D_t2662109048* L_18 = __this->get_counters_26();
		int32_t L_19 = ___i0;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		int32_t L_20 = L_19;
		GameObject_t3674682005 * L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_21);
		Image_t538875265 * L_22 = GameObject_GetComponent_TisImage_t538875265_m2140199269(L_21, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var);
		GameObject_t3674682005 * L_23 = __this->get_slot3_8();
		NullCheck(L_23);
		SpriteRenderer_t2548470764 * L_24 = GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(L_23, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var);
		NullCheck(L_24);
		Sprite_t3199167241 * L_25 = SpriteRenderer_get_sprite_m3481747968(L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		Image_set_sprite_m572551402(L_22, L_25, /*hidden argument*/NULL);
		goto IL_008e;
	}

IL_008e:
	{
		return;
	}
}
// System.Void Slots::getPicture(System.Int32)
extern "C"  void Slots_getPicture_m120001735 (Slots_t79980053 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		Sprite_t3199167241 * L_0 = __this->get_picCanvas_38();
		int32_t L_1 = ___i0;
		Il2CppObject * L_2 = Slots_ChangePic_m1339616125(__this, L_0, L_1, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Slots::ChangePic(UnityEngine.Sprite,System.Int32)
extern Il2CppClass* U3CChangePicU3Ec__Iterator4_t1863457897_il2cpp_TypeInfo_var;
extern const uint32_t Slots_ChangePic_m1339616125_MetadataUsageId;
extern "C"  Il2CppObject * Slots_ChangePic_m1339616125 (Slots_t79980053 * __this, Sprite_t3199167241 * ___pic0, int32_t ___i1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Slots_ChangePic_m1339616125_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CChangePicU3Ec__Iterator4_t1863457897 * V_0 = NULL;
	{
		U3CChangePicU3Ec__Iterator4_t1863457897 * L_0 = (U3CChangePicU3Ec__Iterator4_t1863457897 *)il2cpp_codegen_object_new(U3CChangePicU3Ec__Iterator4_t1863457897_il2cpp_TypeInfo_var);
		U3CChangePicU3Ec__Iterator4__ctor_m1973967650(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CChangePicU3Ec__Iterator4_t1863457897 * L_1 = V_0;
		int32_t L_2 = ___i1;
		NullCheck(L_1);
		L_1->set_i_1(L_2);
		U3CChangePicU3Ec__Iterator4_t1863457897 * L_3 = V_0;
		int32_t L_4 = ___i1;
		NullCheck(L_3);
		L_3->set_U3CU24U3Ei_4(L_4);
		U3CChangePicU3Ec__Iterator4_t1863457897 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_U3CU3Ef__this_5(__this);
		U3CChangePicU3Ec__Iterator4_t1863457897 * L_6 = V_0;
		return L_6;
	}
}
// System.Void Slots::IncreaseNumLock()
extern Il2CppClass* Slots_t79980053_il2cpp_TypeInfo_var;
extern const uint32_t Slots_IncreaseNumLock_m4178141635_MetadataUsageId;
extern "C"  void Slots_IncreaseNumLock_m4178141635 (Slots_t79980053 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Slots_IncreaseNumLock_m4178141635_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Slots_t79980053_il2cpp_TypeInfo_var);
		int32_t L_0 = ((Slots_t79980053_StaticFields*)Slots_t79980053_il2cpp_TypeInfo_var->static_fields)->get_numLock_57();
		((Slots_t79980053_StaticFields*)Slots_t79980053_il2cpp_TypeInfo_var->static_fields)->set_numLock_57(((int32_t)((int32_t)L_0+(int32_t)1)));
		return;
	}
}
// System.Void Slots::IncreaseCredit()
extern "C"  void Slots_IncreaseCredit_m538988649 (Slots_t79980053 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_credits_55();
		__this->set_credits_55(((int32_t)((int32_t)L_0+(int32_t)1)));
		Slots_UpdateCreditHUDWithBlink_m2885438479(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Slots::IncreasePrice(System.Int32)
extern "C"  void Slots_IncreasePrice_m1585369164 (Slots_t79980053 * __this, int32_t ___p0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_price_54();
		int32_t L_1 = ___p0;
		__this->set_price_54(((int32_t)((int32_t)L_0+(int32_t)L_1)));
		Slots_UpdatePriceHUDWithBlink_m232769203(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Slots::ConvertToCredits()
extern "C"  void Slots_ConvertToCredits_m2564345402 (Slots_t79980053 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_price_54();
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0040;
		}
	}
	{
		int32_t L_1 = __this->get_price_54();
		__this->set_price_54(((int32_t)((int32_t)L_1-(int32_t)2)));
		int32_t L_2 = __this->get_credits_55();
		__this->set_credits_55(((int32_t)((int32_t)L_2+(int32_t)1)));
		Slots_UpdatePriceHUD_m2862440549(__this, /*hidden argument*/NULL);
		Slots_UpdatePriceHUDWithBlink_m232769203(__this, /*hidden argument*/NULL);
		Slots_UpdateCreditHUD_m1312826249(__this, /*hidden argument*/NULL);
		Slots_UpdateCreditHUDWithBlink_m2885438479(__this, /*hidden argument*/NULL);
	}

IL_0040:
	{
		AudioSource_t1740077639 * L_3 = __this->get_audio_4();
		AudioClip_t794140988 * L_4 = __this->get_coin_33();
		NullCheck(L_3);
		AudioSource_PlayOneShot_m1217449713(L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Slots::UpdatePriceHUD()
extern "C"  void Slots_UpdatePriceHUD_m2862440549 (Slots_t79980053 * __this, const MethodInfo* method)
{
	{
		PriceHUD_t3182648078 * L_0 = __this->get_priceHUD_2();
		NullCheck(L_0);
		PriceHUD_UpdateText_m3474693101(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Slots::UpdatePriceHUDWithBlink()
extern "C"  void Slots_UpdatePriceHUDWithBlink_m232769203 (Slots_t79980053 * __this, const MethodInfo* method)
{
	{
		PriceHUD_t3182648078 * L_0 = __this->get_priceHUD_2();
		NullCheck(L_0);
		PriceHUD_UpdateTextWithBlink_m3409445931(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Slots::UpdateCreditHUD()
extern "C"  void Slots_UpdateCreditHUD_m1312826249 (Slots_t79980053 * __this, const MethodInfo* method)
{
	{
		CreditsHUD_t1430045213 * L_0 = __this->get_creditsHUD_3();
		NullCheck(L_0);
		CreditsHUD_UpdateText_m2045600444(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Slots::UpdateCreditHUDWithBlink()
extern "C"  void Slots_UpdateCreditHUDWithBlink_m2885438479 (Slots_t79980053 * __this, const MethodInfo* method)
{
	{
		CreditsHUD_t1430045213 * L_0 = __this->get_creditsHUD_3();
		NullCheck(L_0);
		CreditsHUD_UpdateTextWithBlink_m275215932(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Slots::UpdateInventoryWithBlink()
extern const MethodInfo* GameObject_GetComponent_TisInventario_t3778973585_m555484204_MethodInfo_var;
extern const uint32_t Slots_UpdateInventoryWithBlink_m2931405617_MetadataUsageId;
extern "C"  void Slots_UpdateInventoryWithBlink_m2931405617 (Slots_t79980053 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Slots_UpdateInventoryWithBlink_m2931405617_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_inventario_12();
		NullCheck(L_0);
		Inventario_t3778973585 * L_1 = GameObject_GetComponent_TisInventario_t3778973585_m555484204(L_0, /*hidden argument*/GameObject_GetComponent_TisInventario_t3778973585_m555484204_MethodInfo_var);
		NullCheck(L_1);
		Inventario_UpdateInventoryWithBlink_m2469989757(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Slots::UpdateInventory()
extern const MethodInfo* GameObject_GetComponent_TisInventario_t3778973585_m555484204_MethodInfo_var;
extern const uint32_t Slots_UpdateInventory_m1167445543_MetadataUsageId;
extern "C"  void Slots_UpdateInventory_m1167445543 (Slots_t79980053 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Slots_UpdateInventory_m1167445543_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_inventario_12();
		NullCheck(L_0);
		Inventario_t3778973585 * L_1 = GameObject_GetComponent_TisInventario_t3778973585_m555484204(L_0, /*hidden argument*/GameObject_GetComponent_TisInventario_t3778973585_m555484204_MethodInfo_var);
		NullCheck(L_1);
		Inventario_UpdateInventario_m623790324(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Slots::UpdateAll()
extern "C"  void Slots_UpdateAll_m4067659948 (Slots_t79980053 * __this, const MethodInfo* method)
{
	{
		Slots_UpdatePriceHUD_m2862440549(__this, /*hidden argument*/NULL);
		Slots_UpdateCreditHUD_m1312826249(__this, /*hidden argument*/NULL);
		Slots_UpdateInventory_m1167445543(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Slots::BringMan()
extern Il2CppClass* U3CBringManU3Ec__Iterator5_t2482726264_il2cpp_TypeInfo_var;
extern const uint32_t Slots_BringMan_m585588782_MetadataUsageId;
extern "C"  Il2CppObject * Slots_BringMan_m585588782 (Slots_t79980053 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Slots_BringMan_m585588782_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CBringManU3Ec__Iterator5_t2482726264 * V_0 = NULL;
	{
		U3CBringManU3Ec__Iterator5_t2482726264 * L_0 = (U3CBringManU3Ec__Iterator5_t2482726264 *)il2cpp_codegen_object_new(U3CBringManU3Ec__Iterator5_t2482726264_il2cpp_TypeInfo_var);
		U3CBringManU3Ec__Iterator5__ctor_m1175716899(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CBringManU3Ec__Iterator5_t2482726264 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_7(__this);
		U3CBringManU3Ec__Iterator5_t2482726264 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator Slots::BringHUD()
extern Il2CppClass* U3CBringHUDU3Ec__Iterator6_t3883270396_il2cpp_TypeInfo_var;
extern const uint32_t Slots_BringHUD_m580573323_MetadataUsageId;
extern "C"  Il2CppObject * Slots_BringHUD_m580573323 (Slots_t79980053 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Slots_BringHUD_m580573323_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CBringHUDU3Ec__Iterator6_t3883270396 * V_0 = NULL;
	{
		U3CBringHUDU3Ec__Iterator6_t3883270396 * L_0 = (U3CBringHUDU3Ec__Iterator6_t3883270396 *)il2cpp_codegen_object_new(U3CBringHUDU3Ec__Iterator6_t3883270396_il2cpp_TypeInfo_var);
		U3CBringHUDU3Ec__Iterator6__ctor_m2838895903(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CBringHUDU3Ec__Iterator6_t3883270396 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_7(__this);
		U3CBringHUDU3Ec__Iterator6_t3883270396 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator Slots::BringBackHUD()
extern Il2CppClass* U3CBringBackHUDU3Ec__Iterator7_t111350788_il2cpp_TypeInfo_var;
extern const uint32_t Slots_BringBackHUD_m1914403428_MetadataUsageId;
extern "C"  Il2CppObject * Slots_BringBackHUD_m1914403428 (Slots_t79980053 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Slots_BringBackHUD_m1914403428_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CBringBackHUDU3Ec__Iterator7_t111350788 * V_0 = NULL;
	{
		U3CBringBackHUDU3Ec__Iterator7_t111350788 * L_0 = (U3CBringBackHUDU3Ec__Iterator7_t111350788 *)il2cpp_codegen_object_new(U3CBringBackHUDU3Ec__Iterator7_t111350788_il2cpp_TypeInfo_var);
		U3CBringBackHUDU3Ec__Iterator7__ctor_m2315200279(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CBringBackHUDU3Ec__Iterator7_t111350788 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_7(__this);
		U3CBringBackHUDU3Ec__Iterator7_t111350788 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator Slots::BringMap()
extern Il2CppClass* U3CBringMapU3Ec__Iterator8_t1471609017_il2cpp_TypeInfo_var;
extern const uint32_t Slots_BringMap_m585590704_MetadataUsageId;
extern "C"  Il2CppObject * Slots_BringMap_m585590704 (Slots_t79980053 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Slots_BringMap_m585590704_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CBringMapU3Ec__Iterator8_t1471609017 * V_0 = NULL;
	{
		U3CBringMapU3Ec__Iterator8_t1471609017 * L_0 = (U3CBringMapU3Ec__Iterator8_t1471609017 *)il2cpp_codegen_object_new(U3CBringMapU3Ec__Iterator8_t1471609017_il2cpp_TypeInfo_var);
		U3CBringMapU3Ec__Iterator8__ctor_m3075705346(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CBringMapU3Ec__Iterator8_t1471609017 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_7(__this);
		U3CBringMapU3Ec__Iterator8_t1471609017 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator Slots::BringInventory()
extern Il2CppClass* U3CBringInventoryU3Ec__Iterator9_t2737813690_il2cpp_TypeInfo_var;
extern const uint32_t Slots_BringInventory_m1988301168_MetadataUsageId;
extern "C"  Il2CppObject * Slots_BringInventory_m1988301168 (Slots_t79980053 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Slots_BringInventory_m1988301168_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CBringInventoryU3Ec__Iterator9_t2737813690 * V_0 = NULL;
	{
		U3CBringInventoryU3Ec__Iterator9_t2737813690 * L_0 = (U3CBringInventoryU3Ec__Iterator9_t2737813690 *)il2cpp_codegen_object_new(U3CBringInventoryU3Ec__Iterator9_t2737813690_il2cpp_TypeInfo_var);
		U3CBringInventoryU3Ec__Iterator9__ctor_m4202580257(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CBringInventoryU3Ec__Iterator9_t2737813690 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_7(__this);
		U3CBringInventoryU3Ec__Iterator9_t2737813690 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator Slots::HightlightSlot(UnityEngine.GameObject,System.Single)
extern Il2CppClass* U3CHightlightSlotU3Ec__IteratorA_t3561306698_il2cpp_TypeInfo_var;
extern const uint32_t Slots_HightlightSlot_m2437507397_MetadataUsageId;
extern "C"  Il2CppObject * Slots_HightlightSlot_m2437507397 (Slots_t79980053 * __this, GameObject_t3674682005 * ___slot0, float ___t1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Slots_HightlightSlot_m2437507397_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CHightlightSlotU3Ec__IteratorA_t3561306698 * V_0 = NULL;
	{
		U3CHightlightSlotU3Ec__IteratorA_t3561306698 * L_0 = (U3CHightlightSlotU3Ec__IteratorA_t3561306698 *)il2cpp_codegen_object_new(U3CHightlightSlotU3Ec__IteratorA_t3561306698_il2cpp_TypeInfo_var);
		U3CHightlightSlotU3Ec__IteratorA__ctor_m2623113617(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CHightlightSlotU3Ec__IteratorA_t3561306698 * L_1 = V_0;
		float L_2 = ___t1;
		NullCheck(L_1);
		L_1->set_t_0(L_2);
		U3CHightlightSlotU3Ec__IteratorA_t3561306698 * L_3 = V_0;
		float L_4 = ___t1;
		NullCheck(L_3);
		L_3->set_U3CU24U3Et_3(L_4);
		U3CHightlightSlotU3Ec__IteratorA_t3561306698 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_U3CU3Ef__this_4(__this);
		U3CHightlightSlotU3Ec__IteratorA_t3561306698 * L_6 = V_0;
		return L_6;
	}
}
// System.Void Slots::setColorFrame(UnityEngine.GameObject)
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var;
extern const uint32_t Slots_setColorFrame_m2389784440_MetadataUsageId;
extern "C"  void Slots_setColorFrame_m2389784440 (Slots_t79980053 * __this, GameObject_t3674682005 * ___slot0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Slots_setColorFrame_m2389784440_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = ___slot0;
		NullCheck(L_0);
		SpriteRenderer_t2548470764 * L_1 = GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(L_0, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var);
		Color_t4194546905  L_2 = Color_get_grey_m3805481615(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		SpriteRenderer_set_color_m2701854973(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Slots::SlotsRackSwitch()
extern "C"  void Slots_SlotsRackSwitch_m3493919060 (Slots_t79980053 * __this, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = __this->get_SlotpParent_5();
		NullCheck(L_0);
		bool L_1 = GameObject_get_activeSelf_m3858025161(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		GameObject_t3674682005 * L_2 = __this->get_SlotpParent_5();
		NullCheck(L_2);
		GameObject_SetActive_m3538205401(L_2, (bool)0, /*hidden argument*/NULL);
		goto IL_002d;
	}

IL_0021:
	{
		GameObject_t3674682005 * L_3 = __this->get_SlotpParent_5();
		NullCheck(L_3);
		GameObject_SetActive_m3538205401(L_3, (bool)1, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void Slots/<BringBackHUD>c__Iterator7::.ctor()
extern "C"  void U3CBringBackHUDU3Ec__Iterator7__ctor_m2315200279 (U3CBringBackHUDU3Ec__Iterator7_t111350788 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Slots/<BringBackHUD>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBringBackHUDU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m721269541 (U3CBringBackHUDU3Ec__Iterator7_t111350788 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Object Slots/<BringBackHUD>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBringBackHUDU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m2256314553 (U3CBringBackHUDU3Ec__Iterator7_t111350788 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Boolean Slots/<BringBackHUD>c__Iterator7::MoveNext()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1263177918;
extern const uint32_t U3CBringBackHUDU3Ec__Iterator7_MoveNext_m2489602917_MetadataUsageId;
extern "C"  bool U3CBringBackHUDU3Ec__Iterator7_MoveNext_m2489602917 (U3CBringBackHUDU3Ec__Iterator7_t111350788 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CBringBackHUDU3Ec__Iterator7_MoveNext_m2489602917_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0144;
		}
	}
	{
		goto IL_0150;
	}

IL_0021:
	{
		Slots_t79980053 * L_2 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = L_2->get_HUD_9();
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = GameObject_get_transform_m1278640159(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t4282066566  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = (&V_1)->get_y_2();
		__this->set_U3CstartU3E__0_0(L_6);
		float L_7 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CstartTimeU3E__1_1(L_7);
		__this->set_U3CoverTimeU3E__2_2((0.5f));
	}

IL_005a:
	{
		Slots_t79980053 * L_8 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_8);
		GameObject_t3674682005 * L_9 = L_8->get_HUD_9();
		NullCheck(L_9);
		Transform_t1659122786 * L_10 = GameObject_get_transform_m1278640159(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t4282066566  L_11 = Transform_get_position_m2211398607(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		float L_12 = (&V_2)->get_y_2();
		float L_13 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_14 = __this->get_U3CstartTimeU3E__1_1();
		float L_15 = __this->get_U3CoverTimeU3E__2_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_16 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_12, (0.0f), ((float)((float)((float)((float)L_13-(float)L_14))/(float)L_15)), /*hidden argument*/NULL);
		__this->set_U3CmovementU3E__3_3(L_16);
		Slots_t79980053 * L_17 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_17);
		GameObject_t3674682005 * L_18 = L_17->get_HUD_9();
		NullCheck(L_18);
		Transform_t1659122786 * L_19 = GameObject_get_transform_m1278640159(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_t4282066566  L_20 = Transform_get_position_m2211398607(L_19, /*hidden argument*/NULL);
		V_3 = L_20;
		float L_21 = (&V_3)->get_x_1();
		float L_22 = __this->get_U3CmovementU3E__3_3();
		Vector2_t4282066565  L_23;
		memset(&L_23, 0, sizeof(L_23));
		Vector2__ctor_m1517109030(&L_23, L_21, L_22, /*hidden argument*/NULL);
		__this->set_U3CposU3E__4_4(L_23);
		Slots_t79980053 * L_24 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_24);
		GameObject_t3674682005 * L_25 = L_24->get_HUD_9();
		NullCheck(L_25);
		Transform_t1659122786 * L_26 = GameObject_get_transform_m1278640159(L_25, /*hidden argument*/NULL);
		Vector2_t4282066565  L_27 = __this->get_U3CposU3E__4_4();
		Vector3_t4282066566  L_28 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		Transform_set_position_m3111394108(L_26, L_28, /*hidden argument*/NULL);
		Slots_t79980053 * L_29 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_29);
		GameObject_t3674682005 * L_30 = L_29->get_HUD_9();
		NullCheck(L_30);
		Transform_t1659122786 * L_31 = GameObject_get_transform_m1278640159(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		Vector3_t4282066566  L_32 = Transform_get_position_m2211398607(L_31, /*hidden argument*/NULL);
		V_4 = L_32;
		float L_33 = (&V_4)->get_y_2();
		if ((!(((float)L_33) <= ((float)(0.0f)))))
		{
			goto IL_0131;
		}
	}
	{
		Slots_t79980053 * L_34 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_34);
		GameObject_t3674682005 * L_35 = L_34->get_SlotpParent_5();
		NullCheck(L_35);
		GameObject_SetActive_m3538205401(L_35, (bool)1, /*hidden argument*/NULL);
		Slots_t79980053 * L_36 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_36);
		MonoBehaviour_StopCoroutine_m2790918991(L_36, _stringLiteral1263177918, /*hidden argument*/NULL);
	}

IL_0131:
	{
		__this->set_U24current_6(NULL);
		__this->set_U24PC_5(1);
		goto IL_0152;
	}

IL_0144:
	{
		goto IL_005a;
	}
	// Dead block : IL_0149: ldarg.0

IL_0150:
	{
		return (bool)0;
	}

IL_0152:
	{
		return (bool)1;
	}
}
// System.Void Slots/<BringBackHUD>c__Iterator7::Dispose()
extern "C"  void U3CBringBackHUDU3Ec__Iterator7_Dispose_m12691860 (U3CBringBackHUDU3Ec__Iterator7_t111350788 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void Slots/<BringBackHUD>c__Iterator7::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CBringBackHUDU3Ec__Iterator7_Reset_m4256600516_MetadataUsageId;
extern "C"  void U3CBringBackHUDU3Ec__Iterator7_Reset_m4256600516 (U3CBringBackHUDU3Ec__Iterator7_t111350788 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CBringBackHUDU3Ec__Iterator7_Reset_m4256600516_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Slots/<BringHUD>c__Iterator6::.ctor()
extern "C"  void U3CBringHUDU3Ec__Iterator6__ctor_m2838895903 (U3CBringHUDU3Ec__Iterator6_t3883270396 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Slots/<BringHUD>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBringHUDU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3351622813 (U3CBringHUDU3Ec__Iterator6_t3883270396 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Object Slots/<BringHUD>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBringHUDU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m3685126705 (U3CBringHUDU3Ec__Iterator6_t3883270396 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Boolean Slots/<BringHUD>c__Iterator6::MoveNext()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral268358181;
extern const uint32_t U3CBringHUDU3Ec__Iterator6_MoveNext_m1820441181_MetadataUsageId;
extern "C"  bool U3CBringHUDU3Ec__Iterator6_MoveNext_m1820441181 (U3CBringHUDU3Ec__Iterator6_t3883270396 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CBringHUDU3Ec__Iterator6_MoveNext_m1820441181_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0133;
		}
	}
	{
		goto IL_013f;
	}

IL_0021:
	{
		Slots_t79980053 * L_2 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = L_2->get_HUD_9();
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = GameObject_get_transform_m1278640159(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t4282066566  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = (&V_1)->get_y_2();
		__this->set_U3CstartU3E__0_0(L_6);
		float L_7 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CstartTimeU3E__1_1(L_7);
		__this->set_U3CoverTimeU3E__2_2((0.5f));
	}

IL_005a:
	{
		Slots_t79980053 * L_8 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_8);
		GameObject_t3674682005 * L_9 = L_8->get_HUD_9();
		NullCheck(L_9);
		Transform_t1659122786 * L_10 = GameObject_get_transform_m1278640159(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t4282066566  L_11 = Transform_get_position_m2211398607(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		float L_12 = (&V_2)->get_y_2();
		float L_13 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_14 = __this->get_U3CstartTimeU3E__1_1();
		float L_15 = __this->get_U3CoverTimeU3E__2_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_16 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_12, (0.0f), ((float)((float)((float)((float)L_13-(float)L_14))/(float)L_15)), /*hidden argument*/NULL);
		__this->set_U3CmovementU3E__3_3(L_16);
		Slots_t79980053 * L_17 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_17);
		GameObject_t3674682005 * L_18 = L_17->get_HUD_9();
		NullCheck(L_18);
		Transform_t1659122786 * L_19 = GameObject_get_transform_m1278640159(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_t4282066566  L_20 = Transform_get_position_m2211398607(L_19, /*hidden argument*/NULL);
		V_3 = L_20;
		float L_21 = (&V_3)->get_x_1();
		float L_22 = __this->get_U3CmovementU3E__3_3();
		Vector2_t4282066565  L_23;
		memset(&L_23, 0, sizeof(L_23));
		Vector2__ctor_m1517109030(&L_23, L_21, L_22, /*hidden argument*/NULL);
		__this->set_U3CposU3E__4_4(L_23);
		Slots_t79980053 * L_24 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_24);
		GameObject_t3674682005 * L_25 = L_24->get_HUD_9();
		NullCheck(L_25);
		Transform_t1659122786 * L_26 = GameObject_get_transform_m1278640159(L_25, /*hidden argument*/NULL);
		Vector2_t4282066565  L_27 = __this->get_U3CposU3E__4_4();
		Vector3_t4282066566  L_28 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		Transform_set_position_m3111394108(L_26, L_28, /*hidden argument*/NULL);
		Slots_t79980053 * L_29 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_29);
		GameObject_t3674682005 * L_30 = L_29->get_HUD_9();
		NullCheck(L_30);
		Transform_t1659122786 * L_31 = GameObject_get_transform_m1278640159(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		Vector3_t4282066566  L_32 = Transform_get_position_m2211398607(L_31, /*hidden argument*/NULL);
		V_4 = L_32;
		float L_33 = (&V_4)->get_y_2();
		if ((!(((float)L_33) >= ((float)(0.0f)))))
		{
			goto IL_0120;
		}
	}
	{
		Slots_t79980053 * L_34 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_34);
		MonoBehaviour_StopCoroutine_m2790918991(L_34, _stringLiteral268358181, /*hidden argument*/NULL);
	}

IL_0120:
	{
		__this->set_U24current_6(NULL);
		__this->set_U24PC_5(1);
		goto IL_0141;
	}

IL_0133:
	{
		goto IL_005a;
	}
	// Dead block : IL_0138: ldarg.0

IL_013f:
	{
		return (bool)0;
	}

IL_0141:
	{
		return (bool)1;
	}
}
// System.Void Slots/<BringHUD>c__Iterator6::Dispose()
extern "C"  void U3CBringHUDU3Ec__Iterator6_Dispose_m773012892 (U3CBringHUDU3Ec__Iterator6_t3883270396 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void Slots/<BringHUD>c__Iterator6::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CBringHUDU3Ec__Iterator6_Reset_m485328844_MetadataUsageId;
extern "C"  void U3CBringHUDU3Ec__Iterator6_Reset_m485328844 (U3CBringHUDU3Ec__Iterator6_t3883270396 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CBringHUDU3Ec__Iterator6_Reset_m485328844_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Slots/<BringInventory>c__Iterator9::.ctor()
extern "C"  void U3CBringInventoryU3Ec__Iterator9__ctor_m4202580257 (U3CBringInventoryU3Ec__Iterator9_t2737813690 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Slots/<BringInventory>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBringInventoryU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4019722779 (U3CBringInventoryU3Ec__Iterator9_t2737813690 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Object Slots/<BringInventory>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBringInventoryU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m3158291375 (U3CBringInventoryU3Ec__Iterator9_t2737813690 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Boolean Slots/<BringInventory>c__Iterator9::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2252399;
extern Il2CppCodeGenString* _stringLiteral32;
extern Il2CppCodeGenString* _stringLiteral3694608394;
extern const uint32_t U3CBringInventoryU3Ec__Iterator9_MoveNext_m576004123_MetadataUsageId;
extern "C"  bool U3CBringInventoryU3Ec__Iterator9_MoveNext_m576004123 (U3CBringInventoryU3Ec__Iterator9_t2737813690 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CBringInventoryU3Ec__Iterator9_MoveNext_m576004123_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0085;
		}
		if (L_1 == 2)
		{
			goto IL_01b7;
		}
	}
	{
		goto IL_01c3;
	}

IL_0025:
	{
		Slots_t79980053 * L_2 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = L_2->get_HUD_9();
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = GameObject_get_transform_m1278640159(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t4282066566  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = (&V_1)->get_y_2();
		__this->set_U3CstartU3E__0_0(L_6);
		float L_7 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CstartTimeU3E__1_1(L_7);
		__this->set_U3CoverTimeU3E__2_2((0.5f));
		Slots_t79980053 * L_8 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_8);
		Slots_UpdateInventoryWithBlink_m2931405617(L_8, /*hidden argument*/NULL);
	}

IL_0069:
	{
		WaitForSeconds_t1615819279 * L_9 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_9, (1.0f), /*hidden argument*/NULL);
		__this->set_U24current_6(L_9);
		__this->set_U24PC_5(1);
		goto IL_01c5;
	}

IL_0085:
	{
		Slots_t79980053 * L_10 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_10);
		GameObject_t3674682005 * L_11 = L_10->get_SlotpParent_5();
		NullCheck(L_11);
		GameObject_SetActive_m3538205401(L_11, (bool)0, /*hidden argument*/NULL);
		Slots_t79980053 * L_12 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_12);
		GameObject_t3674682005 * L_13 = L_12->get_HUD_9();
		NullCheck(L_13);
		Transform_t1659122786 * L_14 = GameObject_get_transform_m1278640159(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t4282066566  L_15 = Transform_get_position_m2211398607(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		float L_16 = (&V_2)->get_y_2();
		Slots_t79980053 * L_17 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_17);
		float L_18 = L_17->get_heightHUD_56();
		float L_19 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_20 = __this->get_U3CstartTimeU3E__1_1();
		float L_21 = __this->get_U3CoverTimeU3E__2_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_22 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_16, L_18, ((float)((float)((float)((float)L_19-(float)L_20))/(float)L_21)), /*hidden argument*/NULL);
		__this->set_U3CmovementU3E__3_3(L_22);
		Slots_t79980053 * L_23 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_23);
		Transform_t1659122786 * L_24 = Component_get_transform_m4257140443(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_t4282066566  L_25 = Transform_get_position_m2211398607(L_24, /*hidden argument*/NULL);
		V_3 = L_25;
		float L_26 = (&V_3)->get_x_1();
		float L_27 = __this->get_U3CmovementU3E__3_3();
		Vector2_t4282066565  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Vector2__ctor_m1517109030(&L_28, L_26, L_27, /*hidden argument*/NULL);
		__this->set_U3CposU3E__4_4(L_28);
		Slots_t79980053 * L_29 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_29);
		GameObject_t3674682005 * L_30 = L_29->get_HUD_9();
		NullCheck(L_30);
		Transform_t1659122786 * L_31 = GameObject_get_transform_m1278640159(L_30, /*hidden argument*/NULL);
		Vector2_t4282066565  L_32 = __this->get_U3CposU3E__4_4();
		Vector3_t4282066566  L_33 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		NullCheck(L_31);
		Transform_set_position_m3111394108(L_31, L_33, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_34 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 0);
		ArrayElementTypeCheck (L_34, _stringLiteral2252399);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2252399);
		ObjectU5BU5D_t1108656482* L_35 = L_34;
		float L_36 = __this->get_U3CmovementU3E__3_3();
		float L_37 = L_36;
		Il2CppObject * L_38 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_37);
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, 1);
		ArrayElementTypeCheck (L_35, L_38);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_38);
		ObjectU5BU5D_t1108656482* L_39 = L_35;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, 2);
		ArrayElementTypeCheck (L_39, _stringLiteral32);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral32);
		ObjectU5BU5D_t1108656482* L_40 = L_39;
		Slots_t79980053 * L_41 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_41);
		float L_42 = L_41->get_heightHUD_56();
		float L_43 = L_42;
		Il2CppObject * L_44 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_43);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 3);
		ArrayElementTypeCheck (L_40, L_44);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_44);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_45 = String_Concat_m3016520001(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		Slots_t79980053 * L_46 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_46);
		GameObject_t3674682005 * L_47 = L_46->get_HUD_9();
		NullCheck(L_47);
		Transform_t1659122786 * L_48 = GameObject_get_transform_m1278640159(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		Vector3_t4282066566  L_49 = Transform_get_position_m2211398607(L_48, /*hidden argument*/NULL);
		V_4 = L_49;
		float L_50 = (&V_4)->get_y_2();
		Slots_t79980053 * L_51 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_51);
		float L_52 = L_51->get_heightHUD_56();
		if ((!(((float)L_50) >= ((float)L_52))))
		{
			goto IL_01a4;
		}
	}
	{
		Slots_t79980053 * L_53 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_53);
		MonoBehaviour_StopCoroutine_m2790918991(L_53, _stringLiteral3694608394, /*hidden argument*/NULL);
	}

IL_01a4:
	{
		__this->set_U24current_6(NULL);
		__this->set_U24PC_5(2);
		goto IL_01c5;
	}

IL_01b7:
	{
		goto IL_0069;
	}
	// Dead block : IL_01bc: ldarg.0

IL_01c3:
	{
		return (bool)0;
	}

IL_01c5:
	{
		return (bool)1;
	}
}
// System.Void Slots/<BringInventory>c__Iterator9::Dispose()
extern "C"  void U3CBringInventoryU3Ec__Iterator9_Dispose_m1308651806 (U3CBringInventoryU3Ec__Iterator9_t2737813690 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void Slots/<BringInventory>c__Iterator9::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CBringInventoryU3Ec__Iterator9_Reset_m1849013198_MetadataUsageId;
extern "C"  void U3CBringInventoryU3Ec__Iterator9_Reset_m1849013198 (U3CBringInventoryU3Ec__Iterator9_t2737813690 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CBringInventoryU3Ec__Iterator9_Reset_m1849013198_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Slots/<BringMan>c__Iterator5::.ctor()
extern "C"  void U3CBringManU3Ec__Iterator5__ctor_m1175716899 (U3CBringManU3Ec__Iterator5_t2482726264 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Slots/<BringMan>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBringManU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m850929689 (U3CBringManU3Ec__Iterator5_t2482726264 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Object Slots/<BringMan>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBringManU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m92765613 (U3CBringManU3Ec__Iterator5_t2482726264 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Boolean Slots/<BringMan>c__Iterator5::MoveNext()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2959582852;
extern Il2CppCodeGenString* _stringLiteral268363400;
extern const uint32_t U3CBringManU3Ec__Iterator5_MoveNext_m797459673_MetadataUsageId;
extern "C"  bool U3CBringManU3Ec__Iterator5_MoveNext_m797459673 (U3CBringManU3Ec__Iterator5_t2482726264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CBringManU3Ec__Iterator5_MoveNext_m797459673_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_015c;
		}
	}
	{
		goto IL_0168;
	}

IL_0021:
	{
		Slots_t79980053 * L_2 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = L_2->get_HUD_9();
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = GameObject_get_transform_m1278640159(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t4282066566  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = (&V_1)->get_y_2();
		__this->set_U3CstartU3E__0_0(L_6);
		float L_7 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CstartTimeU3E__1_1(L_7);
		__this->set_U3CoverTimeU3E__2_2((0.5f));
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, _stringLiteral2959582852, /*hidden argument*/NULL);
	}

IL_0064:
	{
		Slots_t79980053 * L_8 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_8);
		GameObject_t3674682005 * L_9 = L_8->get_HUD_9();
		NullCheck(L_9);
		Transform_t1659122786 * L_10 = GameObject_get_transform_m1278640159(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t4282066566  L_11 = Transform_get_position_m2211398607(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		float L_12 = (&V_2)->get_y_2();
		Slots_t79980053 * L_13 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_13);
		float L_14 = L_13->get_heightHUD_56();
		float L_15 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_16 = __this->get_U3CstartTimeU3E__1_1();
		float L_17 = __this->get_U3CoverTimeU3E__2_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_18 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_12, ((-L_14)), ((float)((float)((float)((float)L_15-(float)L_16))/(float)L_17)), /*hidden argument*/NULL);
		__this->set_U3CmovementU3E__3_3(L_18);
		Slots_t79980053 * L_19 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_19);
		GameObject_t3674682005 * L_20 = L_19->get_HUD_9();
		NullCheck(L_20);
		Transform_t1659122786 * L_21 = GameObject_get_transform_m1278640159(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_t4282066566  L_22 = Transform_get_position_m2211398607(L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		float L_23 = (&V_3)->get_x_1();
		float L_24 = __this->get_U3CmovementU3E__3_3();
		Vector2_t4282066565  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Vector2__ctor_m1517109030(&L_25, L_23, L_24, /*hidden argument*/NULL);
		__this->set_U3CposU3E__4_4(L_25);
		Slots_t79980053 * L_26 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_26);
		GameObject_t3674682005 * L_27 = L_26->get_HUD_9();
		NullCheck(L_27);
		Transform_t1659122786 * L_28 = GameObject_get_transform_m1278640159(L_27, /*hidden argument*/NULL);
		Vector2_t4282066565  L_29 = __this->get_U3CposU3E__4_4();
		Vector3_t4282066566  L_30 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		NullCheck(L_28);
		Transform_set_position_m3111394108(L_28, L_30, /*hidden argument*/NULL);
		Slots_t79980053 * L_31 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_31);
		GameObject_t3674682005 * L_32 = L_31->get_HUD_9();
		NullCheck(L_32);
		Transform_t1659122786 * L_33 = GameObject_get_transform_m1278640159(L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		Vector3_t4282066566  L_34 = Transform_get_position_m2211398607(L_33, /*hidden argument*/NULL);
		V_4 = L_34;
		float L_35 = (&V_4)->get_y_2();
		Slots_t79980053 * L_36 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_36);
		float L_37 = L_36->get_heightHUD_56();
		if ((!(((float)L_35) <= ((float)((-L_37))))))
		{
			goto IL_0149;
		}
	}
	{
		Slots_t79980053 * L_38 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_38);
		GameObject_t3674682005 * L_39 = L_38->get_SlotpParent_5();
		NullCheck(L_39);
		GameObject_SetActive_m3538205401(L_39, (bool)1, /*hidden argument*/NULL);
		Slots_t79980053 * L_40 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_40);
		MonoBehaviour_StopCoroutine_m2790918991(L_40, _stringLiteral268363400, /*hidden argument*/NULL);
	}

IL_0149:
	{
		__this->set_U24current_6(NULL);
		__this->set_U24PC_5(1);
		goto IL_016a;
	}

IL_015c:
	{
		goto IL_0064;
	}
	// Dead block : IL_0161: ldarg.0

IL_0168:
	{
		return (bool)0;
	}

IL_016a:
	{
		return (bool)1;
	}
}
// System.Void Slots/<BringMan>c__Iterator5::Dispose()
extern "C"  void U3CBringManU3Ec__Iterator5_Dispose_m185824160 (U3CBringManU3Ec__Iterator5_t2482726264 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void Slots/<BringMan>c__Iterator5::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CBringManU3Ec__Iterator5_Reset_m3117117136_MetadataUsageId;
extern "C"  void U3CBringManU3Ec__Iterator5_Reset_m3117117136 (U3CBringManU3Ec__Iterator5_t2482726264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CBringManU3Ec__Iterator5_Reset_m3117117136_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Slots/<BringMap>c__Iterator8::.ctor()
extern "C"  void U3CBringMapU3Ec__Iterator8__ctor_m3075705346 (U3CBringMapU3Ec__Iterator8_t1471609017 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Slots/<BringMap>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBringMapU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2442465178 (U3CBringMapU3Ec__Iterator8_t1471609017 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Object Slots/<BringMap>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBringMapU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m1912440622 (U3CBringMapU3Ec__Iterator8_t1471609017 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Boolean Slots/<BringMap>c__Iterator8::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1334548727;
extern Il2CppCodeGenString* _stringLiteral268363402;
extern const uint32_t U3CBringMapU3Ec__Iterator8_MoveNext_m4274257562_MetadataUsageId;
extern "C"  bool U3CBringMapU3Ec__Iterator8_MoveNext_m4274257562 (U3CBringMapU3Ec__Iterator8_t1471609017 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CBringMapU3Ec__Iterator8_MoveNext_m4274257562_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t4241904616  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t4282066566  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_007a;
		}
		if (L_1 == 2)
		{
			goto IL_01c0;
		}
	}
	{
		goto IL_01cc;
	}

IL_0025:
	{
		Slots_t79980053 * L_2 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = L_2->get_HUD_9();
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = GameObject_get_transform_m1278640159(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t4282066566  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = (&V_1)->get_y_2();
		__this->set_U3CstartU3E__0_0(L_6);
		float L_7 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CstartTimeU3E__1_1(L_7);
		__this->set_U3CoverTimeU3E__2_2((0.1f));
	}

IL_005e:
	{
		WaitForSeconds_t1615819279 * L_8 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_8, (1.0f), /*hidden argument*/NULL);
		__this->set_U24current_6(L_8);
		__this->set_U24PC_5(1);
		goto IL_01ce;
	}

IL_007a:
	{
		Slots_t79980053 * L_9 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_9);
		GameObject_t3674682005 * L_10 = L_9->get_SlotpParent_5();
		NullCheck(L_10);
		GameObject_SetActive_m3538205401(L_10, (bool)0, /*hidden argument*/NULL);
		Slots_t79980053 * L_11 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_11);
		GameObject_t3674682005 * L_12 = L_11->get_HUD_9();
		NullCheck(L_12);
		Transform_t1659122786 * L_13 = GameObject_get_transform_m1278640159(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t4282066566  L_14 = Transform_get_position_m2211398607(L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		float L_15 = (&V_2)->get_y_2();
		Slots_t79980053 * L_16 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_16);
		float L_17 = L_16->get_heightHUD_56();
		float L_18 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_19 = __this->get_U3CstartTimeU3E__1_1();
		float L_20 = __this->get_U3CoverTimeU3E__2_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_21 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_15, ((float)((float)L_17*(float)(3.0f))), ((float)((float)((float)((float)L_18-(float)L_19))/(float)L_20)), /*hidden argument*/NULL);
		__this->set_U3CmovementU3E__3_3(L_21);
		Slots_t79980053 * L_22 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_22);
		GameObject_t3674682005 * L_23 = L_22->get_HUD_9();
		NullCheck(L_23);
		RectTransform_t972643934 * L_24 = GameObject_GetComponent_TisRectTransform_t972643934_m406276429(L_23, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var);
		NullCheck(L_24);
		Rect_t4241904616  L_25 = RectTransform_get_rect_m1566017036(L_24, /*hidden argument*/NULL);
		V_3 = L_25;
		float L_26 = Rect_get_height_m2154960823((&V_3), /*hidden argument*/NULL);
		Slots_t79980053 * L_27 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_27);
		Canvas_t2727140764 * L_28 = L_27->get_canvas_35();
		NullCheck(L_28);
		float L_29 = Canvas_get_scaleFactor_m1187077271(L_28, /*hidden argument*/NULL);
		float L_30 = ((float)((float)((float)((float)L_26*(float)L_29))/(float)(2.0f)));
		Il2CppObject * L_31 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_30);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, _stringLiteral1334548727, /*hidden argument*/NULL);
		Slots_t79980053 * L_32 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_32);
		Transform_t1659122786 * L_33 = Component_get_transform_m4257140443(L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		Vector3_t4282066566  L_34 = Transform_get_position_m2211398607(L_33, /*hidden argument*/NULL);
		V_4 = L_34;
		float L_35 = (&V_4)->get_x_1();
		float L_36 = __this->get_U3CmovementU3E__3_3();
		Vector2_t4282066565  L_37;
		memset(&L_37, 0, sizeof(L_37));
		Vector2__ctor_m1517109030(&L_37, L_35, L_36, /*hidden argument*/NULL);
		__this->set_U3CposU3E__4_4(L_37);
		Slots_t79980053 * L_38 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_38);
		GameObject_t3674682005 * L_39 = L_38->get_HUD_9();
		NullCheck(L_39);
		Transform_t1659122786 * L_40 = GameObject_get_transform_m1278640159(L_39, /*hidden argument*/NULL);
		Vector2_t4282066565  L_41 = __this->get_U3CposU3E__4_4();
		Vector3_t4282066566  L_42 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		NullCheck(L_40);
		Transform_set_position_m3111394108(L_40, L_42, /*hidden argument*/NULL);
		Slots_t79980053 * L_43 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_43);
		GameObject_t3674682005 * L_44 = L_43->get_HUD_9();
		NullCheck(L_44);
		Transform_t1659122786 * L_45 = GameObject_get_transform_m1278640159(L_44, /*hidden argument*/NULL);
		NullCheck(L_45);
		Vector3_t4282066566  L_46 = Transform_get_position_m2211398607(L_45, /*hidden argument*/NULL);
		V_5 = L_46;
		float L_47 = (&V_5)->get_y_2();
		Slots_t79980053 * L_48 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_48);
		float L_49 = L_48->get_heightHUD_56();
		if ((!(((float)L_47) >= ((float)((float)((float)L_49*(float)(3.0f)))))))
		{
			goto IL_01ad;
		}
	}
	{
		Slots_t79980053 * L_50 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_50);
		MonoBehaviour_StopCoroutine_m2790918991(L_50, _stringLiteral268363402, /*hidden argument*/NULL);
	}

IL_01ad:
	{
		__this->set_U24current_6(NULL);
		__this->set_U24PC_5(2);
		goto IL_01ce;
	}

IL_01c0:
	{
		goto IL_005e;
	}
	// Dead block : IL_01c5: ldarg.0

IL_01cc:
	{
		return (bool)0;
	}

IL_01ce:
	{
		return (bool)1;
	}
}
// System.Void Slots/<BringMap>c__Iterator8::Dispose()
extern "C"  void U3CBringMapU3Ec__Iterator8_Dispose_m713620927 (U3CBringMapU3Ec__Iterator8_t1471609017 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void Slots/<BringMap>c__Iterator8::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CBringMapU3Ec__Iterator8_Reset_m722138287_MetadataUsageId;
extern "C"  void U3CBringMapU3Ec__Iterator8_Reset_m722138287 (U3CBringMapU3Ec__Iterator8_t1471609017 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CBringMapU3Ec__Iterator8_Reset_m722138287_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Slots/<ChangePic>c__Iterator4::.ctor()
extern "C"  void U3CChangePicU3Ec__Iterator4__ctor_m1973967650 (U3CChangePicU3Ec__Iterator4_t1863457897 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Slots/<ChangePic>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CChangePicU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m244131888 (U3CChangePicU3Ec__Iterator4_t1863457897 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object Slots/<ChangePic>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CChangePicU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m4092955588 (U3CChangePicU3Ec__Iterator4_t1863457897 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Boolean Slots/<ChangePic>c__Iterator4::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3459213210;
extern const uint32_t U3CChangePicU3Ec__Iterator4_MoveNext_m3652262546_MetadataUsageId;
extern "C"  bool U3CChangePicU3Ec__Iterator4_MoveNext_m3652262546 (U3CChangePicU3Ec__Iterator4_t1863457897 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CChangePicU3Ec__Iterator4_MoveNext_m3652262546_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Color_t4194546905  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Color_t4194546905  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Color_t4194546905  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Color_t4194546905  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Color_t4194546905  V_5;
	memset(&V_5, 0, sizeof(V_5));
	bool V_6 = false;
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0137;
		}
	}
	{
		goto IL_019e;
	}

IL_0021:
	{
		Slots_t79980053 * L_2 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = L_2->get_PicCanvas_17();
		NullCheck(L_3);
		SpriteRenderer_t2548470764 * L_4 = GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(L_3, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var);
		NullCheck(L_4);
		Color_t4194546905  L_5 = SpriteRenderer_get_color_m3519203926(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = (&V_1)->get_r_0();
		Slots_t79980053 * L_7 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_7);
		GameObject_t3674682005 * L_8 = L_7->get_PicCanvas_17();
		NullCheck(L_8);
		SpriteRenderer_t2548470764 * L_9 = GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(L_8, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var);
		NullCheck(L_9);
		Color_t4194546905  L_10 = SpriteRenderer_get_color_m3519203926(L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		float L_11 = (&V_2)->get_g_1();
		Slots_t79980053 * L_12 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_12);
		GameObject_t3674682005 * L_13 = L_12->get_PicCanvas_17();
		NullCheck(L_13);
		SpriteRenderer_t2548470764 * L_14 = GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(L_13, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var);
		NullCheck(L_14);
		Color_t4194546905  L_15 = SpriteRenderer_get_color_m3519203926(L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		float L_16 = (&V_3)->get_b_2();
		Color_t4194546905  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Color__ctor_m2252924356(&L_17, L_6, L_11, L_16, (0.0f), /*hidden argument*/NULL);
		__this->set_U3CcU3E__0_0(L_17);
		Slots_t79980053 * L_18 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_18);
		GameObject_t3674682005 * L_19 = L_18->get_PicCanvas_17();
		NullCheck(L_19);
		SpriteRenderer_t2548470764 * L_20 = GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(L_19, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var);
		Color_t4194546905  L_21 = __this->get_U3CcU3E__0_0();
		NullCheck(L_20);
		SpriteRenderer_set_color_m2701854973(L_20, L_21, /*hidden argument*/NULL);
		Slots_t79980053 * L_22 = __this->get_U3CU3Ef__this_5();
		Slots_t79980053 * L_23 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_23);
		Combinaciones_t3637091598 * L_24 = L_23->get_combinaciones_39();
		int32_t L_25 = __this->get_i_1();
		NullCheck(L_24);
		Sprite_t3199167241 * L_26 = Combinaciones_getPic_m3530152747(L_24, L_25, /*hidden argument*/NULL);
		NullCheck(L_22);
		L_22->set_picCanvas_38(L_26);
		goto IL_0137;
	}

IL_00c9:
	{
		Color_t4194546905 * L_27 = __this->get_address_of_U3CcU3E__0_0();
		Color_t4194546905 * L_28 = L_27;
		float L_29 = L_28->get_a_3();
		L_28->set_a_3(((float)((float)L_29+(float)(0.01f))));
		Slots_t79980053 * L_30 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_30);
		GameObject_t3674682005 * L_31 = L_30->get_PicCanvas_17();
		NullCheck(L_31);
		SpriteRenderer_t2548470764 * L_32 = GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(L_31, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var);
		Color_t4194546905  L_33 = __this->get_U3CcU3E__0_0();
		NullCheck(L_32);
		SpriteRenderer_set_color_m2701854973(L_32, L_33, /*hidden argument*/NULL);
		Slots_t79980053 * L_34 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_34);
		GameObject_t3674682005 * L_35 = L_34->get_PicCanvas_17();
		NullCheck(L_35);
		SpriteRenderer_t2548470764 * L_36 = GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(L_35, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var);
		Slots_t79980053 * L_37 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_37);
		Sprite_t3199167241 * L_38 = L_37->get_picCanvas_38();
		NullCheck(L_36);
		SpriteRenderer_set_sprite_m1519408453(L_36, L_38, /*hidden argument*/NULL);
		WaitForSeconds_t1615819279 * L_39 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_39, (0.01f), /*hidden argument*/NULL);
		__this->set_U24current_3(L_39);
		__this->set_U24PC_2(1);
		goto IL_01a0;
	}

IL_0137:
	{
		Slots_t79980053 * L_40 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_40);
		GameObject_t3674682005 * L_41 = L_40->get_PicCanvas_17();
		NullCheck(L_41);
		SpriteRenderer_t2548470764 * L_42 = GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(L_41, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var);
		NullCheck(L_42);
		Color_t4194546905  L_43 = SpriteRenderer_get_color_m3519203926(L_42, /*hidden argument*/NULL);
		V_4 = L_43;
		float L_44 = (&V_4)->get_a_3();
		if ((((float)L_44) < ((float)(1.0f))))
		{
			goto IL_00c9;
		}
	}
	{
		Slots_t79980053 * L_45 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_45);
		GameObject_t3674682005 * L_46 = L_45->get_PicCanvas_17();
		NullCheck(L_46);
		SpriteRenderer_t2548470764 * L_47 = GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(L_46, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var);
		NullCheck(L_47);
		Color_t4194546905  L_48 = SpriteRenderer_get_color_m3519203926(L_47, /*hidden argument*/NULL);
		V_5 = L_48;
		float L_49 = (&V_5)->get_a_3();
		if ((!(((float)L_49) >= ((float)(1.0f)))))
		{
			goto IL_0197;
		}
	}
	{
		Slots_t79980053 * L_50 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_50);
		MonoBehaviour_StopCoroutine_m2790918991(L_50, _stringLiteral3459213210, /*hidden argument*/NULL);
	}

IL_0197:
	{
		__this->set_U24PC_2((-1));
	}

IL_019e:
	{
		return (bool)0;
	}

IL_01a0:
	{
		return (bool)1;
	}
	// Dead block : IL_01a2: ldloc.s V_6
}
// System.Void Slots/<ChangePic>c__Iterator4::Dispose()
extern "C"  void U3CChangePicU3Ec__Iterator4_Dispose_m2800617183 (U3CChangePicU3Ec__Iterator4_t1863457897 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_2((-1));
		return;
	}
}
// System.Void Slots/<ChangePic>c__Iterator4::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CChangePicU3Ec__Iterator4_Reset_m3915367887_MetadataUsageId;
extern "C"  void U3CChangePicU3Ec__Iterator4_Reset_m3915367887 (U3CChangePicU3Ec__Iterator4_t1863457897 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CChangePicU3Ec__Iterator4_Reset_m3915367887_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Slots/<HightlightSlot>c__IteratorA::.ctor()
extern "C"  void U3CHightlightSlotU3Ec__IteratorA__ctor_m2623113617 (U3CHightlightSlotU3Ec__IteratorA_t3561306698 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Slots/<HightlightSlot>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CHightlightSlotU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2353807275 (U3CHightlightSlotU3Ec__IteratorA_t3561306698 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object Slots/<HightlightSlot>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CHightlightSlotU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m4069587263 (U3CHightlightSlotU3Ec__IteratorA_t3561306698 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean Slots/<HightlightSlot>c__IteratorA::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern const uint32_t U3CHightlightSlotU3Ec__IteratorA_MoveNext_m2347026859_MetadataUsageId;
extern "C"  bool U3CHightlightSlotU3Ec__IteratorA_MoveNext_m2347026859 (U3CHightlightSlotU3Ec__IteratorA_t3561306698 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CHightlightSlotU3Ec__IteratorA_MoveNext_m2347026859_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0042;
		}
	}
	{
		goto IL_0064;
	}

IL_0021:
	{
		goto IL_0042;
	}

IL_0026:
	{
		WaitForSeconds_t1615819279 * L_2 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_2, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_2);
		__this->set_U24PC_1(1);
		goto IL_0066;
	}

IL_0042:
	{
		float L_3 = __this->get_t_0();
		if ((((float)L_3) > ((float)(0.0f))))
		{
			goto IL_0026;
		}
	}
	{
		Slots_t79980053 * L_4 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_4);
		Slots_resetTimer_m934139748(L_4, /*hidden argument*/NULL);
		__this->set_U24PC_1((-1));
	}

IL_0064:
	{
		return (bool)0;
	}

IL_0066:
	{
		return (bool)1;
	}
	// Dead block : IL_0068: ldloc.1
}
// System.Void Slots/<HightlightSlot>c__IteratorA::Dispose()
extern "C"  void U3CHightlightSlotU3Ec__IteratorA_Dispose_m3859633550 (U3CHightlightSlotU3Ec__IteratorA_t3561306698 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void Slots/<HightlightSlot>c__IteratorA::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CHightlightSlotU3Ec__IteratorA_Reset_m269546558_MetadataUsageId;
extern "C"  void U3CHightlightSlotU3Ec__IteratorA_Reset_m269546558 (U3CHightlightSlotU3Ec__IteratorA_t3561306698 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CHightlightSlotU3Ec__IteratorA_Reset_m269546558_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Slots/<roll>c__Iterator3::.ctor()
extern "C"  void U3CrollU3Ec__Iterator3__ctor_m1708876570 (U3CrollU3Ec__Iterator3_t2129678497 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Slots/<roll>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CrollU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3524343810 (U3CrollU3Ec__Iterator3_t2129678497 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Slots/<roll>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CrollU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1348960150 (U3CrollU3Ec__Iterator3_t2129678497 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean Slots/<roll>c__Iterator3::MoveNext()
extern Il2CppClass* Slots_t79980053_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern const uint32_t U3CrollU3Ec__Iterator3_MoveNext_m1645602946_MetadataUsageId;
extern "C"  bool U3CrollU3Ec__Iterator3_MoveNext_m1645602946 (U3CrollU3Ec__Iterator3_t2129678497 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CrollU3Ec__Iterator3_MoveNext_m1645602946_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_021b;
		}
	}
	{
		goto IL_0264;
	}

IL_0021:
	{
		Slots_t79980053 * L_2 = __this->get_U3CU3Ef__this_2();
		Slots_t79980053 * L_3 = L_2;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_credits_55();
		NullCheck(L_3);
		L_3->set_credits_55(((int32_t)((int32_t)L_4-(int32_t)1)));
		Slots_t79980053 * L_5 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_5);
		CreditsHUD_t1430045213 * L_6 = L_5->get_creditsHUD_3();
		NullCheck(L_6);
		CreditsHUD_UpdateText_m2045600444(L_6, /*hidden argument*/NULL);
		goto IL_021b;
	}

IL_0049:
	{
		Slots_t79980053 * L_7 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_7);
		Slots_getCaption_m1409653679(L_7, 0, /*hidden argument*/NULL);
		Slots_t79980053 * L_8 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_8);
		L_8->set_rolling_44((bool)1);
		Slots_t79980053 * L_9 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_9);
		int32_t L_10 = L_9->get_a_50();
		IL2CPP_RUNTIME_CLASS_INIT(Slots_t79980053_il2cpp_TypeInfo_var);
		int32_t L_11 = ((Slots_t79980053_StaticFields*)Slots_t79980053_il2cpp_TypeInfo_var->static_fields)->get_numLock_57();
		if ((((int32_t)L_10) >= ((int32_t)L_11)))
		{
			goto IL_00b5;
		}
	}
	{
		Slots_t79980053 * L_12 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_12);
		SpriteU5BU5D_t2761310900* L_13 = L_12->get_slots_48();
		Slots_t79980053 * L_14 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_14);
		Vinetas_t2126676892 * L_15 = L_14->get_vi_49();
		NullCheck(L_15);
		SpriteU5BU5D_t2761310900* L_16 = L_15->get_listaSprite_22();
		Slots_t79980053 * L_17 = __this->get_U3CU3Ef__this_2();
		Slots_t79980053 * L_18 = L_17;
		NullCheck(L_18);
		int32_t L_19 = L_18->get_a_50();
		int32_t L_20 = Random_Range_m75452833(NULL /*static, unused*/, 1, 2, /*hidden argument*/NULL);
		int32_t L_21 = ((int32_t)((int32_t)L_19+(int32_t)L_20));
		V_1 = L_21;
		NullCheck(L_18);
		L_18->set_a_50(L_21);
		int32_t L_22 = V_1;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_22);
		int32_t L_23 = L_22;
		Sprite_t3199167241 * L_24 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
		ArrayElementTypeCheck (L_13, L_24);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(0), (Sprite_t3199167241 *)L_24);
		goto IL_00e0;
	}

IL_00b5:
	{
		Slots_t79980053 * L_25 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_25);
		SpriteU5BU5D_t2761310900* L_26 = L_25->get_slots_48();
		Slots_t79980053 * L_27 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_27);
		Vinetas_t2126676892 * L_28 = L_27->get_vi_49();
		NullCheck(L_28);
		SpriteU5BU5D_t2761310900* L_29 = L_28->get_listaSprite_22();
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 0);
		int32_t L_30 = 0;
		Sprite_t3199167241 * L_31 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 0);
		ArrayElementTypeCheck (L_26, L_31);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(0), (Sprite_t3199167241 *)L_31);
		Slots_t79980053 * L_32 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_32);
		L_32->set_a_50(0);
	}

IL_00e0:
	{
		Slots_t79980053 * L_33 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_33);
		Slots_SetPosInList_m3074373091(L_33, /*hidden argument*/NULL);
		Slots_t79980053 * L_34 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_34);
		int32_t L_35 = L_34->get_b_51();
		IL2CPP_RUNTIME_CLASS_INIT(Slots_t79980053_il2cpp_TypeInfo_var);
		int32_t L_36 = ((Slots_t79980053_StaticFields*)Slots_t79980053_il2cpp_TypeInfo_var->static_fields)->get_numLock_57();
		if ((((int32_t)L_35) >= ((int32_t)L_36)))
		{
			goto IL_013f;
		}
	}
	{
		Slots_t79980053 * L_37 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_37);
		SpriteU5BU5D_t2761310900* L_38 = L_37->get_slots_48();
		Slots_t79980053 * L_39 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_39);
		Vinetas_t2126676892 * L_40 = L_39->get_vi_49();
		NullCheck(L_40);
		SpriteU5BU5D_t2761310900* L_41 = L_40->get_listaSprite_22();
		Slots_t79980053 * L_42 = __this->get_U3CU3Ef__this_2();
		Slots_t79980053 * L_43 = L_42;
		NullCheck(L_43);
		int32_t L_44 = L_43->get_b_51();
		int32_t L_45 = Random_Range_m75452833(NULL /*static, unused*/, 1, 2, /*hidden argument*/NULL);
		int32_t L_46 = ((int32_t)((int32_t)L_44+(int32_t)L_45));
		V_1 = L_46;
		NullCheck(L_43);
		L_43->set_b_51(L_46);
		int32_t L_47 = V_1;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_47);
		int32_t L_48 = L_47;
		Sprite_t3199167241 * L_49 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 1);
		ArrayElementTypeCheck (L_38, L_49);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(1), (Sprite_t3199167241 *)L_49);
		goto IL_016a;
	}

IL_013f:
	{
		Slots_t79980053 * L_50 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_50);
		SpriteU5BU5D_t2761310900* L_51 = L_50->get_slots_48();
		Slots_t79980053 * L_52 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_52);
		Vinetas_t2126676892 * L_53 = L_52->get_vi_49();
		NullCheck(L_53);
		SpriteU5BU5D_t2761310900* L_54 = L_53->get_listaSprite_22();
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, 0);
		int32_t L_55 = 0;
		Sprite_t3199167241 * L_56 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, 1);
		ArrayElementTypeCheck (L_51, L_56);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(1), (Sprite_t3199167241 *)L_56);
		Slots_t79980053 * L_57 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_57);
		L_57->set_b_51(0);
	}

IL_016a:
	{
		Slots_t79980053 * L_58 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_58);
		Slots_SetPosInList_m3074373091(L_58, /*hidden argument*/NULL);
		Slots_t79980053 * L_59 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_59);
		int32_t L_60 = L_59->get_c_52();
		IL2CPP_RUNTIME_CLASS_INIT(Slots_t79980053_il2cpp_TypeInfo_var);
		int32_t L_61 = ((Slots_t79980053_StaticFields*)Slots_t79980053_il2cpp_TypeInfo_var->static_fields)->get_numLock_57();
		if ((((int32_t)L_60) >= ((int32_t)L_61)))
		{
			goto IL_01c9;
		}
	}
	{
		Slots_t79980053 * L_62 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_62);
		SpriteU5BU5D_t2761310900* L_63 = L_62->get_slots_48();
		Slots_t79980053 * L_64 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_64);
		Vinetas_t2126676892 * L_65 = L_64->get_vi_49();
		NullCheck(L_65);
		SpriteU5BU5D_t2761310900* L_66 = L_65->get_listaSprite_22();
		Slots_t79980053 * L_67 = __this->get_U3CU3Ef__this_2();
		Slots_t79980053 * L_68 = L_67;
		NullCheck(L_68);
		int32_t L_69 = L_68->get_c_52();
		int32_t L_70 = Random_Range_m75452833(NULL /*static, unused*/, 1, 2, /*hidden argument*/NULL);
		int32_t L_71 = ((int32_t)((int32_t)L_69+(int32_t)L_70));
		V_1 = L_71;
		NullCheck(L_68);
		L_68->set_c_52(L_71);
		int32_t L_72 = V_1;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, L_72);
		int32_t L_73 = L_72;
		Sprite_t3199167241 * L_74 = (L_66)->GetAt(static_cast<il2cpp_array_size_t>(L_73));
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, 2);
		ArrayElementTypeCheck (L_63, L_74);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(2), (Sprite_t3199167241 *)L_74);
		goto IL_01f4;
	}

IL_01c9:
	{
		Slots_t79980053 * L_75 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_75);
		SpriteU5BU5D_t2761310900* L_76 = L_75->get_slots_48();
		Slots_t79980053 * L_77 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_77);
		Vinetas_t2126676892 * L_78 = L_77->get_vi_49();
		NullCheck(L_78);
		SpriteU5BU5D_t2761310900* L_79 = L_78->get_listaSprite_22();
		NullCheck(L_79);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_79, 0);
		int32_t L_80 = 0;
		Sprite_t3199167241 * L_81 = (L_79)->GetAt(static_cast<il2cpp_array_size_t>(L_80));
		NullCheck(L_76);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_76, 2);
		ArrayElementTypeCheck (L_76, L_81);
		(L_76)->SetAt(static_cast<il2cpp_array_size_t>(2), (Sprite_t3199167241 *)L_81);
		Slots_t79980053 * L_82 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_82);
		L_82->set_c_52(0);
	}

IL_01f4:
	{
		Slots_t79980053 * L_83 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_83);
		Slots_SetPosInList_m3074373091(L_83, /*hidden argument*/NULL);
		WaitForSeconds_t1615819279 * L_84 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_84, (0.05f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_84);
		__this->set_U24PC_0(1);
		goto IL_0266;
	}

IL_021b:
	{
		Slots_t79980053 * L_85 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_85);
		float L_86 = L_85->get_currentTimer_41();
		if ((((float)L_86) > ((float)(0.0f))))
		{
			goto IL_0049;
		}
	}
	{
		Slots_t79980053 * L_87 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_87);
		L_87->set_rolling_44((bool)0);
		Slots_t79980053 * L_88 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_88);
		Slots_CheckCombination_m4066843381(L_88, /*hidden argument*/NULL);
		Slots_t79980053 * L_89 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_89);
		Slots_resetTimer_m934139748(L_89, /*hidden argument*/NULL);
		Slots_t79980053 * L_90 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_90);
		Slots_resetVars_m2296921103(L_90, /*hidden argument*/NULL);
		__this->set_U24PC_0((-1));
	}

IL_0264:
	{
		return (bool)0;
	}

IL_0266:
	{
		return (bool)1;
	}
	// Dead block : IL_0268: ldloc.2
}
// System.Void Slots/<roll>c__Iterator3::Dispose()
extern "C"  void U3CrollU3Ec__Iterator3_Dispose_m1451159767 (U3CrollU3Ec__Iterator3_t2129678497 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void Slots/<roll>c__Iterator3::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CrollU3Ec__Iterator3_Reset_m3650276807_MetadataUsageId;
extern "C"  void U3CrollU3Ec__Iterator3_Reset_m3650276807 (U3CrollU3Ec__Iterator3_t2129678497 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CrollU3Ec__Iterator3_Reset_m3650276807_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UITween.AnimationParts::.ctor(UITween.AnimationParts/State,System.Boolean,System.Boolean,System.Boolean,UITween.AnimationParts/EndTweenClose,UITween.AnimationParts/CallbackCall,UnityEngine.Events.UnityEvent,UnityEngine.Events.UnityEvent)
extern Il2CppClass* PositionPropetiesAnim_t3297200579_il2cpp_TypeInfo_var;
extern Il2CppClass* ScalePropetiesAnim_t205839632_il2cpp_TypeInfo_var;
extern Il2CppClass* RotationPropetiesAnim_t2262074766_il2cpp_TypeInfo_var;
extern Il2CppClass* FadePropetiesAnim_t145139664_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityEvent_t1266085011_il2cpp_TypeInfo_var;
extern const uint32_t AnimationParts__ctor_m743217775_MetadataUsageId;
extern "C"  void AnimationParts__ctor_m743217775 (AnimationParts_t4290477440 * __this, int32_t ___ObjectState0, bool ___UnscaledTimeAnimation1, bool ___SaveState2, bool ___AtomicAnim3, int32_t ___EndState4, int32_t ___CallCallback5, UnityEvent_t1266085011 * ___IntroEvents6, UnityEvent_t1266085011 * ___ExitEvents7, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationParts__ctor_m743217775_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PositionPropetiesAnim_t3297200579 * L_0 = (PositionPropetiesAnim_t3297200579 *)il2cpp_codegen_object_new(PositionPropetiesAnim_t3297200579_il2cpp_TypeInfo_var);
		PositionPropetiesAnim__ctor_m1322731893(L_0, /*hidden argument*/NULL);
		__this->set_PositionPropetiesAnim_0(L_0);
		ScalePropetiesAnim_t205839632 * L_1 = (ScalePropetiesAnim_t205839632 *)il2cpp_codegen_object_new(ScalePropetiesAnim_t205839632_il2cpp_TypeInfo_var);
		ScalePropetiesAnim__ctor_m593687544(L_1, /*hidden argument*/NULL);
		__this->set_ScalePropetiesAnim_1(L_1);
		RotationPropetiesAnim_t2262074766 * L_2 = (RotationPropetiesAnim_t2262074766 *)il2cpp_codegen_object_new(RotationPropetiesAnim_t2262074766_il2cpp_TypeInfo_var);
		RotationPropetiesAnim__ctor_m1987650058(L_2, /*hidden argument*/NULL);
		__this->set_RotationPropetiesAnim_2(L_2);
		FadePropetiesAnim_t145139664 * L_3 = (FadePropetiesAnim_t145139664 *)il2cpp_codegen_object_new(FadePropetiesAnim_t145139664_il2cpp_TypeInfo_var);
		FadePropetiesAnim__ctor_m3814006280(L_3, /*hidden argument*/NULL);
		__this->set_FadePropetiesAnim_3(L_3);
		__this->set_ObjectState_7(1);
		UnityEvent_t1266085011 * L_4 = (UnityEvent_t1266085011 *)il2cpp_codegen_object_new(UnityEvent_t1266085011_il2cpp_TypeInfo_var);
		UnityEvent__ctor_m1715209183(L_4, /*hidden argument*/NULL);
		__this->set_IntroEvents_10(L_4);
		UnityEvent_t1266085011 * L_5 = (UnityEvent_t1266085011 *)il2cpp_codegen_object_new(UnityEvent_t1266085011_il2cpp_TypeInfo_var);
		UnityEvent__ctor_m1715209183(L_5, /*hidden argument*/NULL);
		__this->set_ExitEvents_11(L_5);
		__this->set_animationDuration_15((1.0f));
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		int32_t L_6 = ___ObjectState0;
		__this->set_ObjectState_7(L_6);
		bool L_7 = ___UnscaledTimeAnimation1;
		__this->set_UnscaledTimeAnimation_4(L_7);
		bool L_8 = ___SaveState2;
		__this->set_SaveState_5(L_8);
		bool L_9 = ___AtomicAnim3;
		__this->set_AtomicAnimation_6(L_9);
		int32_t L_10 = ___EndState4;
		__this->set_EndState_8(L_10);
		int32_t L_11 = ___CallCallback5;
		__this->set_CallCallback_9(L_11);
		UnityEvent_t1266085011 * L_12 = ___IntroEvents6;
		__this->set_IntroEvents_10(L_12);
		UnityEvent_t1266085011 * L_13 = ___ExitEvents7;
		__this->set_ExitEvents_11(L_13);
		return;
	}
}
// System.Void UITween.AnimationParts::add_OnDisableOrDestroy(UITween.AnimationParts/DisableOrDestroy)
extern Il2CppClass* AnimationParts_t4290477440_il2cpp_TypeInfo_var;
extern Il2CppClass* DisableOrDestroy_t4252453067_il2cpp_TypeInfo_var;
extern const uint32_t AnimationParts_add_OnDisableOrDestroy_m475795999_MetadataUsageId;
extern "C"  void AnimationParts_add_OnDisableOrDestroy_m475795999 (Il2CppObject * __this /* static, unused */, DisableOrDestroy_t4252453067 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationParts_add_OnDisableOrDestroy_m475795999_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DisableOrDestroy_t4252453067 * L_0 = ((AnimationParts_t4290477440_StaticFields*)AnimationParts_t4290477440_il2cpp_TypeInfo_var->static_fields)->get_OnDisableOrDestroy_16();
		DisableOrDestroy_t4252453067 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((AnimationParts_t4290477440_StaticFields*)AnimationParts_t4290477440_il2cpp_TypeInfo_var->static_fields)->set_OnDisableOrDestroy_16(((DisableOrDestroy_t4252453067 *)CastclassSealed(L_2, DisableOrDestroy_t4252453067_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UITween.AnimationParts::remove_OnDisableOrDestroy(UITween.AnimationParts/DisableOrDestroy)
extern Il2CppClass* AnimationParts_t4290477440_il2cpp_TypeInfo_var;
extern Il2CppClass* DisableOrDestroy_t4252453067_il2cpp_TypeInfo_var;
extern const uint32_t AnimationParts_remove_OnDisableOrDestroy_m3821145048_MetadataUsageId;
extern "C"  void AnimationParts_remove_OnDisableOrDestroy_m3821145048 (Il2CppObject * __this /* static, unused */, DisableOrDestroy_t4252453067 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationParts_remove_OnDisableOrDestroy_m3821145048_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DisableOrDestroy_t4252453067 * L_0 = ((AnimationParts_t4290477440_StaticFields*)AnimationParts_t4290477440_il2cpp_TypeInfo_var->static_fields)->get_OnDisableOrDestroy_16();
		DisableOrDestroy_t4252453067 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((AnimationParts_t4290477440_StaticFields*)AnimationParts_t4290477440_il2cpp_TypeInfo_var->static_fields)->set_OnDisableOrDestroy_16(((DisableOrDestroy_t4252453067 *)CastclassSealed(L_2, DisableOrDestroy_t4252453067_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UITween.AnimationParts::SetAniamtioDuration(System.Single)
extern "C"  void AnimationParts_SetAniamtioDuration_m2064867261 (AnimationParts_t4290477440 * __this, float ___duration0, const MethodInfo* method)
{
	{
		float L_0 = ___duration0;
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_0017;
		}
	}
	{
		float L_1 = ___duration0;
		__this->set_animationDuration_15(L_1);
		goto IL_001e;
	}

IL_0017:
	{
		___duration0 = (0.01f);
	}

IL_001e:
	{
		return;
	}
}
// System.Single UITween.AnimationParts::GetAnimationDuration()
extern "C"  float AnimationParts_GetAnimationDuration_m3980949642 (AnimationParts_t4290477440 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_animationDuration_15();
		return L_0;
	}
}
// System.Void UITween.AnimationParts::CheckCallbackStatus()
extern "C"  void AnimationParts_CheckCallbackStatus_m2624905701 (AnimationParts_t4290477440 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_CallCallback_9();
		if ((((int32_t)L_0) == ((int32_t)((int32_t)15))))
		{
			goto IL_010a;
		}
	}
	{
		int32_t L_1 = __this->get_CallCallback_9();
		if ((((int32_t)L_1) == ((int32_t)4)))
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_2 = __this->get_CallCallback_9();
		if ((((int32_t)L_2) == ((int32_t)3)))
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_3 = __this->get_CallCallback_9();
		if ((((int32_t)L_3) == ((int32_t)5)))
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_4 = __this->get_CallCallback_9();
		if ((((int32_t)L_4) == ((int32_t)6)))
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_5 = __this->get_CallCallback_9();
		if ((((int32_t)L_5) == ((int32_t)((int32_t)12))))
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_6 = __this->get_CallCallback_9();
		if ((((int32_t)L_6) == ((int32_t)((int32_t)13))))
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_7 = __this->get_CallCallback_9();
		if ((((int32_t)L_7) == ((int32_t)((int32_t)14))))
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_8 = __this->get_CallCallback_9();
		if ((!(((uint32_t)L_8) == ((uint32_t)8))))
		{
			goto IL_008c;
		}
	}

IL_0070:
	{
		int32_t L_9 = __this->get_ObjectState_7();
		if (L_9)
		{
			goto IL_008c;
		}
	}
	{
		UnityEvent_t1266085011 * L_10 = __this->get_IntroEvents_10();
		AnimationParts_CheckCallBack_m2754985244(__this, L_10, /*hidden argument*/NULL);
		goto IL_010a;
	}

IL_008c:
	{
		int32_t L_11 = __this->get_CallCallback_9();
		if ((((int32_t)L_11) == ((int32_t)((int32_t)9))))
		{
			goto IL_00f2;
		}
	}
	{
		int32_t L_12 = __this->get_CallCallback_9();
		if ((((int32_t)L_12) == ((int32_t)7)))
		{
			goto IL_00f2;
		}
	}
	{
		int32_t L_13 = __this->get_CallCallback_9();
		if ((((int32_t)L_13) == ((int32_t)((int32_t)10))))
		{
			goto IL_00f2;
		}
	}
	{
		int32_t L_14 = __this->get_CallCallback_9();
		if ((((int32_t)L_14) == ((int32_t)((int32_t)11))))
		{
			goto IL_00f2;
		}
	}
	{
		int32_t L_15 = __this->get_CallCallback_9();
		if ((((int32_t)L_15) == ((int32_t)((int32_t)12))))
		{
			goto IL_00f2;
		}
	}
	{
		int32_t L_16 = __this->get_CallCallback_9();
		if ((((int32_t)L_16) == ((int32_t)((int32_t)13))))
		{
			goto IL_00f2;
		}
	}
	{
		int32_t L_17 = __this->get_CallCallback_9();
		if ((((int32_t)L_17) == ((int32_t)((int32_t)14))))
		{
			goto IL_00f2;
		}
	}
	{
		int32_t L_18 = __this->get_CallCallback_9();
		if ((!(((uint32_t)L_18) == ((uint32_t)8))))
		{
			goto IL_010a;
		}
	}

IL_00f2:
	{
		int32_t L_19 = __this->get_ObjectState_7();
		if ((!(((uint32_t)L_19) == ((uint32_t)1))))
		{
			goto IL_010a;
		}
	}
	{
		UnityEvent_t1266085011 * L_20 = __this->get_ExitEvents_11();
		AnimationParts_CheckCallBack_m2754985244(__this, L_20, /*hidden argument*/NULL);
	}

IL_010a:
	{
		return;
	}
}
// System.Void UITween.AnimationParts::FinalEnd()
extern Il2CppClass* AnimationParts_t4290477440_il2cpp_TypeInfo_var;
extern const uint32_t AnimationParts_FinalEnd_m4071780225_MetadataUsageId;
extern "C"  void AnimationParts_FinalEnd_m4071780225 (AnimationParts_t4290477440 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationParts_FinalEnd_m4071780225_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AnimationParts_t4290477440 * G_B11_0 = NULL;
	AnimationParts_t4290477440 * G_B10_0 = NULL;
	int32_t G_B12_0 = 0;
	AnimationParts_t4290477440 * G_B12_1 = NULL;
	{
		int32_t L_0 = __this->get_ObjectState_7();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0054;
		}
	}
	{
		int32_t L_1 = __this->get_EndState_8();
		if (L_1)
		{
			goto IL_0032;
		}
	}
	{
		DisableOrDestroy_t4252453067 * L_2 = ((AnimationParts_t4290477440_StaticFields*)AnimationParts_t4290477440_il2cpp_TypeInfo_var->static_fields)->get_OnDisableOrDestroy_16();
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		DisableOrDestroy_t4252453067 * L_3 = ((AnimationParts_t4290477440_StaticFields*)AnimationParts_t4290477440_il2cpp_TypeInfo_var->static_fields)->get_OnDisableOrDestroy_16();
		NullCheck(L_3);
		DisableOrDestroy_Invoke_m1508925664(L_3, (bool)1, __this, /*hidden argument*/NULL);
	}

IL_002d:
	{
		goto IL_0054;
	}

IL_0032:
	{
		int32_t L_4 = __this->get_EndState_8();
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0054;
		}
	}
	{
		DisableOrDestroy_t4252453067 * L_5 = ((AnimationParts_t4290477440_StaticFields*)AnimationParts_t4290477440_il2cpp_TypeInfo_var->static_fields)->get_OnDisableOrDestroy_16();
		if (!L_5)
		{
			goto IL_0054;
		}
	}
	{
		DisableOrDestroy_t4252453067 * L_6 = ((AnimationParts_t4290477440_StaticFields*)AnimationParts_t4290477440_il2cpp_TypeInfo_var->static_fields)->get_OnDisableOrDestroy_16();
		NullCheck(L_6);
		DisableOrDestroy_Invoke_m1508925664(L_6, (bool)0, __this, /*hidden argument*/NULL);
	}

IL_0054:
	{
		bool L_7 = __this->get_SaveState_5();
		if (!L_7)
		{
			goto IL_0077;
		}
	}
	{
		int32_t L_8 = __this->get_ObjectState_7();
		G_B10_0 = __this;
		if (L_8)
		{
			G_B11_0 = __this;
			goto IL_0071;
		}
	}
	{
		G_B12_0 = 1;
		G_B12_1 = G_B10_0;
		goto IL_0072;
	}

IL_0071:
	{
		G_B12_0 = 0;
		G_B12_1 = G_B11_0;
	}

IL_0072:
	{
		NullCheck(G_B12_1);
		G_B12_1->set_ObjectState_7(G_B12_0);
	}

IL_0077:
	{
		return;
	}
}
// System.Void UITween.AnimationParts::Ended()
extern "C"  void AnimationParts_Ended_m91490016 (AnimationParts_t4290477440 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_CallCallback_9();
		if ((((int32_t)L_0) == ((int32_t)((int32_t)15))))
		{
			goto IL_0103;
		}
	}
	{
		int32_t L_1 = __this->get_ObjectState_7();
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0089;
		}
	}
	{
		int32_t L_2 = __this->get_CallCallback_9();
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_007d;
		}
	}
	{
		int32_t L_3 = __this->get_CallCallback_9();
		if ((((int32_t)L_3) == ((int32_t)2)))
		{
			goto IL_007d;
		}
	}
	{
		int32_t L_4 = __this->get_CallCallback_9();
		if ((((int32_t)L_4) == ((int32_t)4)))
		{
			goto IL_007d;
		}
	}
	{
		int32_t L_5 = __this->get_CallCallback_9();
		if ((((int32_t)L_5) == ((int32_t)6)))
		{
			goto IL_007d;
		}
	}
	{
		int32_t L_6 = __this->get_CallCallback_9();
		if ((((int32_t)L_6) == ((int32_t)((int32_t)9))))
		{
			goto IL_007d;
		}
	}
	{
		int32_t L_7 = __this->get_CallCallback_9();
		if ((((int32_t)L_7) == ((int32_t)((int32_t)11))))
		{
			goto IL_007d;
		}
	}
	{
		int32_t L_8 = __this->get_CallCallback_9();
		if ((((int32_t)L_8) == ((int32_t)((int32_t)12))))
		{
			goto IL_007d;
		}
	}
	{
		int32_t L_9 = __this->get_CallCallback_9();
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)14)))))
		{
			goto IL_0089;
		}
	}

IL_007d:
	{
		UnityEvent_t1266085011 * L_10 = __this->get_ExitEvents_11();
		AnimationParts_CheckCallBack_m2754985244(__this, L_10, /*hidden argument*/NULL);
	}

IL_0089:
	{
		int32_t L_11 = __this->get_CallCallback_9();
		if (!L_11)
		{
			goto IL_00ec;
		}
	}
	{
		int32_t L_12 = __this->get_CallCallback_9();
		if ((((int32_t)L_12) == ((int32_t)2)))
		{
			goto IL_00ec;
		}
	}
	{
		int32_t L_13 = __this->get_CallCallback_9();
		if ((((int32_t)L_13) == ((int32_t)5)))
		{
			goto IL_00ec;
		}
	}
	{
		int32_t L_14 = __this->get_CallCallback_9();
		if ((((int32_t)L_14) == ((int32_t)6)))
		{
			goto IL_00ec;
		}
	}
	{
		int32_t L_15 = __this->get_CallCallback_9();
		if ((((int32_t)L_15) == ((int32_t)((int32_t)10))))
		{
			goto IL_00ec;
		}
	}
	{
		int32_t L_16 = __this->get_CallCallback_9();
		if ((((int32_t)L_16) == ((int32_t)((int32_t)11))))
		{
			goto IL_00ec;
		}
	}
	{
		int32_t L_17 = __this->get_CallCallback_9();
		if ((((int32_t)L_17) == ((int32_t)((int32_t)13))))
		{
			goto IL_00ec;
		}
	}
	{
		int32_t L_18 = __this->get_CallCallback_9();
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)14)))))
		{
			goto IL_0103;
		}
	}

IL_00ec:
	{
		int32_t L_19 = __this->get_ObjectState_7();
		if (L_19)
		{
			goto IL_0103;
		}
	}
	{
		UnityEvent_t1266085011 * L_20 = __this->get_IntroEvents_10();
		AnimationParts_CheckCallBack_m2754985244(__this, L_20, /*hidden argument*/NULL);
	}

IL_0103:
	{
		return;
	}
}
// System.Void UITween.AnimationParts::FrameCheck()
extern "C"  void AnimationParts_FrameCheck_m2354148183 (AnimationParts_t4290477440 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_CheckNextFrame_13();
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		bool L_1 = __this->get_CallOnThisFrame_14();
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		AnimationParts_CallCallbackObjects_m3349944439(__this, /*hidden argument*/NULL);
	}

IL_001c:
	{
		bool L_2 = __this->get_CallOnThisFrame_14();
		__this->set_CallOnThisFrame_14((bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0));
	}

IL_002b:
	{
		return;
	}
}
// System.Boolean UITween.AnimationParts::IsObjectOpened()
extern "C"  bool AnimationParts_IsObjectOpened_m1882320386 (AnimationParts_t4290477440 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_ObjectState_7();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_000e;
		}
	}
	{
		return (bool)0;
	}

IL_000e:
	{
		return (bool)1;
	}
}
// System.Void UITween.AnimationParts::ChangeStatus()
extern "C"  void AnimationParts_ChangeStatus_m2262218494 (AnimationParts_t4290477440 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_ObjectState_7();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0018;
		}
	}
	{
		__this->set_ObjectState_7(0);
		goto IL_001f;
	}

IL_0018:
	{
		__this->set_ObjectState_7(1);
	}

IL_001f:
	{
		return;
	}
}
// System.Void UITween.AnimationParts::SetStatus(System.Boolean)
extern "C"  void AnimationParts_SetStatus_m3857703441 (AnimationParts_t4290477440 * __this, bool ___open0, const MethodInfo* method)
{
	{
		bool L_0 = ___open0;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		__this->set_ObjectState_7(0);
		goto IL_0019;
	}

IL_0012:
	{
		__this->set_ObjectState_7(1);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UITween.AnimationParts::CheckCallBack(UnityEngine.Events.UnityEvent)
extern "C"  void AnimationParts_CheckCallBack_m2754985244 (AnimationParts_t4290477440 * __this, UnityEvent_t1266085011 * ___CallbackObject0, const MethodInfo* method)
{
	{
		UnityEvent_t1266085011 * L_0 = ___CallbackObject0;
		__this->set_CallBackObject_12(L_0);
		bool L_1 = __this->get_CheckNextFrame_13();
		__this->set_CheckNextFrame_13((bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0));
		return;
	}
}
// System.Void UITween.AnimationParts::CallCallbackObjects()
extern "C"  void AnimationParts_CallCallbackObjects_m3349944439 (AnimationParts_t4290477440 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_CheckNextFrame_13();
		__this->set_CheckNextFrame_13((bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0));
		UnityEvent_t1266085011 * L_1 = __this->get_CallBackObject_12();
		NullCheck(L_1);
		UnityEvent_Invoke_m2672830205(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITween.AnimationParts/DisableOrDestroy::.ctor(System.Object,System.IntPtr)
extern "C"  void DisableOrDestroy__ctor_m4277289250 (DisableOrDestroy_t4252453067 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UITween.AnimationParts/DisableOrDestroy::Invoke(System.Boolean,UITween.AnimationParts)
extern "C"  void DisableOrDestroy_Invoke_m1508925664 (DisableOrDestroy_t4252453067 * __this, bool ___disable0, AnimationParts_t4290477440 * ___part1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DisableOrDestroy_Invoke_m1508925664((DisableOrDestroy_t4252453067 *)__this->get_prev_9(),___disable0, ___part1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___disable0, AnimationParts_t4290477440 * ___part1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___disable0, ___part1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___disable0, AnimationParts_t4290477440 * ___part1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___disable0, ___part1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UITween.AnimationParts/DisableOrDestroy::BeginInvoke(System.Boolean,UITween.AnimationParts,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t DisableOrDestroy_BeginInvoke_m1610579961_MetadataUsageId;
extern "C"  Il2CppObject * DisableOrDestroy_BeginInvoke_m1610579961 (DisableOrDestroy_t4252453067 * __this, bool ___disable0, AnimationParts_t4290477440 * ___part1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisableOrDestroy_BeginInvoke_m1610579961_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &___disable0);
	__d_args[1] = ___part1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UITween.AnimationParts/DisableOrDestroy::EndInvoke(System.IAsyncResult)
extern "C"  void DisableOrDestroy_EndInvoke_m541561906 (DisableOrDestroy_t4252453067 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UITween.CurrentAnimation::.ctor(UITween.AnimationParts)
extern "C"  void CurrentAnimation__ctor_m921667526 (CurrentAnimation_t3872301071 * __this, AnimationParts_t4290477440 * ___animationPart0, const MethodInfo* method)
{
	{
		__this->set_counterTween_1((2.0f));
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		AnimationParts_t4290477440 * L_0 = ___animationPart0;
		__this->set_animationPart_0(L_0);
		return;
	}
}
// System.Void UITween.CurrentAnimation::AnimationFrame(UnityEngine.RectTransform)
extern "C"  void CurrentAnimation_AnimationFrame_m2355919085 (CurrentAnimation_t3872301071 * __this, RectTransform_t972643934 * ___rectTransform0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_AnimationStates_2();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		float L_1 = __this->get_counterTween_1();
		if ((!(((float)L_1) <= ((float)(1.0f)))))
		{
			goto IL_0029;
		}
	}
	{
		RectTransform_t972643934 * L_2 = ___rectTransform0;
		float L_3 = __this->get_counterTween_1();
		CurrentAnimation_SetAnimationOnFrame_m2296428387(__this, L_2, L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Void UITween.CurrentAnimation::SetAnimationOnFrame(UnityEngine.RectTransform,System.Single)
extern "C"  void CurrentAnimation_SetAnimationOnFrame_m2296428387 (CurrentAnimation_t3872301071 * __this, RectTransform_t972643934 * ___rectTransform0, float ___percentage1, const MethodInfo* method)
{
	{
		AnimationParts_t4290477440 * L_0 = __this->get_animationPart_0();
		NullCheck(L_0);
		PositionPropetiesAnim_t3297200579 * L_1 = L_0->get_PositionPropetiesAnim_0();
		NullCheck(L_1);
		bool L_2 = PositionPropetiesAnim_IsPositionEnabled_m2317462805(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		RectTransform_t972643934 * L_3 = ___rectTransform0;
		float L_4 = ___percentage1;
		CurrentAnimation_MoveAnimation_m4193172284(__this, L_3, L_4, /*hidden argument*/NULL);
	}

IL_001d:
	{
		AnimationParts_t4290477440 * L_5 = __this->get_animationPart_0();
		NullCheck(L_5);
		RotationPropetiesAnim_t2262074766 * L_6 = L_5->get_RotationPropetiesAnim_2();
		NullCheck(L_6);
		bool L_7 = RotationPropetiesAnim_IsRotationEnabled_m2487428981(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003a;
		}
	}
	{
		RectTransform_t972643934 * L_8 = ___rectTransform0;
		float L_9 = ___percentage1;
		CurrentAnimation_RotateAnimation_m3512040390(__this, L_8, L_9, /*hidden argument*/NULL);
	}

IL_003a:
	{
		AnimationParts_t4290477440 * L_10 = __this->get_animationPart_0();
		NullCheck(L_10);
		ScalePropetiesAnim_t205839632 * L_11 = L_10->get_ScalePropetiesAnim_1();
		NullCheck(L_11);
		bool L_12 = ScalePropetiesAnim_IsScaleEnabled_m1438996961(L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0057;
		}
	}
	{
		RectTransform_t972643934 * L_13 = ___rectTransform0;
		float L_14 = ___percentage1;
		CurrentAnimation_ScaleAnimation_m2551231265(__this, L_13, L_14, /*hidden argument*/NULL);
	}

IL_0057:
	{
		AnimationParts_t4290477440 * L_15 = __this->get_animationPart_0();
		NullCheck(L_15);
		FadePropetiesAnim_t145139664 * L_16 = L_15->get_FadePropetiesAnim_3();
		NullCheck(L_16);
		bool L_17 = FadePropetiesAnim_IsFadeEnabled_m3212297909(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0079;
		}
	}
	{
		RectTransform_t972643934 * L_18 = ___rectTransform0;
		NullCheck(L_18);
		Transform_t1659122786 * L_19 = Component_get_transform_m4257140443(L_18, /*hidden argument*/NULL);
		float L_20 = ___percentage1;
		CurrentAnimation_SetAlphaValue_m56629110(__this, L_19, L_20, /*hidden argument*/NULL);
	}

IL_0079:
	{
		return;
	}
}
// System.Void UITween.CurrentAnimation::LateAnimationFrame(UnityEngine.RectTransform)
extern "C"  void CurrentAnimation_LateAnimationFrame_m927731175 (CurrentAnimation_t3872301071 * __this, RectTransform_t972643934 * ___rectTransform0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float G_B6_0 = 0.0f;
	{
		int32_t L_0 = __this->get_AnimationStates_2();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		float L_1 = __this->get_counterTween_1();
		if ((!(((float)L_1) <= ((float)(1.0f)))))
		{
			goto IL_005b;
		}
	}
	{
		AnimationParts_t4290477440 * L_2 = __this->get_animationPart_0();
		NullCheck(L_2);
		bool L_3 = L_2->get_UnscaledTimeAnimation_4();
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		float L_4 = Time_get_unscaledDeltaTime_m285638843(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = L_4;
		goto IL_003b;
	}

IL_0036:
	{
		float L_5 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = L_5;
	}

IL_003b:
	{
		V_0 = G_B6_0;
		float L_6 = __this->get_counterTween_1();
		float L_7 = V_0;
		AnimationParts_t4290477440 * L_8 = __this->get_animationPart_0();
		NullCheck(L_8);
		float L_9 = AnimationParts_GetAnimationDuration_m3980949642(L_8, /*hidden argument*/NULL);
		__this->set_counterTween_1(((float)((float)L_6+(float)((float)((float)L_7/(float)L_9)))));
		goto IL_00c4;
	}

IL_005b:
	{
		int32_t L_10 = __this->get_AnimationStates_2();
		if ((((int32_t)L_10) == ((int32_t)4)))
		{
			goto IL_00c4;
		}
	}
	{
		RectTransform_t972643934 * L_11 = ___rectTransform0;
		CurrentAnimation_SetAnimationOnFrame_m2296428387(__this, L_11, (1.0f), /*hidden argument*/NULL);
		int32_t L_12 = __this->get_AnimationStates_2();
		if ((((int32_t)L_12) == ((int32_t)3)))
		{
			goto IL_00b2;
		}
	}
	{
		int32_t L_13 = __this->get_AnimationStates_2();
		if ((((int32_t)L_13) == ((int32_t)4)))
		{
			goto IL_00b2;
		}
	}
	{
		AnimationParts_t4290477440 * L_14 = __this->get_animationPart_0();
		NullCheck(L_14);
		bool L_15 = L_14->get_AtomicAnimation_6();
		if (!L_15)
		{
			goto IL_00b2;
		}
	}
	{
		AnimationParts_t4290477440 * L_16 = __this->get_animationPart_0();
		NullCheck(L_16);
		AnimationParts_Ended_m91490016(L_16, /*hidden argument*/NULL);
		__this->set_AnimationStates_2(3);
		goto IL_00c4;
	}

IL_00b2:
	{
		AnimationParts_t4290477440 * L_17 = __this->get_animationPart_0();
		NullCheck(L_17);
		AnimationParts_FinalEnd_m4071780225(L_17, /*hidden argument*/NULL);
		__this->set_AnimationStates_2(4);
	}

IL_00c4:
	{
		float L_18 = __this->get_counterTween_1();
		if ((!(((float)L_18) > ((float)(0.9f)))))
		{
			goto IL_010e;
		}
	}
	{
		AnimationParts_t4290477440 * L_19 = __this->get_animationPart_0();
		NullCheck(L_19);
		bool L_20 = L_19->get_AtomicAnimation_6();
		if (L_20)
		{
			goto IL_010e;
		}
	}
	{
		int32_t L_21 = __this->get_AnimationStates_2();
		if ((((int32_t)L_21) == ((int32_t)3)))
		{
			goto IL_010e;
		}
	}
	{
		int32_t L_22 = __this->get_AnimationStates_2();
		if ((((int32_t)L_22) == ((int32_t)4)))
		{
			goto IL_010e;
		}
	}
	{
		AnimationParts_t4290477440 * L_23 = __this->get_animationPart_0();
		NullCheck(L_23);
		AnimationParts_Ended_m91490016(L_23, /*hidden argument*/NULL);
		__this->set_AnimationStates_2(3);
	}

IL_010e:
	{
		AnimationParts_t4290477440 * L_24 = __this->get_animationPart_0();
		NullCheck(L_24);
		AnimationParts_FrameCheck_m2354148183(L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITween.CurrentAnimation::PlayOpenAnimations()
extern "C"  void CurrentAnimation_PlayOpenAnimations_m3798939448 (CurrentAnimation_t3872301071 * __this, const MethodInfo* method)
{
	{
		AnimationParts_t4290477440 * L_0 = __this->get_animationPart_0();
		NullCheck(L_0);
		PositionPropetiesAnim_t3297200579 * L_1 = L_0->get_PositionPropetiesAnim_0();
		NullCheck(L_1);
		bool L_2 = PositionPropetiesAnim_IsPositionEnabled_m2317462805(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004b;
		}
	}
	{
		AnimationParts_t4290477440 * L_3 = __this->get_animationPart_0();
		NullCheck(L_3);
		PositionPropetiesAnim_t3297200579 * L_4 = L_3->get_PositionPropetiesAnim_0();
		NullCheck(L_4);
		AnimationCurve_t3667593487 * L_5 = L_4->get_TweenCurveEnterPos_1();
		AnimationParts_t4290477440 * L_6 = __this->get_animationPart_0();
		NullCheck(L_6);
		PositionPropetiesAnim_t3297200579 * L_7 = L_6->get_PositionPropetiesAnim_0();
		NullCheck(L_7);
		Vector3_t4282066566  L_8 = L_7->get_StartPos_3();
		AnimationParts_t4290477440 * L_9 = __this->get_animationPart_0();
		NullCheck(L_9);
		PositionPropetiesAnim_t3297200579 * L_10 = L_9->get_PositionPropetiesAnim_0();
		NullCheck(L_10);
		Vector3_t4282066566  L_11 = L_10->get_EndPos_4();
		CurrentAnimation_SetCurrentAnimPos_m2494854247(__this, L_5, L_8, L_11, /*hidden argument*/NULL);
	}

IL_004b:
	{
		AnimationParts_t4290477440 * L_12 = __this->get_animationPart_0();
		NullCheck(L_12);
		RotationPropetiesAnim_t2262074766 * L_13 = L_12->get_RotationPropetiesAnim_2();
		NullCheck(L_13);
		bool L_14 = RotationPropetiesAnim_IsRotationEnabled_m2487428981(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0096;
		}
	}
	{
		AnimationParts_t4290477440 * L_15 = __this->get_animationPart_0();
		NullCheck(L_15);
		RotationPropetiesAnim_t2262074766 * L_16 = L_15->get_RotationPropetiesAnim_2();
		NullCheck(L_16);
		AnimationCurve_t3667593487 * L_17 = L_16->get_TweenCurveEnterRot_1();
		AnimationParts_t4290477440 * L_18 = __this->get_animationPart_0();
		NullCheck(L_18);
		RotationPropetiesAnim_t2262074766 * L_19 = L_18->get_RotationPropetiesAnim_2();
		NullCheck(L_19);
		Vector3_t4282066566  L_20 = L_19->get_StartRot_3();
		AnimationParts_t4290477440 * L_21 = __this->get_animationPart_0();
		NullCheck(L_21);
		RotationPropetiesAnim_t2262074766 * L_22 = L_21->get_RotationPropetiesAnim_2();
		NullCheck(L_22);
		Vector3_t4282066566  L_23 = L_22->get_EndRot_4();
		CurrentAnimation_SetCurrentAnimRot_m1281190506(__this, L_17, L_20, L_23, /*hidden argument*/NULL);
	}

IL_0096:
	{
		AnimationParts_t4290477440 * L_24 = __this->get_animationPart_0();
		NullCheck(L_24);
		ScalePropetiesAnim_t205839632 * L_25 = L_24->get_ScalePropetiesAnim_1();
		NullCheck(L_25);
		bool L_26 = ScalePropetiesAnim_IsScaleEnabled_m1438996961(L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00e1;
		}
	}
	{
		AnimationParts_t4290477440 * L_27 = __this->get_animationPart_0();
		NullCheck(L_27);
		ScalePropetiesAnim_t205839632 * L_28 = L_27->get_ScalePropetiesAnim_1();
		NullCheck(L_28);
		AnimationCurve_t3667593487 * L_29 = L_28->get_TweenCurveEnterScale_1();
		AnimationParts_t4290477440 * L_30 = __this->get_animationPart_0();
		NullCheck(L_30);
		ScalePropetiesAnim_t205839632 * L_31 = L_30->get_ScalePropetiesAnim_1();
		NullCheck(L_31);
		Vector3_t4282066566  L_32 = L_31->get_StartScale_3();
		AnimationParts_t4290477440 * L_33 = __this->get_animationPart_0();
		NullCheck(L_33);
		ScalePropetiesAnim_t205839632 * L_34 = L_33->get_ScalePropetiesAnim_1();
		NullCheck(L_34);
		Vector3_t4282066566  L_35 = L_34->get_EndScale_4();
		CurrentAnimation_SetCurrentAnimScale_m4239404253(__this, L_29, L_32, L_35, /*hidden argument*/NULL);
	}

IL_00e1:
	{
		AnimationParts_t4290477440 * L_36 = __this->get_animationPart_0();
		NullCheck(L_36);
		FadePropetiesAnim_t145139664 * L_37 = L_36->get_FadePropetiesAnim_3();
		NullCheck(L_37);
		bool L_38 = FadePropetiesAnim_IsFadeEnabled_m3212297909(L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_011c;
		}
	}
	{
		AnimationParts_t4290477440 * L_39 = __this->get_animationPart_0();
		NullCheck(L_39);
		FadePropetiesAnim_t145139664 * L_40 = L_39->get_FadePropetiesAnim_3();
		NullCheck(L_40);
		float L_41 = FadePropetiesAnim_GetStartFadeValue_m562682939(L_40, /*hidden argument*/NULL);
		AnimationParts_t4290477440 * L_42 = __this->get_animationPart_0();
		NullCheck(L_42);
		FadePropetiesAnim_t145139664 * L_43 = L_42->get_FadePropetiesAnim_3();
		NullCheck(L_43);
		float L_44 = FadePropetiesAnim_GetEndFadeValue_m2770866146(L_43, /*hidden argument*/NULL);
		CurrentAnimation_SetFadeAnimation_m1879468991(__this, L_41, L_44, /*hidden argument*/NULL);
	}

IL_011c:
	{
		__this->set_counterTween_1((0.0f));
		__this->set_AnimationStates_2(1);
		AnimationParts_t4290477440 * L_45 = __this->get_animationPart_0();
		NullCheck(L_45);
		AnimationParts_ChangeStatus_m2262218494(L_45, /*hidden argument*/NULL);
		AnimationParts_t4290477440 * L_46 = __this->get_animationPart_0();
		NullCheck(L_46);
		AnimationParts_CheckCallbackStatus_m2624905701(L_46, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITween.CurrentAnimation::SetStatus(System.Boolean)
extern "C"  void CurrentAnimation_SetStatus_m337475618 (CurrentAnimation_t3872301071 * __this, bool ___status0, const MethodInfo* method)
{
	{
		AnimationParts_t4290477440 * L_0 = __this->get_animationPart_0();
		bool L_1 = ___status0;
		NullCheck(L_0);
		AnimationParts_SetStatus_m3857703441(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITween.CurrentAnimation::PlayCloseAnimations()
extern "C"  void CurrentAnimation_PlayCloseAnimations_m3386424170 (CurrentAnimation_t3872301071 * __this, const MethodInfo* method)
{
	{
		AnimationParts_t4290477440 * L_0 = __this->get_animationPart_0();
		NullCheck(L_0);
		PositionPropetiesAnim_t3297200579 * L_1 = L_0->get_PositionPropetiesAnim_0();
		NullCheck(L_1);
		bool L_2 = PositionPropetiesAnim_IsPositionEnabled_m2317462805(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004b;
		}
	}
	{
		AnimationParts_t4290477440 * L_3 = __this->get_animationPart_0();
		NullCheck(L_3);
		PositionPropetiesAnim_t3297200579 * L_4 = L_3->get_PositionPropetiesAnim_0();
		NullCheck(L_4);
		AnimationCurve_t3667593487 * L_5 = L_4->get_TweenCurveExitPos_2();
		AnimationParts_t4290477440 * L_6 = __this->get_animationPart_0();
		NullCheck(L_6);
		PositionPropetiesAnim_t3297200579 * L_7 = L_6->get_PositionPropetiesAnim_0();
		NullCheck(L_7);
		Vector3_t4282066566  L_8 = L_7->get_EndPos_4();
		AnimationParts_t4290477440 * L_9 = __this->get_animationPart_0();
		NullCheck(L_9);
		PositionPropetiesAnim_t3297200579 * L_10 = L_9->get_PositionPropetiesAnim_0();
		NullCheck(L_10);
		Vector3_t4282066566  L_11 = L_10->get_StartPos_3();
		CurrentAnimation_SetCurrentAnimPos_m2494854247(__this, L_5, L_8, L_11, /*hidden argument*/NULL);
	}

IL_004b:
	{
		AnimationParts_t4290477440 * L_12 = __this->get_animationPart_0();
		NullCheck(L_12);
		RotationPropetiesAnim_t2262074766 * L_13 = L_12->get_RotationPropetiesAnim_2();
		NullCheck(L_13);
		bool L_14 = RotationPropetiesAnim_IsRotationEnabled_m2487428981(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0096;
		}
	}
	{
		AnimationParts_t4290477440 * L_15 = __this->get_animationPart_0();
		NullCheck(L_15);
		RotationPropetiesAnim_t2262074766 * L_16 = L_15->get_RotationPropetiesAnim_2();
		NullCheck(L_16);
		AnimationCurve_t3667593487 * L_17 = L_16->get_TweenCurveExitRot_2();
		AnimationParts_t4290477440 * L_18 = __this->get_animationPart_0();
		NullCheck(L_18);
		RotationPropetiesAnim_t2262074766 * L_19 = L_18->get_RotationPropetiesAnim_2();
		NullCheck(L_19);
		Vector3_t4282066566  L_20 = L_19->get_EndRot_4();
		AnimationParts_t4290477440 * L_21 = __this->get_animationPart_0();
		NullCheck(L_21);
		RotationPropetiesAnim_t2262074766 * L_22 = L_21->get_RotationPropetiesAnim_2();
		NullCheck(L_22);
		Vector3_t4282066566  L_23 = L_22->get_StartRot_3();
		CurrentAnimation_SetCurrentAnimRot_m1281190506(__this, L_17, L_20, L_23, /*hidden argument*/NULL);
	}

IL_0096:
	{
		AnimationParts_t4290477440 * L_24 = __this->get_animationPart_0();
		NullCheck(L_24);
		ScalePropetiesAnim_t205839632 * L_25 = L_24->get_ScalePropetiesAnim_1();
		NullCheck(L_25);
		bool L_26 = ScalePropetiesAnim_IsScaleEnabled_m1438996961(L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00e1;
		}
	}
	{
		AnimationParts_t4290477440 * L_27 = __this->get_animationPart_0();
		NullCheck(L_27);
		ScalePropetiesAnim_t205839632 * L_28 = L_27->get_ScalePropetiesAnim_1();
		NullCheck(L_28);
		AnimationCurve_t3667593487 * L_29 = L_28->get_TweenCurveExitScale_2();
		AnimationParts_t4290477440 * L_30 = __this->get_animationPart_0();
		NullCheck(L_30);
		ScalePropetiesAnim_t205839632 * L_31 = L_30->get_ScalePropetiesAnim_1();
		NullCheck(L_31);
		Vector3_t4282066566  L_32 = L_31->get_EndScale_4();
		AnimationParts_t4290477440 * L_33 = __this->get_animationPart_0();
		NullCheck(L_33);
		ScalePropetiesAnim_t205839632 * L_34 = L_33->get_ScalePropetiesAnim_1();
		NullCheck(L_34);
		Vector3_t4282066566  L_35 = L_34->get_StartScale_3();
		CurrentAnimation_SetCurrentAnimScale_m4239404253(__this, L_29, L_32, L_35, /*hidden argument*/NULL);
	}

IL_00e1:
	{
		AnimationParts_t4290477440 * L_36 = __this->get_animationPart_0();
		NullCheck(L_36);
		FadePropetiesAnim_t145139664 * L_37 = L_36->get_FadePropetiesAnim_3();
		NullCheck(L_37);
		bool L_38 = FadePropetiesAnim_IsFadeEnabled_m3212297909(L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_011c;
		}
	}
	{
		AnimationParts_t4290477440 * L_39 = __this->get_animationPart_0();
		NullCheck(L_39);
		FadePropetiesAnim_t145139664 * L_40 = L_39->get_FadePropetiesAnim_3();
		NullCheck(L_40);
		float L_41 = FadePropetiesAnim_GetEndFadeValue_m2770866146(L_40, /*hidden argument*/NULL);
		AnimationParts_t4290477440 * L_42 = __this->get_animationPart_0();
		NullCheck(L_42);
		FadePropetiesAnim_t145139664 * L_43 = L_42->get_FadePropetiesAnim_3();
		NullCheck(L_43);
		float L_44 = FadePropetiesAnim_GetStartFadeValue_m562682939(L_43, /*hidden argument*/NULL);
		CurrentAnimation_SetFadeAnimation_m1879468991(__this, L_41, L_44, /*hidden argument*/NULL);
	}

IL_011c:
	{
		__this->set_counterTween_1((0.0f));
		__this->set_AnimationStates_2(1);
		AnimationParts_t4290477440 * L_45 = __this->get_animationPart_0();
		NullCheck(L_45);
		AnimationParts_ChangeStatus_m2262218494(L_45, /*hidden argument*/NULL);
		AnimationParts_t4290477440 * L_46 = __this->get_animationPart_0();
		NullCheck(L_46);
		AnimationParts_CheckCallbackStatus_m2624905701(L_46, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITween.CurrentAnimation::SetAnimationPos(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,UnityEngine.RectTransform)
extern "C"  void CurrentAnimation_SetAnimationPos_m4170296672 (CurrentAnimation_t3872301071 * __this, Vector2_t4282066565  ___StartAnchoredPos0, Vector2_t4282066565  ___EndAnchoredPos1, AnimationCurve_t3667593487 * ___EntryTween2, AnimationCurve_t3667593487 * ___ExitTween3, RectTransform_t972643934 * ___rectTransform4, const MethodInfo* method)
{
	{
		AnimationParts_t4290477440 * L_0 = __this->get_animationPart_0();
		NullCheck(L_0);
		PositionPropetiesAnim_t3297200579 * L_1 = L_0->get_PositionPropetiesAnim_0();
		NullCheck(L_1);
		PositionPropetiesAnim_SetPositionEnable_m2258509016(L_1, (bool)1, /*hidden argument*/NULL);
		AnimationParts_t4290477440 * L_2 = __this->get_animationPart_0();
		NullCheck(L_2);
		PositionPropetiesAnim_t3297200579 * L_3 = L_2->get_PositionPropetiesAnim_0();
		Vector2_t4282066565  L_4 = ___StartAnchoredPos0;
		Vector3_t4282066566  L_5 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_6 = ___rectTransform4;
		NullCheck(L_3);
		PositionPropetiesAnim_SetPosStart_m867218417(L_3, L_5, L_6, /*hidden argument*/NULL);
		AnimationParts_t4290477440 * L_7 = __this->get_animationPart_0();
		NullCheck(L_7);
		PositionPropetiesAnim_t3297200579 * L_8 = L_7->get_PositionPropetiesAnim_0();
		Vector2_t4282066565  L_9 = ___EndAnchoredPos1;
		Vector3_t4282066566  L_10 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_11 = ___rectTransform4;
		NullCheck(L_11);
		Transform_t1659122786 * L_12 = Component_get_transform_m4257140443(L_11, /*hidden argument*/NULL);
		NullCheck(L_8);
		PositionPropetiesAnim_SetPosEnd_m272049364(L_8, L_10, L_12, /*hidden argument*/NULL);
		AnimationParts_t4290477440 * L_13 = __this->get_animationPart_0();
		NullCheck(L_13);
		PositionPropetiesAnim_t3297200579 * L_14 = L_13->get_PositionPropetiesAnim_0();
		AnimationCurve_t3667593487 * L_15 = ___EntryTween2;
		AnimationCurve_t3667593487 * L_16 = ___ExitTween3;
		NullCheck(L_14);
		PositionPropetiesAnim_SetAniamtionsCurve_m4194602913(L_14, L_15, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITween.CurrentAnimation::SetAnimationScale(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.AnimationCurve,UnityEngine.AnimationCurve)
extern "C"  void CurrentAnimation_SetAnimationScale_m3631654511 (CurrentAnimation_t3872301071 * __this, Vector2_t4282066565  ___StartAnchoredScale0, Vector2_t4282066565  ___EndAnchoredScale1, AnimationCurve_t3667593487 * ___EntryTween2, AnimationCurve_t3667593487 * ___ExitTween3, const MethodInfo* method)
{
	{
		AnimationParts_t4290477440 * L_0 = __this->get_animationPart_0();
		NullCheck(L_0);
		ScalePropetiesAnim_t205839632 * L_1 = L_0->get_ScalePropetiesAnim_1();
		Vector2_t4282066565  L_2 = ___StartAnchoredScale0;
		Vector3_t4282066566  L_3 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_StartScale_3(L_3);
		AnimationParts_t4290477440 * L_4 = __this->get_animationPart_0();
		NullCheck(L_4);
		ScalePropetiesAnim_t205839632 * L_5 = L_4->get_ScalePropetiesAnim_1();
		NullCheck(L_5);
		ScalePropetiesAnim_SetScaleEnable_m258590894(L_5, (bool)1, /*hidden argument*/NULL);
		AnimationParts_t4290477440 * L_6 = __this->get_animationPart_0();
		NullCheck(L_6);
		ScalePropetiesAnim_t205839632 * L_7 = L_6->get_ScalePropetiesAnim_1();
		Vector2_t4282066565  L_8 = ___EndAnchoredScale1;
		Vector3_t4282066566  L_9 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		L_7->set_EndScale_4(L_9);
		AnimationParts_t4290477440 * L_10 = __this->get_animationPart_0();
		NullCheck(L_10);
		ScalePropetiesAnim_t205839632 * L_11 = L_10->get_ScalePropetiesAnim_1();
		AnimationCurve_t3667593487 * L_12 = ___EntryTween2;
		AnimationCurve_t3667593487 * L_13 = ___ExitTween3;
		NullCheck(L_11);
		ScalePropetiesAnim_SetAniamtionsCurve_m4026535268(L_11, L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITween.CurrentAnimation::SetAnimationRotation(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.AnimationCurve,UnityEngine.AnimationCurve)
extern "C"  void CurrentAnimation_SetAnimationRotation_m468204515 (CurrentAnimation_t3872301071 * __this, Vector2_t4282066565  ___StartAnchoredEulerAng0, Vector2_t4282066565  ___EndAnchoredEulerAng1, AnimationCurve_t3667593487 * ___EntryTween2, AnimationCurve_t3667593487 * ___ExitTween3, const MethodInfo* method)
{
	{
		AnimationParts_t4290477440 * L_0 = __this->get_animationPart_0();
		NullCheck(L_0);
		RotationPropetiesAnim_t2262074766 * L_1 = L_0->get_RotationPropetiesAnim_2();
		NullCheck(L_1);
		RotationPropetiesAnim_SetRotationEnable_m1626508738(L_1, (bool)1, /*hidden argument*/NULL);
		AnimationParts_t4290477440 * L_2 = __this->get_animationPart_0();
		NullCheck(L_2);
		RotationPropetiesAnim_t2262074766 * L_3 = L_2->get_RotationPropetiesAnim_2();
		Vector2_t4282066565  L_4 = ___StartAnchoredEulerAng0;
		Vector3_t4282066566  L_5 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		L_3->set_StartRot_3(L_5);
		AnimationParts_t4290477440 * L_6 = __this->get_animationPart_0();
		NullCheck(L_6);
		RotationPropetiesAnim_t2262074766 * L_7 = L_6->get_RotationPropetiesAnim_2();
		Vector2_t4282066565  L_8 = ___EndAnchoredEulerAng1;
		Vector3_t4282066566  L_9 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		L_7->set_EndRot_4(L_9);
		AnimationParts_t4290477440 * L_10 = __this->get_animationPart_0();
		NullCheck(L_10);
		RotationPropetiesAnim_t2262074766 * L_11 = L_10->get_RotationPropetiesAnim_2();
		AnimationCurve_t3667593487 * L_12 = ___EntryTween2;
		AnimationCurve_t3667593487 * L_13 = ___ExitTween3;
		NullCheck(L_11);
		RotationPropetiesAnim_SetAniamtionsCurve_m1567452918(L_11, L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITween.CurrentAnimation::SetFade(System.Boolean)
extern "C"  void CurrentAnimation_SetFade_m4004030636 (CurrentAnimation_t3872301071 * __this, bool ___OverrideFade0, const MethodInfo* method)
{
	{
		AnimationParts_t4290477440 * L_0 = __this->get_animationPart_0();
		NullCheck(L_0);
		FadePropetiesAnim_t145139664 * L_1 = L_0->get_FadePropetiesAnim_3();
		NullCheck(L_1);
		FadePropetiesAnim_SetFadeEnable_m1937066046(L_1, (bool)1, /*hidden argument*/NULL);
		AnimationParts_t4290477440 * L_2 = __this->get_animationPart_0();
		NullCheck(L_2);
		FadePropetiesAnim_t145139664 * L_3 = L_2->get_FadePropetiesAnim_3();
		bool L_4 = ___OverrideFade0;
		NullCheck(L_3);
		FadePropetiesAnim_SetFadeOverride_m3099760583(L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITween.CurrentAnimation::SetFadeValuesStartEnd(System.Single,System.Single)
extern "C"  void CurrentAnimation_SetFadeValuesStartEnd_m1730649248 (CurrentAnimation_t3872301071 * __this, float ___startAlphaValue0, float ___endAlphaValue1, const MethodInfo* method)
{
	{
		AnimationParts_t4290477440 * L_0 = __this->get_animationPart_0();
		NullCheck(L_0);
		FadePropetiesAnim_t145139664 * L_1 = L_0->get_FadePropetiesAnim_3();
		float L_2 = ___startAlphaValue0;
		float L_3 = ___endAlphaValue1;
		NullCheck(L_1);
		FadePropetiesAnim_SetFadeValues_m1935912618(L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UITween.CurrentAnimation::IsObjectOpened()
extern "C"  bool CurrentAnimation_IsObjectOpened_m1592603601 (CurrentAnimation_t3872301071 * __this, const MethodInfo* method)
{
	{
		AnimationParts_t4290477440 * L_0 = __this->get_animationPart_0();
		NullCheck(L_0);
		bool L_1 = AnimationParts_IsObjectOpened_m1882320386(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UITween.CurrentAnimation::SetAniamtioDuration(System.Single)
extern "C"  void CurrentAnimation_SetAniamtioDuration_m816778444 (CurrentAnimation_t3872301071 * __this, float ___duration0, const MethodInfo* method)
{
	{
		AnimationParts_t4290477440 * L_0 = __this->get_animationPart_0();
		float L_1 = ___duration0;
		NullCheck(L_0);
		AnimationParts_SetAniamtioDuration_m2064867261(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UITween.CurrentAnimation::GetAnimationDuration()
extern "C"  float CurrentAnimation_GetAnimationDuration_m719667481 (CurrentAnimation_t3872301071 * __this, const MethodInfo* method)
{
	{
		AnimationParts_t4290477440 * L_0 = __this->get_animationPart_0();
		NullCheck(L_0);
		float L_1 = AnimationParts_GetAnimationDuration_m3980949642(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UITween.CurrentAnimation::SetCurrentAnimPos(UnityEngine.AnimationCurve,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void CurrentAnimation_SetCurrentAnimPos_m2494854247 (CurrentAnimation_t3872301071 * __this, AnimationCurve_t3667593487 * ___currentAnimationCurvePos0, Vector3_t4282066566  ___currentStartPos1, Vector3_t4282066566  ___currentEndPos2, const MethodInfo* method)
{
	{
		AnimationCurve_t3667593487 * L_0 = ___currentAnimationCurvePos0;
		__this->set_currentAnimationCurvePos_3(L_0);
		Vector3_t4282066566  L_1 = ___currentStartPos1;
		__this->set_currentStartPos_4(L_1);
		Vector3_t4282066566  L_2 = ___currentEndPos2;
		__this->set_currentEndPos_5(L_2);
		return;
	}
}
// System.Void UITween.CurrentAnimation::MoveAnimation(UnityEngine.RectTransform,System.Single)
extern "C"  void CurrentAnimation_MoveAnimation_m4193172284 (CurrentAnimation_t3872301071 * __this, RectTransform_t972643934 * ____rectTransform0, float ____counterTween1, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		AnimationCurve_t3667593487 * L_0 = __this->get_currentAnimationCurvePos_3();
		float L_1 = ____counterTween1;
		NullCheck(L_0);
		float L_2 = AnimationCurve_Evaluate_m547727012(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t4282066566  L_3 = __this->get_currentEndPos_5();
		Vector3_t4282066566  L_4 = __this->get_currentStartPos_4();
		Vector3_t4282066566  L_5 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = V_0;
		Vector3_t4282066566  L_7 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		RectTransform_t972643934 * L_8 = ____rectTransform0;
		Vector3_t4282066566  L_9 = __this->get_currentStartPos_4();
		Vector3_t4282066566  L_10 = V_1;
		Vector3_t4282066566  L_11 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		Vector2_t4282066565  L_12 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_8);
		RectTransform_set_anchoredPosition_m1498949997(L_8, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITween.CurrentAnimation::SetCurrentAnimScale(UnityEngine.AnimationCurve,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void CurrentAnimation_SetCurrentAnimScale_m4239404253 (CurrentAnimation_t3872301071 * __this, AnimationCurve_t3667593487 * ___currentAnimationCurveScale0, Vector3_t4282066566  ___currentStartScale1, Vector3_t4282066566  ___currentEndScale2, const MethodInfo* method)
{
	{
		AnimationCurve_t3667593487 * L_0 = ___currentAnimationCurveScale0;
		__this->set_currentAnimationCurveScale_6(L_0);
		Vector3_t4282066566  L_1 = ___currentStartScale1;
		__this->set_currentStartScale_7(L_1);
		Vector3_t4282066566  L_2 = ___currentEndScale2;
		__this->set_currentEndScale_8(L_2);
		return;
	}
}
// System.Void UITween.CurrentAnimation::ScaleAnimation(UnityEngine.RectTransform,System.Single)
extern "C"  void CurrentAnimation_ScaleAnimation_m2551231265 (CurrentAnimation_t3872301071 * __this, RectTransform_t972643934 * ____rectTransform0, float ____counterTween1, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		AnimationCurve_t3667593487 * L_0 = __this->get_currentAnimationCurveScale_6();
		float L_1 = ____counterTween1;
		NullCheck(L_0);
		float L_2 = AnimationCurve_Evaluate_m547727012(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t4282066566  L_3 = __this->get_currentEndScale_8();
		Vector3_t4282066566  L_4 = __this->get_currentStartScale_7();
		Vector3_t4282066566  L_5 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = V_0;
		Vector3_t4282066566  L_7 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		RectTransform_t972643934 * L_8 = ____rectTransform0;
		Vector3_t4282066566  L_9 = __this->get_currentStartScale_7();
		Vector3_t4282066566  L_10 = V_1;
		Vector3_t4282066566  L_11 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_localScale_m310756934(L_8, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITween.CurrentAnimation::SetCurrentAnimRot(UnityEngine.AnimationCurve,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void CurrentAnimation_SetCurrentAnimRot_m1281190506 (CurrentAnimation_t3872301071 * __this, AnimationCurve_t3667593487 * ___currentAnimationCurveRot0, Vector3_t4282066566  ___currentStartRot1, Vector3_t4282066566  ___currentEndRot2, const MethodInfo* method)
{
	{
		AnimationCurve_t3667593487 * L_0 = ___currentAnimationCurveRot0;
		__this->set_currentAnimationCurveRot_9(L_0);
		Vector3_t4282066566  L_1 = ___currentStartRot1;
		__this->set_currentStartRot_10(L_1);
		Vector3_t4282066566  L_2 = ___currentEndRot2;
		__this->set_currentEndRot_11(L_2);
		return;
	}
}
// System.Void UITween.CurrentAnimation::RotateAnimation(UnityEngine.RectTransform,System.Single)
extern "C"  void CurrentAnimation_RotateAnimation_m3512040390 (CurrentAnimation_t3872301071 * __this, RectTransform_t972643934 * ____rectTransform0, float ____counterTween1, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		AnimationCurve_t3667593487 * L_0 = __this->get_currentAnimationCurveRot_9();
		float L_1 = ____counterTween1;
		NullCheck(L_0);
		float L_2 = AnimationCurve_Evaluate_m547727012(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t4282066566  L_3 = __this->get_currentEndRot_11();
		Vector3_t4282066566  L_4 = __this->get_currentStartRot_10();
		Vector3_t4282066566  L_5 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = V_0;
		Vector3_t4282066566  L_7 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		RectTransform_t972643934 * L_8 = ____rectTransform0;
		Vector3_t4282066566  L_9 = __this->get_currentStartRot_10();
		Vector3_t4282066566  L_10 = V_1;
		Vector3_t4282066566  L_11 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_localEulerAngles_m3898859559(L_8, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITween.CurrentAnimation::SetFadeAnimation(System.Single,System.Single)
extern "C"  void CurrentAnimation_SetFadeAnimation_m1879468991 (CurrentAnimation_t3872301071 * __this, float ___startAlphaValue0, float ___endAlphaValue1, const MethodInfo* method)
{
	{
		float L_0 = ___startAlphaValue0;
		__this->set_startAlphaValue_12(L_0);
		float L_1 = ___endAlphaValue1;
		__this->set_endAlphaValue_13(L_1);
		return;
	}
}
// System.Void UITween.CurrentAnimation::SetAlphaValue(UnityEngine.Transform,System.Single)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMaskableGraphic_t3186046376_m2118237702_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisReferencedFrom_t1396252131_m2689967426_MethodInfo_var;
extern const uint32_t CurrentAnimation_SetAlphaValue_m56629110_MetadataUsageId;
extern "C"  void CurrentAnimation_SetAlphaValue_m56629110 (CurrentAnimation_t3872301071 * __this, Transform_t1659122786 * ____objectToSetAlpha0, float ____counterTween1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CurrentAnimation_SetAlphaValue_m56629110_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MaskableGraphic_t3186046376 * V_0 = NULL;
	Color_t4194546905  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	Transform_t1659122786 * V_3 = NULL;
	{
		Transform_t1659122786 * L_0 = ____objectToSetAlpha0;
		NullCheck(L_0);
		MaskableGraphic_t3186046376 * L_1 = Component_GetComponent_TisMaskableGraphic_t3186046376_m2118237702(L_0, /*hidden argument*/Component_GetComponent_TisMaskableGraphic_t3186046376_m2118237702_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0059;
		}
	}
	{
		Transform_t1659122786 * L_3 = ____objectToSetAlpha0;
		NullCheck(L_3);
		MaskableGraphic_t3186046376 * L_4 = Component_GetComponent_TisMaskableGraphic_t3186046376_m2118237702(L_3, /*hidden argument*/Component_GetComponent_TisMaskableGraphic_t3186046376_m2118237702_MethodInfo_var);
		V_0 = L_4;
		MaskableGraphic_t3186046376 * L_5 = V_0;
		NullCheck(L_5);
		Color_t4194546905  L_6 = VirtFuncInvoker0< Color_t4194546905  >::Invoke(21 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_5);
		V_1 = L_6;
		float L_7 = ____counterTween1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_8 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_7, (0.0f), (1.0f), /*hidden argument*/NULL);
		____counterTween1 = L_8;
		float L_9 = __this->get_startAlphaValue_12();
		float L_10 = __this->get_endAlphaValue_13();
		float L_11 = __this->get_startAlphaValue_12();
		float L_12 = ____counterTween1;
		float L_13 = fabsf(((float)((float)L_9+(float)((float)((float)((float)((float)L_10-(float)L_11))*(float)L_12)))));
		(&V_1)->set_a_3(L_13);
		MaskableGraphic_t3186046376 * L_14 = V_0;
		Color_t4194546905  L_15 = V_1;
		NullCheck(L_14);
		VirtActionInvoker1< Color_t4194546905  >::Invoke(22 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_14, L_15);
	}

IL_0059:
	{
		Transform_t1659122786 * L_16 = ____objectToSetAlpha0;
		NullCheck(L_16);
		int32_t L_17 = Transform_get_childCount_m2107810675(L_16, /*hidden argument*/NULL);
		if ((((int32_t)L_17) <= ((int32_t)0)))
		{
			goto IL_00c1;
		}
	}
	{
		V_2 = 0;
		goto IL_00b5;
	}

IL_006c:
	{
		Transform_t1659122786 * L_18 = ____objectToSetAlpha0;
		int32_t L_19 = V_2;
		NullCheck(L_18);
		Transform_t1659122786 * L_20 = Transform_GetChild_m4040462992(L_18, L_19, /*hidden argument*/NULL);
		V_3 = L_20;
		Transform_t1659122786 * L_21 = V_3;
		NullCheck(L_21);
		GameObject_t3674682005 * L_22 = Component_get_gameObject_m1170635899(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		bool L_23 = GameObject_get_activeSelf_m3858025161(L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00b1;
		}
	}
	{
		Transform_t1659122786 * L_24 = V_3;
		NullCheck(L_24);
		ReferencedFrom_t1396252131 * L_25 = Component_GetComponent_TisReferencedFrom_t1396252131_m2689967426(L_24, /*hidden argument*/Component_GetComponent_TisReferencedFrom_t1396252131_m2689967426_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_26 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00a9;
		}
	}
	{
		AnimationParts_t4290477440 * L_27 = __this->get_animationPart_0();
		NullCheck(L_27);
		FadePropetiesAnim_t145139664 * L_28 = L_27->get_FadePropetiesAnim_3();
		NullCheck(L_28);
		bool L_29 = FadePropetiesAnim_IsFadeOverrideEnabled_m1365175625(L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00b1;
		}
	}

IL_00a9:
	{
		Transform_t1659122786 * L_30 = V_3;
		float L_31 = ____counterTween1;
		CurrentAnimation_SetAlphaValue_m56629110(__this, L_30, L_31, /*hidden argument*/NULL);
	}

IL_00b1:
	{
		int32_t L_32 = V_2;
		V_2 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00b5:
	{
		int32_t L_33 = V_2;
		Transform_t1659122786 * L_34 = ____objectToSetAlpha0;
		NullCheck(L_34);
		int32_t L_35 = Transform_get_childCount_m2107810675(L_34, /*hidden argument*/NULL);
		if ((((int32_t)L_33) < ((int32_t)L_35)))
		{
			goto IL_006c;
		}
	}

IL_00c1:
	{
		return;
	}
}
// System.Void UITween.FadePropetiesAnim::.ctor()
extern "C"  void FadePropetiesAnim__ctor_m3814006280 (FadePropetiesAnim_t145139664 * __this, const MethodInfo* method)
{
	{
		__this->set_endFade_3((1.0f));
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITween.FadePropetiesAnim::SetFadeEnable(System.Boolean)
extern "C"  void FadePropetiesAnim_SetFadeEnable_m1937066046 (FadePropetiesAnim_t145139664 * __this, bool ___enabled0, const MethodInfo* method)
{
	{
		bool L_0 = ___enabled0;
		__this->set_fadeInOutEnabled_0(L_0);
		return;
	}
}
// System.Void UITween.FadePropetiesAnim::SetFadeValues(System.Single,System.Single)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral832234380;
extern const uint32_t FadePropetiesAnim_SetFadeValues_m1935912618_MetadataUsageId;
extern "C"  void FadePropetiesAnim_SetFadeValues_m1935912618 (FadePropetiesAnim_t145139664 * __this, float ___startFade0, float ___endFade1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FadePropetiesAnim_SetFadeValues_m1935912618_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___endFade1;
		float L_1 = ___startFade0;
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral832234380, /*hidden argument*/NULL);
		return;
	}

IL_0012:
	{
		float L_2 = ___startFade0;
		__this->set_startFade_2(L_2);
		float L_3 = ___endFade1;
		__this->set_endFade_3(L_3);
		return;
	}
}
// System.Single UITween.FadePropetiesAnim::GetStartFadeValue()
extern "C"  float FadePropetiesAnim_GetStartFadeValue_m562682939 (FadePropetiesAnim_t145139664 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_startFade_2();
		return L_0;
	}
}
// System.Single UITween.FadePropetiesAnim::GetEndFadeValue()
extern "C"  float FadePropetiesAnim_GetEndFadeValue_m2770866146 (FadePropetiesAnim_t145139664 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_endFade_3();
		return L_0;
	}
}
// System.Boolean UITween.FadePropetiesAnim::IsFadeEnabled()
extern "C"  bool FadePropetiesAnim_IsFadeEnabled_m3212297909 (FadePropetiesAnim_t145139664 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_fadeInOutEnabled_0();
		return L_0;
	}
}
// System.Void UITween.FadePropetiesAnim::SetFadeOverride(System.Boolean)
extern "C"  void FadePropetiesAnim_SetFadeOverride_m3099760583 (FadePropetiesAnim_t145139664 * __this, bool ___enabled0, const MethodInfo* method)
{
	{
		bool L_0 = ___enabled0;
		__this->set_fadeOverride_1(L_0);
		return;
	}
}
// System.Boolean UITween.FadePropetiesAnim::IsFadeOverrideEnabled()
extern "C"  bool FadePropetiesAnim_IsFadeOverrideEnabled_m1365175625 (FadePropetiesAnim_t145139664 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_fadeOverride_1();
		return L_0;
	}
}
// System.Void UITween.PositionPropetiesAnim::.ctor()
extern "C"  void PositionPropetiesAnim__ctor_m1322731893 (PositionPropetiesAnim_t3297200579 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITween.PositionPropetiesAnim::SetPositionEnable(System.Boolean)
extern "C"  void PositionPropetiesAnim_SetPositionEnable_m2258509016 (PositionPropetiesAnim_t3297200579 * __this, bool ___enabled0, const MethodInfo* method)
{
	{
		bool L_0 = ___enabled0;
		__this->set_positionEnabled_0(L_0);
		return;
	}
}
// System.Boolean UITween.PositionPropetiesAnim::IsPositionEnabled()
extern "C"  bool PositionPropetiesAnim_IsPositionEnabled_m2317462805 (PositionPropetiesAnim_t3297200579 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_positionEnabled_0();
		return L_0;
	}
}
// System.Void UITween.PositionPropetiesAnim::SetPosStart(UnityEngine.Vector3,UnityEngine.RectTransform)
extern "C"  void PositionPropetiesAnim_SetPosStart_m867218417 (PositionPropetiesAnim_t3297200579 * __this, Vector3_t4282066566  ___StartPos0, RectTransform_t972643934 * ___rectTr1, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___StartPos0;
		__this->set_StartPos_3(L_0);
		return;
	}
}
// System.Void UITween.PositionPropetiesAnim::SetPosEnd(UnityEngine.Vector3,UnityEngine.Transform)
extern "C"  void PositionPropetiesAnim_SetPosEnd_m272049364 (PositionPropetiesAnim_t3297200579 * __this, Vector3_t4282066566  ___EndPos0, Transform_t1659122786 * ___rectTr1, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___EndPos0;
		__this->set_EndPos_4(L_0);
		return;
	}
}
// System.Void UITween.PositionPropetiesAnim::SetAniamtionsCurve(UnityEngine.AnimationCurve,UnityEngine.AnimationCurve)
extern "C"  void PositionPropetiesAnim_SetAniamtionsCurve_m4194602913 (PositionPropetiesAnim_t3297200579 * __this, AnimationCurve_t3667593487 * ___EntryTween0, AnimationCurve_t3667593487 * ___ExitTween1, const MethodInfo* method)
{
	{
		AnimationCurve_t3667593487 * L_0 = ___EntryTween0;
		__this->set_TweenCurveEnterPos_1(L_0);
		AnimationCurve_t3667593487 * L_1 = ___ExitTween1;
		__this->set_TweenCurveExitPos_2(L_1);
		return;
	}
}
// System.Void UITween.RotationPropetiesAnim::.ctor()
extern "C"  void RotationPropetiesAnim__ctor_m1987650058 (RotationPropetiesAnim_t2262074766 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITween.RotationPropetiesAnim::SetRotationEnable(System.Boolean)
extern "C"  void RotationPropetiesAnim_SetRotationEnable_m1626508738 (RotationPropetiesAnim_t2262074766 * __this, bool ___enabled0, const MethodInfo* method)
{
	{
		bool L_0 = ___enabled0;
		__this->set_rotationEnabled_0(L_0);
		return;
	}
}
// System.Boolean UITween.RotationPropetiesAnim::IsRotationEnabled()
extern "C"  bool RotationPropetiesAnim_IsRotationEnabled_m2487428981 (RotationPropetiesAnim_t2262074766 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_rotationEnabled_0();
		return L_0;
	}
}
// System.Void UITween.RotationPropetiesAnim::SetAniamtionsCurve(UnityEngine.AnimationCurve,UnityEngine.AnimationCurve)
extern "C"  void RotationPropetiesAnim_SetAniamtionsCurve_m1567452918 (RotationPropetiesAnim_t2262074766 * __this, AnimationCurve_t3667593487 * ___EntryTween0, AnimationCurve_t3667593487 * ___ExitTween1, const MethodInfo* method)
{
	{
		AnimationCurve_t3667593487 * L_0 = ___EntryTween0;
		__this->set_TweenCurveEnterRot_1(L_0);
		AnimationCurve_t3667593487 * L_1 = ___ExitTween1;
		__this->set_TweenCurveExitRot_2(L_1);
		return;
	}
}
// System.Void UITween.ScalePropetiesAnim::.ctor()
extern "C"  void ScalePropetiesAnim__ctor_m593687544 (ScalePropetiesAnim_t205839632 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITween.ScalePropetiesAnim::SetScaleEnable(System.Boolean)
extern "C"  void ScalePropetiesAnim_SetScaleEnable_m258590894 (ScalePropetiesAnim_t205839632 * __this, bool ___enabled0, const MethodInfo* method)
{
	{
		bool L_0 = ___enabled0;
		__this->set_scaleEnabled_0(L_0);
		return;
	}
}
// System.Boolean UITween.ScalePropetiesAnim::IsScaleEnabled()
extern "C"  bool ScalePropetiesAnim_IsScaleEnabled_m1438996961 (ScalePropetiesAnim_t205839632 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_scaleEnabled_0();
		return L_0;
	}
}
// System.Void UITween.ScalePropetiesAnim::SetAniamtionsCurve(UnityEngine.AnimationCurve,UnityEngine.AnimationCurve)
extern "C"  void ScalePropetiesAnim_SetAniamtionsCurve_m4026535268 (ScalePropetiesAnim_t205839632 * __this, AnimationCurve_t3667593487 * ___EntryTween0, AnimationCurve_t3667593487 * ___ExitTween1, const MethodInfo* method)
{
	{
		AnimationCurve_t3667593487 * L_0 = ___EntryTween0;
		__this->set_TweenCurveEnterScale_1(L_0);
		AnimationCurve_t3667593487 * L_1 = ___ExitTween1;
		__this->set_TweenCurveExitScale_2(L_1);
		return;
	}
}
// System.Void Vinetas::.ctor()
extern "C"  void Vinetas__ctor_m525007695 (Vinetas_t2126676892 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vinetas::Start()
extern "C"  void Vinetas_Start_m3767112783 (Vinetas_t2126676892 * __this, const MethodInfo* method)
{
	{
		SpriteU5BU5D_t2761310900* L_0 = __this->get_vinetas_21();
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		int32_t L_1 = 0;
		Sprite_t3199167241 * L_2 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1));
		__this->set_camino_2(L_2);
		SpriteU5BU5D_t2761310900* L_3 = __this->get_vinetas_21();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		int32_t L_4 = 1;
		Sprite_t3199167241 * L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		__this->set_bosque_3(L_5);
		SpriteU5BU5D_t2761310900* L_6 = __this->get_vinetas_21();
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		int32_t L_7 = 2;
		Sprite_t3199167241 * L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		__this->set_casa_4(L_8);
		SpriteU5BU5D_t2761310900* L_9 = __this->get_vinetas_21();
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 3);
		int32_t L_10 = 3;
		Sprite_t3199167241 * L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		__this->set_puerta_5(L_11);
		SpriteU5BU5D_t2761310900* L_12 = __this->get_vinetas_21();
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		int32_t L_13 = 4;
		Sprite_t3199167241 * L_14 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		__this->set_felpudo_6(L_14);
		SpriteU5BU5D_t2761310900* L_15 = __this->get_vinetas_21();
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 5);
		int32_t L_16 = 5;
		Sprite_t3199167241 * L_17 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		__this->set_llave_7(L_17);
		SpriteU5BU5D_t2761310900* L_18 = __this->get_vinetas_21();
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 6);
		int32_t L_19 = 6;
		Sprite_t3199167241 * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		__this->set_cerradura_8(L_20);
		SpriteU5BU5D_t2761310900* L_21 = __this->get_vinetas_21();
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 7);
		int32_t L_22 = 7;
		Sprite_t3199167241 * L_23 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		__this->set_entrada_9(L_23);
		SpriteU5BU5D_t2761310900* L_24 = __this->get_vinetas_21();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 8);
		int32_t L_25 = 8;
		Sprite_t3199167241 * L_26 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		__this->set_reloj_10(L_26);
		SpriteU5BU5D_t2761310900* L_27 = __this->get_vinetas_21();
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, ((int32_t)9));
		int32_t L_28 = ((int32_t)9);
		Sprite_t3199167241 * L_29 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		__this->set_escaleras_11(L_29);
		SpriteU5BU5D_t2761310900* L_30 = __this->get_vinetas_21();
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, ((int32_t)10));
		int32_t L_31 = ((int32_t)10);
		Sprite_t3199167241 * L_32 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		__this->set_alcoba_12(L_32);
		SpriteU5BU5D_t2761310900* L_33 = __this->get_vinetas_21();
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, ((int32_t)11));
		int32_t L_34 = ((int32_t)11);
		Sprite_t3199167241 * L_35 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		__this->set_lienzo_13(L_35);
		SpriteU5BU5D_t2761310900* L_36 = __this->get_vinetas_21();
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)12));
		int32_t L_37 = ((int32_t)12);
		Sprite_t3199167241 * L_38 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		__this->set_metafisica_14(L_38);
		SpriteU5BU5D_t2761310900* L_39 = __this->get_vinetas_21();
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, ((int32_t)13));
		int32_t L_40 = ((int32_t)13);
		Sprite_t3199167241 * L_41 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		__this->set_tunel_15(L_41);
		SpriteU5BU5D_t2761310900* L_42 = __this->get_vinetas_21();
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, ((int32_t)14));
		int32_t L_43 = ((int32_t)14);
		Sprite_t3199167241 * L_44 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		__this->set_escalera_16(L_44);
		SpriteU5BU5D_t2761310900* L_45 = __this->get_vinetas_21();
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, ((int32_t)15));
		int32_t L_46 = ((int32_t)15);
		Sprite_t3199167241 * L_47 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
		__this->set_sotano_17(L_47);
		SpriteU5BU5D_t2761310900* L_48 = __this->get_vinetas_21();
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, ((int32_t)16));
		int32_t L_49 = ((int32_t)16);
		Sprite_t3199167241 * L_50 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_49));
		__this->set_caverna_18(L_50);
		SpriteU5BU5D_t2761310900* L_51 = __this->get_vinetas_21();
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, ((int32_t)17));
		int32_t L_52 = ((int32_t)17);
		Sprite_t3199167241 * L_53 = (L_51)->GetAt(static_cast<il2cpp_array_size_t>(L_52));
		__this->set_master_19(L_53);
		SpriteU5BU5D_t2761310900* L_54 = __this->get_vinetas_21();
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, ((int32_t)18));
		int32_t L_55 = ((int32_t)18);
		Sprite_t3199167241 * L_56 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
		__this->set_piedra_20(L_56);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
