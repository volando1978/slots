﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Combinaciones
struct Combinaciones_t3637091598;
// System.String
struct String_t;
// UnityEngine.Sprite
struct Sprite_t3199167241;

#include "codegen/il2cpp-codegen.h"

// System.Void Combinaciones::.ctor()
extern "C"  void Combinaciones__ctor_m1182110749 (Combinaciones_t3637091598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Combinaciones::Start()
extern "C"  void Combinaciones_Start_m129248541 (Combinaciones_t3637091598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Combinaciones::getText(System.Int32)
extern "C"  String_t* Combinaciones_getText_m3595448498 (Combinaciones_t3637091598 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite Combinaciones::getPic(System.Int32)
extern "C"  Sprite_t3199167241 * Combinaciones_getPic_m3530152747 (Combinaciones_t3637091598 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite Combinaciones::getChar(System.Int32)
extern "C"  Sprite_t3199167241 * Combinaciones_getChar_m294333753 (Combinaciones_t3637091598 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
