﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// Slots
struct Slots_t79980053;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Slots/<BringBackHUD>c__Iterator7
struct  U3CBringBackHUDU3Ec__Iterator7_t111350788  : public Il2CppObject
{
public:
	// System.Single Slots/<BringBackHUD>c__Iterator7::<start>__0
	float ___U3CstartU3E__0_0;
	// System.Single Slots/<BringBackHUD>c__Iterator7::<startTime>__1
	float ___U3CstartTimeU3E__1_1;
	// System.Single Slots/<BringBackHUD>c__Iterator7::<overTime>__2
	float ___U3CoverTimeU3E__2_2;
	// System.Single Slots/<BringBackHUD>c__Iterator7::<movement>__3
	float ___U3CmovementU3E__3_3;
	// UnityEngine.Vector2 Slots/<BringBackHUD>c__Iterator7::<pos>__4
	Vector2_t4282066565  ___U3CposU3E__4_4;
	// System.Int32 Slots/<BringBackHUD>c__Iterator7::$PC
	int32_t ___U24PC_5;
	// System.Object Slots/<BringBackHUD>c__Iterator7::$current
	Il2CppObject * ___U24current_6;
	// Slots Slots/<BringBackHUD>c__Iterator7::<>f__this
	Slots_t79980053 * ___U3CU3Ef__this_7;

public:
	inline static int32_t get_offset_of_U3CstartU3E__0_0() { return static_cast<int32_t>(offsetof(U3CBringBackHUDU3Ec__Iterator7_t111350788, ___U3CstartU3E__0_0)); }
	inline float get_U3CstartU3E__0_0() const { return ___U3CstartU3E__0_0; }
	inline float* get_address_of_U3CstartU3E__0_0() { return &___U3CstartU3E__0_0; }
	inline void set_U3CstartU3E__0_0(float value)
	{
		___U3CstartU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E__1_1() { return static_cast<int32_t>(offsetof(U3CBringBackHUDU3Ec__Iterator7_t111350788, ___U3CstartTimeU3E__1_1)); }
	inline float get_U3CstartTimeU3E__1_1() const { return ___U3CstartTimeU3E__1_1; }
	inline float* get_address_of_U3CstartTimeU3E__1_1() { return &___U3CstartTimeU3E__1_1; }
	inline void set_U3CstartTimeU3E__1_1(float value)
	{
		___U3CstartTimeU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CoverTimeU3E__2_2() { return static_cast<int32_t>(offsetof(U3CBringBackHUDU3Ec__Iterator7_t111350788, ___U3CoverTimeU3E__2_2)); }
	inline float get_U3CoverTimeU3E__2_2() const { return ___U3CoverTimeU3E__2_2; }
	inline float* get_address_of_U3CoverTimeU3E__2_2() { return &___U3CoverTimeU3E__2_2; }
	inline void set_U3CoverTimeU3E__2_2(float value)
	{
		___U3CoverTimeU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CmovementU3E__3_3() { return static_cast<int32_t>(offsetof(U3CBringBackHUDU3Ec__Iterator7_t111350788, ___U3CmovementU3E__3_3)); }
	inline float get_U3CmovementU3E__3_3() const { return ___U3CmovementU3E__3_3; }
	inline float* get_address_of_U3CmovementU3E__3_3() { return &___U3CmovementU3E__3_3; }
	inline void set_U3CmovementU3E__3_3(float value)
	{
		___U3CmovementU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U3CposU3E__4_4() { return static_cast<int32_t>(offsetof(U3CBringBackHUDU3Ec__Iterator7_t111350788, ___U3CposU3E__4_4)); }
	inline Vector2_t4282066565  get_U3CposU3E__4_4() const { return ___U3CposU3E__4_4; }
	inline Vector2_t4282066565 * get_address_of_U3CposU3E__4_4() { return &___U3CposU3E__4_4; }
	inline void set_U3CposU3E__4_4(Vector2_t4282066565  value)
	{
		___U3CposU3E__4_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CBringBackHUDU3Ec__Iterator7_t111350788, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CBringBackHUDU3Ec__Iterator7_t111350788, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_7() { return static_cast<int32_t>(offsetof(U3CBringBackHUDU3Ec__Iterator7_t111350788, ___U3CU3Ef__this_7)); }
	inline Slots_t79980053 * get_U3CU3Ef__this_7() const { return ___U3CU3Ef__this_7; }
	inline Slots_t79980053 ** get_address_of_U3CU3Ef__this_7() { return &___U3CU3Ef__this_7; }
	inline void set_U3CU3Ef__this_7(Slots_t79980053 * value)
	{
		___U3CU3Ef__this_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
