﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ActivateOnEnable/<Start>c__IteratorB
struct U3CStartU3Ec__IteratorB_t1046475311;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ActivateOnEnable/<Start>c__IteratorB::.ctor()
extern "C"  void U3CStartU3Ec__IteratorB__ctor_m2820488524 (U3CStartU3Ec__IteratorB_t1046475311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ActivateOnEnable/<Start>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m937595280 (U3CStartU3Ec__IteratorB_t1046475311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ActivateOnEnable/<Start>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m1359590692 (U3CStartU3Ec__IteratorB_t1046475311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ActivateOnEnable/<Start>c__IteratorB::MoveNext()
extern "C"  bool U3CStartU3Ec__IteratorB_MoveNext_m1328487184 (U3CStartU3Ec__IteratorB_t1046475311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActivateOnEnable/<Start>c__IteratorB::Dispose()
extern "C"  void U3CStartU3Ec__IteratorB_Dispose_m263390857 (U3CStartU3Ec__IteratorB_t1046475311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActivateOnEnable/<Start>c__IteratorB::Reset()
extern "C"  void U3CStartU3Ec__IteratorB_Reset_m466921465 (U3CStartU3Ec__IteratorB_t1046475311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
