﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PriceHUD
struct PriceHUD_t3182648078;
// CreditsHUD
struct CreditsHUD_t1430045213;
// UnityEngine.AudioSource
struct AudioSource_t1740077639;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// UnityEngine.AudioClip
struct AudioClip_t794140988;
// UnityEngine.Canvas
struct Canvas_t2727140764;
// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// Combinaciones
struct Combinaciones_t3637091598;
// CounterChecking
struct CounterChecking_t39316438;
// System.Boolean[]
struct BooleanU5BU5D_t3456302923;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2761310900;
// Vinetas
struct Vinetas_t2126676892;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_Slots_State3376307191.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Slots
struct  Slots_t79980053  : public MonoBehaviour_t667441552
{
public:
	// PriceHUD Slots::priceHUD
	PriceHUD_t3182648078 * ___priceHUD_2;
	// CreditsHUD Slots::creditsHUD
	CreditsHUD_t1430045213 * ___creditsHUD_3;
	// UnityEngine.AudioSource Slots::audio
	AudioSource_t1740077639 * ___audio_4;
	// UnityEngine.GameObject Slots::SlotpParent
	GameObject_t3674682005 * ___SlotpParent_5;
	// UnityEngine.GameObject Slots::slot1
	GameObject_t3674682005 * ___slot1_6;
	// UnityEngine.GameObject Slots::slot2
	GameObject_t3674682005 * ___slot2_7;
	// UnityEngine.GameObject Slots::slot3
	GameObject_t3674682005 * ___slot3_8;
	// UnityEngine.GameObject Slots::HUD
	GameObject_t3674682005 * ___HUD_9;
	// UnityEngine.GameObject Slots::HitButton
	GameObject_t3674682005 * ___HitButton_10;
	// UnityEngine.GameObject Slots::ThreeButtons
	GameObject_t3674682005 * ___ThreeButtons_11;
	// UnityEngine.GameObject Slots::inventario
	GameObject_t3674682005 * ___inventario_12;
	// UnityEngine.GameObject Slots::StartButton
	GameObject_t3674682005 * ___StartButton_13;
	// UnityEngine.GameObject Slots::Audio
	GameObject_t3674682005 * ___Audio_14;
	// UnityEngine.GameObject Slots::Caption
	GameObject_t3674682005 * ___Caption_15;
	// UnityEngine.GameObject Slots::PicChar
	GameObject_t3674682005 * ___PicChar_16;
	// UnityEngine.GameObject Slots::PicCanvas
	GameObject_t3674682005 * ___PicCanvas_17;
	// UnityEngine.GameObject Slots::Tabla
	GameObject_t3674682005 * ___Tabla_18;
	// UnityEngine.GameObject Slots::HitStopText
	GameObject_t3674682005 * ___HitStopText_19;
	// UnityEngine.GameObject Slots::counterScreen1
	GameObject_t3674682005 * ___counterScreen1_20;
	// UnityEngine.GameObject Slots::counterScreen2
	GameObject_t3674682005 * ___counterScreen2_21;
	// UnityEngine.GameObject Slots::counterScreen3
	GameObject_t3674682005 * ___counterScreen3_22;
	// UnityEngine.GameObject Slots::counterChar
	GameObject_t3674682005 * ___counterChar_23;
	// UnityEngine.GameObject Slots::panelInventario
	GameObject_t3674682005 * ___panelInventario_24;
	// UnityEngine.GameObject Slots::panelGame
	GameObject_t3674682005 * ___panelGame_25;
	// UnityEngine.GameObject[] Slots::counters
	GameObjectU5BU5D_t2662109048* ___counters_26;
	// UnityEngine.GameObject[] Slots::slotsG
	GameObjectU5BU5D_t2662109048* ___slotsG_27;
	// UnityEngine.AudioClip Slots::win
	AudioClip_t794140988 * ___win_28;
	// UnityEngine.AudioClip Slots::winBig
	AudioClip_t794140988 * ___winBig_29;
	// UnityEngine.AudioClip Slots::result
	AudioClip_t794140988 * ___result_30;
	// UnityEngine.AudioClip Slots::start
	AudioClip_t794140988 * ___start_31;
	// UnityEngine.AudioClip Slots::hit
	AudioClip_t794140988 * ___hit_32;
	// UnityEngine.AudioClip Slots::coin
	AudioClip_t794140988 * ___coin_33;
	// UnityEngine.Color Slots::blueFrame
	Color_t4194546905  ___blueFrame_34;
	// UnityEngine.Canvas Slots::canvas
	Canvas_t2727140764 * ___canvas_35;
	// UnityEngine.UI.Text Slots::caption
	Text_t9039225 * ___caption_36;
	// UnityEngine.Sprite Slots::picChar
	Sprite_t3199167241 * ___picChar_37;
	// UnityEngine.Sprite Slots::picCanvas
	Sprite_t3199167241 * ___picCanvas_38;
	// Combinaciones Slots::combinaciones
	Combinaciones_t3637091598 * ___combinaciones_39;
	// CounterChecking Slots::counterChecking
	CounterChecking_t39316438 * ___counterChecking_40;
	// System.Single Slots::currentTimer
	float ___currentTimer_41;
	// System.Single Slots::timer
	float ___timer_42;
	// System.Single Slots::timeHightlight
	float ___timeHightlight_43;
	// System.Boolean Slots::rolling
	bool ___rolling_44;
	// System.Boolean Slots::choice
	bool ___choice_45;
	// System.Boolean[] Slots::frozenSlots
	BooleanU5BU5D_t3456302923* ___frozenSlots_46;
	// System.Boolean Slots::frozenRound
	bool ___frozenRound_47;
	// UnityEngine.Sprite[] Slots::slots
	SpriteU5BU5D_t2761310900* ___slots_48;
	// Vinetas Slots::vi
	Vinetas_t2126676892 * ___vi_49;
	// System.Int32 Slots::a
	int32_t ___a_50;
	// System.Int32 Slots::b
	int32_t ___b_51;
	// System.Int32 Slots::c
	int32_t ___c_52;
	// System.Int32 Slots::tempFrozen
	int32_t ___tempFrozen_53;
	// System.Int32 Slots::price
	int32_t ___price_54;
	// System.Int32 Slots::credits
	int32_t ___credits_55;
	// System.Single Slots::heightHUD
	float ___heightHUD_56;
	// Slots/State Slots::currentState
	int32_t ___currentState_58;

public:
	inline static int32_t get_offset_of_priceHUD_2() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___priceHUD_2)); }
	inline PriceHUD_t3182648078 * get_priceHUD_2() const { return ___priceHUD_2; }
	inline PriceHUD_t3182648078 ** get_address_of_priceHUD_2() { return &___priceHUD_2; }
	inline void set_priceHUD_2(PriceHUD_t3182648078 * value)
	{
		___priceHUD_2 = value;
		Il2CppCodeGenWriteBarrier(&___priceHUD_2, value);
	}

	inline static int32_t get_offset_of_creditsHUD_3() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___creditsHUD_3)); }
	inline CreditsHUD_t1430045213 * get_creditsHUD_3() const { return ___creditsHUD_3; }
	inline CreditsHUD_t1430045213 ** get_address_of_creditsHUD_3() { return &___creditsHUD_3; }
	inline void set_creditsHUD_3(CreditsHUD_t1430045213 * value)
	{
		___creditsHUD_3 = value;
		Il2CppCodeGenWriteBarrier(&___creditsHUD_3, value);
	}

	inline static int32_t get_offset_of_audio_4() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___audio_4)); }
	inline AudioSource_t1740077639 * get_audio_4() const { return ___audio_4; }
	inline AudioSource_t1740077639 ** get_address_of_audio_4() { return &___audio_4; }
	inline void set_audio_4(AudioSource_t1740077639 * value)
	{
		___audio_4 = value;
		Il2CppCodeGenWriteBarrier(&___audio_4, value);
	}

	inline static int32_t get_offset_of_SlotpParent_5() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___SlotpParent_5)); }
	inline GameObject_t3674682005 * get_SlotpParent_5() const { return ___SlotpParent_5; }
	inline GameObject_t3674682005 ** get_address_of_SlotpParent_5() { return &___SlotpParent_5; }
	inline void set_SlotpParent_5(GameObject_t3674682005 * value)
	{
		___SlotpParent_5 = value;
		Il2CppCodeGenWriteBarrier(&___SlotpParent_5, value);
	}

	inline static int32_t get_offset_of_slot1_6() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___slot1_6)); }
	inline GameObject_t3674682005 * get_slot1_6() const { return ___slot1_6; }
	inline GameObject_t3674682005 ** get_address_of_slot1_6() { return &___slot1_6; }
	inline void set_slot1_6(GameObject_t3674682005 * value)
	{
		___slot1_6 = value;
		Il2CppCodeGenWriteBarrier(&___slot1_6, value);
	}

	inline static int32_t get_offset_of_slot2_7() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___slot2_7)); }
	inline GameObject_t3674682005 * get_slot2_7() const { return ___slot2_7; }
	inline GameObject_t3674682005 ** get_address_of_slot2_7() { return &___slot2_7; }
	inline void set_slot2_7(GameObject_t3674682005 * value)
	{
		___slot2_7 = value;
		Il2CppCodeGenWriteBarrier(&___slot2_7, value);
	}

	inline static int32_t get_offset_of_slot3_8() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___slot3_8)); }
	inline GameObject_t3674682005 * get_slot3_8() const { return ___slot3_8; }
	inline GameObject_t3674682005 ** get_address_of_slot3_8() { return &___slot3_8; }
	inline void set_slot3_8(GameObject_t3674682005 * value)
	{
		___slot3_8 = value;
		Il2CppCodeGenWriteBarrier(&___slot3_8, value);
	}

	inline static int32_t get_offset_of_HUD_9() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___HUD_9)); }
	inline GameObject_t3674682005 * get_HUD_9() const { return ___HUD_9; }
	inline GameObject_t3674682005 ** get_address_of_HUD_9() { return &___HUD_9; }
	inline void set_HUD_9(GameObject_t3674682005 * value)
	{
		___HUD_9 = value;
		Il2CppCodeGenWriteBarrier(&___HUD_9, value);
	}

	inline static int32_t get_offset_of_HitButton_10() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___HitButton_10)); }
	inline GameObject_t3674682005 * get_HitButton_10() const { return ___HitButton_10; }
	inline GameObject_t3674682005 ** get_address_of_HitButton_10() { return &___HitButton_10; }
	inline void set_HitButton_10(GameObject_t3674682005 * value)
	{
		___HitButton_10 = value;
		Il2CppCodeGenWriteBarrier(&___HitButton_10, value);
	}

	inline static int32_t get_offset_of_ThreeButtons_11() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___ThreeButtons_11)); }
	inline GameObject_t3674682005 * get_ThreeButtons_11() const { return ___ThreeButtons_11; }
	inline GameObject_t3674682005 ** get_address_of_ThreeButtons_11() { return &___ThreeButtons_11; }
	inline void set_ThreeButtons_11(GameObject_t3674682005 * value)
	{
		___ThreeButtons_11 = value;
		Il2CppCodeGenWriteBarrier(&___ThreeButtons_11, value);
	}

	inline static int32_t get_offset_of_inventario_12() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___inventario_12)); }
	inline GameObject_t3674682005 * get_inventario_12() const { return ___inventario_12; }
	inline GameObject_t3674682005 ** get_address_of_inventario_12() { return &___inventario_12; }
	inline void set_inventario_12(GameObject_t3674682005 * value)
	{
		___inventario_12 = value;
		Il2CppCodeGenWriteBarrier(&___inventario_12, value);
	}

	inline static int32_t get_offset_of_StartButton_13() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___StartButton_13)); }
	inline GameObject_t3674682005 * get_StartButton_13() const { return ___StartButton_13; }
	inline GameObject_t3674682005 ** get_address_of_StartButton_13() { return &___StartButton_13; }
	inline void set_StartButton_13(GameObject_t3674682005 * value)
	{
		___StartButton_13 = value;
		Il2CppCodeGenWriteBarrier(&___StartButton_13, value);
	}

	inline static int32_t get_offset_of_Audio_14() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___Audio_14)); }
	inline GameObject_t3674682005 * get_Audio_14() const { return ___Audio_14; }
	inline GameObject_t3674682005 ** get_address_of_Audio_14() { return &___Audio_14; }
	inline void set_Audio_14(GameObject_t3674682005 * value)
	{
		___Audio_14 = value;
		Il2CppCodeGenWriteBarrier(&___Audio_14, value);
	}

	inline static int32_t get_offset_of_Caption_15() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___Caption_15)); }
	inline GameObject_t3674682005 * get_Caption_15() const { return ___Caption_15; }
	inline GameObject_t3674682005 ** get_address_of_Caption_15() { return &___Caption_15; }
	inline void set_Caption_15(GameObject_t3674682005 * value)
	{
		___Caption_15 = value;
		Il2CppCodeGenWriteBarrier(&___Caption_15, value);
	}

	inline static int32_t get_offset_of_PicChar_16() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___PicChar_16)); }
	inline GameObject_t3674682005 * get_PicChar_16() const { return ___PicChar_16; }
	inline GameObject_t3674682005 ** get_address_of_PicChar_16() { return &___PicChar_16; }
	inline void set_PicChar_16(GameObject_t3674682005 * value)
	{
		___PicChar_16 = value;
		Il2CppCodeGenWriteBarrier(&___PicChar_16, value);
	}

	inline static int32_t get_offset_of_PicCanvas_17() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___PicCanvas_17)); }
	inline GameObject_t3674682005 * get_PicCanvas_17() const { return ___PicCanvas_17; }
	inline GameObject_t3674682005 ** get_address_of_PicCanvas_17() { return &___PicCanvas_17; }
	inline void set_PicCanvas_17(GameObject_t3674682005 * value)
	{
		___PicCanvas_17 = value;
		Il2CppCodeGenWriteBarrier(&___PicCanvas_17, value);
	}

	inline static int32_t get_offset_of_Tabla_18() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___Tabla_18)); }
	inline GameObject_t3674682005 * get_Tabla_18() const { return ___Tabla_18; }
	inline GameObject_t3674682005 ** get_address_of_Tabla_18() { return &___Tabla_18; }
	inline void set_Tabla_18(GameObject_t3674682005 * value)
	{
		___Tabla_18 = value;
		Il2CppCodeGenWriteBarrier(&___Tabla_18, value);
	}

	inline static int32_t get_offset_of_HitStopText_19() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___HitStopText_19)); }
	inline GameObject_t3674682005 * get_HitStopText_19() const { return ___HitStopText_19; }
	inline GameObject_t3674682005 ** get_address_of_HitStopText_19() { return &___HitStopText_19; }
	inline void set_HitStopText_19(GameObject_t3674682005 * value)
	{
		___HitStopText_19 = value;
		Il2CppCodeGenWriteBarrier(&___HitStopText_19, value);
	}

	inline static int32_t get_offset_of_counterScreen1_20() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___counterScreen1_20)); }
	inline GameObject_t3674682005 * get_counterScreen1_20() const { return ___counterScreen1_20; }
	inline GameObject_t3674682005 ** get_address_of_counterScreen1_20() { return &___counterScreen1_20; }
	inline void set_counterScreen1_20(GameObject_t3674682005 * value)
	{
		___counterScreen1_20 = value;
		Il2CppCodeGenWriteBarrier(&___counterScreen1_20, value);
	}

	inline static int32_t get_offset_of_counterScreen2_21() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___counterScreen2_21)); }
	inline GameObject_t3674682005 * get_counterScreen2_21() const { return ___counterScreen2_21; }
	inline GameObject_t3674682005 ** get_address_of_counterScreen2_21() { return &___counterScreen2_21; }
	inline void set_counterScreen2_21(GameObject_t3674682005 * value)
	{
		___counterScreen2_21 = value;
		Il2CppCodeGenWriteBarrier(&___counterScreen2_21, value);
	}

	inline static int32_t get_offset_of_counterScreen3_22() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___counterScreen3_22)); }
	inline GameObject_t3674682005 * get_counterScreen3_22() const { return ___counterScreen3_22; }
	inline GameObject_t3674682005 ** get_address_of_counterScreen3_22() { return &___counterScreen3_22; }
	inline void set_counterScreen3_22(GameObject_t3674682005 * value)
	{
		___counterScreen3_22 = value;
		Il2CppCodeGenWriteBarrier(&___counterScreen3_22, value);
	}

	inline static int32_t get_offset_of_counterChar_23() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___counterChar_23)); }
	inline GameObject_t3674682005 * get_counterChar_23() const { return ___counterChar_23; }
	inline GameObject_t3674682005 ** get_address_of_counterChar_23() { return &___counterChar_23; }
	inline void set_counterChar_23(GameObject_t3674682005 * value)
	{
		___counterChar_23 = value;
		Il2CppCodeGenWriteBarrier(&___counterChar_23, value);
	}

	inline static int32_t get_offset_of_panelInventario_24() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___panelInventario_24)); }
	inline GameObject_t3674682005 * get_panelInventario_24() const { return ___panelInventario_24; }
	inline GameObject_t3674682005 ** get_address_of_panelInventario_24() { return &___panelInventario_24; }
	inline void set_panelInventario_24(GameObject_t3674682005 * value)
	{
		___panelInventario_24 = value;
		Il2CppCodeGenWriteBarrier(&___panelInventario_24, value);
	}

	inline static int32_t get_offset_of_panelGame_25() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___panelGame_25)); }
	inline GameObject_t3674682005 * get_panelGame_25() const { return ___panelGame_25; }
	inline GameObject_t3674682005 ** get_address_of_panelGame_25() { return &___panelGame_25; }
	inline void set_panelGame_25(GameObject_t3674682005 * value)
	{
		___panelGame_25 = value;
		Il2CppCodeGenWriteBarrier(&___panelGame_25, value);
	}

	inline static int32_t get_offset_of_counters_26() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___counters_26)); }
	inline GameObjectU5BU5D_t2662109048* get_counters_26() const { return ___counters_26; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_counters_26() { return &___counters_26; }
	inline void set_counters_26(GameObjectU5BU5D_t2662109048* value)
	{
		___counters_26 = value;
		Il2CppCodeGenWriteBarrier(&___counters_26, value);
	}

	inline static int32_t get_offset_of_slotsG_27() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___slotsG_27)); }
	inline GameObjectU5BU5D_t2662109048* get_slotsG_27() const { return ___slotsG_27; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_slotsG_27() { return &___slotsG_27; }
	inline void set_slotsG_27(GameObjectU5BU5D_t2662109048* value)
	{
		___slotsG_27 = value;
		Il2CppCodeGenWriteBarrier(&___slotsG_27, value);
	}

	inline static int32_t get_offset_of_win_28() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___win_28)); }
	inline AudioClip_t794140988 * get_win_28() const { return ___win_28; }
	inline AudioClip_t794140988 ** get_address_of_win_28() { return &___win_28; }
	inline void set_win_28(AudioClip_t794140988 * value)
	{
		___win_28 = value;
		Il2CppCodeGenWriteBarrier(&___win_28, value);
	}

	inline static int32_t get_offset_of_winBig_29() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___winBig_29)); }
	inline AudioClip_t794140988 * get_winBig_29() const { return ___winBig_29; }
	inline AudioClip_t794140988 ** get_address_of_winBig_29() { return &___winBig_29; }
	inline void set_winBig_29(AudioClip_t794140988 * value)
	{
		___winBig_29 = value;
		Il2CppCodeGenWriteBarrier(&___winBig_29, value);
	}

	inline static int32_t get_offset_of_result_30() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___result_30)); }
	inline AudioClip_t794140988 * get_result_30() const { return ___result_30; }
	inline AudioClip_t794140988 ** get_address_of_result_30() { return &___result_30; }
	inline void set_result_30(AudioClip_t794140988 * value)
	{
		___result_30 = value;
		Il2CppCodeGenWriteBarrier(&___result_30, value);
	}

	inline static int32_t get_offset_of_start_31() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___start_31)); }
	inline AudioClip_t794140988 * get_start_31() const { return ___start_31; }
	inline AudioClip_t794140988 ** get_address_of_start_31() { return &___start_31; }
	inline void set_start_31(AudioClip_t794140988 * value)
	{
		___start_31 = value;
		Il2CppCodeGenWriteBarrier(&___start_31, value);
	}

	inline static int32_t get_offset_of_hit_32() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___hit_32)); }
	inline AudioClip_t794140988 * get_hit_32() const { return ___hit_32; }
	inline AudioClip_t794140988 ** get_address_of_hit_32() { return &___hit_32; }
	inline void set_hit_32(AudioClip_t794140988 * value)
	{
		___hit_32 = value;
		Il2CppCodeGenWriteBarrier(&___hit_32, value);
	}

	inline static int32_t get_offset_of_coin_33() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___coin_33)); }
	inline AudioClip_t794140988 * get_coin_33() const { return ___coin_33; }
	inline AudioClip_t794140988 ** get_address_of_coin_33() { return &___coin_33; }
	inline void set_coin_33(AudioClip_t794140988 * value)
	{
		___coin_33 = value;
		Il2CppCodeGenWriteBarrier(&___coin_33, value);
	}

	inline static int32_t get_offset_of_blueFrame_34() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___blueFrame_34)); }
	inline Color_t4194546905  get_blueFrame_34() const { return ___blueFrame_34; }
	inline Color_t4194546905 * get_address_of_blueFrame_34() { return &___blueFrame_34; }
	inline void set_blueFrame_34(Color_t4194546905  value)
	{
		___blueFrame_34 = value;
	}

	inline static int32_t get_offset_of_canvas_35() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___canvas_35)); }
	inline Canvas_t2727140764 * get_canvas_35() const { return ___canvas_35; }
	inline Canvas_t2727140764 ** get_address_of_canvas_35() { return &___canvas_35; }
	inline void set_canvas_35(Canvas_t2727140764 * value)
	{
		___canvas_35 = value;
		Il2CppCodeGenWriteBarrier(&___canvas_35, value);
	}

	inline static int32_t get_offset_of_caption_36() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___caption_36)); }
	inline Text_t9039225 * get_caption_36() const { return ___caption_36; }
	inline Text_t9039225 ** get_address_of_caption_36() { return &___caption_36; }
	inline void set_caption_36(Text_t9039225 * value)
	{
		___caption_36 = value;
		Il2CppCodeGenWriteBarrier(&___caption_36, value);
	}

	inline static int32_t get_offset_of_picChar_37() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___picChar_37)); }
	inline Sprite_t3199167241 * get_picChar_37() const { return ___picChar_37; }
	inline Sprite_t3199167241 ** get_address_of_picChar_37() { return &___picChar_37; }
	inline void set_picChar_37(Sprite_t3199167241 * value)
	{
		___picChar_37 = value;
		Il2CppCodeGenWriteBarrier(&___picChar_37, value);
	}

	inline static int32_t get_offset_of_picCanvas_38() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___picCanvas_38)); }
	inline Sprite_t3199167241 * get_picCanvas_38() const { return ___picCanvas_38; }
	inline Sprite_t3199167241 ** get_address_of_picCanvas_38() { return &___picCanvas_38; }
	inline void set_picCanvas_38(Sprite_t3199167241 * value)
	{
		___picCanvas_38 = value;
		Il2CppCodeGenWriteBarrier(&___picCanvas_38, value);
	}

	inline static int32_t get_offset_of_combinaciones_39() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___combinaciones_39)); }
	inline Combinaciones_t3637091598 * get_combinaciones_39() const { return ___combinaciones_39; }
	inline Combinaciones_t3637091598 ** get_address_of_combinaciones_39() { return &___combinaciones_39; }
	inline void set_combinaciones_39(Combinaciones_t3637091598 * value)
	{
		___combinaciones_39 = value;
		Il2CppCodeGenWriteBarrier(&___combinaciones_39, value);
	}

	inline static int32_t get_offset_of_counterChecking_40() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___counterChecking_40)); }
	inline CounterChecking_t39316438 * get_counterChecking_40() const { return ___counterChecking_40; }
	inline CounterChecking_t39316438 ** get_address_of_counterChecking_40() { return &___counterChecking_40; }
	inline void set_counterChecking_40(CounterChecking_t39316438 * value)
	{
		___counterChecking_40 = value;
		Il2CppCodeGenWriteBarrier(&___counterChecking_40, value);
	}

	inline static int32_t get_offset_of_currentTimer_41() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___currentTimer_41)); }
	inline float get_currentTimer_41() const { return ___currentTimer_41; }
	inline float* get_address_of_currentTimer_41() { return &___currentTimer_41; }
	inline void set_currentTimer_41(float value)
	{
		___currentTimer_41 = value;
	}

	inline static int32_t get_offset_of_timer_42() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___timer_42)); }
	inline float get_timer_42() const { return ___timer_42; }
	inline float* get_address_of_timer_42() { return &___timer_42; }
	inline void set_timer_42(float value)
	{
		___timer_42 = value;
	}

	inline static int32_t get_offset_of_timeHightlight_43() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___timeHightlight_43)); }
	inline float get_timeHightlight_43() const { return ___timeHightlight_43; }
	inline float* get_address_of_timeHightlight_43() { return &___timeHightlight_43; }
	inline void set_timeHightlight_43(float value)
	{
		___timeHightlight_43 = value;
	}

	inline static int32_t get_offset_of_rolling_44() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___rolling_44)); }
	inline bool get_rolling_44() const { return ___rolling_44; }
	inline bool* get_address_of_rolling_44() { return &___rolling_44; }
	inline void set_rolling_44(bool value)
	{
		___rolling_44 = value;
	}

	inline static int32_t get_offset_of_choice_45() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___choice_45)); }
	inline bool get_choice_45() const { return ___choice_45; }
	inline bool* get_address_of_choice_45() { return &___choice_45; }
	inline void set_choice_45(bool value)
	{
		___choice_45 = value;
	}

	inline static int32_t get_offset_of_frozenSlots_46() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___frozenSlots_46)); }
	inline BooleanU5BU5D_t3456302923* get_frozenSlots_46() const { return ___frozenSlots_46; }
	inline BooleanU5BU5D_t3456302923** get_address_of_frozenSlots_46() { return &___frozenSlots_46; }
	inline void set_frozenSlots_46(BooleanU5BU5D_t3456302923* value)
	{
		___frozenSlots_46 = value;
		Il2CppCodeGenWriteBarrier(&___frozenSlots_46, value);
	}

	inline static int32_t get_offset_of_frozenRound_47() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___frozenRound_47)); }
	inline bool get_frozenRound_47() const { return ___frozenRound_47; }
	inline bool* get_address_of_frozenRound_47() { return &___frozenRound_47; }
	inline void set_frozenRound_47(bool value)
	{
		___frozenRound_47 = value;
	}

	inline static int32_t get_offset_of_slots_48() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___slots_48)); }
	inline SpriteU5BU5D_t2761310900* get_slots_48() const { return ___slots_48; }
	inline SpriteU5BU5D_t2761310900** get_address_of_slots_48() { return &___slots_48; }
	inline void set_slots_48(SpriteU5BU5D_t2761310900* value)
	{
		___slots_48 = value;
		Il2CppCodeGenWriteBarrier(&___slots_48, value);
	}

	inline static int32_t get_offset_of_vi_49() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___vi_49)); }
	inline Vinetas_t2126676892 * get_vi_49() const { return ___vi_49; }
	inline Vinetas_t2126676892 ** get_address_of_vi_49() { return &___vi_49; }
	inline void set_vi_49(Vinetas_t2126676892 * value)
	{
		___vi_49 = value;
		Il2CppCodeGenWriteBarrier(&___vi_49, value);
	}

	inline static int32_t get_offset_of_a_50() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___a_50)); }
	inline int32_t get_a_50() const { return ___a_50; }
	inline int32_t* get_address_of_a_50() { return &___a_50; }
	inline void set_a_50(int32_t value)
	{
		___a_50 = value;
	}

	inline static int32_t get_offset_of_b_51() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___b_51)); }
	inline int32_t get_b_51() const { return ___b_51; }
	inline int32_t* get_address_of_b_51() { return &___b_51; }
	inline void set_b_51(int32_t value)
	{
		___b_51 = value;
	}

	inline static int32_t get_offset_of_c_52() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___c_52)); }
	inline int32_t get_c_52() const { return ___c_52; }
	inline int32_t* get_address_of_c_52() { return &___c_52; }
	inline void set_c_52(int32_t value)
	{
		___c_52 = value;
	}

	inline static int32_t get_offset_of_tempFrozen_53() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___tempFrozen_53)); }
	inline int32_t get_tempFrozen_53() const { return ___tempFrozen_53; }
	inline int32_t* get_address_of_tempFrozen_53() { return &___tempFrozen_53; }
	inline void set_tempFrozen_53(int32_t value)
	{
		___tempFrozen_53 = value;
	}

	inline static int32_t get_offset_of_price_54() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___price_54)); }
	inline int32_t get_price_54() const { return ___price_54; }
	inline int32_t* get_address_of_price_54() { return &___price_54; }
	inline void set_price_54(int32_t value)
	{
		___price_54 = value;
	}

	inline static int32_t get_offset_of_credits_55() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___credits_55)); }
	inline int32_t get_credits_55() const { return ___credits_55; }
	inline int32_t* get_address_of_credits_55() { return &___credits_55; }
	inline void set_credits_55(int32_t value)
	{
		___credits_55 = value;
	}

	inline static int32_t get_offset_of_heightHUD_56() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___heightHUD_56)); }
	inline float get_heightHUD_56() const { return ___heightHUD_56; }
	inline float* get_address_of_heightHUD_56() { return &___heightHUD_56; }
	inline void set_heightHUD_56(float value)
	{
		___heightHUD_56 = value;
	}

	inline static int32_t get_offset_of_currentState_58() { return static_cast<int32_t>(offsetof(Slots_t79980053, ___currentState_58)); }
	inline int32_t get_currentState_58() const { return ___currentState_58; }
	inline int32_t* get_address_of_currentState_58() { return &___currentState_58; }
	inline void set_currentState_58(int32_t value)
	{
		___currentState_58 = value;
	}
};

struct Slots_t79980053_StaticFields
{
public:
	// System.Int32 Slots::numLock
	int32_t ___numLock_57;

public:
	inline static int32_t get_offset_of_numLock_57() { return static_cast<int32_t>(offsetof(Slots_t79980053_StaticFields, ___numLock_57)); }
	inline int32_t get_numLock_57() const { return ___numLock_57; }
	inline int32_t* get_address_of_numLock_57() { return &___numLock_57; }
	inline void set_numLock_57(int32_t value)
	{
		___numLock_57 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
