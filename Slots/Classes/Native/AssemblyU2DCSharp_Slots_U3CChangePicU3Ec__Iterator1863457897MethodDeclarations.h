﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Slots/<ChangePic>c__Iterator4
struct U3CChangePicU3Ec__Iterator4_t1863457897;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Slots/<ChangePic>c__Iterator4::.ctor()
extern "C"  void U3CChangePicU3Ec__Iterator4__ctor_m1973967650 (U3CChangePicU3Ec__Iterator4_t1863457897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Slots/<ChangePic>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CChangePicU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m244131888 (U3CChangePicU3Ec__Iterator4_t1863457897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Slots/<ChangePic>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CChangePicU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m4092955588 (U3CChangePicU3Ec__Iterator4_t1863457897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Slots/<ChangePic>c__Iterator4::MoveNext()
extern "C"  bool U3CChangePicU3Ec__Iterator4_MoveNext_m3652262546 (U3CChangePicU3Ec__Iterator4_t1863457897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots/<ChangePic>c__Iterator4::Dispose()
extern "C"  void U3CChangePicU3Ec__Iterator4_Dispose_m2800617183 (U3CChangePicU3Ec__Iterator4_t1863457897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots/<ChangePic>c__Iterator4::Reset()
extern "C"  void U3CChangePicU3Ec__Iterator4_Reset_m3915367887 (U3CChangePicU3Ec__Iterator4_t1863457897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
