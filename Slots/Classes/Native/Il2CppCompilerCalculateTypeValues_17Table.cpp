﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UITween_RotationPropetiesAnim2262074766.h"
#include "AssemblyU2DCSharp_UITween_FadePropetiesAnim145139664.h"
#include "AssemblyU2DCSharp_UITween_AnimationParts4290477440.h"
#include "AssemblyU2DCSharp_UITween_AnimationParts_State451125205.h"
#include "AssemblyU2DCSharp_UITween_AnimationParts_EndTweenC1761908940.h"
#include "AssemblyU2DCSharp_UITween_AnimationParts_CallbackC2423460991.h"
#include "AssemblyU2DCSharp_UITween_AnimationParts_DisableOr4252453067.h"
#include "AssemblyU2DCSharp_Vinetas2126676892.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (RotationPropetiesAnim_t2262074766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1700[5] = 
{
	RotationPropetiesAnim_t2262074766::get_offset_of_rotationEnabled_0(),
	RotationPropetiesAnim_t2262074766::get_offset_of_TweenCurveEnterRot_1(),
	RotationPropetiesAnim_t2262074766::get_offset_of_TweenCurveExitRot_2(),
	RotationPropetiesAnim_t2262074766::get_offset_of_StartRot_3(),
	RotationPropetiesAnim_t2262074766::get_offset_of_EndRot_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (FadePropetiesAnim_t145139664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1701[4] = 
{
	FadePropetiesAnim_t145139664::get_offset_of_fadeInOutEnabled_0(),
	FadePropetiesAnim_t145139664::get_offset_of_fadeOverride_1(),
	FadePropetiesAnim_t145139664::get_offset_of_startFade_2(),
	FadePropetiesAnim_t145139664::get_offset_of_endFade_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (AnimationParts_t4290477440), -1, sizeof(AnimationParts_t4290477440_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1703[17] = 
{
	AnimationParts_t4290477440::get_offset_of_PositionPropetiesAnim_0(),
	AnimationParts_t4290477440::get_offset_of_ScalePropetiesAnim_1(),
	AnimationParts_t4290477440::get_offset_of_RotationPropetiesAnim_2(),
	AnimationParts_t4290477440::get_offset_of_FadePropetiesAnim_3(),
	AnimationParts_t4290477440::get_offset_of_UnscaledTimeAnimation_4(),
	AnimationParts_t4290477440::get_offset_of_SaveState_5(),
	AnimationParts_t4290477440::get_offset_of_AtomicAnimation_6(),
	AnimationParts_t4290477440::get_offset_of_ObjectState_7(),
	AnimationParts_t4290477440::get_offset_of_EndState_8(),
	AnimationParts_t4290477440::get_offset_of_CallCallback_9(),
	AnimationParts_t4290477440::get_offset_of_IntroEvents_10(),
	AnimationParts_t4290477440::get_offset_of_ExitEvents_11(),
	AnimationParts_t4290477440::get_offset_of_CallBackObject_12(),
	AnimationParts_t4290477440::get_offset_of_CheckNextFrame_13(),
	AnimationParts_t4290477440::get_offset_of_CallOnThisFrame_14(),
	AnimationParts_t4290477440::get_offset_of_animationDuration_15(),
	AnimationParts_t4290477440_StaticFields::get_offset_of_OnDisableOrDestroy_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (State_t451125205)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1704[3] = 
{
	State_t451125205::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (EndTweenClose_t1761908940)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1705[4] = 
{
	EndTweenClose_t1761908940::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (CallbackCall_t2423460991)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1706[17] = 
{
	CallbackCall_t2423460991::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (DisableOrDestroy_t4252453067), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (Vinetas_t2126676892), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1708[21] = 
{
	Vinetas_t2126676892::get_offset_of_camino_2(),
	Vinetas_t2126676892::get_offset_of_bosque_3(),
	Vinetas_t2126676892::get_offset_of_casa_4(),
	Vinetas_t2126676892::get_offset_of_puerta_5(),
	Vinetas_t2126676892::get_offset_of_felpudo_6(),
	Vinetas_t2126676892::get_offset_of_llave_7(),
	Vinetas_t2126676892::get_offset_of_cerradura_8(),
	Vinetas_t2126676892::get_offset_of_entrada_9(),
	Vinetas_t2126676892::get_offset_of_reloj_10(),
	Vinetas_t2126676892::get_offset_of_escaleras_11(),
	Vinetas_t2126676892::get_offset_of_alcoba_12(),
	Vinetas_t2126676892::get_offset_of_lienzo_13(),
	Vinetas_t2126676892::get_offset_of_metafisica_14(),
	Vinetas_t2126676892::get_offset_of_tunel_15(),
	Vinetas_t2126676892::get_offset_of_escalera_16(),
	Vinetas_t2126676892::get_offset_of_sotano_17(),
	Vinetas_t2126676892::get_offset_of_caverna_18(),
	Vinetas_t2126676892::get_offset_of_master_19(),
	Vinetas_t2126676892::get_offset_of_piedra_20(),
	Vinetas_t2126676892::get_offset_of_vinetas_21(),
	Vinetas_t2126676892::get_offset_of_listaSprite_22(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
