﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite
struct Sprite_t3199167241;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2761310900;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vinetas
struct  Vinetas_t2126676892  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Sprite Vinetas::camino
	Sprite_t3199167241 * ___camino_2;
	// UnityEngine.Sprite Vinetas::bosque
	Sprite_t3199167241 * ___bosque_3;
	// UnityEngine.Sprite Vinetas::casa
	Sprite_t3199167241 * ___casa_4;
	// UnityEngine.Sprite Vinetas::puerta
	Sprite_t3199167241 * ___puerta_5;
	// UnityEngine.Sprite Vinetas::felpudo
	Sprite_t3199167241 * ___felpudo_6;
	// UnityEngine.Sprite Vinetas::llave
	Sprite_t3199167241 * ___llave_7;
	// UnityEngine.Sprite Vinetas::cerradura
	Sprite_t3199167241 * ___cerradura_8;
	// UnityEngine.Sprite Vinetas::entrada
	Sprite_t3199167241 * ___entrada_9;
	// UnityEngine.Sprite Vinetas::reloj
	Sprite_t3199167241 * ___reloj_10;
	// UnityEngine.Sprite Vinetas::escaleras
	Sprite_t3199167241 * ___escaleras_11;
	// UnityEngine.Sprite Vinetas::alcoba
	Sprite_t3199167241 * ___alcoba_12;
	// UnityEngine.Sprite Vinetas::lienzo
	Sprite_t3199167241 * ___lienzo_13;
	// UnityEngine.Sprite Vinetas::metafisica
	Sprite_t3199167241 * ___metafisica_14;
	// UnityEngine.Sprite Vinetas::tunel
	Sprite_t3199167241 * ___tunel_15;
	// UnityEngine.Sprite Vinetas::escalera
	Sprite_t3199167241 * ___escalera_16;
	// UnityEngine.Sprite Vinetas::sotano
	Sprite_t3199167241 * ___sotano_17;
	// UnityEngine.Sprite Vinetas::caverna
	Sprite_t3199167241 * ___caverna_18;
	// UnityEngine.Sprite Vinetas::master
	Sprite_t3199167241 * ___master_19;
	// UnityEngine.Sprite Vinetas::piedra
	Sprite_t3199167241 * ___piedra_20;
	// UnityEngine.Sprite[] Vinetas::vinetas
	SpriteU5BU5D_t2761310900* ___vinetas_21;
	// UnityEngine.Sprite[] Vinetas::listaSprite
	SpriteU5BU5D_t2761310900* ___listaSprite_22;

public:
	inline static int32_t get_offset_of_camino_2() { return static_cast<int32_t>(offsetof(Vinetas_t2126676892, ___camino_2)); }
	inline Sprite_t3199167241 * get_camino_2() const { return ___camino_2; }
	inline Sprite_t3199167241 ** get_address_of_camino_2() { return &___camino_2; }
	inline void set_camino_2(Sprite_t3199167241 * value)
	{
		___camino_2 = value;
		Il2CppCodeGenWriteBarrier(&___camino_2, value);
	}

	inline static int32_t get_offset_of_bosque_3() { return static_cast<int32_t>(offsetof(Vinetas_t2126676892, ___bosque_3)); }
	inline Sprite_t3199167241 * get_bosque_3() const { return ___bosque_3; }
	inline Sprite_t3199167241 ** get_address_of_bosque_3() { return &___bosque_3; }
	inline void set_bosque_3(Sprite_t3199167241 * value)
	{
		___bosque_3 = value;
		Il2CppCodeGenWriteBarrier(&___bosque_3, value);
	}

	inline static int32_t get_offset_of_casa_4() { return static_cast<int32_t>(offsetof(Vinetas_t2126676892, ___casa_4)); }
	inline Sprite_t3199167241 * get_casa_4() const { return ___casa_4; }
	inline Sprite_t3199167241 ** get_address_of_casa_4() { return &___casa_4; }
	inline void set_casa_4(Sprite_t3199167241 * value)
	{
		___casa_4 = value;
		Il2CppCodeGenWriteBarrier(&___casa_4, value);
	}

	inline static int32_t get_offset_of_puerta_5() { return static_cast<int32_t>(offsetof(Vinetas_t2126676892, ___puerta_5)); }
	inline Sprite_t3199167241 * get_puerta_5() const { return ___puerta_5; }
	inline Sprite_t3199167241 ** get_address_of_puerta_5() { return &___puerta_5; }
	inline void set_puerta_5(Sprite_t3199167241 * value)
	{
		___puerta_5 = value;
		Il2CppCodeGenWriteBarrier(&___puerta_5, value);
	}

	inline static int32_t get_offset_of_felpudo_6() { return static_cast<int32_t>(offsetof(Vinetas_t2126676892, ___felpudo_6)); }
	inline Sprite_t3199167241 * get_felpudo_6() const { return ___felpudo_6; }
	inline Sprite_t3199167241 ** get_address_of_felpudo_6() { return &___felpudo_6; }
	inline void set_felpudo_6(Sprite_t3199167241 * value)
	{
		___felpudo_6 = value;
		Il2CppCodeGenWriteBarrier(&___felpudo_6, value);
	}

	inline static int32_t get_offset_of_llave_7() { return static_cast<int32_t>(offsetof(Vinetas_t2126676892, ___llave_7)); }
	inline Sprite_t3199167241 * get_llave_7() const { return ___llave_7; }
	inline Sprite_t3199167241 ** get_address_of_llave_7() { return &___llave_7; }
	inline void set_llave_7(Sprite_t3199167241 * value)
	{
		___llave_7 = value;
		Il2CppCodeGenWriteBarrier(&___llave_7, value);
	}

	inline static int32_t get_offset_of_cerradura_8() { return static_cast<int32_t>(offsetof(Vinetas_t2126676892, ___cerradura_8)); }
	inline Sprite_t3199167241 * get_cerradura_8() const { return ___cerradura_8; }
	inline Sprite_t3199167241 ** get_address_of_cerradura_8() { return &___cerradura_8; }
	inline void set_cerradura_8(Sprite_t3199167241 * value)
	{
		___cerradura_8 = value;
		Il2CppCodeGenWriteBarrier(&___cerradura_8, value);
	}

	inline static int32_t get_offset_of_entrada_9() { return static_cast<int32_t>(offsetof(Vinetas_t2126676892, ___entrada_9)); }
	inline Sprite_t3199167241 * get_entrada_9() const { return ___entrada_9; }
	inline Sprite_t3199167241 ** get_address_of_entrada_9() { return &___entrada_9; }
	inline void set_entrada_9(Sprite_t3199167241 * value)
	{
		___entrada_9 = value;
		Il2CppCodeGenWriteBarrier(&___entrada_9, value);
	}

	inline static int32_t get_offset_of_reloj_10() { return static_cast<int32_t>(offsetof(Vinetas_t2126676892, ___reloj_10)); }
	inline Sprite_t3199167241 * get_reloj_10() const { return ___reloj_10; }
	inline Sprite_t3199167241 ** get_address_of_reloj_10() { return &___reloj_10; }
	inline void set_reloj_10(Sprite_t3199167241 * value)
	{
		___reloj_10 = value;
		Il2CppCodeGenWriteBarrier(&___reloj_10, value);
	}

	inline static int32_t get_offset_of_escaleras_11() { return static_cast<int32_t>(offsetof(Vinetas_t2126676892, ___escaleras_11)); }
	inline Sprite_t3199167241 * get_escaleras_11() const { return ___escaleras_11; }
	inline Sprite_t3199167241 ** get_address_of_escaleras_11() { return &___escaleras_11; }
	inline void set_escaleras_11(Sprite_t3199167241 * value)
	{
		___escaleras_11 = value;
		Il2CppCodeGenWriteBarrier(&___escaleras_11, value);
	}

	inline static int32_t get_offset_of_alcoba_12() { return static_cast<int32_t>(offsetof(Vinetas_t2126676892, ___alcoba_12)); }
	inline Sprite_t3199167241 * get_alcoba_12() const { return ___alcoba_12; }
	inline Sprite_t3199167241 ** get_address_of_alcoba_12() { return &___alcoba_12; }
	inline void set_alcoba_12(Sprite_t3199167241 * value)
	{
		___alcoba_12 = value;
		Il2CppCodeGenWriteBarrier(&___alcoba_12, value);
	}

	inline static int32_t get_offset_of_lienzo_13() { return static_cast<int32_t>(offsetof(Vinetas_t2126676892, ___lienzo_13)); }
	inline Sprite_t3199167241 * get_lienzo_13() const { return ___lienzo_13; }
	inline Sprite_t3199167241 ** get_address_of_lienzo_13() { return &___lienzo_13; }
	inline void set_lienzo_13(Sprite_t3199167241 * value)
	{
		___lienzo_13 = value;
		Il2CppCodeGenWriteBarrier(&___lienzo_13, value);
	}

	inline static int32_t get_offset_of_metafisica_14() { return static_cast<int32_t>(offsetof(Vinetas_t2126676892, ___metafisica_14)); }
	inline Sprite_t3199167241 * get_metafisica_14() const { return ___metafisica_14; }
	inline Sprite_t3199167241 ** get_address_of_metafisica_14() { return &___metafisica_14; }
	inline void set_metafisica_14(Sprite_t3199167241 * value)
	{
		___metafisica_14 = value;
		Il2CppCodeGenWriteBarrier(&___metafisica_14, value);
	}

	inline static int32_t get_offset_of_tunel_15() { return static_cast<int32_t>(offsetof(Vinetas_t2126676892, ___tunel_15)); }
	inline Sprite_t3199167241 * get_tunel_15() const { return ___tunel_15; }
	inline Sprite_t3199167241 ** get_address_of_tunel_15() { return &___tunel_15; }
	inline void set_tunel_15(Sprite_t3199167241 * value)
	{
		___tunel_15 = value;
		Il2CppCodeGenWriteBarrier(&___tunel_15, value);
	}

	inline static int32_t get_offset_of_escalera_16() { return static_cast<int32_t>(offsetof(Vinetas_t2126676892, ___escalera_16)); }
	inline Sprite_t3199167241 * get_escalera_16() const { return ___escalera_16; }
	inline Sprite_t3199167241 ** get_address_of_escalera_16() { return &___escalera_16; }
	inline void set_escalera_16(Sprite_t3199167241 * value)
	{
		___escalera_16 = value;
		Il2CppCodeGenWriteBarrier(&___escalera_16, value);
	}

	inline static int32_t get_offset_of_sotano_17() { return static_cast<int32_t>(offsetof(Vinetas_t2126676892, ___sotano_17)); }
	inline Sprite_t3199167241 * get_sotano_17() const { return ___sotano_17; }
	inline Sprite_t3199167241 ** get_address_of_sotano_17() { return &___sotano_17; }
	inline void set_sotano_17(Sprite_t3199167241 * value)
	{
		___sotano_17 = value;
		Il2CppCodeGenWriteBarrier(&___sotano_17, value);
	}

	inline static int32_t get_offset_of_caverna_18() { return static_cast<int32_t>(offsetof(Vinetas_t2126676892, ___caverna_18)); }
	inline Sprite_t3199167241 * get_caverna_18() const { return ___caverna_18; }
	inline Sprite_t3199167241 ** get_address_of_caverna_18() { return &___caverna_18; }
	inline void set_caverna_18(Sprite_t3199167241 * value)
	{
		___caverna_18 = value;
		Il2CppCodeGenWriteBarrier(&___caverna_18, value);
	}

	inline static int32_t get_offset_of_master_19() { return static_cast<int32_t>(offsetof(Vinetas_t2126676892, ___master_19)); }
	inline Sprite_t3199167241 * get_master_19() const { return ___master_19; }
	inline Sprite_t3199167241 ** get_address_of_master_19() { return &___master_19; }
	inline void set_master_19(Sprite_t3199167241 * value)
	{
		___master_19 = value;
		Il2CppCodeGenWriteBarrier(&___master_19, value);
	}

	inline static int32_t get_offset_of_piedra_20() { return static_cast<int32_t>(offsetof(Vinetas_t2126676892, ___piedra_20)); }
	inline Sprite_t3199167241 * get_piedra_20() const { return ___piedra_20; }
	inline Sprite_t3199167241 ** get_address_of_piedra_20() { return &___piedra_20; }
	inline void set_piedra_20(Sprite_t3199167241 * value)
	{
		___piedra_20 = value;
		Il2CppCodeGenWriteBarrier(&___piedra_20, value);
	}

	inline static int32_t get_offset_of_vinetas_21() { return static_cast<int32_t>(offsetof(Vinetas_t2126676892, ___vinetas_21)); }
	inline SpriteU5BU5D_t2761310900* get_vinetas_21() const { return ___vinetas_21; }
	inline SpriteU5BU5D_t2761310900** get_address_of_vinetas_21() { return &___vinetas_21; }
	inline void set_vinetas_21(SpriteU5BU5D_t2761310900* value)
	{
		___vinetas_21 = value;
		Il2CppCodeGenWriteBarrier(&___vinetas_21, value);
	}

	inline static int32_t get_offset_of_listaSprite_22() { return static_cast<int32_t>(offsetof(Vinetas_t2126676892, ___listaSprite_22)); }
	inline SpriteU5BU5D_t2761310900* get_listaSprite_22() const { return ___listaSprite_22; }
	inline SpriteU5BU5D_t2761310900** get_address_of_listaSprite_22() { return &___listaSprite_22; }
	inline void set_listaSprite_22(SpriteU5BU5D_t2761310900* value)
	{
		___listaSprite_22 = value;
		Il2CppCodeGenWriteBarrier(&___listaSprite_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
