﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Inventario
struct Inventario_t3778973585;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void Inventario::.ctor()
extern "C"  void Inventario__ctor_m2752969770 (Inventario_t3778973585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Inventario::UpdateInventario()
extern "C"  void Inventario_UpdateInventario_m623790324 (Inventario_t3778973585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Inventario::IncreaseAmount(System.Int32)
extern "C"  void Inventario_IncreaseAmount_m734132901 (Inventario_t3778973585 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Inventario::UpdateInventoryWithBlink()
extern "C"  void Inventario_UpdateInventoryWithBlink_m2469989757 (Inventario_t3778973585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Inventario::BlinkPic(System.Single)
extern "C"  Il2CppObject * Inventario_BlinkPic_m470484731 (Inventario_t3778973585 * __this, float ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Inventario::Clear()
extern "C"  void Inventario_Clear_m159103061 (Inventario_t3778973585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
