﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Slots/<HightlightSlot>c__IteratorA
struct U3CHightlightSlotU3Ec__IteratorA_t3561306698;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Slots/<HightlightSlot>c__IteratorA::.ctor()
extern "C"  void U3CHightlightSlotU3Ec__IteratorA__ctor_m2623113617 (U3CHightlightSlotU3Ec__IteratorA_t3561306698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Slots/<HightlightSlot>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CHightlightSlotU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2353807275 (U3CHightlightSlotU3Ec__IteratorA_t3561306698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Slots/<HightlightSlot>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CHightlightSlotU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m4069587263 (U3CHightlightSlotU3Ec__IteratorA_t3561306698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Slots/<HightlightSlot>c__IteratorA::MoveNext()
extern "C"  bool U3CHightlightSlotU3Ec__IteratorA_MoveNext_m2347026859 (U3CHightlightSlotU3Ec__IteratorA_t3561306698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots/<HightlightSlot>c__IteratorA::Dispose()
extern "C"  void U3CHightlightSlotU3Ec__IteratorA_Dispose_m3859633550 (U3CHightlightSlotU3Ec__IteratorA_t3561306698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Slots/<HightlightSlot>c__IteratorA::Reset()
extern "C"  void U3CHightlightSlotU3Ec__IteratorA_Reset_m269546558 (U3CHightlightSlotU3Ec__IteratorA_t3561306698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
