﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2761310900;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Graficos
struct  Graficos_t156140270  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Sprite[] Graficos::sprites
	SpriteU5BU5D_t2761310900* ___sprites_2;

public:
	inline static int32_t get_offset_of_sprites_2() { return static_cast<int32_t>(offsetof(Graficos_t156140270, ___sprites_2)); }
	inline SpriteU5BU5D_t2761310900* get_sprites_2() const { return ___sprites_2; }
	inline SpriteU5BU5D_t2761310900** get_address_of_sprites_2() { return &___sprites_2; }
	inline void set_sprites_2(SpriteU5BU5D_t2761310900* value)
	{
		___sprites_2 = value;
		Il2CppCodeGenWriteBarrier(&___sprites_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
